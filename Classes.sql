--first pass
--INSERT INTO [Financial Planning].[dbo].[GradesAuditCheck] (Studentname, SSN, SyStudentID, Campus, IsBrightwood,
--SchoolStatus, ProgramVersion, Term, TermStartDate, TermEndDate, Class, AdGradeLetterCode, SchedID, DateGradePosted,
--DateLstMod)
--select distinct StudentName = RTRIM(SS.LastName)+', '+RTRIM(ss.FirstName), SS.SSN, AE.SyStudentID, 
--	RTRIM(SC.Descrip) as Campus, IsBrightwood = 
--		Case WHEN SC.SyCampusID IN (49,50,51,52,53,54,55,56,57,58,59,60,61,62,63,64,65,66,67,68,69,70,71,72,73,74,75,
--			76,77,80,81,82,83,84,85,86,87,88) THEN 1 ELSE 0 END, 
--	RTRIM(SSS.Descrip) as SchoolStatus, RTRIM(APV.Descrip) as ProgramVersion, RTRIM(AT.Code) as Term, 
--	AT.StartDate as TermStartDate, AT.EndDate as TermEndDate, RTRIM(AES.Descrip) as Class, 
--	RTRIM(AES.AdGradeLetterCode), AES.AdEnrollSchedID as SchedID, 
--	AES.DateGradePosted, AES.DateLstMod 
--from AdEnrollSched (nolock) as AES  
--join AdEnroll (nolock) as AE on AE.AdEnrollID = AES.AdEnrollID AND AE.SySchoolStatusID in (13,111,128,92,60,61)
--	AND datediff(d, getdate(), DateGradePosted) >= -180
--join AdClassSched (nolock) as ACS on ACS.AdClassSchedID = AES.AdClassSchedID
--join syStudent (nolock) as SS on SS.SyStudentId = AE.SyStudentID
--join SyCampus (nolock) as SC on SC.SyCampusID = SS.SyCampusID
--join SySchoolStatus (nolock) as SSS on SSS.SySchoolStatusID = AE.SySchoolStatusID
--join AdProgramVersion (nolock) as APV on APV.AdProgramVersionID = AE.adProgramVersionID
--left outer join AdTermRelationship (nolock) as ATR on ATR.ChildAdTermID = AES.AdTermID 
--join AdTerm (nolock) as AT on (AT.AdTermID = AES.AdTermID AND SC.SyCampusID IN 
--	(49,50,51,52,53,54,55,56,57,58,59,60,61,62,63,64,65,66,67,68,69,70,71,72,73,74,75,76,77,80,81,82,83,84,85,86,87,
--	88)) or (SC.SyCampusID NOT IN (49,50,51,52,53,54,55,56,57,58,59,60,61,62,63,64,65,66,67,68,69,70,71,72,73,74,75,
--	76,77,80,81,82,83,84,85,86,87,88) AND AT.AdTermID = ATR.ParentAdTermID)

--ORDER BY SchedID

--daily run
declare @iDays int;
if (datename(dw,getdate())='Monday')
begin
	set @iDays = -3;
end
else
begin
	set @iDays = -1;
end
INSERT INTO [Financial Planning].[dbo].[GradesAuditCheck] (Studentname, SSN, SyStudentID, Campus, IsBrightwood,
SchoolStatus, ProgramVersion, Term, TermStartDate, TermEndDate, Class, AdGradeLetterCode,  SchedID, DateGradePosted,
DateLstMod)
select distinct StudentName = RTRIM(SS.LastName)+', '+RTRIM(ss.FirstName), SS.SSN, AE.SyStudentID, 
	RTRIM(SC.Descrip) as Campus, IsBrightwood = 
		Case WHEN SC.SyCampusID IN (49,50,51,52,53,54,55,56,57,58,59,60,61,62,63,64,65,66,67,68,69,70,71,72,73,74,75,
			76,77,80,81,82,83,84,85,86,87,88) THEN 1 ELSE 0 END, 
	RTRIM(SSS.Descrip) as SchoolStatus, RTRIM(APV.Descrip) as ProgramVersion, RTRIM(AT.Code) as Term, 
	AT.StartDate as TermStartDate, AT.EndDate as TermEndDate, RTRIM(AES.Descrip) as Class, 
	RTRIM(AES.AdGradeLetterCode), AES.AdEnrollSchedID as SchedID, 
	AES.DateGradePosted, AES.DateLstMod 
from AdEnrollSched (nolock) as AES  
join AdEnroll (nolock) as AE on AE.AdEnrollID = AES.AdEnrollID AND AE.SySchoolStatusID in (13,111,128,92,60,61)
	AND datediff(d, getdate(), DateGradePosted) = @iDays
join AdClassSched (nolock) as ACS on ACS.AdClassSchedID = AES.AdClassSchedID
join syStudent (nolock) as SS on SS.SyStudentId = AE.SyStudentID
join SyCampus (nolock) as SC on SC.SyCampusID = SS.SyCampusID
join SySchoolStatus (nolock) as SSS on SSS.SySchoolStatusID = AE.SySchoolStatusID
join AdProgramVersion (nolock) as APV on APV.AdProgramVersionID = AE.adProgramVersionID
left outer join AdTermRelationship (nolock) as ATR on ATR.ChildAdTermID = AES.AdTermID 
join AdTerm (nolock) as AT on (AT.AdTermID = AES.AdTermID AND SC.SyCampusID IN 
	(49,50,51,52,53,54,55,56,57,58,59,60,61,62,63,64,65,66,67,68,69,70,71,72,73,74,75,76,77,80,81,82,83,84,85,86,87,
	88)) or (SC.SyCampusID NOT IN (49,50,51,52,53,54,55,56,57,58,59,60,61,62,63,64,65,66,67,68,69,70,71,72,73,74,75,
	76,77,80,81,82,83,84,85,86,87,88) AND AT.AdTermID = ATR.ParentAdTermID)
WHERE AdEnrollSchedID NOT IN (SELECT distinct SchedID FROM [Financial Planning].[dbo].[GradesAuditCheck])



 


