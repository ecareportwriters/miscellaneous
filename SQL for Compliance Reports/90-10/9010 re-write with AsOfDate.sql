declare @AsOfDate as datetime, @BOY as Datetime

set @AsOfDate = '2016-12-31 23:59:00.000'  --Enddate for report
set @BOY = '2016-01-01 00:00:00.000';      --Beginning of year



with CrntYrCohort_CTE
as 
(
Select distinct SyStudentID	
from CampusVue.dbo.SaTrans (nolock) as STN
where datepart(year,stn.date ) = datepart(year,@AsOfDate)
union 

select  STN2.SyStudentID
from CampusVue.dbo.SaTrans (nolock) as STN2
group by STN2.SyStudentID
having SUM( case when year(stn2.date) < datepart(year,@AsOfDate) then Case STN2.Type 
																	  When 'I' Then STN2.Amount
																	  When 'D' Then STN2.Amount
																	  Else STN2.Amount * -1 end
				   ELSE 0 END  )<>0
)
, Base_cte 
as(
select StudentName = RTRIM(SS.Lastname)+', '+RTRIM(SS.FirstName)
	 , SS.SSN
	 , SS.SyStudentId 
	 , SSS.Descrip as SchoolStatus
	 , SC.Descrip as Campus
	 , Convert(Varchar(6),SC.OPEID) as OPEID
	 , SS.StartDate 
	 , PriorYrBal = SUM( case when datepart(year,stn.date ) <= datepart(year,@AsOfDate)-1  then 
																							Case STN.Type 
																							  When 'I' Then STN.Amount
																							  When 'D' Then STN.Amount
																							  Else STN.Amount * -1 end 
							  else 0 
							   end )
	 , CurrentYrInstChrgs = case when  SUM( case when STN.Date between @BOY and @AsOfDate and SBC.TitleIVCharge = 1 then 
																										Case STN.Type 
																											When 'I' Then STN.Amount
																											When 'D' Then STN.Amount
																											Else STN.Amount * -1 end 
							  else 0 
							   end )	>0 	then SUM( case when STN.Date between @BOY and @AsOfDate and SBC.TitleIVCharge = 1 then 
																											Case STN.Type 
																												When 'I' Then STN.Amount
																												When 'D' Then STN.Amount
																												Else STN.Amount * -1 end 
							  else 0 
							   end ) else 0 end 			       
	 , TotalIC =  (case when  SUM( case when STN.Date between @BOY and @AsOfDate and SBC.TitleIVCharge = 1 then 
																		Case STN.Type 
																			When 'I' Then STN.Amount
																			When 'D' Then STN.Amount
																			Else STN.Amount * -1 end 
							  else 0 
							   end )	>0 	then SUM( case when STN.Date between @BOY and @AsOfDate and SBC.TitleIVCharge = 1 then 
																						Case STN.Type 
																							When 'I' Then STN.Amount
																							When 'D' Then STN.Amount
																							Else STN.Amount * -1 end 
							  else 0 
							   end ) else 0 end )
					+/*Add prior year balance*/
					case when SUM( case when datepart(year,stn.date ) <= datepart(year,@AsOfDate)-1  then 
																							Case STN.Type 
																							  When 'I' Then STN.Amount
																							  When 'D' Then STN.Amount
																							  Else STN.Amount * -1 end 
							  else 0 
							   end )>0 
						then SUM( case when datepart(year,stn.date ) <= datepart(year,@AsOfDate)-1  then 
																							Case STN.Type 
																							  When 'I' Then STN.Amount
																							  When 'D' Then STN.Amount
																							  Else STN.Amount * -1 end 
							  else 0 
							   end )
						else 0 
					end 
			
	 , CyTuition =  sum( case when STN.SaBillCode IN('TUIT','TUITADJ'
													 ,'TUIT CR','INT TUIT','TUITCRAD','TUITCRSF','TERMGRAD','TERMNIGH' )and STN.Date between @BOY and @AsOfDate then 
																						   Case STN.Type 
																						   When 'I' Then STN.Amount
																						   When 'D' Then STN.Amount
																						   Else STN.Amount * -1 end  else 0 end )
		 
	 , BDebt = sum( case when STN.SaBillCode IN('BDEBT','WAIVBD','BADDEBT')and STN.Date between @BOY and @AsOfDate then 
																	  Case STN.Type 
																	  When 'I' Then STN.Amount
																	  When 'D' Then STN.Amount  
																	  Else STN.Amount * -1 end  else 0 end )
	  , SLM = sum( case when STN.SaBillCode IN('SLMPLAN')and STN.Date between @BOY and @AsOfDate then 
																	  Case STN.Type 
																	  When 'I' Then STN.Amount
																	  When 'D' Then STN.Amount
																	  Else STN.Amount * -1 end  else 0 end )
	  , PYBDebtBal = sum( case when STN.SaBillCode IN('BDEBT','WAIVBD','COBD','BADDEBT')and STN.Date between @BOY and @AsOfDate then 
																	  Case STN.Type 
																	  When 'I' Then STN.Amount
																	  When 'D' Then STN.Amount
																	  Else STN.Amount * -1 end  else 0 end )	 	 
	 , CYBooks = sum( case when STN.SaBillCode IN('BOOK') and STN.Date between @BOY and @AsOfDate then 
															Case STN.Type 
															  When 'I' Then STN.Amount
															  When 'D' Then STN.Amount
															  Else STN.Amount * -1 end  else 0 end )
		
	 , CYFees = sum( case when STN.SaBillCode IN('FEEMED',  'FEEOT'  ,'FEE ACT','NURSE'
												,'FEERE','FEESLAB', 'FEEACT','FEELAB'
												,'FEECERT','FEE CLUB','FEETECH','FEECOMP') and STN.Date between @BOY and @AsOfDate then
																							  Case STN.Type 
																							   When 'I' Then STN.Amount
																							   When 'D' Then STN.Amount
																							   Else STN.Amount * -1 end  else 0 end )
		  
	 , CYApp = sum( case when STN.SaBillCode IN('FEEAPP') and STN.Date between @BOY and @AsOfDate then
															 Case STN.Type 
															  When 'I' Then STN.Amount
															  When 'D' Then STN.Amount
															  Else STN.Amount * -1 end  else 0 end )
		  
	 , CYCOBD = sum( case when STN.SaBillCode IN('COBD') and STN.Date between @BOY and @AsOfDate then
															 Case STN.Type 
															  When 'I' Then STN.Amount
															  When 'D' Then STN.Amount
															  Else STN.Amount * -1 end  else 0 end )
		 
	 , CYOther = sum( case when STN.SaBillCode IN('FEETMT'  ,'FEEMISC' , 'FEENSF'   ,
													'FEETPLAN' ,'FEEKIT' ,'FEECW',	 
													'AMDEP','FEETO')and STN.Date between @BOY and @AsOfDate then 
																		Case STN.Type 
																		  When 'I' Then STN.Amount
																		  When 'D' Then STN.Amount
																		  Else STN.Amount * -1 end  else 0 end )	
													
	 , Collect = sum( case when STN.SaBillCode IN('COLLECT')and STN.Date between @BOY and @AsOfDate then 
																Case STN.Type 
																  When 'I' Then STN.Amount
																  When 'D' Then STN.Amount
																  Else STN.Amount * -1 end  else 0 end )
	 , Waiver = SUM(case when STN.SaBillCode in ('WAIVTUIT',
														  'WAIVMILT',
														  'WAIVRE'  ,
														  'EEWAIV',
														  'WAIVFEE','WIADISC') 
						   and STN.Date between @BOY and @AsOfDate then  Case STN.Type 
																						  When 'I' Then STN.Amount
																						  When 'D' Then STN.Amount
																						  Else STN.Amount * -1 end  else 0 end )
	
	 , CurrentBal = SUM( Case STN.Type 
						  When 'I' Then STN.Amount
						  When 'D' Then STN.Amount
						  Else STN.Amount * -1 end  ) 
	 , CurrentBalExcludeStipends = SUM( case when (FR.SaStipendID is null or FR.SaStipendID = 0 ) then  Case STN.Type 
																				  When 'I' Then STN.Amount
																				  When 'D' Then STN.Amount
																				  Else STN.Amount * -1 end 
											   else 0 end  )  
	 , SubPaid = SUM(case when FFS.faFundSourceID = 5 and STN.Date between @BOY and @AsOfDate
													  and (FR.SaStipendID is null or FR.SaStipendID = 0 ) then 
																												Case STN.Type 
																												  When 'I' Then STN.Amount
																												  When 'D' Then STN.Amount
																												  Else STN.Amount * -1 end
					  else 0 
					  end  )
	 , UnSubPaid = SUM(case when FFS.faFundSourceID = 7 and STN.Date between @BOY and @AsOfDate
														and (FR.SaStipendID is null or FR.SaStipendID = 0 ) then 
																												Case STN.Type 
																												  When 'I' Then STN.Amount
																												  When 'D' Then STN.Amount
																												  Else STN.Amount * -1 end
					  else 0 
					  end  )			  
	 , PlusPaid = SUM(case when FFS.faFundSourceID in (9,60) and STN.Date between @BOY and @AsOfDate
															 and (FR.SaStipendID is null or FR.SaStipendID = 0 ) then 
																												Case STN.Type 
																												  When 'I' Then STN.Amount
																												  When 'D' Then STN.Amount
																												  Else STN.Amount * -1 end
					  else 0 
					  end  )
	 , PellPaid = SUM(case when FFS.faFundSourceID = 1 and STN.Date between @BOY and @AsOfDate
													   and (FR.SaStipendID is null or FR.SaStipendID = 0 ) then 
																												Case STN.Type 
																												  When 'I' Then STN.Amount
																												  When 'D' Then STN.Amount
																												  Else STN.Amount * -1 end
					  else 0 
					  end  )
	 , SEOGPaid=  SUM(case when FFS.faFundSourceID = 2 and STN.Date between @BOY and @AsOfDate
													   and (FR.SaStipendID is null or FR.SaStipendID = 0 ) then 
																												Case STN.Type 
																												  When 'I' Then STN.Amount
																												  When 'D' Then STN.Amount
																												  Else STN.Amount * -1 end * ( case when FFS.FaFundSourceID =2 then 
																																							case when SS.SyCampusID >= 49 and FSA.AwardYear = '2015-16'  then 1 
																																								 else 0.75 
																																							end 
																																				     else 1 end )
					  else 0 
					  end  )
	 , T4Paid = case when SUM(case when FFS.Title4 = 1 and STN.Date between @BOY and @AsOfDate 
										     and (FR.SaStipendID is null or FR.SaStipendID = 0 ) then 
																												Case STN.Type 
																												  When 'I' Then STN.Amount
																												  When 'D' Then STN.Amount
																												  Else STN.Amount * -1 end * ( case when FFS.FaFundSourceID =2 then 
																																							case when SS.SyCampusID >= 49 and FSA.AwardYear = '2015-16'  then 1 
																																								 else 0.75 
																																							end 
																																				     else 1 end )
					  else 0 
					  end  )>0 then 0 else 	  SUM(case when FFS.Title4 = 1 and STN.Date between @BOY and @AsOfDate
														  and (FR.SaStipendID is null or FR.SaStipendID = 0 ) then 
																												Case STN.Type 
																												  When 'I' Then STN.Amount
																												  When 'D' Then STN.Amount
																												  Else STN.Amount * -1 end * ( case when FFS.FaFundSourceID =2 then 
																																							case when SS.SyCampusID >= 49 and FSA.AwardYear = '2015-16'  then 1 
																																								 else 0.75 
																																							end 
																																				     else 1 end )
													  else 0 
													  end  )
			     end 
				 + /*Add prior year credit balance for non-GAA*/
					case when SS.Sycampusid NOT IN (select sycampusid from campusvue.dbo.SyCampus where descrip like 'GAA%') then 
						case when 	 SUM( case when datepart(year,stn.date ) <= datepart(year,@AsOfDate)-1  then 
																								Case STN.Type 
																								  When 'I' Then STN.Amount
																								  When 'D' Then STN.Amount
																								  Else STN.Amount * -1 end 
											  else 0 
											   end )<0 THEN  SUM( case when datepart(year,stn.date ) <= datepart(year,@AsOfDate)-1  then 
																								Case STN.Type 
																								  When 'I' Then STN.Amount
																								  When 'D' Then STN.Amount
																								  Else STN.Amount * -1 end 
																		  else 0 
																		   end )
							 ELSE 0 
						  END 
					  else 0 
					  END 
				
	 , T4Refunds = SUM(case when FFS.Title4 = 1 and STN.Date between @BOY and @AsOfDate  
										     and (FR.SaStipendID is null or FR.SaStipendID = 0 )
										     and FR.FaRefundID is not null then 
																				Case STN.Type 
																					When 'I' Then STN.Amount
																					When 'D' Then STN.Amount
																					Else STN.Amount * -1 end* ( case when FFS.FaFundSourceID =2 then 
																														case when SS.SyCampusID >= 49 and FSA.AwardYear = '2015-16'  then 1 
																																else 0.75 
																														end 
																													else 1 end )
					  else 0 
					  end  )	 
		
	 --needs fund source codes  select * from fafundsource where title4 = 0 and active = 1 order by descrip	
	 , PreJumPaid = SUM(case when  FFS.OthNonFedResource = 1 
							   and STN.Date between @BOY and @AsOfDate
							   and (FR.SaStipendID is null or FR.SaStipendID = 0 )
							 then 
								Case STN.Type 
								  When 'I' Then STN.Amount
								  When 'D' Then STN.Amount
								 Else STN.Amount * -1 end
					  else 0 
					  end  )
	 --needs fund source codes
	 , Discounts = SUM(case when  FFS.institutional  =1 and STN.Date between @BOY and @AsOfDate
													    and (FR.SaStipendID is null or FR.SaStipendID = 0 )
													    then 
															Case STN.Type 
															  When 'I' Then STN.Amount
															  When 'D' Then STN.Amount
															  Else STN.Amount * -1 end
					  else 0 
					  end  ) 
	 , CashPaid = SUM(case when FFS.Title4 =0 
							and FFS.Institutional =0  
							and FFS.OthNonFedResource = 0 
	 --faFundSourceID in (27,	3,	39,	54,	17,		42,	77,	75,	79,	62,	63,
		--												64,	65,	71,	70,	69,	68, 67,	66,	72,	52,	73,	74,
		--												49,	50,	80,	43,	48,	76, 58,	78,	59,28,	36,	55		 
															 						
		--											    ) 
														and STN.Date between @BOY and @AsOfDate
													    and (FR.SaStipendID is null or FR.SaStipendID = 0 )
													    then 
															Case STN.Type 
															  When 'I' Then STN.Amount
															  When 'D' Then STN.Amount
															  Else STN.Amount * -1 end
					  else 0 
					  end  )
					+ /*Add prior year credit balance for GAA*/
					case when SS.Sycampusid in (select sycampusid from CampusVue.dbo.sycampus where descrip like 'GAA%') then 
						case when 	 SUM( case when datepart(year,stn.date ) <= datepart(year,@AsOfDate)-1  then 
																								Case STN.Type 
																								  When 'I' Then STN.Amount
																								  When 'D' Then STN.Amount
																								  Else STN.Amount * -1 end 
											  else 0 
											   end )<0 THEN  SUM( case when datepart(year,stn.date ) <= datepart(year,@AsOfDate)-1  then 
																								Case STN.Type 
																								  When 'I' Then STN.Amount
																								  When 'D' Then STN.Amount
																								  Else STN.Amount * -1 end 
																		  else 0 
																		   end )
							 ELSE 0 
						  END 
					  else 0 
					  END
	 , TotalPaid = 	
	              /*Add total paid to any negative Prior Year balance*/
				   case when SUM(case when  FFS.faFundSourceID is not null and FFS.Institutional = 0 
													--in (27,	3,	39,	54,	17,	42,	77,	75,	79,	62,	63,
													--	64,	65,	71,	70,	69,	68, 67,	66,	72,	52,	73,	74,
													--	49,	50,	80,	43,	48,	76, 58,	78,	59, 28,	36,	55  --cash
													--	,20,13,34,37,11				--presumption jumpers
													--	,5,7,9,60,1,2			
													--    ) 
														and STN.Date between @BOY and @AsOfDate
													    and (FR.SaStipendID is null or FR.SaStipendID = 0 )
													    then 
															Case STN.Type 
															  When 'I' Then STN.Amount
															  When 'D' Then STN.Amount
															  Else STN.Amount * -1 end  * ( case when FFS.FaFundSourceID =2 then 
																									case when SS.SyCampusID >= 49 and FSA.AwardYear = '2015-16'  then 1 
																											else 0.75 
																									end 
																								else 1 end )
										  else 0 
										  end  )>0 then 0 else 	SUM(case when  FFS.faFundSourceID is not null and FFS.Institutional = 0 
																									--in (27,	3,	39,	54,	17,	42,	77,	75,	79,	62,	63,
																									--	64,	65,	71,	70,	69,	68, 67,	66,	72,	52,	73,	74,
																									--	49,	50,	80,	43,	48,	76, 58,	78,	59, 28,	36,	55  --cash
																									--	,20,13,34,37,11				--presumption jumpers
																									--	,5,7,9,60,1,2			
																									--	) 
																										and STN.Date between @BOY and @AsOfDate
																										and (FR.SaStipendID is null or FR.SaStipendID = 0 )
																										then 
																											Case STN.Type 
																												When 'I' Then STN.Amount
																												When 'D' Then STN.Amount
																												Else STN.Amount * -1 end  * ( case when FFS.FaFundSourceID =2 then 
																																						case when SS.SyCampusID >= 49 and FSA.AwardYear = '2015-16'  then 1 
																																								else 0.75 
																																						end 
																																					else 1 end )
																							else 0 
																							end  )
							end 		  	  
					+ /*Add prior year credit balance*/
					case when 	 SUM( case when datepart(year,stn.date ) <= datepart(year,@AsOfDate)-1  then 
																							Case STN.Type 
																							  When 'I' Then STN.Amount
																							  When 'D' Then STN.Amount
																							  Else STN.Amount * -1 end 
										  else 0 
										   end )<0 THEN  SUM( case when datepart(year,stn.date ) <= datepart(year,@AsOfDate)-1  then 
																							Case STN.Type 
																							  When 'I' Then STN.Amount
																							  When 'D' Then STN.Amount
																							  Else STN.Amount * -1 end 
																	  else 0 
																	   end )
						 ELSE 0 
					  END 

from CampusVue.dbo.satrans (nolock) as STN
	join CampusVue.dbo.syStudent (nolock) as SS
		on SS.SyStudentId = STN.SyStudentID
	join CampusVue.dbo.SyCampus (nolock) as SC 
		on SC.SyCampusID = SS.SyCampusID
	join CampusVue.dbo.SySchoolStatus (nolock) as SSS
		on SSS.SySchoolStatusID = SS.SySchoolStatusID 
	left join CampusVue.dbo.FaStudentAid (nolock) as FSA
		on FSA.FaStudentAidId = STN.FaStudentAidID
	left join CampusVue.dbo.FaFundSource (nolock) as FFS
		on FFS.faFundSourceID = FSA.FaFundSourceID
	left join CampusVue.dbo.SaBillCode (nolock) as SBC
		on SBC.Code = STN.SaBillCode
	left join CampusVue.dbo.FaRefund (nolock) as FR
		on FR.FaRefundID = STN.FaRefundID
	join CrntYrCohort_CTE as CYC
		on CYC.SyStudentID = STN.SyStudentID
--where STN.SyStudentID = 257421692



group by SS.Lastname
	 , SS.FirstName
	 , SS.SSN
	 , SS.SyStudentId 
	 , SSS.Descrip 
	 , SC.Descrip
	 , SS.StartDate
	 , SS.Sycampusid
	 , Convert(Varchar(6),SC.OPEID)

)

select * 
	 , Numer =  case 
				 when -PreJumPaid <=0 then 
				   
				   case	when -T4Paid > ( case when TotalIC  +(case when Discounts <0 then Discounts else 0 end) >-TotalPaid  then -TotalPaid 
											 when -Discounts > TotalIC then 0
											  else TotalIC  +(case when Discounts <0 then Discounts else 0 end) 
										   end 
										  ) then ( case when TotalIC  +(case when Discounts <0 then Discounts else 0 end) >-TotalPaid  then -TotalPaid 
														when -Discounts > TotalIC then 0
														 else TotalIC  +(case when Discounts <0 then Discounts else 0 end) 
													end 
													)
						when -T4Paid <= ( case when TotalIC  +(case when Discounts <0 then Discounts else 0 end) >-TotalPaid  then -TotalPaid 
												when -Discounts > TotalIC then 0
												 else TotalIC  +(case when Discounts <0 then Discounts else 0 end) 
											end 
											 ) then -T4Paid
					  end 
				  
				  when -PreJumPaid >0 then 
					
					case when -PreJumPaid > (case when TotalIC  +(case when Discounts <0 then Discounts else 0 end) >-TotalPaid  then -TotalPaid 
													when -Discounts > TotalIC then 0
													 else TotalIC  +(case when Discounts <0 then Discounts else 0 end) 
												end 
												 ) then 0 
						 when -PreJumPaid <= ( case when TotalIC  +(case when Discounts <0 then Discounts else 0 end) >-TotalPaid  then -TotalPaid 
													when -Discounts > TotalIC then 0
													 else TotalIC  +(case when Discounts <0 then Discounts else 0 end) 
												end 
												 ) then 
													 case	when -T4Paid > ( case when TotalIC  +(case when Discounts <0 then Discounts else 0 end) >-TotalPaid  then -TotalPaid 
																				 when -Discounts > TotalIC then 0
																				 else TotalIC  +(case when Discounts <0 then Discounts else 0 end) 
																			end 
																			 )+ PreJumPaid then ( case when TotalIC  +(case when Discounts <0 then Discounts else 0 end) >-TotalPaid  then -TotalPaid 
																										 when -Discounts > TotalIC then 0
																										 else TotalIC  +(case when Discounts <0 then Discounts else 0 end) 
																									end 
																									 )+PreJumPaid
															when -T4Paid <= ( case when TotalIC  +(case when Discounts <0 then Discounts else 0 end) >-TotalPaid  then -TotalPaid 
																					 when -Discounts > TotalIC then 0
																					 else TotalIC  +(case when Discounts <0 then Discounts else 0 end) 
																				end  )+ PreJumPaid then -T4Paid
													 end 
				 
					 end 
		   end 
	 , Denominator =case when TotalIC  +(case when Discounts <0  then Discounts else 0 end) >-TotalPaid  then -TotalPaid 
					     when -Discounts > TotalIC then 0
						 else TotalIC +(case when Discounts <0 then Discounts else 0 end) 
					end 
from Base_cte as B



