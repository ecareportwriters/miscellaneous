

if object_ID('tempdb..##OPEID') is not null 
begin 
drop table ##OPEID
end 

if object_ID('tempdb..##NewStarts') is not null 
begin 
drop table ##NewStarts
end 


declare @lodate datetime , @HiDate datetime , @AY varchar(7) = '2015-16' , @I as datetime , @OPEID as Varchar(6) 

declare @Table as Table ( OPEID varchar(6)
						, BOMonth datetime
						, [Month] int
						, UGCont int
						, UGNEW  int
						, GCont  int 
						, GNew	 int
						)

set @lodate = (select startdate			
				from Campusvue.dbo.FaYear (nolock) as FY
				where FY.Code like @AY)
				
set @HiDate = (select EndDate			
				from Campusvue.dbo.FaYear (nolock) as FY
				where FY.Code like @AY)

Select Distinct OPEID
	into ##OPEID
	from CampusVue.dbo.SyCampus
	where OPEID is not null

set @I = @lodate

set @OPEID = (Select top 1 OPEID from ##OPEID) 

while @OPEID is not null  and @OPEID <>''
begin


	while @I<=@hidate
	begin 
	insert into @Table
	select @OPEID
		  , @I
		  , datepart(month,@I)
		  , 0
		  , 0 
		  , 0
		  , 0
	set @I =  dateadd(month,1,@I)
	end 


set @I = @lodate
delete 
from ##OPEID 
where OPEID = @OPEID 

set @OPEID = (Select top 1 OPEID from ##OPEID) 

end 



select SC.OPeid
	  , datepart(month, ssc.dateadded) as [Month]
	  , Sum(Case when APV.AdDegreeID in ( 4	--MA      	Master of Arts
									,14	--MS      	Master of Science
									,18	--MBA     	Master of Business Administration	
									) then 1
			else 0
		end) as GRadCount
	, Sum(Case when APV.AdDegreeID in ( 4	--MA      	Master of Arts
									,14	--MS      	Master of Science
									,18	--MBA     	Master of Business Administration	
									) then 0
			else 1
		end) as UnderGRadCount
into ##NewStarts
from Campusvue.dbo.Systatchange (nolock) as SSC
join Campusvue.dbo.adenroll (nolock) as AE
 on ae.adenrollid = ssc.adenrollid 
join Campusvue.dbo.sycampus (nolock) as SC
	on SC.SyCampusId = AE.SyCampusID 
join Campusvue.dbo.SyStatus (nolock) as STn
	on STn.SyStatusid = SSC.NewSyStatusID 
join Campusvue.dbo.SyStatus (nolock) as STp
	on STp.SyStatusid = SSC.PrevSyStatusID  
join CampusVue.dbo.AdProgramVersion (nolock) as APV
	on APV.AdProgramVersionID = AE.AdProgramVersionID

where SSC.DateAdded between '2015-07-01' and getdate()
  and Stp.Category = 'E' 
  and Stn.Category = 'A'
group by SC.Opeid
	 , datepart(month, ssc.dateadded)


update T 
set UGNEW = UnderGRadCount
  , GNew = GradCount
from @Table T
join ##NewStarts N
on T.OPEID = N.OPEID 
and T.[Month] = N.[MOnth]



select   T.BOMonth
      , A.OPEID
      , A.GraduateStudent
	  , A.SyStudentID
	
from (Select distinct BOMonth from  @Table) as T
cross apply [Financial Planning].dbo.[fn_FISAP_BOMonthPop2](T.BOMonth)as A




select * from @Table 



drop table ##NewStarts
drop table ##OPEID