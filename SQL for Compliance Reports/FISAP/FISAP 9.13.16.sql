declare @AY as varchar(7)
declare @AYStart as datetime
declare @AYEnd as Datetime
declare @FIT as int 
Declare @ISIRsource as int 
declare @FaISIRAwardyearSchemaID as int



set @ISIRsource = 1
 -- Set this variable to:  
										--   1. CV ISIR Tables 
										--   2. dbo.ISIRsForFISAP

set @AY = '2015-16'												--Set Award Year for report
set @AYStart = (select startdate								--Get start date of designated award year
				from campusvue.dbo.FaYear
				where Code = @AY)
set @AYEnd = (select EndDate									--Get end date of designated award year
				from campusvue.dbo.FaYear
				where Code = @AY)
set @FaISIRAwardyearSchemaID = (Select FaIsirAwardYearSchemaID 
									from Campusvue.dbo.FaIsirAwardYearSchema (nolock) as FIAY
									where FIAY.FaYearID =(select FaYearID									--Get end date of designated award year
															from campusvue.dbo.FaYear
															where Code = @AY))
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
/****
Gather ISIR Data
****/
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
if @ISIRsource = 1

------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
/*If it is pulling from CV tables*/
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

begin 		
declare @IsirLink as Table (SSN  varchar(11)
							,TransID  varchar(13)
							, FaIsirMainID int
							, SyStudentID int
							)			;

with Base_cte
as 
(
select  FIM.ISIRSSN
     , left(FIM.ISIRSsn,3)+'-'+right(left(FIM.ISIRSsn,5),2)+'-'+right(FIM.ISIRSsn,4) as SSN
	 , max(FIM.TransactionId) as TransID

from campusvue.dbo.FaISIRMain (nolock) as FIM
--JOIN campusvue.dbo.FaISIRSparseData (NOLOCK)as FISD
--	on FISD.FaISIRMainId = FIM.FaISIRMainId
where FIM.FaISIRAwardYearSchemaId = @FaISIRAwardyearSchemaID
--  and FIM.FaISIRMainId <> 141489
--  and FISD.FaISIRSparseDataId <>141615
--  and (FISD.c207 is null or FISD.C207 = '')
--  and CONVERT(date, FISD.C163,112)= (select max(CONVERT(date, FISD2.C163,112))
--												from campusvue.dbo.FaISIRMain (nolock) as FIM2
--												JOIN campusvue.dbo.FaISIRSparseData (NOLOCK)as FISD2
--													on FISD2.FaISIRMainId = FIM2.FaISIRMainId
--												where FIM2.ISIRSsn = FIM.ISIRSsn
--												  and (FISD2.c207 is null or FISD2.C207 = '')
--												  --and FIM2.FaISIRAwardYear
--												  and FIM2.FaISIRMainId <> 141489
--												  and FISD2.FaISIRSparseDataId <>141615)
group by FIM.ISIRSsn
)
insert into @IsirLink 
		select B.SSN
			 , B.TransID
			 , FIM.FaISIRMainId
			 , SS.SyStudentId
		from Base_cte as B
		 join campusvue.dbo.FaISIRMain (nolock) as FIM
			on FIM.TransactionId =  B.TransID
		--join campusvue.dbo.faisirstudentmatch (nolock) as FISM
		--	on FISM.faisirmainid = FIM.faisirmainid
		join campusvue.dbo.syStudent (nolock) as SS
			on SS.ssn = B.SSN
		where FIM.FaISIRAwardYearSchemaId = @FaISIRAwardyearSchemaID; 
end
--select * from @Isirlink
--select * from campusvue.dbo.faisirawardyearschema (nolock) as FISM  select * from campusvue.dbo.fayear
--select top 1  * from campusvue.dbo.faisirmain where isirssn = 632363514
if @ISIRsource = 2 --632-36-3514

------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
/*If it is pulling from Portal tables*/
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

begin 
	declare @IsirLink2 as Table (
	[LastIsirTransactionId] [nvarchar](255) NULL,
	[DateAdded] [datetime] NULL,
	[SSN] [nvarchar](255) NULL,
	[SyStudentID] int null, 
	[DOB] [float] NULL,
	[FTI] [float] NULL,
	[Auto0EFCFlag] [bit] NULL,
	[FirstBachDegree] [float] NULL,
	[STI] [float] NULL,
	[TI] [float] NULL,
	[pefc] [float] NULL,
	[DependencyStatus] [nvarchar](255) NULL,
	[SelectedForVerification] [nvarchar](1) NULL
					);

with Base2_cte
as 
(Select SSN
	 , max(dateadded) as dateadded
	from [Financial Planning].[dbo].[1415ISIRFISAP]
	group by ssn )

insert into @IsirLink2 
select -- [FAYear] ,
	[LastIsirTransactionId] ,
	IFF.[DateAdded],
	IFF.[SSN] ,
	SyStudentId = ISNULL( SS.SyStudentId,''),
	IFF.[DOB] ,
	[FTI] = case RIGHT(FTI,1)
					when '{' then CASE IsNumeric( LEFT(FTI,6)) WHEN 1 THEN CAST( LEFT(FTI,6) AS FLOAT) ELSE 0 END * 10 + 0.00 
				when 'A' then CASE IsNumeric( LEFT(FTI,6)) WHEN 1 THEN CAST( LEFT(FTI,6) AS FLOAT) ELSE 0 END *10 + 1.00 
				when 'B' then CASE IsNumeric( LEFT(FTI,6)) WHEN 1 THEN CAST( LEFT(FTI,6) AS FLOAT) ELSE 0 END *10 +2.00 
				when 'C' then CASE IsNumeric( LEFT(FTI,6)) WHEN 1 THEN CAST( LEFT(FTI,6) AS FLOAT) ELSE 0 END *10 +3.00 
				when 'D' then CASE IsNumeric( LEFT(FTI,6)) WHEN 1 THEN CAST( LEFT(FTI,6) AS FLOAT) ELSE 0 END *10 +4.00 
				when 'E' then CASE IsNumeric( LEFT(FTI,6)) WHEN 1 THEN CAST( LEFT(FTI,6) AS FLOAT) ELSE 0 END *10 +5.00 
				when 'F' then CASE IsNumeric( LEFT(FTI,6)) WHEN 1 THEN CAST( LEFT(FTI,6) AS FLOAT) ELSE 0 END *10 +6.00 
				when 'G' then CASE IsNumeric( LEFT(FTI,6)) WHEN 1 THEN CAST( LEFT(FTI,6) AS FLOAT) ELSE 0 END *10 +7.00 
				when 'H' then CASE IsNumeric( LEFT(FTI,6)) WHEN 1 THEN CAST( LEFT(FTI,6) AS FLOAT) ELSE 0 END *10 +8.00 
				when 'I' then CASE IsNumeric( LEFT(FTI,6)) WHEN 1 THEN CAST( LEFT(FTI,6) AS FLOAT) ELSE 0 END *10 +9.00 
				when '}' then CASE IsNumeric( LEFT(FTI,6)) WHEN 1 THEN CAST( LEFT(FTI,6) AS FLOAT) ELSE 0 END *10 +0.00 
				when 'J' then CASE IsNumeric( LEFT(FTI,6)) WHEN 1 THEN CAST( LEFT(FTI,6) AS FLOAT) ELSE 0 END *10 +1.00 
				when 'K' then CASE IsNumeric( LEFT(FTI,6)) WHEN 1 THEN CAST( LEFT(FTI,6) AS FLOAT) ELSE 0 END *10 +2.00 
				when 'L' then CASE IsNumeric( LEFT(FTI,6)) WHEN 1 THEN CAST( LEFT(FTI,6) AS FLOAT) ELSE 0 END *10 +3.00 
				when 'M' then CASE IsNumeric( LEFT(FTI,6)) WHEN 1 THEN CAST( LEFT(FTI,6) AS FLOAT) ELSE 0 END *10 +4.00 
				when 'N' then CASE IsNumeric( LEFT(FTI,6)) WHEN 1 THEN CAST( LEFT(FTI,6) AS FLOAT) ELSE 0 END *10 +5.00 
				when 'O' then CASE IsNumeric( LEFT(FTI,6)) WHEN 1 THEN CAST( LEFT(FTI,6) AS FLOAT) ELSE 0 END *10 +6.00 
				when 'P' then CASE IsNumeric( LEFT(FTI,6)) WHEN 1 THEN CAST( LEFT(FTI,6) AS FLOAT) ELSE 0 END *10 +7.00 
				when 'Q' then CASE IsNumeric( LEFT(FTI,6)) WHEN 1 THEN CAST( LEFT(FTI,6) AS FLOAT) ELSE 0 END *10 +8.00 
				when 'R' then CASE IsNumeric( LEFT(FTI,6)) WHEN 1 THEN CAST( LEFT(FTI,6) AS FLOAT) ELSE 0 END *10 +9.00 
			 else CASE IsNumeric( LEFT(FTI,6)) WHEN 1 THEN CAST( LEFT(FTI,6) AS FLOAT) ELSE 0 END
			 end ,
	[Auto0EFCFlag]= case [Auto0EFCFlag]
					 when 'Y' then 1
					 else 0 
					 end 	,
	[FirstBachDegree] ,
	[STI] =case RIGHT(STI,1)
				when '{' then CASE IsNumeric( LEFT(STI,6)) WHEN 1 THEN CAST( LEFT(STI,6) AS FLOAT) ELSE 0 END* 10 + 0.00 
				when 'A' then CASE IsNumeric( LEFT(STI,6)) WHEN 1 THEN CAST( LEFT(STI,6) AS FLOAT) ELSE 0 END *10 + 1.00 
				when 'B' then CASE IsNumeric( LEFT(STI,6)) WHEN 1 THEN CAST( LEFT(STI,6) AS FLOAT) ELSE 0 END *10 +2.00 
				when 'C' then CASE IsNumeric( LEFT(STI,6)) WHEN 1 THEN CAST( LEFT(STI,6) AS FLOAT) ELSE 0 END *10 +3.00 
				when 'D' then CASE IsNumeric( LEFT(STI,6)) WHEN 1 THEN CAST( LEFT(STI,6) AS FLOAT) ELSE 0 END *10 +4.00 
				when 'E' then CASE IsNumeric( LEFT(STI,6)) WHEN 1 THEN CAST( LEFT(STI,6) AS FLOAT) ELSE 0 END *10 +5.00 
				when 'F' then CASE IsNumeric( LEFT(STI,6)) WHEN 1 THEN CAST( LEFT(STI,6) AS FLOAT) ELSE 0 END *10 +6.00 
				when 'G' then CASE IsNumeric( LEFT(STI,6)) WHEN 1 THEN CAST( LEFT(STI,6) AS FLOAT) ELSE 0 END *10 +7.00 
				when 'H' then CASE IsNumeric( LEFT(STI,6)) WHEN 1 THEN CAST( LEFT(STI,6) AS FLOAT) ELSE 0 END *10 +8.00 
				when 'I' then CASE IsNumeric( LEFT(STI,6)) WHEN 1 THEN CAST( LEFT(STI,6) AS FLOAT) ELSE 0 END *10 +9.00 
				when '}' then CASE IsNumeric( LEFT(STI,6)) WHEN 1 THEN CAST( LEFT(STI,6) AS FLOAT) ELSE 0 END *10 +0.00 
				when 'J' then CASE IsNumeric( LEFT(STI,6)) WHEN 1 THEN CAST( LEFT(STI,6) AS FLOAT) ELSE 0 END *10 +1.00 
				when 'K' then CASE IsNumeric( LEFT(STI,6)) WHEN 1 THEN CAST( LEFT(STI,6) AS FLOAT) ELSE 0 END *10 +2.00 
				when 'L' then CASE IsNumeric( LEFT(STI,6)) WHEN 1 THEN CAST( LEFT(STI,6) AS FLOAT) ELSE 0 END *10 +3.00 
				when 'M' then CASE IsNumeric( LEFT(STI,6)) WHEN 1 THEN CAST( LEFT(STI,6) AS FLOAT) ELSE 0 END *10 +4.00 
				when 'N' then CASE IsNumeric( LEFT(STI,6)) WHEN 1 THEN CAST( LEFT(STI,6) AS FLOAT) ELSE 0 END *10 +5.00 
				when 'O' then CASE IsNumeric( LEFT(STI,6)) WHEN 1 THEN CAST( LEFT(STI,6) AS FLOAT) ELSE 0 END *10 +6.00 
				when 'P' then CASE IsNumeric( LEFT(STI,6)) WHEN 1 THEN CAST( LEFT(STI,6) AS FLOAT) ELSE 0 END *10 +7.00 
				when 'Q' then CASE IsNumeric( LEFT(STI,6)) WHEN 1 THEN CAST( LEFT(STI,6) AS FLOAT) ELSE 0 END *10 +8.00 
				when 'R' then CASE IsNumeric( LEFT(STI,6)) WHEN 1 THEN CAST( LEFT(STI,6) AS FLOAT) ELSE 0 END *10 +9.00 
			 else CASE IsNumeric( LEFT(STI,6)) WHEN 1 THEN CAST( LEFT(STI,6) AS FLOAT) ELSE 0 END
			 end  ,
	[TI]=case RIGHT(TI,1)
					when '{' then CASE IsNumeric( LEFT(TI,6)) WHEN 1 THEN CAST( LEFT(TI,6) AS FLOAT) ELSE 0 END* 10 + 0.00 
				when 'A' then CASE IsNumeric( LEFT(TI,6)) WHEN 1 THEN CAST( LEFT(TI,6) AS FLOAT) ELSE 0 END *10 + 1.00 
				when 'B' then CASE IsNumeric( LEFT(TI,6)) WHEN 1 THEN CAST( LEFT(TI,6) AS FLOAT) ELSE 0 END *10 +2.00 
				when 'C' then CASE IsNumeric( LEFT(TI,6)) WHEN 1 THEN CAST( LEFT(TI,6) AS FLOAT) ELSE 0 END *10 +3.00 
				when 'D' then CASE IsNumeric( LEFT(TI,6)) WHEN 1 THEN CAST( LEFT(TI,6) AS FLOAT) ELSE 0 END *10 +4.00 
				when 'E' then CASE IsNumeric( LEFT(TI,6)) WHEN 1 THEN CAST( LEFT(TI,6) AS FLOAT) ELSE 0 END *10 +5.00 
				when 'F' then CASE IsNumeric( LEFT(TI,6)) WHEN 1 THEN CAST( LEFT(TI,6) AS FLOAT) ELSE 0 END *10 +6.00 
				when 'G' then CASE IsNumeric( LEFT(TI,6)) WHEN 1 THEN CAST( LEFT(TI,6) AS FLOAT) ELSE 0 END *10 +7.00 
				when 'H' then CASE IsNumeric( LEFT(TI,6)) WHEN 1 THEN CAST( LEFT(TI,6) AS FLOAT) ELSE 0 END *10 +8.00 
				when 'I' then CASE IsNumeric( LEFT(TI,6)) WHEN 1 THEN CAST( LEFT(TI,6) AS FLOAT) ELSE 0 END *10 +9.00 
				when '}' then CASE IsNumeric( LEFT(TI,6)) WHEN 1 THEN CAST( LEFT(TI,6) AS FLOAT) ELSE 0 END *10 +0.00 
				when 'J' then CASE IsNumeric( LEFT(TI,6)) WHEN 1 THEN CAST( LEFT(TI,6) AS FLOAT) ELSE 0 END *10 +1.00 
				when 'K' then CASE IsNumeric( LEFT(TI,6)) WHEN 1 THEN CAST( LEFT(TI,6) AS FLOAT) ELSE 0 END *10 +2.00 
				when 'L' then CASE IsNumeric( LEFT(TI,6)) WHEN 1 THEN CAST( LEFT(TI,6) AS FLOAT) ELSE 0 END *10 +3.00 
				when 'M' then CASE IsNumeric( LEFT(TI,6)) WHEN 1 THEN CAST( LEFT(TI,6) AS FLOAT) ELSE 0 END *10 +4.00 
				when 'N' then CASE IsNumeric( LEFT(TI,6)) WHEN 1 THEN CAST( LEFT(TI,6) AS FLOAT) ELSE 0 END *10 +5.00 
				when 'O' then CASE IsNumeric( LEFT(TI,6)) WHEN 1 THEN CAST( LEFT(TI,6) AS FLOAT) ELSE 0 END *10 +6.00 
				when 'P' then CASE IsNumeric( LEFT(TI,6)) WHEN 1 THEN CAST( LEFT(TI,6) AS FLOAT) ELSE 0 END *10 +7.00 
				when 'Q' then CASE IsNumeric( LEFT(TI,6)) WHEN 1 THEN CAST( LEFT(TI,6) AS FLOAT) ELSE 0 END *10 +8.00 
				when 'R' then CASE IsNumeric( LEFT(TI,6)) WHEN 1 THEN CAST( LEFT(TI,6) AS FLOAT) ELSE 0 END *10 +9.00 
			 else CASE IsNumeric( LEFT(TI,6)) WHEN 1 THEN CAST( LEFT(TI,6) AS FLOAT) ELSE 0 END
			 end  ,
--	[ISIRVersion],
	[pefc] ,
	[DependencyStatus] ,
	[SelectedForVerification] 

from [Financial Planning].[dbo].[1415ISIRFISAP] (nolock) as IFF
left join campusvue.dbo.syStudent (nolock) as SS
	on SS.SSN = IFF.SSN	
join Base2_Cte as B
	on B.ssn = IFF.SSN
	and B.Dateadded = IFF.dateadded
	
	
		
end ;
select * from @isirlink2
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
/****
Gather SEOG Disbursements
****/
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------


if OBJECT_ID('tempdb..#SEOGdisbursed') is not null
begin
drop table #SEOGdisbursed
end 


create table #SEOGdisbursed
(SSN varchar(30)
,systudentid int
,SEOGdisbursed money
)

insert into #SEOGdisbursed 
select SS.SSN
	 , SS.SyStudentId
	 , SUM(stn.amount) as SEOGDisbursed
from CampusVue.dbo.SaTrans (nolock) as STN
	join CampusVue.dbo.syStudent (nolock) as SS
		on ss.systudentid = Stn.systudentid
	join campusvue.dbo.fastudentaid (nolock) as FSA
		on FSA.FaStudentAidId = STN.FaStudentAidID
	left join CampusVue.dbo.FaRefund (nolock) as Fr
		on Fr.FaRefundID = STN.FaRefundID
where fsa.fafundsourceid = 2 
  and  fsa.awardyear = @AY 
  and fr.sastipendid is null 
group by ss.ssn
	   , SS.SyStudentId  
 having sum(STN.Amount) >0 
 
 create clustered index IX_SyStudentID on #SEOGDisbursed (systudentid)
 
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
/****
Part II, Sections D-F, Questions 7,22, 25-40 : Population, Tuition Charged, Population by dependency, income, and degree type
****/
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

if OBJECT_ID('tempdb..#Compilation1') is not null
begin
drop table #Compilation1
end 

create table #Compilation1
(
SyStudentID int 
,SSN varchar(30) 
, OPEID varchar(6) 
,GraduateLevel int
,TuitFee money
, SEOGDisbursed money
, ISIRID varchar(13)
, Dependency varchar(4)
, AutoZero int
, Degree  VarChar(20)
, FISAP_Total_Income int
, FISAP_Total_Income_Category varchar(max)
, IncCategory varchar(max)
)


if @ISIRsource = 1 
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
/*If it is pulling from CV tables*/
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

begin 				
with GradList_cte
as 
(
select distinct AE.SyStudentID
from campusvue.dbo.AdAttend (nolock) as ATT
	join campusvue.dbo.AdEnroll (nolock) as AE
		on AE.AdEnrollID = ATT.AdEnrollID
	join campusvue.dbo.AdProgramVersion (nolock) as APV
		on APV.AdProgramVersionID = AE.adProgramVersionID
	join campusvue.dbo.AdDegree (nolock) as AG
		on AG.AdDegreeID = APV.AdDegreeID
where ATT.Date between @AYStart and dateadd(day,30,@AYEnd)
  and AG.GraduateLevel = 1

)
,SEOG_Recipients_CTE
as 
(
select STN.SyStudentID
	 , Grad = case when GL.SyStudentID is null then 0 else 1 end 
from CampusVue.dbo.SaTrans (nolock) as STN
	join CampusVue.dbo.FaStudentAid (nolock) as FSA
		on FSA.FaStudentAidId = STN.FaStudentAidID
	left join CampusVue.dbo.FaRefund (nolock) as FR
		on FR.FaRefundID = STN.FaRefundID
	left join GradList_cte as GL
		on GL.SyStudentID = STN.SyStudentID
	join  campusvue.dbo.systudent (nolock) as SS
		on SS.systudentid = stn.systudentid
where FSA.FaFundSourceID = 2 
  and FSA.AwardYear = @AY
  and FR.SaStipendID is null 

 group by STN.SyStudentID , GL.SyStudentID
 having SUM(STN.Amount)>0
)
, StdList1_cte 
as 
(
select distinct AE.SyStudentID
	          , Grad = case when GL.SyStudentID is null then 0 else 1 end 
from campusvue.dbo.AdAttend (nolock) as ATT
	join campusvue.dbo.AdEnroll (nolock) as AE
		on AE.AdEnrollID = ATT.AdEnrollID
	left join GradList_cte as GL
		on GL.SyStudentID = AE.SyStudentID
where ATT.Date between  @AYStart and dateadd(day,30,@AYEnd)
  and AE.SySchoolStatusID <> 114
 
)
, StdList_cte 
as 
(
select SyStudentID = ISNULL(SL.SyStudentID,SE.SystudentID)
	 , Grad = ISNULL(SL.Grad,SE.Grad)
from StdList1_cte as SL
full join SEOG_Recipients_CTE as SE
	on SE.SyStudentID = SL.SyStudentID
)

insert into #Compilation1
select SL.SyStudentID
	 , SS.SSN
	 , SC.OPEID
	 , AG.GraduateLevel
	 , TuitFee = SUM( case when SBC.BillType in('S','B') 
					  and SBC.TitleIVCharge = 1
					  and STN.Date between  @AYStart and @AYEnd
							then 	Case STN.Type 
									  When 'I' Then STN.Amount
									  When 'D' Then STN.Amount
									  Else STN.Amount * -1 end
							else 0 end  )
	  , ISNULL(SEOG.SEOGdisbursed,0) as SEOGDisbursed
	  , FLI.TransID
	  , Dependency = case FISD.C161
						when 'D' then 'D'
						when 'I' then 'I'
						when 'X' then 'D'
						when 'Y' then 'I'
					end  
	  , AuotZero = case FISD.C210 
					  when 'Y' then 1 
					  else 0 
				   end
	  , Degree = case FISD.c34 
					when 1 then 'With Bac/Prof'
					when 2 then 'Without Bac/Prof'
				  end 
					
	  , FISAP_Total_Income =  isnull(FISD.C293,case FISD.C161 when 'D' then FISD.C288+FISD.C273 when 'X' then FISD.C288+FISD.C273 else FISD.C273 end   )			   
	  , FISAP_Total_Income_Category  = case when FISD.C161 in ('D','X') then 
											  isnull(case  when  isnull(FISD.C293,case FISD.C161 when 'D' then FISD.C288+FISD.C273 when 'X' then FISD.C288+FISD.C273 else FISD.C273 end   )		 between -999999 and  2999 then '$0 - $2999'
													when  isnull(FISD.C293,case FISD.C161 when 'D' then FISD.C288+FISD.C273 when 'X' then FISD.C288+FISD.C273 else FISD.C273 end   )		 between    3000 and  5999 then '$3000 - $5999'
													when  isnull(FISD.C293,case FISD.C161 when 'D' then FISD.C288+FISD.C273 when 'X' then FISD.C288+FISD.C273 else FISD.C273 end   )		 between    6000 and  8999 then '$6000 - $8999'
													when  isnull(FISD.C293,case FISD.C161 when 'D' then FISD.C288+FISD.C273 when 'X' then FISD.C288+FISD.C273 else FISD.C273 end   )		 between    9000 and 11999 then '$9000 - $11999'
													when  isnull(FISD.C293,case FISD.C161 when 'D' then FISD.C288+FISD.C273 when 'X' then FISD.C288+FISD.C273 else FISD.C273 end   )		 between   12000 and 14999 then '$12000 - $14999'
													when  isnull(FISD.C293,case FISD.C161 when 'D' then FISD.C288+FISD.C273 when 'X' then FISD.C288+FISD.C273 else FISD.C273 end   )		 between   15000 and 17999 then '$15000 - $17999'
													when  isnull(FISD.C293,case FISD.C161 when 'D' then FISD.C288+FISD.C273 when 'X' then FISD.C288+FISD.C273 else FISD.C273 end   )		 between   18000 and 23999 then '$18000 - $23999'
													when  isnull(FISD.C293,case FISD.C161 when 'D' then FISD.C288+FISD.C273 when 'X' then FISD.C288+FISD.C273 else FISD.C273 end   )		 between   24000 and 29999 then '$24000 - $29999'
													when  isnull(FISD.C293,case FISD.C161 when 'D' then FISD.C288+FISD.C273 when 'X' then FISD.C288+FISD.C273 else FISD.C273 end   )		 between   30000 and 35999 then '$30000 - $35999'
													when  isnull(FISD.C293,case FISD.C161 when 'D' then FISD.C288+FISD.C273 when 'X' then FISD.C288+FISD.C273 else FISD.C273 end   )		 between   36000 and 41999 then '$36000 - $41999'
													when  isnull(FISD.C293,case FISD.C161 when 'D' then FISD.C288+FISD.C273 when 'X' then FISD.C288+FISD.C273 else FISD.C273 end   )		 between   42000 and 47999 then '$42000 - $47999'
													when  isnull(FISD.C293,case FISD.C161 when 'D' then FISD.C288+FISD.C273 when 'X' then FISD.C288+FISD.C273 else FISD.C273 end   )		 between   48000 and 53999 then '$48000 - $53999'
													when  isnull(FISD.C293,case FISD.C161 when 'D' then FISD.C288+FISD.C273 when 'X' then FISD.C288+FISD.C273 else FISD.C273 end   )		 between   54000 and 59999 then '$54000 - $59999'
													when  isnull(FISD.C293,case FISD.C161 when 'D' then FISD.C288+FISD.C273 when 'X' then FISD.C288+FISD.C273 else FISD.C273 end   )		   >=      60000           then '60000 and over '
													else null
												end ,'$0 - $2999')
										   when FISD.C161 in ('I','Y') then 
											  isnull( case when  isnull(FISD.C293,case FISD.C161 when 'D' then FISD.C288+FISD.C273 when 'X' then FISD.C288+FISD.C273 else FISD.C273 end   )		 between -999999 and    999 then '$0 - $999'
													when  isnull(FISD.C293,case FISD.C161 when 'D' then FISD.C288+FISD.C273 when 'X' then FISD.C288+FISD.C273 else FISD.C273 end   )		 between    1000 and   1999 then '$1000 - $1999'
													when  isnull(FISD.C293,case FISD.C161 when 'D' then FISD.C288+FISD.C273 when 'X' then FISD.C288+FISD.C273 else FISD.C273 end   )		 between    2000 and   2999 then '$2000 - $2999'
													when  isnull(FISD.C293,case FISD.C161 when 'D' then FISD.C288+FISD.C273 when 'X' then FISD.C288+FISD.C273 else FISD.C273 end   )		 between    3000 and   3999 then '$3000 - $3999'
													when  isnull(FISD.C293,case FISD.C161 when 'D' then FISD.C288+FISD.C273 when 'X' then FISD.C288+FISD.C273 else FISD.C273 end   )		 between    4000 and   4999 then '$4000 - $4999'
													when  isnull(FISD.C293,case FISD.C161 when 'D' then FISD.C288+FISD.C273 when 'X' then FISD.C288+FISD.C273 else FISD.C273 end   )		 between    5000 and   5999 then '$5000 - $5999'
													when  isnull(FISD.C293,case FISD.C161 when 'D' then FISD.C288+FISD.C273 when 'X' then FISD.C288+FISD.C273 else FISD.C273 end   )		 between    6000 and   7999 then '$6000 - $7999'
													when  isnull(FISD.C293,case FISD.C161 when 'D' then FISD.C288+FISD.C273 when 'X' then FISD.C288+FISD.C273 else FISD.C273 end   )		 between    8000 and   9999 then '$8000 - $9999'
													when  isnull(FISD.C293,case FISD.C161 when 'D' then FISD.C288+FISD.C273 when 'X' then FISD.C288+FISD.C273 else FISD.C273 end   )		 between   10000 and  11999 then '$10000 - $11999'
													when  isnull(FISD.C293,case FISD.C161 when 'D' then FISD.C288+FISD.C273 when 'X' then FISD.C288+FISD.C273 else FISD.C273 end   )		 between   12000 and  13999 then '$12000 - $13999'
													when  isnull(FISD.C293,case FISD.C161 when 'D' then FISD.C288+FISD.C273 when 'X' then FISD.C288+FISD.C273 else FISD.C273 end   )		 between   14000 and  15999 then '$14000 - $15999'
													when  isnull(FISD.C293,case FISD.C161 when 'D' then FISD.C288+FISD.C273 when 'X' then FISD.C288+FISD.C273 else FISD.C273 end   )		 between   16000 and  17999 then '$16000 - $17999'
													when  isnull(FISD.C293,case FISD.C161 when 'D' then FISD.C288+FISD.C273 when 'X' then FISD.C288+FISD.C273 else FISD.C273 end   )		 between   18000 and  19999 then '$18000 - $19999'
													when  isnull(FISD.C293,case FISD.C161 when 'D' then FISD.C288+FISD.C273 when 'X' then FISD.C288+FISD.C273 else FISD.C273 end   )		   >=      20000            then '$20000 - $over '
													else null
												end ,'$0 - $999')
							                else null
							              end
							              
, case when  FISD.C161 = 'D' then 
			isnull(case when isnull(FISD.C293,case FISD.C161 when 'D' then FISD.C288+FISD.C273 when 'X' then FISD.C288+FISD.C273 else FISD.C273 end   ) between -999999 and  5999 then  '$0-$5999'
				 when isnull(FISD.C293,case FISD.C161 when 'D' then FISD.C288+FISD.C273 when 'X' then FISD.C288+FISD.C273 else FISD.C273 end   ) between    6000 and  11999 then  '$6000-$11999'
				 when isnull(FISD.C293,case FISD.C161 when 'D' then FISD.C288+FISD.C273 when 'X' then FISD.C288+FISD.C273 else FISD.C273 end   ) between   12000 and  23999 then  '$12000-$23999'
				 when isnull(FISD.C293,case FISD.C161 when 'D' then FISD.C288+FISD.C273 when 'X' then FISD.C288+FISD.C273 else FISD.C273 end   ) between   24000 and  29999 then  '$24000-$29999'
				 when isnull(FISD.C293,case FISD.C161 when 'D' then FISD.C288+FISD.C273 when 'X' then FISD.C288+FISD.C273 else FISD.C273 end   ) between   30000 and  41999 then  '$30000-$41999'
				 when isnull(FISD.C293,case FISD.C161 when 'D' then FISD.C288+FISD.C273 when 'X' then FISD.C288+FISD.C273 else FISD.C273 end   ) between   42000 and  59999 then  '$42000-$59999'
				 when isnull(FISD.C293,case FISD.C161 when 'D' then FISD.C288+FISD.C273 when 'X' then FISD.C288+FISD.C273 else FISD.C273 end   ) between   60000 and  69999 then  '$60000-$69999'
				 when isnull(FISD.C293,case FISD.C161 when 'D' then FISD.C288+FISD.C273 when 'X' then FISD.C288+FISD.C273 else FISD.C273 end   ) between   70000 and  79999 then  '$70000-$79999'
				 when isnull(FISD.C293,case FISD.C161 when 'D' then FISD.C288+FISD.C273 when 'X' then FISD.C288+FISD.C273 else FISD.C273 end   ) between   80000 and  89999 then  '$80000-$89999'
				 when isnull(FISD.C293,case FISD.C161 when 'D' then FISD.C288+FISD.C273 when 'X' then FISD.C288+FISD.C273 else FISD.C273 end   ) between   90000 and  99999 then  '$90000-$99999'
				 when isnull(FISD.C293,case FISD.C161 when 'D' then FISD.C288+FISD.C273 when 'X' then FISD.C288+FISD.C273 else FISD.C273 end   )   >=     100000            then  '$100000-$and over '
				 else NULL		 
			 end ,'$0-$5999')
			when FISD.C161 = 'I' then 
			isnull(case when isnull(FISD.C293,case FISD.C161 when 'D' then FISD.C288+FISD.C273 when 'X' then FISD.C288+FISD.C273 else FISD.C273 end   ) between  -999999 and  1999  then  '$0-$1999'
				 when isnull(FISD.C293,case FISD.C161 when 'D' then FISD.C288+FISD.C273 when 'X' then FISD.C288+FISD.C273 else FISD.C273 end   ) between   2000 and  3999  then  '$2000-$3999'
				 when isnull(FISD.C293,case FISD.C161 when 'D' then FISD.C288+FISD.C273 when 'X' then FISD.C288+FISD.C273 else FISD.C273 end   ) between   4000 and  7999  then  '$4000-$7999'
				 when isnull(FISD.C293,case FISD.C161 when 'D' then FISD.C288+FISD.C273 when 'X' then FISD.C288+FISD.C273 else FISD.C273 end   ) between   8000 and  11999 then  '$8000-$11999'
				 when isnull(FISD.C293,case FISD.C161 when 'D' then FISD.C288+FISD.C273 when 'X' then FISD.C288+FISD.C273 else FISD.C273 end   ) between   12000 and  15999 then  '$12000-$15999'
				 when isnull(FISD.C293,case FISD.C161 when 'D' then FISD.C288+FISD.C273 when 'X' then FISD.C288+FISD.C273 else FISD.C273 end   ) between   16000 and  19999 then  '$16000-$19999'
				 when isnull(FISD.C293,case FISD.C161 when 'D' then FISD.C288+FISD.C273 when 'X' then FISD.C288+FISD.C273 else FISD.C273 end   ) between   20000 and  24999 then  '$20000-$24999'
				 when isnull(FISD.C293,case FISD.C161 when 'D' then FISD.C288+FISD.C273 when 'X' then FISD.C288+FISD.C273 else FISD.C273 end   ) between   25000 and  29999 then  '$25000-$29999'
				 when isnull(FISD.C293,case FISD.C161 when 'D' then FISD.C288+FISD.C273 when 'X' then FISD.C288+FISD.C273 else FISD.C273 end   ) between   30000 and  34999 then  '$30000-$34999'
				 when isnull(FISD.C293,case FISD.C161 when 'D' then FISD.C288+FISD.C273 when 'X' then FISD.C288+FISD.C273 else FISD.C273 end   ) between   35000 and  39999 then  '$35000-$39999'
				 when isnull(FISD.C293,case FISD.C161 when 'D' then FISD.C288+FISD.C273 when 'X' then FISD.C288+FISD.C273 else FISD.C273 end   )   >=      40000            then  '$40000-$and over '

				 else NULL
			end ,'$0-$1999')
		  end as IncCategory
from StdList_cte as SL
	left join campusvue.dbo.SaTrans (nolock) as STN
		on SL.SyStudentID = STN.SyStudentID
	left join campusvue.dbo.AdEnroll (nolock) as AE
		on AE.AdEnrollID = STN.AdEnrollID
    left join campusvue.dbo.AdProgramVersion (nolock) as APV
		on APV.AdProgramVersionID = AE.adProgramVersionID
	left join campusvue.dbo.AdDegree (nolock) as AG
		on AG.AdDegreeID = APV.AdDegreeID	
	left join campusvue.dbo.SaBillCode (nolock) as SBC
		on SBC.Code = STN.SaBillCode 
	left join campusvue.dbo.FaStudentAid (nolock) as FSA
		 on FSA.FaStudentAidId = STN.FaStudentAidID
	left join campusvue.dbo.FaRefund (nolock) as FR
		on FR.FaRefundID = STN.FaRefundID
	join campusvue.dbo.syStudent (nolock) as SS
		on SS.SyStudentId = SL.SyStudentID
	left join @IsirLink as FLI
		on FLI.SyStudentID = SS.SyStudentId 
	left join campusvue.dbo.FaISIRMain (nolock) as FIM
		on FIM.FaISIRMainId = FLI.FaISIRMainId
	left join campusvue.dbo.FaISIRSparseData (nolock) as FISD
		on FISD.FaISIRMainId = FIM.FaISIRMainId
	left join #SEOGdisbursed as SEOG	
		on SEOG.systudentid = SL.SyStudentID
	    and SL.Grad = 0 
	join Campusvue.dbo.Sycampus(nolock) as SC
		on SC.SyCampusID = SS.SycampusID 
where AG.GraduateLevel is not null
 
	group by  SL.SyStudentID
	 , AG.GraduateLevel
	 , SS.SSN
	 , SC.OPEID
	 , FLI.TransID
	 , FISD.C293
	 , FISD.FaISIRMainId
	 , FISD.FaISIRSparseDataId
     , FISD.C161
	 , FISD.C210 
	 , FISD.C288
	 , FISD.C273
	 , FISD.C34
	 , SEOG.SEOGdisbursed
	 
create nonclustered index NIX_SSSN on #Compilation1(SystudentID,SSN)
end 
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
if @ISIRsource = 2 
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
/*If it is pulling from Portal tables*/
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

begin 
with GradList_cte
as 
(
select distinct AE.SyStudentID
from campusvue.dbo.AdAttend (nolock) as ATT
	join campusvue.dbo.AdEnroll (nolock) as AE
		on AE.AdEnrollID = ATT.AdEnrollID
	join campusvue.dbo.AdProgramVersion (nolock) as APV
		on APV.AdProgramVersionID = AE.adProgramVersionID
	join campusvue.dbo.AdDegree (nolock) as AG
		on AG.AdDegreeID = APV.AdDegreeID
where ATT.Date between @AYStart and dateadd(day,30,@AYEnd)
  and AG.GraduateLevel = 1

)
,SEOG_Recipients_CTE
as 
(
select STN.SyStudentID
	 , Grad = case when GL.SyStudentID is null then 0 else 1 end 
from CampusVue.dbo.SaTrans (nolock) as STN
	join CampusVue.dbo.FaStudentAid (nolock) as FSA
		on FSA.FaStudentAidId = STN.FaStudentAidID
	left join CampusVue.dbo.FaRefund (nolock) as FR
		on FR.FaRefundID = STN.FaRefundID
	left join GradList_cte as GL
		on GL.SyStudentID = STN.SyStudentID
	join campusvue.dbo.systudent (nolock) as SS
		on SS.systudentid = stn.systudentid
where FSA.FaFundSourceID in (2,682)
  and FSA.AwardYear = @AY
  and FR.SaStipendID is null 

 group by STN.SyStudentID , GL.SyStudentID
 having SUM(STN.Amount)>0
)
, StdList1_cte 
as 
(
select distinct AE.SyStudentID
	          , Grad = case when GL.SyStudentID is null then 0 else 1 end 
from campusvue.dbo.AdAttend (nolock) as ATT
	join campusvue.dbo.AdEnroll (nolock) as AE
		on AE.AdEnrollID = ATT.AdEnrollID
	left join GradList_cte as GL
		on GL.SyStudentID = AE.SyStudentID
where ATT.Date between  @AYStart and dateadd(day,30,@AYEnd)
  and AE.SySchoolStatusID <> 114
 
)
, StdList_cte 
as 
(
select SyStudentID = ISNULL(SL.SyStudentID,SE.SystudentID)
	 , Grad = ISNULL(SL.Grad,SE.Grad)
from StdList1_cte as SL
full join SEOG_Recipients_CTE as SE
	on SE.SyStudentID = SL.SyStudentID
)

insert into #Compilation1
select SL.SyStudentID
	 , SS.SSN
	 , SC.OPEID
	 , AG.GraduateLevel
	 , TuitFee = SUM( case when SBC.BillType in('S','B') 
					  and SBC.TitleIVCharge = 1
					  and  STN.Date between  @AYStart and @AYEnd
							then 	Case STN.Type 
									  When 'I' Then STN.Amount
									  When 'D' Then STN.Amount
									  Else STN.Amount * -1 end
							else 0 end  )
	  , ISNULL(SEOG.SEOGdisbursed,0) as SEOGDisbursed
	  , FLI.[LastIsirTransactionId]
	  , Dependency = case FLI.DependencyStatus
						when 'D' then 'D'
						when 'I' then 'I'
						when 'X' then 'D'
						when 'Y' then 'I'
					end  
	  , AuotZero = FLI.Auto0EFCFlag
	  , Degree = case FLI.FirstBachDegree 
					when 1 then 'With Bac/Prof'
					when 2 then 'Without Bac/Prof'
				  end 
					
	  , FISAP_Total_Income = isnull(FLI.FTI,case FLI.DependencyStatus when 'D' then FLI.STI+FLI.TI when 'X' then FLI.STI+FLI.TI else FLI.STI end   )		   
	  , FISAP_Total_Income_Category  = case when FLI.DependencyStatus in ('D','X') then 
											  case  when  isnull(FLI.FTI,case FLI.DependencyStatus when 'D' then FLI.STI+FLI.TI when 'X' then FLI.STI+FLI.TI else FLI.STI end   )	 between -999999 and  2999 then '$0 - $2999'
													when  isnull(FLI.FTI,case FLI.DependencyStatus when 'D' then FLI.STI+FLI.TI when 'X' then FLI.STI+FLI.TI else FLI.STI end   )	 between    3000 and  5999 then '$3000 - $5999'
													when  isnull(FLI.FTI,case FLI.DependencyStatus when 'D' then FLI.STI+FLI.TI when 'X' then FLI.STI+FLI.TI else FLI.STI end   )	 between    6000 and  8999 then '$6000 - $8999'
													when  isnull(FLI.FTI,case FLI.DependencyStatus when 'D' then FLI.STI+FLI.TI when 'X' then FLI.STI+FLI.TI else FLI.STI end   )	 between    9000 and 11999 then '$9000 - $11999'
													when  isnull(FLI.FTI,case FLI.DependencyStatus when 'D' then FLI.STI+FLI.TI when 'X' then FLI.STI+FLI.TI else FLI.STI end   )	 between   12000 and 14999 then '$12000 - $14999'
													when  isnull(FLI.FTI,case FLI.DependencyStatus when 'D' then FLI.STI+FLI.TI when 'X' then FLI.STI+FLI.TI else FLI.STI end   )	 between   15000 and 17999 then '$15000 - $17999'
													when  isnull(FLI.FTI,case FLI.DependencyStatus when 'D' then FLI.STI+FLI.TI when 'X' then FLI.STI+FLI.TI else FLI.STI end   )	 between   18000 and 23999 then '$18000 - $23999'
													when  isnull(FLI.FTI,case FLI.DependencyStatus when 'D' then FLI.STI+FLI.TI when 'X' then FLI.STI+FLI.TI else FLI.STI end   )	 between   24000 and 29999 then '$24000 - $29999'
													when  isnull(FLI.FTI,case FLI.DependencyStatus when 'D' then FLI.STI+FLI.TI when 'X' then FLI.STI+FLI.TI else FLI.STI end   )	 between   30000 and 35999 then '$30000 - $35999'
													when  isnull(FLI.FTI,case FLI.DependencyStatus when 'D' then FLI.STI+FLI.TI when 'X' then FLI.STI+FLI.TI else FLI.STI end   )	 between   36000 and 41999 then '$36000 - $41999'
													when  isnull(FLI.FTI,case FLI.DependencyStatus when 'D' then FLI.STI+FLI.TI when 'X' then FLI.STI+FLI.TI else FLI.STI end   )	 between   42000 and 47999 then '$42000 - $47999'
													when  isnull(FLI.FTI,case FLI.DependencyStatus when 'D' then FLI.STI+FLI.TI when 'X' then FLI.STI+FLI.TI else FLI.STI end   )	 between   48000 and 53999 then '$48000 - $53999'
													when  isnull(FLI.FTI,case FLI.DependencyStatus when 'D' then FLI.STI+FLI.TI when 'X' then FLI.STI+FLI.TI else FLI.STI end   )	 between   54000 and 59999 then '$54000 - $59999'
													when  isnull(FLI.FTI,case FLI.DependencyStatus when 'D' then FLI.STI+FLI.TI when 'X' then FLI.STI+FLI.TI else FLI.STI end   )	   >=      60000           then '60000 and over '
													else null
												end 
										   when FLI.DependencyStatus in ('I','Y') then 
											   case when  isnull(FLI.FTI,case FLI.DependencyStatus when 'D' then FLI.STI+FLI.TI when 'X' then FLI.STI+FLI.TI else FLI.STI end   )	 between -999999 and    999 then '$0 - $999'
													when  isnull(FLI.FTI,case FLI.DependencyStatus when 'D' then FLI.STI+FLI.TI when 'X' then FLI.STI+FLI.TI else FLI.STI end   )	 between    1000 and   1999 then '$1000 - $1999'
													when  isnull(FLI.FTI,case FLI.DependencyStatus when 'D' then FLI.STI+FLI.TI when 'X' then FLI.STI+FLI.TI else FLI.STI end   )	 between    2000 and   2999 then '$2000 - $2999'
													when  isnull(FLI.FTI,case FLI.DependencyStatus when 'D' then FLI.STI+FLI.TI when 'X' then FLI.STI+FLI.TI else FLI.STI end   )	 between    3000 and   3999 then '$3000 - $3999'
													when  isnull(FLI.FTI,case FLI.DependencyStatus when 'D' then FLI.STI+FLI.TI when 'X' then FLI.STI+FLI.TI else FLI.STI end   )	 between    4000 and   4999 then '$4000 - $4999'
													when  isnull(FLI.FTI,case FLI.DependencyStatus when 'D' then FLI.STI+FLI.TI when 'X' then FLI.STI+FLI.TI else FLI.STI end   )	 between    5000 and   5999 then '$5000 - $5999'
													when  isnull(FLI.FTI,case FLI.DependencyStatus when 'D' then FLI.STI+FLI.TI when 'X' then FLI.STI+FLI.TI else FLI.STI end   )	 between    6000 and   7999 then '$6000 - $7999'
													when  isnull(FLI.FTI,case FLI.DependencyStatus when 'D' then FLI.STI+FLI.TI when 'X' then FLI.STI+FLI.TI else FLI.STI end   )	 between    8000 and   9999 then '$8000 - $9999'
													when  isnull(FLI.FTI,case FLI.DependencyStatus when 'D' then FLI.STI+FLI.TI when 'X' then FLI.STI+FLI.TI else FLI.STI end   )	 between   10000 and  11999 then '$10000 - $11999'
													when  isnull(FLI.FTI,case FLI.DependencyStatus when 'D' then FLI.STI+FLI.TI when 'X' then FLI.STI+FLI.TI else FLI.STI end   )	 between   12000 and  13999 then '$12000 - $13999'
													when  isnull(FLI.FTI,case FLI.DependencyStatus when 'D' then FLI.STI+FLI.TI when 'X' then FLI.STI+FLI.TI else FLI.STI end   )	 between   14000 and  15999 then '$14000 - $15999'
													when  isnull(FLI.FTI,case FLI.DependencyStatus when 'D' then FLI.STI+FLI.TI when 'X' then FLI.STI+FLI.TI else FLI.STI end   )	 between   16000 and  17999 then '$16000 - $17999'
													when  isnull(FLI.FTI,case FLI.DependencyStatus when 'D' then FLI.STI+FLI.TI when 'X' then FLI.STI+FLI.TI else FLI.STI end   )	 between   18000 and  19999 then '$18000 - $19999'
													when  isnull(FLI.FTI,case FLI.DependencyStatus when 'D' then FLI.STI+FLI.TI when 'X' then FLI.STI+FLI.TI else FLI.STI end   )	   >=      20000            then '$20000 - $over '
													else null
												end 
							                else null
							              end 

	 , case when  FLI.DependencyStatus = 'D' then 
			case when isnull(FLI.FTI,case FLI.DependencyStatus when 'D' then FLI.STI+FLI.TI when 'X' then FLI.STI+FLI.TI else FLI.STI end   ) between -999999 and  5999 then  '$0-$5999'
				 when isnull(FLI.FTI,case FLI.DependencyStatus when 'D' then FLI.STI+FLI.TI when 'X' then FLI.STI+FLI.TI else FLI.STI end   ) between    6000 and  11999 then  '$6000-$11999'
				 when isnull(FLI.FTI,case FLI.DependencyStatus when 'D' then FLI.STI+FLI.TI when 'X' then FLI.STI+FLI.TI else FLI.STI end   ) between   12000 and  23999 then  '$12000-$23999'
				 when isnull(FLI.FTI,case FLI.DependencyStatus when 'D' then FLI.STI+FLI.TI when 'X' then FLI.STI+FLI.TI else FLI.STI end   ) between   24000 and  29999 then  '$24000-$29999'
				 when isnull(FLI.FTI,case FLI.DependencyStatus when 'D' then FLI.STI+FLI.TI when 'X' then FLI.STI+FLI.TI else FLI.STI end   ) between   30000 and  41999 then  '$30000-$41999'
				 when isnull(FLI.FTI,case FLI.DependencyStatus when 'D' then FLI.STI+FLI.TI when 'X' then FLI.STI+FLI.TI else FLI.STI end   ) between   42000 and  59999 then  '$42000-$59999'
				 when isnull(FLI.FTI,case FLI.DependencyStatus when 'D' then FLI.STI+FLI.TI when 'X' then FLI.STI+FLI.TI else FLI.STI end   ) between   60000 and  69999 then  '$60000-$69999'
				 when isnull(FLI.FTI,case FLI.DependencyStatus when 'D' then FLI.STI+FLI.TI when 'X' then FLI.STI+FLI.TI else FLI.STI end   ) between   70000 and  79999 then  '$70000-$79999'
				 when isnull(FLI.FTI,case FLI.DependencyStatus when 'D' then FLI.STI+FLI.TI when 'X' then FLI.STI+FLI.TI else FLI.STI end   ) between   80000 and  89999 then  '$80000-$89999'
				 when isnull(FLI.FTI,case FLI.DependencyStatus when 'D' then FLI.STI+FLI.TI when 'X' then FLI.STI+FLI.TI else FLI.STI end   ) between   90000 and  99999 then  '$90000-$99999'
				 when isnull(FLI.FTI,case FLI.DependencyStatus when 'D' then FLI.STI+FLI.TI when 'X' then FLI.STI+FLI.TI else FLI.STI end   )   >=     100000            then  '$100000-$and over '
				 else NULL		 
			 end 
			when FLI.DependencyStatus = 'I' then 
			case when isnull(FLI.FTI,case FLI.DependencyStatus when 'D' then FLI.STI+FLI.TI when 'X' then FLI.STI+FLI.TI else FLI.STI end   ) between  -999999 and  1999  then  '$0-$1999'
				 when isnull(FLI.FTI,case FLI.DependencyStatus when 'D' then FLI.STI+FLI.TI when 'X' then FLI.STI+FLI.TI else FLI.STI end   ) between   2000 and  3999  then  '$2000-$3999'
				 when isnull(FLI.FTI,case FLI.DependencyStatus when 'D' then FLI.STI+FLI.TI when 'X' then FLI.STI+FLI.TI else FLI.STI end   ) between   4000 and  7999  then  '$4000-$7999'
				 when isnull(FLI.FTI,case FLI.DependencyStatus when 'D' then FLI.STI+FLI.TI when 'X' then FLI.STI+FLI.TI else FLI.STI end   ) between   8000 and  11999 then  '$8000-$11999'
				 when isnull(FLI.FTI,case FLI.DependencyStatus when 'D' then FLI.STI+FLI.TI when 'X' then FLI.STI+FLI.TI else FLI.STI end   ) between   12000 and  15999 then  '$12000-$15999'
				 when isnull(FLI.FTI,case FLI.DependencyStatus when 'D' then FLI.STI+FLI.TI when 'X' then FLI.STI+FLI.TI else FLI.STI end   ) between   16000 and  19999 then  '$16000-$19999'
				 when isnull(FLI.FTI,case FLI.DependencyStatus when 'D' then FLI.STI+FLI.TI when 'X' then FLI.STI+FLI.TI else FLI.STI end   ) between   20000 and  24999 then  '$20000-$24999'
				 when isnull(FLI.FTI,case FLI.DependencyStatus when 'D' then FLI.STI+FLI.TI when 'X' then FLI.STI+FLI.TI else FLI.STI end   ) between   25000 and  29999 then  '$25000-$29999'
				 when isnull(FLI.FTI,case FLI.DependencyStatus when 'D' then FLI.STI+FLI.TI when 'X' then FLI.STI+FLI.TI else FLI.STI end   ) between   30000 and  34999 then  '$30000-$34999'
				 when isnull(FLI.FTI,case FLI.DependencyStatus when 'D' then FLI.STI+FLI.TI when 'X' then FLI.STI+FLI.TI else FLI.STI end   ) between   35000 and  39999 then  '$35000-$39999'
				 when isnull(FLI.FTI,case FLI.DependencyStatus when 'D' then FLI.STI+FLI.TI when 'X' then FLI.STI+FLI.TI else FLI.STI end   )   >=      40000            then  '$40000-$and over '

				 else NULL
			end 
		  end as IncCategory
from StdList_cte as SL
	left join campusvue.dbo.SaTrans (nolock) as STN
		on SL.SyStudentID = STN.SyStudentID
	left join campusvue.dbo.AdEnroll (nolock) as AE
		on AE.AdEnrollID = STN.AdEnrollID
    left join campusvue.dbo.AdProgramVersion (nolock) as APV
		on APV.AdProgramVersionID = AE.adProgramVersionID
	left join campusvue.dbo.AdDegree (nolock) as AG
		on AG.AdDegreeID = APV.AdDegreeID	
	left join campusvue.dbo.SaBillCode (nolock) as SBC
		on SBC.Code = STN.SaBillCode 
	left join campusvue.dbo.FaStudentAid (nolock) as FSA
		 on FSA.FaStudentAidId = STN.FaStudentAidID
	left join campusvue.dbo.FaRefund (nolock) as FR
		on FR.FaRefundID = STN.FaRefundID
	join campusvue.dbo.syStudent (nolock) as SS
		on SS.SyStudentId = SL.SyStudentID
	left join @IsirLink2 as FLI
		on FLI.SyStudentID = SS.SyStudentId 
	left join #SEOGdisbursed as SEOG	
		on SEOG.systudentid = SL.SyStudentID
		and SL.Grad= 0 
	join Campusvue.dbo.Sycampus(nolock) as SC
		on SC.SyCampusID = SS.SycampusID 
where AG.GraduateLevel is not null

	group by  SL.SyStudentID
	 , AG.GraduateLevel
	 , SC.OPEID
	 , SS.SSN
	 , FLI.Auto0EFCFlag
	 , FLI.DependencyStatus
	 , FLI.FirstBachDegree
	 , FLI.[LastIsirTransactionId]
	 , FLI.STI
	 , FLI.FTI
	 , FLI.TI
	 , SEOG.SEOGdisbursed
	 
create nonclustered index NIX_SSSN on #Compilation1(SystudentID,SSN)
end 



--select C.OPEID
--	 , sum(Case when C.GraduateLevel = 0 then TuitFee else 0 end) as UGTotalTuitionandFees
--	 , sum(Case when C.GraduateLevel = 1 then TuitFee else 0 end) as GRTotalTuitionandFees
--	 , sum(Case when C.Dependency = 'D' and C.Degree = 'Without Bac/Prof' and C.GraduateLevel = 0 then C.AutoZero else 0 end ) as [DependUGWithoutDegreeAutoZero]
--	 , sum(Case when C.Dependency = 'D' and C.Degree = 'With Bac/Prof' and C.GraduateLevel = 0  then C.AutoZero else 0 end ) as [DepUGWithDegreeAutoZero]
--	 , sum(Case when C.Dependency = 'D' and C.Degree = 'With Bac/Prof' and C.GraduateLevel = 0  then C.AutoZero else 0 end ) as [IndUGWithDegreeAutoZero]
--	 , sum(Case when C.Dependency = 'D' and C.Degree = 'With Bac/Prof' and C.GraduateLevel = 0  then C.AutoZero else 0 end ) as [IndUGWithDegreeAutoZero]
--	 , sum(Case when C.GraduateLevel = 0  then C.AutoZero else 0 end ) as [GradAutoZero]
--	 , sum(Case when C.GraduateLevel = 0 and FISAP_Total_Income_Category = 
select * 
from #Compilation1 as C
--where C.ISIRID is not null


------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
/****
Part II, Section E, Question 23 : Pell Disbursed for specified award year

Part II, Section E, Question 24 : State Grants paid in applicable award year
****/
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

select SC.OPEID
	 , PellDisb = sum( case when FSA.FaFundSourceID =1       and FSA.AwardYear = @AY then case STN.Type	
																							when 'I' then STN.Amount
																							when 'D' then STN.Amount
																							else -STN.Amount
																							end
						     else 0 
						end )
	 , StateGrantsDisb = sum( case when FSA.FaFundSourceID in (
																868	    , --Alaska State Grant
																6443	, --California State Grant
																6468	, --California State Grant A
																6469	, --California State Grant B
																6470	, --California State Grant C
																4243	, --Illinois State Grant
																5367	, --Indiana State Grant
																3108	, --Maryland State Grant
																6533	, --Nevada State Grant
																841		, --Ohio State Grant
																940		, --Ohio State Grant Part Time
																900		, --PA  State Grant
																4183	, --PA State Grant - Fall
																4278	, --PA State Grant - Fall
																4280	, --PA State Grant - Spring
																4184	, --PA State Grant - Spring
																4122	, --PA State Grant - Summer
																4281	, --PA State Grant - Summer
																4279	, --PA State Grant - Winter
																4182	, --PA State Grant - Winter
																34		, --State Grant
																5387	 --Tennessee State Grant

																)     
											 and STN.Date between @AYStart and @AYEnd							
																		 then case STN.Type	
																			when 'I' then STN.Amount
																			when 'D' then STN.Amount
																			else -STN.Amount
																			end
										 else 0 
									end )
from campusvue.dbo.SaTrans (nolock) as STN
join campusvue.dbo.FaStudentAid (nolock) as FSA
	on FSA.FaStudentAidId = STN.FaStudentAidID
left join campusvue.dbo.FaRefund (nolock) as FR
	on FR.FaRefundID = STN.FaRefundID
join campusvue.dbo.systudent (nolock) as SS
	on ss.systudentid = stn.systudentid
join Campusvue.dbo.SyCampus (nolock) as SC
	on SC.SyCampusId = SS.SyCampusID 
where FR.SaStipendID is null

	group by SC.OPEID
      

	  
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
/****
Part VI, Section A, Questions 1-26 : SEOG disbursed by taxable and untaxed income category and student type
****/
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------



if @ISIRsource = 1 

------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
/*If it is pulling from CV tables*/
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

begin 

if object_id('Tempdb..##FWS') is not null
begin 
drop table ##FWS
end

if object_id('Tempdb..##SEOGDisb') is not null
begin 
drop table ##SEOGDisb
end 

if object_id('Tempdb..##GradList') is not null
begin 
drop table ##GradList
end 


select distinct AE.SyStudentID
into ##GradList
from campusvue.dbo.AdAttend (nolock) as ATT
	join campusvue.dbo.AdEnroll (nolock) as AE
		on AE.AdEnrollID = ATT.AdEnrollID
	join campusvue.dbo.AdProgramVersion (nolock) as APV
		on APV.AdProgramVersionID = AE.adProgramVersionID
	join campusvue.dbo.AdDegree (nolock) as AG
		on AG.AdDegreeID = APV.AdDegreeID
where ATT.Date between @AYStart and dateadd(day,30,@AYEnd)
  and AG.GraduateLevel = 1

create clustered index IX_ID on ##GradList (systudentid)

select SEOGDisb = sum(case when FSA.AwardYear = '2015-16' then Case when FSA.FaFundSourceID = 2 and SS.SyCampusID <49  then (case STN.Type	
																																when 'I' then STN.Amount
																																when 'D' then STN.Amount
																																else -STN.Amount
																																end)*.75
																	   when FSA.FaFundSourceID = 2 and SS.SyCampusID >=49  then (case STN.Type	
																																	when 'I' then STN.Amount
																																	when 'D' then STN.Amount
																																	else -STN.Amount
																																	end)
																		else 0 
																	end 
							else (case STN.Type	
									when 'I' then STN.Amount
									when 'D' then STN.Amount
									else -STN.Amount
									end) *.75
						end )
	 , Match = sum(case when FSA.AwardYear = '2015-16' then Case when FSA.FaFundSourceID = 2 and SS.SyCampusID <49  then (case STN.Type	
																															when 'I' then STN.Amount
																															when 'D' then STN.Amount
																															else -STN.Amount
																															end) *.25
															   when FSA.FaFundSourceID = 2 and SS.SyCampusID >=49  then 0
																   when FSA.FaFundSourceID = 682 then (case STN.Type	
																											when 'I' then STN.Amount
																											when 'D' then STN.Amount
																											else -STN.Amount
																											end)
																	else 0 
																end
					 else (case STN.Type	
							when 'I' then STN.Amount
							when 'D' then STN.Amount
							else -STN.Amount
							end)*.25
					 end  )
	 , STN.SyStudentID
	 , SC.OPEID
	 , FLI.TransID
		  , Dependency = case FISD.C161
							when 'D' then 'D'
							when 'I' then 'I'
							when 'X' then 'D'
							when 'Y' then 'I'
						end  
		  , AuotZero = case FISD.C210 
						  when 'Y' then 1 
						  else 0 
					   end
		  , FISAP_Total_Income  = isnull(FISD.C293,case FISD.C161
														when 'D' then FISD.C288+FISD.C273
														when 'X' then FISD.C288+FISD.C273
														else FISD.C273
													 end   )
into ##SEOGDisb
from campusvue.dbo.SaTrans (nolock) as STN
join campusvue.dbo.FaStudentAid (nolock) as FSA
	on FSA.FaStudentAidId = STN.FaStudentAidID
join campusvue.dbo.syStudent (nolock) as SS
	on SS.SyStudentId = STN.SyStudentID
left join campusvue.dbo.FaRefund (nolock) as FR
	on FR.FaRefundID = STN.FaRefundID
left join @IsirLink as FLI
		on FLI.SyStudentID = SS.SyStudentId 
	left join campusvue.dbo.FaISIRMain (nolock) as FIM
		on FIM.FaISIRMainId = FLI.FaISIRMainId
	left join campusvue.dbo.FaISIRSparseData (nolock) as FISD
		on FISD.FaISIRMainId = FIM.FaISIRMainId
join campusvue.dbo.sycampus (nolock) as SC
	on SC.SycampusID = SS.SyCampusID
where FR.SaStipendID is null
      and FSA.fafundsourceid in (2,682)
      and FSA.AwardYear = @AY
	 
group by SS.SyStudentId
	 , FLI.TransID
	 , FISD.C293
	 , FISD.FaISIRMainId
	 , FISD.FaISIRSparseDataId
     , FISD.C161
	 , FISD.C210 
	 , FISD.C288
	 , FISD.C273
	 , STN.SyStudentID
	 , SC.OPEID
create clustered index IX_SEOG on ##SEOGDisb (systudentid)


select FWS.SyStudentID 
	 , FWS.[Org Level 3]
	 , FWS.[Actual Draw]
	 , FWS.[Reimbursement Rate]
	 , FWS.OPEID
	 , DisbursedFWS = FWS.[Actual Draw]/FWS.[Reimbursement Rate]
	 , FWSMatch = (FWS.[Actual Draw]/FWS.[Reimbursement Rate])-FWS.[Actual Draw] 
	 , FLI.TransID
	 , Dependency = isnull(case FISD.C161
								when 'D' then 'D'
								when 'I' then 'I'
								when 'X' then 'D'
								when 'Y' then 'I'
							end  ,'I')
	,  AuotZero = isnull(case FISD.C210 
							when 'Y' then 1 
							else 0 
							end,0)
	, FISAP_Total_Income  = isnull(isnull(FISD.C293,case FISD.C161
												when 'D' then FISD.C288+FISD.C273
												when 'X' then FISD.C288+FISD.C273
												else FISD.C273
												end   ),0)
into ##FWS
from [Financial Planning].dbo.[1516FWS] FWS
	left join @IsirLink as FLI
		on FLI.SyStudentID = FWS.SyStudentId 
	left join campusvue.dbo.FaISIRMain (nolock) as FIM
		on FIM.FaISIRMainId = FLI.FaISIRMainId
	left join campusvue.dbo.FaISIRSparseData (nolock) as FISD
		on FISD.FaISIRMainId = FIM.FaISIRMainId
Where FWS.[Include in FISAP] = 'Yes' and FWS.[Actual Draw]> 0

create nonclustered index NIX_FWS on ##FWS (systudentid)

select SYID.systudentid 
	 , isnull(SD.OPEID,FWS.OPEID) as OPEID
	 , isnull(FWS.[Org Level 3],'') as FWSJob
	 , isnull(FWS.[Actual Draw],0) as FWSDrawn
	 , isnull(FWS.[Reimbursement Rate],0) as FWSReRate
	 , isnull(FWS.[DisbursedFWS],0) as FWSDisbursed
	 , isnull(FWS.[FWSMatch],0) as FWSMatch
	 , isnull(SD.SEOGDisb,0) as SEOGDisb
	 , isnull(SD.Match,0) as SEOGMatch
	 , isnull(SD.TransID,FWS.TransID) as TransID
	 , isnull(isnull(SD.Dependency,FWS.Dependency),'I') as Dependency
	 , isnull(isnull(SD.AuotZero,FWS.AuotZero),0) as AuotZero
	 , isnull(SD.FISAP_Total_Income,FWS.FISAP_Total_Income) as TotalIncome
	 , case when isnull(SD.Dependency, FWS.Dependency) = 'D' then 
			case when isnull(SD.FISAP_Total_Income,FWS.FISAP_Total_Income) between -999999 and  5999 then  '$0-$5999'
				 when isnull(SD.FISAP_Total_Income,FWS.FISAP_Total_Income) between    6000 and  11999 then  '$6000-$11999'
				 when isnull(SD.FISAP_Total_Income,FWS.FISAP_Total_Income) between   12000 and  23999 then  '$12000-$23999'
				 when isnull(SD.FISAP_Total_Income,FWS.FISAP_Total_Income) between   24000 and  29999 then  '$24000-$29999'
				 when isnull(SD.FISAP_Total_Income,FWS.FISAP_Total_Income) between   30000 and  41999 then  '$30000-$41999'
				 when isnull(SD.FISAP_Total_Income,FWS.FISAP_Total_Income) between   42000 and  59999 then  '$42000-$59999'
				 when isnull(SD.FISAP_Total_Income,FWS.FISAP_Total_Income) between   60000 and  69999 then  '$60000-$69999'
				 when isnull(SD.FISAP_Total_Income,FWS.FISAP_Total_Income) between   70000 and  79999 then  '$70000-$79999'
				 when isnull(SD.FISAP_Total_Income,FWS.FISAP_Total_Income) between   80000 and  89999 then  '$80000-$89999'
				 when isnull(SD.FISAP_Total_Income,FWS.FISAP_Total_Income) between   90000 and  99999 then  '$90000-$99999'
				 when isnull(SD.FISAP_Total_Income,FWS.FISAP_Total_Income)   >=     100000            then  '$100000-$and over '
				 else '$0-$5999'		 
			 end 
			when isnull(isnull(SD.Dependency, FWS.Dependency),'I') = 'I' then 
			case when isnull(SD.FISAP_Total_Income,FWS.FISAP_Total_Income) between -999999 and  1999  then  '$0-$1999'
				 when isnull(SD.FISAP_Total_Income,FWS.FISAP_Total_Income) between    2000 and  3999  then  '$2000-$3999'
				 when isnull(SD.FISAP_Total_Income,FWS.FISAP_Total_Income) between    4000 and  7999  then  '$4000-$7999'
				 when isnull(SD.FISAP_Total_Income,FWS.FISAP_Total_Income) between    8000 and  11999 then  '$8000-$11999'
				 when isnull(SD.FISAP_Total_Income,FWS.FISAP_Total_Income) between   12000 and  15999 then  '$12000-$15999'
				 when isnull(SD.FISAP_Total_Income,FWS.FISAP_Total_Income) between   16000 and  19999 then  '$16000-$19999'
				 when isnull(SD.FISAP_Total_Income,FWS.FISAP_Total_Income) between   20000 and  24999 then  '$20000-$24999'
				 when isnull(SD.FISAP_Total_Income,FWS.FISAP_Total_Income) between   25000 and  29999 then  '$25000-$29999'
				 when isnull(SD.FISAP_Total_Income,FWS.FISAP_Total_Income) between   30000 and  34999 then  '$30000-$34999'
				 when isnull(SD.FISAP_Total_Income,FWS.FISAP_Total_Income) between   35000 and  39999 then  '$35000-$39999'
				 when isnull(SD.FISAP_Total_Income,FWS.FISAP_Total_Income)   >=      40000            then  '$40000-$and over '

				 else '$0-$1999'
			end 
		  end as IncCategory
	, EnrollStat = (select top 1 EnrSt.Descrip 
						from campusvue.dbo.AdEnroll (nolock) as AE
							join campusvue.dbo.AdAttStat (nolock) as EnrSt
								on EnrSt.AdAttStatID = AE.adAttStatID
					where AE.SyStudentID = SYID.SyStudentID
					  order by AE.AdEnrollID desc)

	 , Grad = case when G.SyStudentID is not null then 1 
				   else 0 
				 end 
from 
(select distinct systudentid
	from ##SEOGDisb as SD
  union 
  Select systudentid 
  from ##FWS) as SYID
left join ##SEOGDisb as SD
 on sd.systudentid = SYID.SyStudentID
left join ##FWS as FWS
	on FWS.Systudentid = SYID.SyStudentid
left Join ##GradList G
	on G.SyStudentid = SYID.SyStudentID 

drop table ##FWS
drop table ##SEOGDisb
drop table ##GradList

end 


if @ISIRsource = 2 
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
/*If it is pulling from Portal tables*/
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

begin 
WITH  SEOGDisb_cte
as 
(
select SEOGDisb = sum(STN.Amount)
	 , STN.SyStudentID
	 , ss.SSN
	 , FLI.[LastIsirTransactionId]
		  , Dependency = case FLI.DependencyStatus
							when 'D' then 'D'
							when 'I' then 'I'
							when 'X' then 'D'
							when 'Y' then 'I'
						end  
		  , AutoZero = FLI.Auto0EFCFlag
		  , FISAP_Total_Income  = isnull(FLI.FTI,case FLI.DependencyStatus when 'D' then FLI.STI+FLI.TI when 'X' then FLI.STI+FLI.TI else FLI.STI end   )
	-- , StatGrantDisb = (case when FSA.FaFundSourceID = 34 and   then  STN.Amount else 0 end)
from campusvue.dbo.SaTrans (nolock) as STN
join campusvue.dbo.FaStudentAid (nolock) as FSA
	on FSA.FaStudentAidId = STN.FaStudentAidID
join campusvue.dbo.syStudent (nolock) as SS
	on SS.SyStudentId = STN.SyStudentID
left join campusvue.dbo.FaRefund (nolock) as FR
	on FR.FaRefundID = STN.FaRefundID
left join @IsirLink2 as FLI
		on FLI.SyStudentID = SS.SyStudentId 

where FR.SaStipendID is null
      and FSA.fafundsourceid = 2
      and FSA.AwardYear = @AY
group by SS.SyStudentId
	 , FLI.[LastIsirTransactionId]
	 , FLI.Auto0EFCFlag
	 , FLI.DependencyStatus
	 , FLI.FTI
	 , FLI.STI
	 , FLI.TI
	 , STN.SyStudentID
	 	 , ss.SSN
)


select isnull(SD.SEOGDisb,0) as SEOGDisbursed
	 , isnull(FWS.[Actual Draw]/FWS.[Reimbursement Rate],0) as FWSPaid 
	 , isnull(SD.Systudentid,SS.Systudentid) as Systudentid 
	 , isnull(SD.SSN, FWS.[SSN (Formatted)]) as SSN 
	 , isnull(SD.[LastIsirTransactionId],FLI.[LastIsirTransactionId]) as [LastIsirTransactionId]
	 , Dependency = isnull( dependency, case FLI.DependencyStatus
											when 'D' then 'D'
											when 'I' then 'I'
											when 'X' then 'D'
											when 'Y' then 'I'
										end ) 
	 , AutoZero = isnull(sd.AutoZero,FLI.Auto0EFCFlag)
	 , case when isnull( dependency, case FLI.DependencyStatus
											when 'D' then 'D'
											when 'I' then 'I'
											when 'X' then 'D'
											when 'Y' then 'I'
										end )  = 'D' then 
			case when isnull(SD.FISAP_Total_Income,isnull(FLI.FTI,case FLI.DependencyStatus when 'D' then FLI.STI+FLI.TI when 'X' then FLI.STI+FLI.TI else FLI.STI end  )) between -999999 and  5999 then  '$0-$5999'
				 when isnull(SD.FISAP_Total_Income,isnull(FLI.FTI,case FLI.DependencyStatus when 'D' then FLI.STI+FLI.TI when 'X' then FLI.STI+FLI.TI else FLI.STI end  )) between    6000 and  11999 then  '$6000-$11999'
				 when isnull(SD.FISAP_Total_Income,isnull(FLI.FTI,case FLI.DependencyStatus when 'D' then FLI.STI+FLI.TI when 'X' then FLI.STI+FLI.TI else FLI.STI end  )) between   12000 and  23999 then  '$12000-$23999'
				 when isnull(SD.FISAP_Total_Income,isnull(FLI.FTI,case FLI.DependencyStatus when 'D' then FLI.STI+FLI.TI when 'X' then FLI.STI+FLI.TI else FLI.STI end  )) between   24000 and  29999 then  '$24000-$29999'
				 when isnull(SD.FISAP_Total_Income,isnull(FLI.FTI,case FLI.DependencyStatus when 'D' then FLI.STI+FLI.TI when 'X' then FLI.STI+FLI.TI else FLI.STI end  )) between   30000 and  41999 then  '$30000-$41999'
				 when isnull(SD.FISAP_Total_Income,isnull(FLI.FTI,case FLI.DependencyStatus when 'D' then FLI.STI+FLI.TI when 'X' then FLI.STI+FLI.TI else FLI.STI end  )) between   42000 and  59999 then  '$42000-$59999'
				 when isnull(SD.FISAP_Total_Income,isnull(FLI.FTI,case FLI.DependencyStatus when 'D' then FLI.STI+FLI.TI when 'X' then FLI.STI+FLI.TI else FLI.STI end  )) between   60000 and  69999 then  '$60000-$69999'
				 when isnull(SD.FISAP_Total_Income,isnull(FLI.FTI,case FLI.DependencyStatus when 'D' then FLI.STI+FLI.TI when 'X' then FLI.STI+FLI.TI else FLI.STI end  )) between   70000 and  79999 then  '$70000-$79999'
				 when isnull(SD.FISAP_Total_Income,isnull(FLI.FTI,case FLI.DependencyStatus when 'D' then FLI.STI+FLI.TI when 'X' then FLI.STI+FLI.TI else FLI.STI end  )) between   80000 and  89999 then  '$80000-$89999'
				 when isnull(SD.FISAP_Total_Income,isnull(FLI.FTI,case FLI.DependencyStatus when 'D' then FLI.STI+FLI.TI when 'X' then FLI.STI+FLI.TI else FLI.STI end  )) between   90000 and  99999 then  '$90000-$99999'
				 when isnull(SD.FISAP_Total_Income,isnull(FLI.FTI,case FLI.DependencyStatus when 'D' then FLI.STI+FLI.TI when 'X' then FLI.STI+FLI.TI else FLI.STI end  ))   >=     100000            then  '$100000-$and over '
				 else NULL		 
			 end 
			when isnull( dependency, case FLI.DependencyStatus
											when 'D' then 'D'
											when 'I' then 'I'
											when 'X' then 'D'
											when 'Y' then 'I'
										end )  = 'I' then 
			case when isnull(SD.FISAP_Total_Income,isnull(FLI.FTI,case FLI.DependencyStatus when 'D' then FLI.STI+FLI.TI when 'X' then FLI.STI+FLI.TI else FLI.STI end  )) between -999999 and  1999  then  '$0-$1999'
				 when isnull(SD.FISAP_Total_Income,isnull(FLI.FTI,case FLI.DependencyStatus when 'D' then FLI.STI+FLI.TI when 'X' then FLI.STI+FLI.TI else FLI.STI end  )) between    2000 and  3999  then  '$2000-$3999'
				 when isnull(SD.FISAP_Total_Income,isnull(FLI.FTI,case FLI.DependencyStatus when 'D' then FLI.STI+FLI.TI when 'X' then FLI.STI+FLI.TI else FLI.STI end  )) between    4000 and  7999  then  '$4000-$7999'
				 when isnull(SD.FISAP_Total_Income,isnull(FLI.FTI,case FLI.DependencyStatus when 'D' then FLI.STI+FLI.TI when 'X' then FLI.STI+FLI.TI else FLI.STI end  )) between    8000 and  11999 then  '$8000-$11999'
				 when isnull(SD.FISAP_Total_Income,isnull(FLI.FTI,case FLI.DependencyStatus when 'D' then FLI.STI+FLI.TI when 'X' then FLI.STI+FLI.TI else FLI.STI end  )) between   12000 and  15999 then  '$12000-$15999'
				 when isnull(SD.FISAP_Total_Income,isnull(FLI.FTI,case FLI.DependencyStatus when 'D' then FLI.STI+FLI.TI when 'X' then FLI.STI+FLI.TI else FLI.STI end  )) between   16000 and  19999 then  '$16000-$19999'
				 when isnull(SD.FISAP_Total_Income,isnull(FLI.FTI,case FLI.DependencyStatus when 'D' then FLI.STI+FLI.TI when 'X' then FLI.STI+FLI.TI else FLI.STI end  )) between   20000 and  24999 then  '$20000-$24999'
				 when isnull(SD.FISAP_Total_Income,isnull(FLI.FTI,case FLI.DependencyStatus when 'D' then FLI.STI+FLI.TI when 'X' then FLI.STI+FLI.TI else FLI.STI end  )) between   25000 and  29999 then  '$25000-$29999'
				 when isnull(SD.FISAP_Total_Income,isnull(FLI.FTI,case FLI.DependencyStatus when 'D' then FLI.STI+FLI.TI when 'X' then FLI.STI+FLI.TI else FLI.STI end  )) between   30000 and  34999 then  '$30000-$34999'
				 when isnull(SD.FISAP_Total_Income,isnull(FLI.FTI,case FLI.DependencyStatus when 'D' then FLI.STI+FLI.TI when 'X' then FLI.STI+FLI.TI else FLI.STI end  )) between   35000 and  39999 then  '$35000-$39999'
				 when isnull(SD.FISAP_Total_Income,isnull(FLI.FTI,case FLI.DependencyStatus when 'D' then FLI.STI+FLI.TI when 'X' then FLI.STI+FLI.TI else FLI.STI end  ))   >=      40000            then  '$40000-$and over '

				 else NULL
			end 
		  end as IncCategory
	, EnrollStat = (select top 1 EnrSt.Descrip 
						from campusvue.dbo.AdEnroll (nolock) as AE
							join campusvue.dbo.AdAttStat (nolock) as EnrSt
								on EnrSt.AdAttStatID = AE.adAttStatID
					where AE.SyStudentID = SD.SyStudentID
					  order by AE.AdEnrollID desc)

   
from SEOGDisb_cte as SD
full join  [Financial Planning].dbo.[1415FWS] (nolock) FWS
	on fws.[SSN (Formatted)]= sd.SSN
left join campusvue.dbo.sycampus (nolock) as SC
	on sc.descrip = fws.campus
left join campusvue.dbo.systudent (nolock) as SS
	on ss.ssn = fws.[SSN (Formatted)]
	and ss.sycampusid = sc.sycampusid 
left join @IsirLink2 as FLI
		on FLI.SyStudentID = SS.SyStudentId 


end 
	       

------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
/*

FWS

*/
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------


select * 
from [Financial Planning].dbo.[1415FWS] (nolock) FWS
