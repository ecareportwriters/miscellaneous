select replace(ssn,'-','')
	 , firstname
	 , convert(varchar(4),year(dob))+ case when Month(dob)<10 then '0' +convert(varchar(2), month(dob))
										   else convert(varchar(2), month(dob))
										  end  +case when day(dob)<10 then '0' +convert(varchar(2), day(dob))
										   else convert(varchar(2), day(dob))
										  end 
from systudent (nolock)
where ssn in ('640-48-5246',
'629-03-4218',
'421-21-6997',
'426-51-8716',
'513-70-5666',
'254-55-6551',
'412-63-0076',
'421-19-5172',
'629-12-6437',
'417-21-6917',
'424-21-7166',
'427-65-1222',
'615-32-7686',
'416-27-8148',
'271-78-9138',
'426-69-0253',
'438-31-7104',
'594-24-3769',
'079-74-4965',
'175-72-9216',
'249-81-5660',
'449-63-4699',
'418-19-5948',
'228-69-4815',
'421-82-0374',
'258-63-2326',
'638-16-1939',
'425-49-3683',
'264-60-3814',
'249-83-0838',
'269-82-0215'

)