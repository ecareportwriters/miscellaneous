
/****** Object:  StoredProcedure [dbo].[NECB_GE]    Script Date: 9/29/2016 3:31:29 PM ******/
SET ANSI_NULLS ON

SET QUOTED_IDENTIFIER ON




	declare @FaYearID AS VARCHAR(2)
 
 
set @fayearid = '17'
	

IF object_id('Tempdb..##Work2') IS NOT NULL  
BEGIN 
DROP TABLE ##Work2
END

IF object_id('Tempdb..##work') IS NOT NULL  
BEGIN 
DROP TABLE ##work
END

IF object_id('Tempdb..##T4Recipients') IS NOT NULL  
BEGIN 
DROP TABLE ##T4Recipients
END

IF object_id('Tempdb..##Work3') IS NOT NULL  
BEGIN 
DROP TABLE ##Work3
END


SELECT SSSold.descrip AS PREV
	, SSSnew.descrip AS New 
	, SSC.dateadded
	, SSC.effectivedate
	, SSC.storedlda
	, SSC.prevsyschoolstatusid
	, SSC.newsyschoolstatusid
	, SSC.systudentid
	, SSC.adenrollid
	, SSC.systatchangeid
	
INTO ##work
FROM CampusVue_NECB.dbo.systatchange AS SSC WITH (NOLOCK)
INNER JOIN CampusVue_NECB.dbo.SySchoolStatus AS SSSnew WITH (NOLOCK)
				ON SSSnew.SySchoolStatusID = SSC.newSySchoolstatusid
INNER JOIN CampusVue_NECB.dbo.SySchoolStatus AS SSSold WITH (NOLOCK) 
				ON SSSold.SySchoolStatusID = SSC.prevSySchoolstatusid

WHERE  (
 (
 newsyschoolstatusid IN (	13,   --Active
							14,   --Active - Academic Probation
							53,   --Pending Graduate
							58,   --Active-SC
							69,   --Active Academic Warning
							71,   --Active Academic Suspension
							79,   --Active - Documents Missing
							101,  --Active - Academic Warning
							124,  --Active - Waiting for Class-DONT USE
							126,  --Active - Waiting for Class
							152,  --Active Academic Probation
							158,  --Active-Provisionally
							179,  --Agency Active
							198   --Agency Pending Grad
							)
 AND prevsyschoolstatusid NOT IN ( 13,   --Active
								   14,   --Active - Academic Probation
								   53,   --Pending Graduate
								   58,   --Active-SC
								   69,   --Active Academic Warning
								   71,   --Active Academic Suspension
								   79,   --Active - Documents Missing
								   101,  --Active - Academic Warning
								   124,  --Active - Waiting for Class-DONT USE
								   126,  --Active - Waiting for Class
								   152,  --Active Academic Probation
								   158,  --Active-Provisionally
								   179,  --Agency Active
								   198   --Agency Pending Grad
								  )
)
OR 
 (
 newsyschoolstatusid IN (  15,  --Leave of Absence
							17,  --Graduate
							20,  --Dismissed
							24,  --Transfer To Other Program
							25,  --Transfer To Other Campus
							54,  --Withdrawn
							60,  --Leave of Absence-SC
							61,  --Withdrawn-SC
							70,  --Leave of Absence Probation
							95,  --Drop
							139,  --Administrative Withdrawal
							175,  --Step Out
							182  --Step Out -- SBNA
							)
 AND prevsyschoolstatusid NOT IN ( 15,  --Leave of Absence
								   17,  --Graduate
								   20,  --Dismissed
								   24,  --Transfer To Other Program
								   25,  --Transfer To Other Campus
								   54,  --Withdrawn
								   60,  --Leave of Absence-SC
								   61,  --Withdrawn-SC
								   70,  --Leave of Absence Probation
								   95,  --Drop
								   139,  --Administrative Withdrawal
								   175,  --Step Out
								   182  --Step Out -- SBNA
								  )
))

SELECT DISTINCT STN.systudentid
INTO ##T4Recipients
FROM CampusVue_NECB.dbo.satrans AS STN WITH (NOLOCK) 
INNER JOIN CampusVue_NECB.dbo.fastudentaid AS FSA WITH (NOLOCK) 
			ON FSA.Fastudentaidid = STN.FaStudentAidID
LEFT JOIN CampusVue_NECB.dbo.farefund AS FR WITH (NOLOCK) 
			ON fr.FaRefundId = STN.FaRefundID
WHERE FSA.FaFundSourceID in (21,23,22,20,15,14,2,12,3,13,1,4,30)
GROUP BY STN.SyStudentID
HAVING  SUM(CASE WHEN FSA.FaFundSourceID IN (21,23,22,20,15,14,2,12,3,13,1,4,30) AND FR.sastipendid IS NULL then STN.amount ELSE 0 END) > 0


SELECT W.*
     , W3.DateAdded AS DateInactive
	 , W3.NewSySchoolStatusID AS InactiveStatus
INTO ##Work2
FROM ##Work AS W
CROSS APPLY (SELECT TOP 1 W2.*
			FROM ##work AS W2
			WHERE w2.dateadded  >= W.DateAdded 
			AND W2.adenrollid = W.Adenrollid
			AND	(w2.newsyschoolstatusid IN (15,  --Leave of Absence
											17,  --Graduate
											20,  --Dismissed
											24,  --Transfer To Other Program
											25,  --Transfer To Other Campus
											54,  --Withdrawn
											60,  --Leave of Absence-SC
											61,  --Withdrawn-SC
											70,  --Leave of Absence Probation
											95,  --Drop
											139,  --Administrative Withdrawal
											175,  --Step Out
											182  --Step Out -- SBNA
											)
			AND w2.PrevSySchoolStatusID NOT IN (15,  --Leave of Absence
												17,  --Graduate
												20,  --Dismissed
												24,  --Transfer To Other Program
												25,  --Transfer To Other Campus
												54,  --Withdrawn
												60,  --Leave of Absence-SC
												61,  --Withdrawn-SC
												70,  --Leave of Absence Probation
												95,  --Drop
												139,  --Administrative Withdrawal
												175,  --Step Out
												182  --Step Out -- SBNA
												))
			ORDER BY DateAdded 
			) AS W3
WHERE

 (W.newsyschoolstatusid IN (13,   --Active
							14,   --Active - Academic Probation
							53,   --Pending Graduate
							58,   --Active-SC
							69,   --Active Academic Warning
							71,   --Active Academic Suspension
							79,   --Active - Documents Missing
							101,  --Active - Academic Warning
							124,  --Active - Waiting for Class-DONT USE
							126,  --Active - Waiting for Class
							152,  --Active Academic Probation
							158,  --Active-Provisionally
							179,  --Agency Active
							198   --Agency Pending Grad
							)
 AND W.prevsyschoolstatusid NOT IN (13,   --Active
									14,   --Active - Academic Probation
									53,   --Pending Graduate
									58,   --Active-SC
									69,   --Active Academic Warning
									71,   --Active Academic Suspension
									79,   --Active - Documents Missing
									101,  --Active - Academic Warning
									124,  --Active - Waiting for Class-DONT USE
									126,  --Active - Waiting for Class
									152,  --Active Academic Probation
									158,  --Active-Provisionally
									179,  --Agency Active
									198,   --Agency Pending Grad
									18	--complete
									))
ORDER BY W.adenrollid, dateadded 


SELECT FY.Descrip AS AwardYear
	 , FY.Fayearid
	 , FY.Startdate AS AYStart
	 , FY.EndDate AS AYEnd
	 , APV.adprogramversionid
	 , (SELECT  SUM(CASE stn3.type 
					WHEN 'I' THEN stn3.amount
					WHEN 'D' THEN stn3.amount
					ELSE -stn3.amount
					END)
		FROM CampusVue_NECB.dbo.satrans AS STN3 WITH (NOLOCK) 
		INNER JOIN CampusVue_NECB.dbo.adterm AS AT WITH (NOLOCK) 
				ON at.adtermid = stn3.adtermid
		INNER JOIN  CampusVue_NECB.dbo.adenroll AS AE3 WITH (NOLOCK) 
				ON ae3.systudentid = stn3.systudentid --ae3.adenrollid = stn3.adenrollid
			    AND at.startdate BETWEEN ae3.startdate AND ae3.lda
		INNER JOIN CampusVue_NECB.dbo.adprogramversion AS APV3 WITH (NOLOCK) 
				ON apv3.adprogramversionid = ae3.adprogramversionid
		INNER JOIN CampusVue_NECB.dbo.adprogramversionprogramgroup AS APG3 WITH (NOLOCK) 
				ON apg3.adprogramversionid = apv3.adprogramversionid
		INNER JOIN CampusVue_NECB.dbo.FaStudentAid AS FSA WITH (NOLOCK) 
				ON fsa.fastudentaidid = stn3.fastudentaidid
		LEFT JOIN CampusVue_NECB.dbo.farefund AS FR WITH (NOLOCK) 
				ON fr.farefundid = stn3.farefundid
		WHERE apg3.adprogramgroupid = apg.adprogramgroupid
		AND ae3.systudentid = ae.systudentid
		AND stn3.date <= CASE WHEN W.DateInactive < FY.EndDate THEN W.Dateinactive ELSE FY.EndDate END    
		AND fsa.fafundsourceid IN (21,23,22,20,15,14,2,12,3,13) 
		AND FR.sastipendid IS NULL 
		) AS Loans

	 , (SELECT SUM(CASE stn3.type 
					WHEN 'I' THEN stn3.amount
					WHEN 'D' THEN stn3.amount
					ELSE -stn3.amount
					END)
		FROM CampusVue_NECB.dbo.satrans AS STN3 WITH (NOLOCK) 
		INNER JOIN CampusVue_NECB.dbo.adterm AS AT WITH (NOLOCK) 
				ON at.adtermid = stn3.adtermid
		INNER JOIN  CampusVue_NECB.dbo.adenroll AS AE3 WITH (NOLOCK) 
				ON ae3.systudentid = stn3.systudentid --ae3.adenrollid = stn3.adenrollid
				AND (ae3.startdate IS NOT NULL AND ae3.lda IS NOT NULL)
				AND at.startdate BETWEEN ae3.startdate AND ae3.lda
		INNER JOIN CampusVue_NECB.dbo.adprogramversion AS APV3 WITH (NOLOCK) 
				ON apv3.adprogramversionid = ae3.adprogramversionid
		INNER JOIN CampusVue_NECB.dbo.adprogramversionprogramgroup AS APG3 WITH (NOLOCK) 
				ON apg3.adprogramversionid = apv3.adprogramversionid
		INNER JOIN CampusVue_NECB.dbo.FaStudentAid AS FSA WITH (NOLOCK) 
				ON fsa.fastudentaidid = stn3.fastudentaidid
		LEFT JOIN CampusVue_NECB.dbo.farefund AS FR WITH (NOLOCK) 
				ON fr.farefundid = stn3.farefundid
		WHERE apg3.adprogramgroupid = apg.adprogramgroupid
		AND ae3.systudentid = ae.systudentid
		AND stn3.date <= CASE WHEN W.DateInactive < FY.EndDate THEN W.Dateinactive ELSE FY.EndDate END    
		AND fsa.fafundsourceid IN (1,4,30) 
		AND FR.sastipendid IS NULL 
		) AS Grants

	 , (SELECT SUM(CASE stn3.type 
					WHEN 'I' THEN stn3.amount
					WHEN 'D' then stn3.amount
					ELSE -stn3.amount
					END)
		FROM CampusVue_NECB.dbo.satrans AS STN3 WITH (NOLOCK) 
		INNER JOIN CampusVue_NECB.dbo.adterm AS AT WITH (NOLOCK) 
				ON at.adtermid = stn3.adtermid
		INNER JOIN CampusVue_NECB.dbo.adenroll AS AE3 WITH (NOLOCK) 
				ON ae3.systudentid = stn3.systudentid --ae3.adenrollid = stn3.adenrollid
				AND (ae3.startdate IS NOT NULL and ae3.lda IS NOT NULL)
				AND at.startdate BETWEEN ae3.startdate AND ae3.lda
		INNER JOIN CampusVue_NECB.dbo.adprogramversion AS APV3 WITH (NOLOCK) 
				ON apv3.adprogramversionid = ae3.adprogramversionid
		INNER JOIN CampusVue_NECB.dbo.adprogramversionprogramgroup AS APG3 WITH (NOLOCK) 
				ON apg3.adprogramversionid = apv3.adprogramversionid
		INNER JOIN CampusVue_NECB.dbo.FaStudentAid AS FSA WITH (NOLOCK) 
				ON fsa.fastudentaidid = stn3.fastudentaidid
		LEFT JOIN CampusVue_NECB.dbo.farefund AS FR WITH (NOLOCK) 
				ON fr.farefundid = stn3.farefundid
		WHERE apg3.adprogramgroupid = apg.adprogramgroupid
		AND ae3.systudentid = ae.systudentid
		AND stn3.date <= CASE WHEN W.DateInactive < FY.EndDate THEN W.Dateinactive ELSE FY.EndDate  END    
		AND fsa.fafundsourceid IN (5,73) 
		AND FR.sastipendid IS NULL 
		) * -1 AS AltLoans  

	 , (SELECT SUM(CASE stn3.type 
					WHEN 'I' THEN stn3.amount
					WHEN 'D' THEN stn3.amount
					ELSE -stn3.amount
					END)
		FROM CampusVue_NECB.dbo.satrans AS STN3 WITH (NOLOCK) 
		INNER JOIN CampusVue_NECB.dbo.adterm AS AT WITH (NOLOCK) 
				ON at.adtermid = stn3.adtermid
		INNER JOIN CampusVue_NECB.dbo.adenroll AS AE3 WITH (NOLOCK) 
				ON ae3.systudentid = stn3.systudentid --ae3.adenrollid = stn3.adenrollid
			    AND (ae3.startdate IS NOT NULL and ae3.lda IS NOT NULL)
				AND at.startdate BETWEEN ae3.startdate AND ae3.lda 
		INNER JOIN CampusVue_NECB.dbo.adprogramversion AS APV3 WITH (NOLOCK) 
				ON apv3.adprogramversionid = ae3.adprogramversionid
		INNER JOIN CampusVue_NECB.dbo.adprogramversionprogramgroup AS APG3 WITH (NOLOCK) 
				ON apg3.adprogramversionid = apv3.adprogramversionid
		WHERE apg3.adprogramgroupid = apg.adprogramgroupid
		AND ae3.systudentid = ae.systudentid
		AND stn3.date <= CASE WHEN W.DateInactive < FY.EndDate THEN W.Dateinactive ELSE FY.EndDate END    
		AND stn3.sabillcode IN ('TUIT','REG','TUITADJ','MEMDISO','MEMDISO','MEMDISC','PARTDISO','TECH','PARTDIS','APPFEE','GRADFEE','TUITIOL','REGFEE','ADMIN2','WRITEOFF','TUIT CR','WAIVTUIT','PTDISC','RTDISC','EEWAIV','ERESOURC')							  
		) AS Billed  

	 , (SELECT SUM(CASE stn3.type 
					WHEN 'I' THEN stn3.amount
					WHEN 'D' THEN stn3.amount
					ELSE -stn3.amount
					END)
		FROM CampusVue_NECB.dbo.satrans AS STN3 WITH (NOLOCK) 
		INNER JOIN CampusVue_NECB.dbo.adterm AS AT WITH (NOLOCK) 
				ON at.adtermid = stn3.adtermid
		INNER JOIN CampusVue_NECB.dbo.adenroll AS AE3 WITH (NOLOCK) 
				ON ae3.systudentid = stn3.systudentid --ae3.adenrollid = stn3.adenrollid
			    AND (ae3.startdate IS NOT NULL AND ae3.lda IS NOT NULL)
				AND at.startdate BETWEEN ae3.startdate AND ae3.lda
		INNER JOIN CampusVue_NECB.dbo.adprogramversion AS APV3 WITH (NOLOCK) 
				ON apv3.adprogramversionid = ae3.adprogramversionid
		INNER JOIN CampusVue_NECB.dbo.adprogramversionprogramgroup AS APG3 WITH (NOLOCK) 
				ON apg3.adprogramversionid = apv3.adprogramversionid
		WHERE apg3.adprogramgroupid = apg.adprogramgroupid
		AND ae3.systudentid = ae.systudentid
		AND stn3.date <= CASE WHEN W.DateInactive < FY.EndDate THEN W.Dateinactive ELSE FY.EndDate END    
		AND stn3.sabillcode IN ('BOOK','LIBRARY','EBOOK')
		) AS Books
		
	 , (SELECT COUNT(DISTINCT stn3.adtermid)
		FROM CampusVue_NECB.dbo.satrans AS STN3 WITH (NOLOCK) 
		INNER JOIN CampusVue_NECB.dbo.adterm AS AT WITH (NOLOCK) 
				ON at.adtermid = stn3.adtermid
		INNER JOIN CampusVue_NECB.dbo.adenroll AS AE3 WITH (NOLOCK) 
				ON ae3.systudentid = stn3.systudentid --ae3.adenrollid = stn3.adenrollid
			    AND (ae3.startdate IS NOT NULL AND ae3.lda IS NOT NULL)
				AND at.startdate BETWEEN ae3.startdate AND ae3.lda
		INNER JOIN CampusVue_NECB.dbo.adprogramversion AS APV3 WITH (NOLOCK) 
				ON apv3.adprogramversionid = ae3.adprogramversionid
		INNER JOIN CampusVue_NECB.dbo.adprogramversionprogramgroup AS APG3 WITH (NOLOCK) 
				ON apg3.adprogramversionid = apv3.adprogramversionid
		WHERE apg3.adprogramgroupid = apg.adprogramgroupid
		AND ae3.systudentid = ae.systudentid
		AND stn3.date <= CASE WHEN W.DateInactive < FY.EndDate THEN W.Dateinactive ELSE FY.EndDate END   
		) AS Terms
	 , W.*
INTO ##work3
FROM ##Work2 AS W
INNER JOIN CampusVue_NECB.dbo.fayear AS FY WITH (NOLOCK) 
	ON FY.StartDate BETWEEN W.DateAdded AND W.DateInactive
	OR W.dateadded BETWEEN FY.StartDate AND FY.EndDate
	OR W.DateInactive BETWEEN FY.StartDate AND FY.EndDate
INNER JOIN ##T4Recipients AS T
	ON T.systudentid = W.Systudentid
INNER JOIN CampusVue_NECB.dbo.adenroll AS AE WITH (NOLOCK) 
	ON AE.AdEnrollID = W.AdEnrollID
INNER JOIN CampusVue_NECB.dbo.adprogramversion AS APV WITH (NOLOCK) 
	ON APV.Adprogramversionid = ae.adprogramversionid 
INNER JOIN CampusVue_NECB.dbo.adprogramversionprogramgroup AS APG WITH (NOLOCK) 
	ON apg.adprogramversionid = apv.adprogramversionid
--left join satrans WITH (NOLOCK) as STN
--	on stn.systudentid = W.systudentid
--left join fastudentaid WITH (NOLOCK) as FSA
--	on fsa.fastudentaidid = stn.fastudentaidid 
--left join fafundsource WITH (NOLOCK) as FFS
--	on ffs.fafundsourceid = fsa.fafundsourceid
--left join FaRefund WITH (NOLOCK) as FR
--	on FR.farefundid = stn.farefundid
--left join adenroll WITH (NOLOCK) as AE2
--	on ae2.adenrollid = stn.adenrollid
--left join adprogramversionprogramgroup WITH (NOLOCK) as APV2
--	on apv2.adprogramversionid = ae2.adprogramversionid
--	and apv2.adprogramgroupid = apg.adprogramgroupid

WHERE fy.fayearid =  @FaYearID 
--and apv.CipCode = @CipCode
 --W.systudentid = 51211
GROUP BY  FY.Descrip
		, FY.Fayearid
		, FY.Startdate 
		, FY.EndDate
		, W.PREV
		, W.New 
		, W.dateadded
		, W.effectivedate
		, W.storedlda
		, W.prevsyschoolstatusid
		, W.newsyschoolstatusid
		, W.systudentid
		, W.adenrollid
		, W.systatchangeid
		, W.DateInactive
		, W.InactiveStatus
		, apv.adprogramversionid
		, apg.adprogramgroupid
		, ae.systudentid

	
SELECT LEFT(W.Awardyear,4) + '20' + RIGHT(W.awardyear,2) AS AwardYear
	 , SSN = REPLACE(SS.SSN,'-','')
	 , RTRIM(LEFT(REPLACE(REPLACE(REPLACE(SS.FirstName,CHAR(13),''),CHAR(10),''),',',''),35)) AS FirstName
	 , RTRIM(LEFT(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(SS.MiddleName,CHAR(13),''),CHAR(10),''),'.',''),'(',''),')',''),',',''),35)) AS MiddleName
	 , RTRIM(LEFT(REPLACE(REPLACE(SS.LastName,CHAR(13),''),',',''),35)) AS LastName
	 , DOB = ISNULL(CONVERT(VARCHAR(10),SS.DOB,112),'0000000000')
	 , InstitutionCode = '03965300' 
	 , InstitutionName = 'New England College of Business and Finance'
	 , Filler1 = ''
	 , ProgramName = APV.Descrip
	 , CONVERT(VARCHAR(6),REPLACE(APV.CIPcode,'.','')) AS CIPCode
	 , Degree = CASE WHEN AD.Descrip LIKE '%single%' THEN '01' 
					 WHEN AD.Descrip LIKE '%Diploma%' THEN  '01'
					 WHEN AD.Descrip LIKE '%Certificate%' THEN '01'
					 WHEN AD.Descrip LIKE '%Associate%' THEN '02'
					 WHEN AD.Descrip LIKE '%Bachelor%' THEN '03'
					 WHEN AD.Descrip LIKE '%Master%' THEN '05'
					 WHEN APV.Descrip= 'Accounting and Finance - Forensic Accounting' THEN '08'
					 WHEN APV.Descrip= 'Accounting and Finance - Basic' THEN '08'
					 WHEN APV.Descrip= 'Accounting and Finance - Intermediate' THEN '08'
				END 
	 , MorDResidency = 'N'
	 , Filler2 = ' '
	 , StartDate = CONVERT(VARCHAR(10),CASE WHEN AE.StartDate > W.AYEnd THEN W.EffectiveDate ELSE AE.StartDate END ,112)
	 , CONVERT(VARCHAR(10), CASE WHEN (SELECT MIN(EffectiveDate)
									   FROM ##work3 AS W3
									   WHERE W3.systudentid = W.systudentid 
									   AND W3.adprogramversionid = W.adprogramversionid 
									   ) < W.AYStart
								 THEN W.AYStart
			                     ELSE  (SELECT MIN(EffectiveDate)
										FROM ##work3 AS W3
										WHERE W3.systudentid = W.systudentid 
										AND W3.adprogramversionid = W.adprogramversionid 
										) 
								 END ,112) AS ProgramFirstAttendance

	 , CASE WHEN ISNULL(W.DateInactive,'3000-01-01') > W.AYEnd THEN 'E'
		    WHEN ISNULL(W.DateInactive,'3000-01-01')  <= W.AYend AND W.InactiveStatus = 17 THEN 'G'
		    ELSE 'W'
		    END AS [Program Attendance Status During Award Year]

	 , [Program Attendance Status Date] = CONVERT(VARCHAR(10),CASE WHEN ISNULL(W.DateInactive,'3000-01-01') <= W.AYEnd THEN W.Dateinactive 
																   WHEN ISNULL(W.DateInactive,'3000-01-01')<=W.AYStart THEN W.AYStart
															  ELSE W.AYEND
															  END ,112)
	 , PrivateLoans = CASE WHEN W.Dateinactive <= W.AYEnd AND W.AltLoans >0 THEN W.AltLoans ELSE 0 END 
	 , InstitutionalDebt = CASE WHEN W.Dateinactive <= W.AYEnd THEN (SELECT SUM(CASE STN.type 
																				WHEN 'I' THEN STN.Amount
																				WHEN 'D' THEN STN.Amount
																				ELSE STN.amount *-1 
																				END)
																	 FROM CampusVue_NECB.dbo.satrans AS STN WITH (NOLOCK) 
																	 WHERE stn.systudentid = W.systudentid
																	 AND stn.date<=DATEADD(DAY,30,W.Dateinactive)
																	 )
							ELSE 0 
							END 

	 , Billed = CASE WHEN W.Dateinactive <= W.AYEnd THEN W.billed ELSE 0 END 
	 , BooksAndSupplies =  CASE WHEN (W.Terms/2 *800)<W.Books THEN W.books ELSE (W.Terms/2 *800) END 
	 , [Program Length] = CASE WHEN LEN(CONVERT(VARCHAR(MAX),CONVERT(INT,APV.totalweeks)))<6 THEN LEFT('000000',6-LEN(CONVERT(VARCHAR(MAX),CONVERT(INT,APV.totalweeks)))) + CONVERT(VARCHAR(MAX),CONVERT(INT,APV.totalweeks))
								ELSE LEFT(CONVERT(VARCHAR(MAX),CONVERT(INT,APV.totalweeks)),6)
								END 
	 , [Program Length Measurement] = 'W'
	 , EnrollmentStatus =  CASE (SELECT TOP 1 AAS.Descrip
								 FROM CampusVue_NECB.dbo.systatchange AS SSC WITH (NOLOCK) 
								 INNER JOIN CampusVue_NECB.dbo.adAttStat AS AAS WITH (NOLOCK) 
											ON AAS.AdAttStatID = SSC.NewAdAttStatID
								 WHERE SSC.dateadded <= CASE WHEN W.Dateinactive <= W.AYEnd THEN W.Dateinactive 
															 WHEN W.DateAdded<=W.AYStart THEN W.AYStart
															 ELSE W.DateAdded
															 END
								 ORDER BY ssc.dateadded DESC)
							WHEN 'Full Time' THEN 'F'
							WHEN '3/4 Time' THEN 'Q'
							WHEN 'Less than half time' THEN 'L'
							WHEN 'Half Time' THEN 'H'
						   ELSE 'N'
						   END 
	
FROM ##work3 AS W
INNER JOIN CampusVue_NECB.dbo.systudent AS SS WITH (NOLOCK) 
			ON SS.systudentid = W.systudentid
INNER JOIN CampusVue_NECB.dbo.adenroll AS AE WITH (NOLOCK) 
			ON AE.adenrollid = W.adenrollid
INNER JOIN CampusVue_NECB.dbo.adprogramversion AS APV WITH (NOLOCK) 
			ON APV.adprogramversionid = W.adprogramversionid
INNER JOIN CampusVue_NECB.dbo.addegree AS AD WITH (NOLOCK) 
			ON AD.addegreeid = APV.addegreeid
INNER JOIN CampusVue_NECB.dbo.systatchange AS SSC WITH (NOLOCK) 
			ON SSC.systatchangeid = W.systatchangeid
	


DROP TABLE ##Work2
DROP TABLE ##work
DROP TABLE ##T4Recipients
DROP TABLE ##Work3



