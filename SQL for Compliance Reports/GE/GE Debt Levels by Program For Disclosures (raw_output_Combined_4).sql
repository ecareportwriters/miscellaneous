declare @CohortLength int 
declare @AwardYear varchar(7)
declare @LowDate datetime
declare @HighDate datetime

set @CohortLength = 4           
set @AwardYear = '2014-15'            

-- sets the low date according to the award year and cohort length
set @LowDate = convert(datetime,convert(varchar(4),left(@AwardYear,4)-(@cohortlength+2)) +'-07-01')


-- sets the high date according to the award year and cohort length
Set @HighDate = CONVERT(datetime,convert(varchar(4),left(@AwardYear,4)-2)+'-06-30')

print @LowDate
print @HighDate

if object_id('Tempdb..#Cohort') is not null 
begin 
drop table #cohort
end


if object_id('Tempdb..#AwardInfo') is not null 
begin 
drop table #AwardInfo
end

create table #cohort   --this table will be used to assemble the list of graduates
 (
 SyStudentId int
, sycampusid int 
, campuszip int
,CIPCode varchar(7)
,Program varchar(150)
,NormalProgramLength_Months int
, startdate datetime
, graddate  datetime
, lda datetime
, Degree varchar(5)
, FaAcademicCalendar int
)


create table #AwardInfo
 (
 SyStudentId int
, sycampusid int 
, campuszip int
,CIPCode varchar(7)
,Program varchar(150)
,NormalProgramLength_Months int
, startdate datetime
, graddate  datetime
, lda datetime
, Degree  varchar(5)
, FaAcademicCalendar int
, Brand varchar(4)
, Campus varchar(100)
, BalanceOnGradDate money 
, BalanceOnGradDateP30 Money
, Billed money
,Pell money
,T4Loans money
,Loans money
, TotalDebt money
, IncludeInCohort int 
)



--***********Legacy ECA

insert into #cohort 
select SS.SyStudentId
,SS.sycampusid
, sc.zip as CampusZip
,APV.CIPCode
,APV.Descrip as Program
,convert(int,APV.TotalMonths) as NormalProgramLength_Months
, ae.startdate
, ae.graddate
, ae.lda
, Degree= case when apv.code in ('131MA-D','131MA-C') then '48hrs'
			else
			   	case when AD.Descrip like '%single%'then 'N/A' 
					  when AD.Descrip like '%Diploma%' then  'DP/CT'
					  when AD.Descrip like '%Certificate%' then 'DP/CT'
					  when AD.Descrip like '%Associate%' then 'ASSC'
					  when AD.Descrip like '%Bachelor%' then 'BACH'
					  when AD.Descrip like '%Master%' then 'MAST'
					  when APV.Descrip= 'Accounting and Finance - Forensic Accounting'  then 'CERT'
					  when APV.Descrip= 'Accounting and Finance - Basic'  then 'CERT'
					  when APV.Descrip= 'Accounting and Finance - Intermediate'  then 'CERT'
					end 
			end 
, apv.FaAcademicCalendar
from Campusvue.Dbo.AdEnroll (nolock) as AE
join Campusvue.Dbo.Systudent (nolock) as SS
	on SS.systudentid=AE.SyStudentID
join Campusvue.Dbo.AdProgramVersion (nolock) AS APV
	on APV.AdProgramVersionID=AE.AdProgramVersionID
join Campusvue.Dbo.AdDegree (nolock) AS AD
	on AD.AdDegreeID=APV.AdDegreeID
join Campusvue.Dbo.sycampus (nolock) as SC
	on sc.sycampusid = ss.sycampusid
where AE.LDA between @LowDate and @HighDate and AE.SySchoolStatusID=17 and AE.sycampusid < 49


--***********NECB

insert into #cohort 
select SS.SyStudentId
,SS.sycampusid
, sc.zip as CampusZip
,APV.CIPCode
,APV.Descrip as Program
,convert(int,APV.TotalMonths) as NormalProgramLength_Months
, ae.startdate
, ae.graddate
, ae.lda
, Degree=case when AD.Descrip like '%single%'then 'N/A' 
		      when AD.Descrip like '%Diploma%' then  'DP/CT'
			  when AD.Descrip like '%Certificate%' then 'DP/CT'
			  when AD.Descrip like '%Associate%' then 'ASSC'
			  when AD.Descrip like '%Bachelor%' then 'BACH'
			  when AD.Descrip like '%Master%' then 'MAST'
			  when APV.Descrip= 'Accounting and Finance - Forensic Accounting'  then 'CERT'
			  when APV.Descrip= 'Accounting and Finance - Basic'  then 'CERT'
			  when APV.Descrip= 'Accounting and Finance - Intermediate'  then 'CERT'
			end 
,apv.FaAcademicCalendar
from campusvue_necb.dbo.AdEnroll (nolock) as AE
join campusvue_necb.dbo.Systudent (nolock) as SS
	on SS.systudentid=AE.SyStudentID
join campusvue_necb.dbo.AdProgramVersion (nolock) AS APV
	on APV.AdProgramVersionID=AE.AdProgramVersionID
join campusvue_necb.dbo.AdDegree (nolock) AS AD
	on AD.AdDegreeID=APV.AdDegreeID
join campusvue_necb.dbo.sycampus (nolock) as SC
	on sc.sycampusid = ss.sycampusid
where AE.LDA between @LowDate and @HighDate and AE.SySchoolStatusID=17 


--***********Brightwood

insert into #cohort 
select SS.SyStudentId
,SS.sycampusid
, sc.zip as CampusZip
,APV.CIPCode
,APV.Descrip as Program
,convert(int,APV.TotalMonths) as NormalProgramLength_Months
, ae.startdate
, ae.graddate
, ae.lda
, Degree=case when AD.Descrip like '%single%'then 'N/A' 
		      when AD.Descrip like '%Diploma%' then  'DP/CT'
			  when AD.Descrip like '%Certificate%' then 'DP/CT'
			  when AD.Descrip like '%Associate%' then 'ASSC'
			  when AD.Descrip like '%Bachelor%' then 'BACH'
			  when AD.Descrip like '%Master%' then 'MAST'
			  when APV.Descrip= 'Accounting and Finance - Forensic Accounting'  then 'CERT'
			  when APV.Descrip= 'Accounting and Finance - Basic'  then 'CERT'
			  when APV.Descrip= 'Accounting and Finance - Intermediate'  then 'CERT'
			  end
, apv.FaAcademicCalendar
from Campusvue.Dbo.AdEnroll (nolock) as AE
join Campusvue.Dbo.Systudent (nolock) as SS
	on SS.systudentid=AE.SyStudentID
join Campusvue.Dbo.AdProgramVersion (nolock) AS APV
	on APV.AdProgramVersionID=AE.AdProgramVersionID
join Campusvue.Dbo.AdDegree (nolock) AS AD
	on AD.AdDegreeID=APV.AdDegreeID
join Campusvue.Dbo.sycampus (nolock) as SC
	on sc.sycampusid = ss.sycampusid
where AE.LDA between @LowDate and @HighDate and AE.SySchoolStatusID=17 and SC.OfficialSchoolName like '%Brightwood%' 





create nonclustered index NIX_SIDCIDGDTSDTZIP on #cohort (systudentid, sycampusid,campuszip,startdate,graddate) 



--***********Legacy ECA

insert into #AwardInfo
select C.*
	 , Brand = Case when C.sycampusid in (39,20,18,21,17) then 'GAA '
		            when C.sycampusid = 28 then 'ECO '
					else 'VC  '
					end 
	 , sc.Descrip as Campus
	  , BalanceOnGradDate = sum(case when stn.date<=C.GradDate then case stn.type
																	when 'I' then Stn.amount
																	when 'D' then stn.amount
																	else -1*stn.amount 
																	end 
									else 0
								end )
	 , BalanceOnGradDateP30 = sum(case when stn.date<=dateadd(day,30,C.GradDate) then case stn.type
																						when 'I' then Stn.amount
																						when 'D' then stn.amount
																						else -1*stn.amount 
																						end 
									   else 0
								   end )
	 , sum( case when C.FaAcademicCalendar <>5 then  case when AT.Startdate < C.GradDate and stn.sabillcode in ('TUIT','INTTUIT','TUIT CR','EEWAIV','WAIVTUIT','FEE ACT','FEEOT','FEERE','TRANSADJ','TUITADJ','TUITCRSF','TUITCRAD','TERMGRAD','TERMNIGH','WRITOFF','WIADISC'
																							--NECB Codes 'AMDEP','APPFEE','BOOK','EEWAIV','PARTDIS','PARTDISO','PTDISC','RTDISC','TUIT','TUITADJ','TUIT CR','TUITIOL','WAIVTUIT','WRITEOFF','WOTRIAL'
																							
																							) then stn.amount else 0 end
											   else case when stn.date <= C.GradDate and  stn.sabillcode in ('TUIT','INTTUIT','TUIT CR','EEWAIV','WAIVTUIT','FEE ACT','FEEOT','FEERE','TRANSADJ','TUITADJ','TUITCRSF','TUITCRAD','TERMGRAD','TERMNIGH','WRITOFF','WIADISC'
																							--NECB Codes 'AMDEP','APPFEE','BOOK','EEWAIV','PARTDIS','PARTDISO','PTDISC','RTDISC','TUIT','TUITADJ','TUIT CR','TUITIOL','WAIVTUIT','WRITEOFF','WOTRIAL'
																							
																							) then stn.amount else 0 end
			 end  ) as Billed
	 , sum(case when C.FaAcademicCalendar <>5 then case when AT.Startdate < C.GradDate and  fsa.fafundsourceid =1  and fr.sastipendid is null  then stn.amount else 0 end
											  else case when stn.date <= C.GradDate and fsa.fafundsourceid =1  and fr.sastipendid is null  then stn.amount else 0 end
			end ) as Pell
	 , sum(case when C.FaAcademicCalendar <>5 then case when AT.Startdate <= C.GradDate and  fsa.fafundsourceid in (  5  --Direct Subsidized Loan
																													,7  --Direct Loan Unsubsidized Loan
																													,60 --Direct Loan Grad PLUS
																													,6  --FFEL Subsidized
																													,8  --FFEL Unsubsidized
																													/* NECB Codes
																														2   --Subsidized Loan
																														,3   --Unsubsidized Loan
																														,12  --Subsidized Stafford Loan
																														,13  --Unsubsidized Stafford Loan
																														,15  --GRAD Plus Loan
																														,20  --Federal Direct Subsidized Stafford Loan
																														,21  --Fed. Direct Unsubsidized Stafford Loan
																														,23  --Federal Direct Graduate PLUS Loan
																														*/) and fr.sastipendid is null then stn.amount else 0 end 
												else case when stn.date <= C.GradDate and fsa.fafundsourceid in (   5  --Direct Subsidized Loan
																																		,7  --Direct Loan Unsubsidized Loan
																																		,60 --Direct Loan Grad PLUS
																																		,6  --FFEL Subsidized
																																		,8  --FFEL Unsubsidized
																																		/* NECB Codes
																																			2   --Subsidized Loan
																																			,3   --Unsubsidized Loan
																																			,12  --Subsidized Stafford Loan
																																			,13  --Unsubsidized Stafford Loan
																																			,15  --GRAD Plus Loan
																																			,20  --Federal Direct Subsidized Stafford Loan
																																			,21  --Fed. Direct Unsubsidized Stafford Loan
																																			,23  --Federal Direct Graduate PLUS Loan
																																			*/) and fr.sastipendid is null then stn.amount else 0 end
			end ) as T4Loans
	  , sum(case when C.FaAcademicCalendar <>5 then case when AT.Startdate <= C.GradDate and  fsa.fafundsourceid in (    3	--Alternative Loans
																													 , 39   --Bridge Loan
																													 , 54	--Career Loan Program
																													 , 29	--Career Loan Program
																													 , 21	--Institutional Loan
																													 , 36	--Term Loan
																													 , 31	--Tiered Risk Loan
																													/* NECB Codes  select * from campusvue_NECB.dbo.fafundsource order by descrip
																													5	--Alternative Loans
																													, 7	--Alternative Loans
																													, 9	--Loan
																													*/) and fr.sastipendid is null then stn.amount else 0 end
												else case when stn.date <= C.GradDate and fsa.fafundsourceid in ( 3	--Alternative Loans
																																	 , 39   --Bridge Loan
																																	 , 54	--Career Loan Program
																																	 , 29	--Career Loan Program
																																	 , 21	--Institutional Loan
																																	 , 36	--Term Loan
																																	 , 31	--Tiered Risk Loan
																																	/* NECB Codes  select * from campusvue_NECB.dbo.fafundsource order by descrip
																																	5	--Alternative Loans
																																	, 7	--Alternative Loans
																																	, 9	--Loan
																																	*/) and fr.sastipendid is null then stn.amount else 0 end 
			end ) as Loans
	  , sum(case when C.FaAcademicCalendar <>5 then case when AT.Startdate < C.GradDate and  fsa.fafundsourceid in ( 5  --Direct Subsidized Loan
																													,7  --Direct Loan Unsubsidized Loan
																													,60 --Direct Loan Grad PLUS
																													,6  --FFEL Subsidized
																													,8  --FFEL Unsubsidized
																													, 3	--Alternative Loans
																													, 39   --Bridge Loan
																													, 54	--Career Loan Program
																													, 29	--Career Loan Program
																													, 21	--Institutional Loan
																													, 36	--Term Loan
																													, 31	--Tiered Risk Loan
																												/* NECB Codes  select * from campusvue_NECB.dbo.fafundsource order by descrip
																													5	--Alternative Loans
																													, 7	--Alternative Loans
																													, 9	--Loan
																												*/) and fr.sastipendid is null then stn.amount else 0 end 
												else case when stn.date <= C.GradDate and fsa.fafundsourceid in (   5  --Direct Subsidized Loan
																																		,7  --Direct Loan Unsubsidized Loan
																																		,60 --Direct Loan Grad PLUS
																																		,6  --FFEL Subsidized
																																		,8  --FFEL Unsubsidized
																																		, 3	--Alternative Loans
																																		, 39   --Bridge Loan
																																		, 54	--Career Loan Program
																																		, 29	--Career Loan Program
																																		, 21	--Institutional Loan
																																		, 36	--Term Loan
																																		, 31	--Tiered Risk Loan
																																	/* NECB Codes  select * from campusvue_NECB.dbo.fafundsource order by descrip
																																		5	--Alternative Loans
																																		, 7	--Alternative Loans
																																		, 9	--Loan
																																	*/) and fr.sastipendid is null then stn.amount else 0 end
		end ) as TotalDebt
	 , 0
from campusvue.dbo.satrans (nolock) as STN
join CampusVue.dbo.SyCampus (nolock) as SC
	on sc.sycampusid = stn.sycampusid
join #cohort as C
	on C.sycampusid = stn.SyCampusID
	and C.systudentid = stn.systudentid
	and C.campuszip = cast(SC.zip as int)
left join campusvue.dbo.fastudentaid (nolock) as FSA
	on fsa.fastudentaidid = stn.fastudentaidid
left join campusvue.dbo.farefund (nolock) as FR
	on FR.farefundid = stn.farefundid
join campusvue.dbo.adterm (nolock) as AT
	on at.adtermid = stn.adtermid
where
	SC.SyCampusID < 49
group by C.Systudentid
	   , C.sycampusid  
	   , C.campuszip 
	   , C.CIPCode 
       , C.Program 
	   , C.NormalProgramLength_Months
       , C.startdate 
       , C.graddate  
       , C.lda 
       , C.Degree
	   , sc.Descrip
	   , C.FaAcademicCalendar





--***********NECB

insert into #AwardInfo
select C.*
	 , Brand = 'NECB'
	 , sc.Descrip as Campus
	 , BalanceOnGradDate = sum(case when stn.date<=C.GradDate then case stn.type
																	when 'I' then Stn.amount
																	when 'D' then stn.amount
																	else -1*stn.amount 
																	end 
									else 0
								end )
	 , BalanceOnGradDateP30 = sum(case when stn.date<=dateadd(day,30,C.GradDate) then case stn.type
																						when 'I' then Stn.amount
																						when 'D' then stn.amount
																						else -1*stn.amount 
																						end 
									   else 0
								   end )
	 , sum( case when AT.Startdate < C.GradDate and stn.sabillcode in (--VC Codes'TUIT','INTTUIT','TUIT CR','EEWAIV','WAIVTUIT','FEE ACT','FEEOT','FEERE','TRANSADJ','TUITADJ','TUITCRSF','TUITCRAD','TERMGRAD','TERMNIGH','WRITOFF','WIADISC'
																							 'AMDEP','APPFEE','BOOK','EEWAIV','PARTDIS','PARTDISO','PTDISC','RTDISC','TUIT','TUITADJ','TUIT CR','TUITIOL','WAIVTUIT','WRITEOFF','WOTRIAL'
																							) then stn.amount else 0 end ) as Billed
	 , sum(case when AT.Startdate < C.GradDate and  fsa.fafundsourceid =1  and fr.sastipendid is null  then stn.amount else 0 end) as Pell
	 , sum(case when AT.Startdate < C.GradDate and  fsa.fafundsourceid in (/* VC Codes 
																								 5  --Direct Subsidized Loan
																								,7  --Direct Loan Unsubsidized Loan
																								,60 --Direct Loan Grad PLUS
																								,6  --FFEL Subsidized
																								,8  --FFEL Unsubsidized
																								*/
																								 2   --Subsidized Loan
																								,3   --Unsubsidized Loan
																								,12  --Subsidized Stafford Loan
																								,13  --Unsubsidized Stafford Loan
																								,15  --GRAD Plus Loan
																								,20  --Federal Direct Subsidized Stafford Loan
																								,21  --Fed. Direct Unsubsidized Stafford Loan
																								,23  --Federal Direct Graduate PLUS Loan
																									) and fr.sastipendid is null then stn.amount else 0 end ) as Loans
	, sum(case when AT.Startdate < C.GradDate and  fsa.fafundsourceid in ( /* VC Codes 3	--Alternative Loans
																								 , 39   --Bridge Loan
																								 , 54	--Career Loan Program
																								 , 29	--Career Loan Program
																								 , 21	--Institutional Loan
																								 , 36	--Term Loan
																								 , 31	--Tiered Risk Loan*/
																								  5	--Alternative Loans
																								 , 7	--Alternative Loans
																								 , 9	--Loan
																								) and fr.sastipendid is null then stn.amount else 0 end ) as Loans
	 , sum(case when AT.Startdate < C.GradDate and  fsa.fafundsourceid in (/*VC Codes 5  --Direct Subsidized Loan
																			,7  --Direct Loan Unsubsidized Loan
																			,60 --Direct Loan Grad PLUS
																			,6  --FFEL Subsidized
																			,8  --FFEL Unsubsidized
																	        , 3	--Alternative Loans
																			, 39   --Bridge Loan
																			, 54	--Career Loan Program
																			, 29	--Career Loan Program
																			, 21	--Institutional Loan
																			, 36	--Term Loan
																			, 31	--Tiered Risk Loan*/
																		    2   --Subsidized Loan
																			,3   --Unsubsidized Loan
																			,12  --Subsidized Stafford Loan
																			,13  --Unsubsidized Stafford Loan
																			,15  --GRAD Plus Loan
																			,20  --Federal Direct Subsidized Stafford Loan
																			,21  --Fed. Direct Unsubsidized Stafford Loan
																			,23  --Federal Direct Graduate PLUS Loan
																			, 5	--Alternative Loans
																			, 7	--Alternative Loans
																			, 9	--Loan
																		) and fr.sastipendid is null then stn.amount else 0 end ) as TotalDebt
	 , 0
from campusvue_necb.dbo.satrans (nolock) as STN
join campusvue_necb.dbo.SyCampus (nolock) as SC
	on sc.sycampusid = stn.sycampusid
join #cohort as C
	on C.sycampusid = stn.SyCampusID
	and C.systudentid = stn.systudentid
	and C.campuszip = cast(SC.zip as int)
left join campusvue_necb.dbo.fastudentaid (nolock) as FSA
	on fsa.fastudentaidid = stn.fastudentaidid
left join campusvue_necb.dbo.farefund (nolock) as FR
	on FR.farefundid = stn.farefundid
join campusvue_necb.dbo.adterm (nolock) as AT
	on at.adtermid = stn.adtermid
group by C.Systudentid
	   , C.sycampusid  
	   , C.campuszip 
	   , C.CIPCode 
       , C.Program 
	   , C.NormalProgramLength_Months
       , C.startdate 
       , C.graddate  
       , C.lda 
       , C.Degree
	   , sc.Descrip
	   , C.FaAcademicCalendar



--***********Brightwood



insert into #AwardInfo
select C.*
	 , Brand = 'BW  '
	 , sc.Descrip as Campus
	  , BalanceOnGradDate = sum(case when stn.date<=C.GradDate then case stn.type
																	when 'I' then Stn.amount
																	when 'D' then stn.amount
																	else -1*stn.amount 
																	end 
									else 0
								end )
	 , BalanceOnGradDateP30 = sum(case when stn.date<=dateadd(day,30,C.GradDate) then case stn.type
																						when 'I' then Stn.amount
																						when 'D' then stn.amount
																						else -1*stn.amount 
																						end 
									   else 0
								   end )
	 , sum( case when C.FaAcademicCalendar <>5 then  case when stn.date < C.GradDate and stn.sabillcode in ('TUIT','INTTUIT','TUIT CR','EEWAIV','WAIVTUIT','FEE ACT','FEEOT','FEERE','TRANSADJ','TUITADJ','TUITCRSF','TUITCRAD','TERMGRAD','TERMNIGH','WRITOFF','WIADISC'
																							--NECB Codes 'AMDEP','APPFEE','BOOK','EEWAIV','PARTDIS','PARTDISO','PTDISC','RTDISC','TUIT','TUITADJ','TUIT CR','TUITIOL','WAIVTUIT','WRITEOFF','WOTRIAL'
																							
																							) then stn.amount else 0 end
											   else case when stn.date <= C.GradDate and  stn.sabillcode in ('TUIT','INTTUIT','TUIT CR','EEWAIV','WAIVTUIT','FEE ACT','FEEOT','FEERE','TRANSADJ','TUITADJ','TUITCRSF','TUITCRAD','TERMGRAD','TERMNIGH','WRITOFF','WIADISC'
																							--NECB Codes 'AMDEP','APPFEE','BOOK','EEWAIV','PARTDIS','PARTDISO','PTDISC','RTDISC','TUIT','TUITADJ','TUIT CR','TUITIOL','WAIVTUIT','WRITEOFF','WOTRIAL'
																							
																							) then stn.amount else 0 end
			 end  ) as Billed
	 , sum(case when C.FaAcademicCalendar <>5 then case when stn.date < C.GradDate and  fsa.fafundsourceid =1  and fr.sastipendid is null  then stn.amount else 0 end
											  else case when stn.date <= C.GradDate and fsa.fafundsourceid =1  and fr.sastipendid is null  then stn.amount else 0 end
			end ) as Pell
	 , sum(case when C.FaAcademicCalendar <>5 then case when stn.date < C.GradDate and  fsa.fafundsourceid in (  5  --Direct Subsidized Loan
																													,7  --Direct Loan Unsubsidized Loan
																													,60 --Direct Loan Grad PLUS
																													,6  --FFEL Subsidized
																													,8  --FFEL Unsubsidized
																													/* NECB Codes
																														2   --Subsidized Loan
																														,3   --Unsubsidized Loan
																														,12  --Subsidized Stafford Loan
																														,13  --Unsubsidized Stafford Loan
																														,15  --GRAD Plus Loan
																														,20  --Federal Direct Subsidized Stafford Loan
																														,21  --Fed. Direct Unsubsidized Stafford Loan
																														,23  --Federal Direct Graduate PLUS Loan
																														*/) and fr.sastipendid is null then stn.amount else 0 end 
												else case when stn.date <= C.GradDate and fsa.fafundsourceid in (   5  --Direct Subsidized Loan
																																		,7  --Direct Loan Unsubsidized Loan
																																		,60 --Direct Loan Grad PLUS
																																		,6  --FFEL Subsidized
																																		,8  --FFEL Unsubsidized
																																		/* NECB Codes
																																			2   --Subsidized Loan
																																			,3   --Unsubsidized Loan
																																			,12  --Subsidized Stafford Loan
																																			,13  --Unsubsidized Stafford Loan
																																			,15  --GRAD Plus Loan
																																			,20  --Federal Direct Subsidized Stafford Loan
																																			,21  --Fed. Direct Unsubsidized Stafford Loan
																																			,23  --Federal Direct Graduate PLUS Loan
																																			*/) and fr.sastipendid is null then stn.amount else 0 end
			end ) as T4Loans
	  , sum(case when C.FaAcademicCalendar <>5 then case when stn.date < C.GradDate and  fsa.fafundsourceid in (    3	--Alternative Loans
																													 , 39   --Bridge Loan
																													 , 54	--Career Loan Program
																													 , 29	--Career Loan Program
																													 , 21	--Institutional Loan
																													 , 36	--Term Loan
																													 , 31	--Tiered Risk Loan
																													 ,731	--Alternative Loan
																													 ,2113	--Career Training Loan
																													 ,1079	--Chase Select Alternative Loan- All KHE
																													 ,977	--CitiAssist Alternative Loan - CommonLine
																													 ,7835	--Citibank Loan EFT Payment
																													 ,7836	--Citibank Loan Payment
																													 ,958	--Citibank Universal  Loan
																													 ,978	--Citibank Universal Loan - CommonLine
																													 ,9		--Direct Plus Loan
																													 ,4497	--Discover Private Loan
																													 ,1112	--Kaplan Choice Loan
																													 ,807	--Kaplan Loan
																													 ,1213	--Kaplan Value Loan
																													 ,802	--Key Loans
																													 ,6386	--NLSC Quest Loan
																													 ,691	--One Choice Alternative Loan
																													 ,693	--Outside Loans
																													 ,938	--Sallie Mae Signature Loan
																													 ,606	--SLM Alternative Loan
																													 ,815	--TERI Loan
																													 ,2272	--Wells Fargo Collegiate Loan
																													 ,1124	--Wells Fargo Connection Loan
																													 ,676	--Perkins

																													/* NECB Codes  select * from campusvue_NECB.dbo.fafundsource order by descrip
																													5	--Alternative Loans
																													, 7	--Alternative Loans
																													, 9	--Loan
																													*/) and fr.sastipendid is null then stn.amount else 0 end
												else case when stn.date <= C.GradDate and fsa.fafundsourceid in ( 3	--Alternative Loans
																																	 , 39   --Bridge Loan
																																	 , 54	--Career Loan Program
																																	 , 29	--Career Loan Program
																																	 , 21	--Institutional Loan
																																	 , 36	--Term Loan
																																	 , 31	--Tiered Risk Loan
																																	 ,731	--Alternative Loan
																																	 ,2113	--Career Training Loan
																																	 ,1079	--Chase Select Alternative Loan- All KHE
																																	 ,977	--CitiAssist Alternative Loan - CommonLine
																																	 ,7835	--Citibank Loan EFT Payment
																																	 ,7836	--Citibank Loan Payment
																																	 ,958	--Citibank Universal  Loan
																																	 ,978	--Citibank Universal Loan - CommonLine
																																	 ,9		--Direct Plus Loan
																																	 ,4497	--Discover Private Loan
																																	 ,1112	--Kaplan Choice Loan
																																	 ,807	--Kaplan Loan
																																	 ,1213	--Kaplan Value Loan
																																	 ,802	--Key Loans
																																	 ,6386	--NLSC Quest Loan
																																	 ,691	--One Choice Alternative Loan
																																	 ,693	--Outside Loans
																																	 ,938	--Sallie Mae Signature Loan
																																	 ,606	--SLM Alternative Loan
																																	 ,815	--TERI Loan
																																	 ,2272	--Wells Fargo Collegiate Loan
																																	 ,1124	--Wells Fargo Connection Loan
																																	 ,676	--Perkins
																																	/* NECB Codes  select * from campusvue_NECB.dbo.fafundsource order by descrip
																																	5	--Alternative Loans
																																	, 7	--Alternative Loans
																																	, 9	--Loan
																																	*/) and fr.sastipendid is null then stn.amount else 0 end 
			end ) as Loans
	  , sum(case when C.FaAcademicCalendar <>5 then case when stn.date < C.GradDate and  fsa.fafundsourceid in ( 5  --Direct Subsidized Loan
																													,7  --Direct Loan Unsubsidized Loan
																													,60 --Direct Loan Grad PLUS
																													,6  --FFEL Subsidized
																													,8  --FFEL Unsubsidized
																													, 3	--Alternative Loans
																													, 39   --Bridge Loan
																													, 54	--Career Loan Program
																													, 29	--Career Loan Program
																													, 21	--Institutional Loan
																													, 36	--Term Loan
																													, 31	--Tiered Risk Loan
																													,731	--Alternative Loan
																													,2113	--Career Training Loan
																													,1079	--Chase Select Alternative Loan- All KHE
																													,977	--CitiAssist Alternative Loan - CommonLine
																													,7835	--Citibank Loan EFT Payment
																													,7836	--Citibank Loan Payment
																													,958	--Citibank Universal  Loan
																													,978	--Citibank Universal Loan - CommonLine
																													,9		--Direct Plus Loan
																													,4497	--Discover Private Loan
																													,1112	--Kaplan Choice Loan
																													,807	--Kaplan Loan
																													,1213	--Kaplan Value Loan
																													,802	--Key Loans
																													,6386	--NLSC Quest Loan
																													,691	--One Choice Alternative Loan
																													,693	--Outside Loans
																													,938	--Sallie Mae Signature Loan
																													,606	--SLM Alternative Loan
																													,815	--TERI Loan
																													,2272	--Wells Fargo Collegiate Loan
																													,1124	--Wells Fargo Connection Loan
																													,676	--Perkins
																												/* NECB Codes  select * from campusvue_NECB.dbo.fafundsource order by descrip
																													5	--Alternative Loans
																													, 7	--Alternative Loans
																													, 9	--Loan
																												*/) and fr.sastipendid is null then stn.amount else 0 end 
												else case when stn.date <= C.GradDate and fsa.fafundsourceid in (   5  --Direct Subsidized Loan
																																		,7  --Direct Loan Unsubsidized Loan
																																		,60 --Direct Loan Grad PLUS
																																		,6  --FFEL Subsidized
																																		,8  --FFEL Unsubsidized
																																		, 3	--Alternative Loans
																																		, 39   --Bridge Loan
																																		, 54	--Career Loan Program
																																		, 29	--Career Loan Program
																																		, 21	--Institutional Loan
																																		, 36	--Term Loan
																																		, 31	--Tiered Risk Loan
																																		,731	--Alternative Loan
																																		 ,2113	--Career Training Loan
																																		 ,1079	--Chase Select Alternative Loan- All KHE
																																		 ,977	--CitiAssist Alternative Loan - CommonLine
																																		 ,7835	--Citibank Loan EFT Payment
																																		 ,7836	--Citibank Loan Payment
																																		 ,958	--Citibank Universal  Loan
																																		 ,978	--Citibank Universal Loan - CommonLine
																																		 ,9		--Direct Plus Loan
																																		 ,4497	--Discover Private Loan
																																		 ,1112	--Kaplan Choice Loan
																																		 ,807	--Kaplan Loan
																																		 ,1213	--Kaplan Value Loan
																																		 ,802	--Key Loans
																																		 ,6386	--NLSC Quest Loan
																																		 ,691	--One Choice Alternative Loan
																																		 ,693	--Outside Loans
																																		 ,938	--Sallie Mae Signature Loan
																																		 ,606	--SLM Alternative Loan
																																		 ,815	--TERI Loan
																																		 ,2272	--Wells Fargo Collegiate Loan
																																		 ,1124	--Wells Fargo Connection Loan
																																		 ,676	--Perkins
																																	/* NECB Codes  select * from campusvue_NECB.dbo.fafundsource order by descrip
																																		5	--Alternative Loans
																																		, 7	--Alternative Loans
																																		, 9	--Loan
																																	*/) and fr.sastipendid is null then stn.amount else 0 end
		end ) as TotalDebt
	 , 0
from campusvue.dbo.satrans (nolock) as STN
join CampusVue.dbo.SyCampus (nolock) as SC
	on sc.sycampusid = stn.sycampusid
join #cohort as C
	on C.sycampusid = stn.SyCampusID
	and C.systudentid = stn.systudentid
	and C.campuszip = cast(SC.zip as int)
left join campusvue.dbo.fastudentaid (nolock) as FSA
	on fsa.fastudentaidid = stn.fastudentaidid
left join campusvue.dbo.farefund (nolock) as FR
	on FR.farefundid = stn.farefundid
--join campusvue.dbo.adterm (nolock) as AT
--	on at.adtermid = stn.adtermid
where
	SC.OfficialSchoolName like '%Brightwood%'

group by 
C.Systudentid
	   , C.sycampusid  
	   , C.campuszip 
	   , C.CIPCode 
       , C.Program 
	   , C.NormalProgramLength_Months
       , C.startdate 
       , C.graddate  
       , C.lda 
       , C.Degree
	   , sc.Descrip
	   , C.FaAcademicCalendar



update #AwardInfo
set IncludeInCohort = case when T4Loans > 0 then 1 
						    else 0 end 



/*

SELECT A.Brand
	 , A.Campus
	 , A.Program
	 , A.NormalProgramLength_Months
	 , A.Degree
	 , count(systudentid) as TotalCompleters
	 , sum(A.IncludeInCohort ) as T4LoanRecipients
	 , T4LoanMedian=MAX(T4LoanMedian)
	 , BalOnGradMedian=MAX(BalOnGradMedian)
	 , BalOnGradP30Median=MAX(BalOnGradP30Median)
	 , AltLoanMedian=MAX(AltLoanMedian)
FROM
(
   SELECT *
      ,T4LoanMedian= percentile_cont(0.5) WITHIN GROUP (ORDER BY TotalDebt) OVER (PARTITION BY campus, program, degree)
	  ,BalOnGradMedian= percentile_cont(0.5) WITHIN GROUP (ORDER BY BalanceOnGradDate) OVER (PARTITION BY campus, program, degree)
	  ,BalOnGradP30Median= percentile_cont(0.5) WITHIN GROUP (ORDER BY BalanceOnGradDateP30) OVER (PARTITION BY campus, program, degree)
	  ,AltLoanMedian= percentile_cont(0.5) WITHIN GROUP (ORDER BY Loans) OVER (PARTITION BY campus, program, degree)
   FROM #Awardinfo

	where 
	CIPCode is not null
	and
	includeincohort = 1 
) a
GROUP BY  A.Program
	 , A.NormalProgramLength_Months
	 , A.Campus
	 , a.degree
	 , a.brand  --*/



/*========================================================================================================================================================*/
/*========================================================================================================================================================*/




--/*

;with

cteCampusProgram_keys
(
SyCampusID
,Zip
,SyStudentID
,AdEnrollID
,AdProgramVersionID
,SySchoolStatusID
,StartDate
,GradDate
,GradYear
,GradMonth
,NormalProgramLength_Months
,Months_ActualEnroll_A
--,Months_ActualEnroll_B
,IsNormal_A
--,IsNormal_B
)

as

(
select
	SyCampusID = 
		case
		when ade.SyCampusID = 46
		then 12
		else ade.SyCampusID
		end
	,cast(cam.Zip as int) as Zip
	,ade.SyStudentID
	,ade.AdEnrollID
	,ade.AdPRogramVersionID
	,ade.SySchoolStatusID
	,ade.StartDate
	,ade.GradDate
	,datepart(year,ade.GradDate) as GradYear
	,datepart(month,ade.GradDate) as GradMonth
	,convert(int,prm.TotalMonths) as NormalProgramLength_Months
	,datediff(month,ade.StartDate, ade.GradDate) as Months_ActualEnroll_A
	--,Acdemics.dbo.fn_FullMonthsSeparation(ade.StartDate,ade.GradDate) as Months_ActualEnroll_B
	,IsNormal_A = case
		when datediff(month,ade.StartDate, ade.GradDate) <= convert(int,prm.TotalMonths)
		then 1
		else 0
		end
	--,IsNormal_B = case
	--	when Academics.dbo.fn_FullMonthsSeparation(ade.StartDate,ade.GradDate) <= convert(int,prm.TotalMonths)
	--	then 1
	--	else 0
	--	end
from
	CampusVue.dbo.AdEnroll ade (nolock)
	inner join CampusVue.dbo.AdProgramVersion prm (nolock) on ade.AdPRogramVersionID = prm.AdProgramVersionID
	inner join CampusVue.dbo.SyCampus cam (nolock) on cam.SyCampusID = 
		case
		when ade.SyCampusID = 46
		then 12
		else ade.SyCampusID
		end
where
	ade.SySchoolStatusID = 17



UNION


select
	ade.SyCampusID
	,cast(cam.Zip as int) as Zip
	,ade.SyStudentID
	,ade.AdEnrollID
	,ade.AdPRogramVersionID
	,ade.SySchoolStatusID
	,ade.StartDate
	,ade.GradDate
	,datepart(year,ade.GradDate) as GradYear
	,datepart(month,ade.GradDate) as GradMonth
	,convert(int,prm.TotalMonths) as NormalProgramLength_Months
	,datediff(month,ade.StartDate, ade.GradDate) as Months_ActualEnroll_A
	--,Academics.dbo.fn_FullMonthsSeparation(ade.StartDate,ade.GradDate) as Months_ActualEnroll_B
	,IsNormal_A = case
		when datediff(month,ade.StartDate, ade.GradDate) <= convert(int,prm.TotalMonths)
		then 1
		else 0
		end
	--,IsNormal_B = case
	--	when Academics.dbo.fn_FullMonthsSeparation(ade.StartDate,ade.GradDate) <= convert(int,prm.TotalMonths)
	--	then 1
	--	else 0
	--	end
from
	CampusVue_NECB.dbo.AdEnroll ade (nolock)
	inner join CampusVue_NECB.dbo.AdProgramVersion prm (nolock) on ade.AdPRogramVersionID = prm.AdProgramVersionID
	inner join CampusVue_NECB.dbo.SyCampus cam (nolock) on ade.SyCampusID = cam.SyCAmpusID
where
	ade.SySchoolStatusID = 17




)
,

cteSOC
(
Campus
,SyCampusID
,Zip
,SyStudentID
,AdEnrollID
,SchoolStatus
,GradDate
,GradYear
,GradMonth
,NormalProgramLength_Months
,Months_ActualEnroll_A
--,Months_ActualEnroll_B
,IsNormal_A
--,IsNormal_B
,Program
,AdProgramVersionID
,CIPCode	
,SOCCode
,IsSOCCodeFail
,EntryCounter
)

as

(
select
	ltrim(rtrim(cam.descrip)) as Campus
	,cam.SyCampusID
	,a.Zip
	,a.SyStudentID
	,a.AdEnrollID
	,ltrim(rtrim(sts.descrip)) as SchoolStatus
	,a.GradDate
	,a.GradYear
	,a.GradMonth
	,a.NormalProgramLength_Months
	,a.Months_ActualEnroll_A
	--,a.Months_ActualEnroll_B
	,a.IsNormal_A
	--,a.IsNormal_B
	,ltrim(rtrim(prm.descrip)) as Program
	,a.AdProgramVersionID
	,prm.CIPCode 
	,soc.DefaultSOCCode as SOCCode
	,IsSOCCodeFail = case when soc.DefaultSOCCode is null then 1 else 0 end
	,EntryCounter = 1
from
	cteCampusProgram_keys a
	inner join CampusVue.dbo.AdProgramVersion prm (nolock) on a.AdProgramVersionID = prm.AdProgramVersionID	
	inner join CampusVue.dbo.SySchoolStatus sts (nolock) on a.SySchoolStatusID = sts.SySchoolStatusID
	inner join CampusVue.dbo.SyCampus cam (nolock) on a.SyCampusID = cam.SyCampusID
	left outer join CampusVue.dbo.AdCIPToSOCDefault soc (nolock) on 
		soc.CIPCode = 
			case
			when prm.descrip = 'Culinary Arts' and prm.CIPCode = '12.0599' then '12.0503'
			when prm.descrip = 'Respiratory Care' and prm.CIPCode = '51.0812' then '51.0908'
			when prm.descrip = 'Business Office Specialist' and prm.CIPCode = '52.0499' then '52.0401'
			else prm.CIPCode
			end 



UNION


select
	ltrim(rtrim(cam.descrip)) as Campus
	,cam.SyCampusID
	,a.Zip
	,a.SyStudentID
	,a.AdEnrollID
	,ltrim(rtrim(sts.descrip)) as SchoolStatus
	,a.GradDate
	,a.GradYear
	,a.GradMonth
	,a.NormalProgramLength_Months
	,a.Months_ActualEnroll_A
	--,a.Months_ActualEnroll_B
	,a.IsNormal_A
	--,a.IsNormal_B
	,ltrim(rtrim(prm.descrip)) as Program
	,a.AdProgramVersionID
	,prm.CIPCode 
	,soc.DefaultSOCCode as SOCCode
	,IsSOCCodeFail = case when soc.DefaultSOCCode is null then 1 else 0 end
	,EntryCounter = 1
from
	cteCampusProgram_keys a
	inner join CampusVue_NECB.dbo.AdProgramVersion prm (nolock) on a.AdProgramVersionID = prm.AdProgramVersionID	
	inner join CampusVue_NECB.dbo.SySchoolStatus sts (nolock) on a.SySchoolStatusID = sts.SySchoolStatusID
	inner join CampusVue_NECB.dbo.SyCampus cam (nolock) on a.SyCampusID = cam.SyCampusID
	left outer join CampusVue_NECB.dbo.AdCIPToSOCDefault soc (nolock) on prm.CIPCode = soc.CIPCode


)






/*================================================================================================================================================*/



--select distinct
--	row_number() over (order by a.[Campus],a.[OCC_CODE],b.GradDate,b.SyStudentID) as ID
--	,a.[PRIM_STATE]
--	,a.[AREA]
--	,a.[AREA_NAME]
--	,a.[County]
--	,a.[Campus]
--	,a.[SyCampusID]
--	,b.Zip as CampusZip
--	,b.SyStudentID
--	--,b.AdEnrollID
--	--,b.SchoolStatus
--	,b.GradDate
--	--,b.GradYear
--	--,b.GradMonth
--	,b.NormalProgramLength_Months
--	,b.Months_ActualEnroll_Method_A
--	,b.Months_ActualEnroll_Method_B
--	,b.IsNormal_Method_A
--	,b.IsNormal_Method_B
--	,b.[CIPCode]
--	,b.[ECA_Program_Title]
--	,a.[OCC_CODE]
--	,a.[OCC_TITLE]
--	,a.[OCC_GROUP]
--	,a.[TOT_EMP]
--	,a.[EMP_PRSE]
--	,a.[JOBS_1000]
--	,a.[LOC QUOTIENT]
--	,a.[H_MEAN]
--	,a.[A_MEAN]
--	,a.[MEAN_PRSE]
--	,a.[H_PCT10]
--	,a.[H_PCT25]
--	,a.[H_MEDIAN]
--	,a.[H_PCT75]
--	,a.[H_PCT90]
--	,a.[A_PCT10]
--	,a.[A_PCT25]
--	,a.[A_MEDIAN]
--	,A_MEDIAN_Exceeds_80K = 
--		case
--		when a.[A_MEDIAN] > 80000
--		then 1
--		else 0
--		end
--	,A_MEDIAN_Exceeds_90K = 
--		case
--		when a.[A_MEDIAN] > 90000
--		then 1
--		else 0
--		end
--	,A_MEDIAN_Exceeds_100K = 
--		case
--		when a.[A_MEDIAN] > 100000
--		then 1
--		else 0
--		end
--	,a.[A_PCT75]
--	,a.[A_PCT90]
--	,a.[ANNUAL]
--	,a.[HOURLY]
--into ##temp_BLS
--from 
--	Academics.dbo.ES_GECLDv5 a
--	inner join
--		(
--		select
--			SOCCode as [OCC_CODE]
--			,CIPCode as [CIPCode]
--			,Program as [ECA_Program_Title]
--			,SyCampusID
--			,Zip
--			,SyStudentID
--			,AdEnrollID
--			,SchoolStatus
--			,GradDate
--			,GradYear
--			,GradMonth
--			,NormalProgramLength_Months
--			,Months_ActualEnroll_A as Months_ActualEnroll_Method_A
--			,Months_ActualEnroll_B as Months_ActualEnroll_Method_B
--			,IsNormal_A as IsNormal_Method_A
--			,IsNormal_B as IsNormal_Method_B
--		from
--			cteSOC
--		) b on a.[OCC_CODE] = b.[OCC_CODE]
--			and a.SyCampusID = b.SyCampusID
--			and cast(a.CampusZip as int) = b.Zip





/*========================================================================================================================================================*/
/*========================================================================================================================================================*/







select distinct
	ss.ssn,SC.OPEID, a.* 
	--,b.NormalProgramLength_Months
	--,b.Months_ActualEnroll_Method_A
	--,b.Months_ActualEnroll_Method_B
	--,b.IsNormal_Method_A
	--,b.IsNormal_Method_B
	--,b.[PRIM_STATE]
	--,b.[AREA]
	--,b.[AREA_NAME]
	--,b.[County]
	--,b.[OCC_CODE]
	--,b.[OCC_TITLE]
	--,b.[OCC_GROUP]
	--,b.[TOT_EMP]
	--,b.[EMP_PRSE]
	--,b.[JOBS_1000]
	--,b.[LOC QUOTIENT]
	--,b.[H_MEAN]
	--,b.[A_MEAN]
	--,b.[MEAN_PRSE]
	--,b.[H_PCT10]
	--,b.[H_PCT25]
	--,b.[H_MEDIAN]
	--,b.[H_PCT75]
	--,b.[H_PCT90]
	--,b.[A_PCT10]
	--,b.[A_PCT25]
	--,b.[A_MEDIAN]
	--,b.A_MEDIAN_Exceeds_80K
	--,b.A_MEDIAN_Exceeds_90K
	--,b.A_MEDIAN_Exceeds_100K
	--,b.[A_PCT75]
	--,b.[A_PCT90]
	--,b.[ANNUAL]
	--,b.[HOURLY]


from 
	#AwardInfo a
	left join campusvue.dbo.systudent (nolock) as SS
		on ss.systudentid = a.systudentid
	left join campusvue.dbo.sycampus (nolock) as SC
		on sc.sycampusid = ss.sycampusid
	--left outer join ##temp_BLS b on a.SyStudentID = b.SySTudentID
	--	and a.GradDate = b.GradDate
	--	and a.Program = b.[ECA_Program_Title]
	--	and a.NormalProgramLength_Months = a.NormalProgramLength_Months
	--	and a.campuszip = cast(b.CampusZip as int)
where 
	a.CIPCode is not null
	and
	a.includeincohort = 1 

drop table #cohort
drop table #AwardInfo
--drop table ##temp_BLS  --*/
