
-- =====================================================================
-- Author:		Courtney Date
-- Create date: 8/16/2016
-- Description:	Gainful Employment
---- ************************** 
--         Change History 
---- **************************
--** PR   Date			  Author		    Description  
--** --   --------		  -------		    -----------------------------------------------------------
--** 1    08/16/16		Courtney Davis		Original Code
-- ================================================================================================



--Declare and set variables to specify the award year and pull the start and end dates of htat award year
declare @AwardYear as varchar(7) = '2015-16'
declare @AYStart as datetime, @AYEnd as DateTime
declare @AYOut as varchar(8)

set @AYOut =  left(@AwardYear,4)+'20'+Right(@awardyear,2)

set @AYStart = (Select StartDate
				from [CVPROD].CampusVue.dbo.Fayear WITH (NOLOCK)
				where Descrip = @AwardYear)

set @AYEnd = (Select EndDate
				from [CVPROD].CampusVue.dbo.Fayear WITH (NOLOCK)
				where Descrip = @AwardYear)


--Clean up the temp tables 
if object_ID('TempDb..##Attended') is not null  
begin 
drop table ##Attended
end 

if object_ID('TempDb..##EnrollStat') is not null  
begin 
drop table ##EnrollStat
end 

if object_ID('TempDb..##EnrollCohort') is not null  
begin
drop Table ##EnrollCohort
end 

if object_ID('TempDb..##T4Recipients') is not null  
begin
drop table ##T4Recipients
end 

if object_ID('TempDb..##TUITandLoans') is not null  
begin
drop table ##TUITandLoans
end 


--Step 1.Gather a list of all the students that posted attendance within the bounds of the award year
Select Distinct ADT.AdenrollID
into ##Attended
from [CVPROD].CampusVue.dbo.AdAttend   as ADT WITH (NOLOCK)
where ADT.Attend >0 
  and ADT.Date between @AYStart and @AYEnd

create clustered index IX_AdEnrollID on ##Attended (AdenrollID)

--Step 2.Pull all of the status changes that are relevant to the award year for the list of students gathered above
Select SSC.AdenrollID 
	 , SSC.SyStudentID 
	 , NewActive = SSS.Descrip
	 , SSC.DateAdded as DateActive
	 , SSC.EffectiveDate as ActiveEffectiveDate
	 , NewInactive = NxtOut.NewInactive
	 , NxtOut.NewSySchoolStatusID as InactiveStatusID
	 , NxtOut.DateAdded as DateInactive
	 , NxtOut.EffectiveDate as InactiveEffectiveDate
	 , FirstAtt.FirstDateAttd
	 , ssc.systatchangeid
into ##EnrollCohort
from [CVPROD].CampusVue.dbo.SyStatChange   as SSC WITH (NOLOCK)
 Join [CVPROD].CampusVue.dbo.SyStatus   as SST WITH (NOLOCK)
	on SST.SyStatusID = SSC.NewSyStatusID
 Join [CVPROD].CampusVue.dbo.SyStatus   as SSTi WITH (NOLOCK)
	on SSTi.SyStatusID = SSC.PrevSyStatusID
 join [CVPROD].CampusVue.dbo.SySchoolStatus   as SSS WITH (NOLOCK)
	on SSS.SySchoolStatusID = SSC.NewSySchoolStatusID
 join ##Attended   as A WITH (NOLOCK)
	on A.AdEnrollID = SSC.AdEnrollID
 join [CVPROD].CampusVue.dbo.AdEnroll   as AE WITH (NOLOCK)
	on AE.AdEnrollID = A.AdEnrollID
 join [CVPROD].CampusVue.dbo.AdProgramVersion   as APV WITH (NOLOCK)
	on APV.AdProgramVersionID = AE.AdProgramVersionID
 outer apply (Select Top 1 SSC2.*,SSS2.Descrip as NewInactive 
				from [CVPROD].CampusVue.dbo.SyStatChange   as SSC2 WITH (NOLOCK)
				  Join [CVPROD].CampusVue.dbo.SyStatus   as SST2 WITH (NOLOCK)
					on SST2.SyStatusID = SSC2.NewSyStatusID
				  join [CVPROD].CampusVue.dbo.SySchoolStatus   as SSS2 WITH (NOLOCK)
					on SSS2.SySchoolStatusID = SSC2.NewSySchoolStatusID
				 where SSC2.AdEnrollID = SSC.AdEnrollID
				   and SST2.Category = 'P'
				   and SSC2.DateAdded> SSC.DateAdded
				   and ssc2.effectivedate between @AYStart and @AYend
				  Order by SSC2.DateAdded) as NxtOut --finds the inactive status associated with the entrance
Outer Apply (
			Select min(ADT.Date) FirstDateAttd
			from [CVPROD].CampusVue.dbo.AdAttend   as ADT  WITH (NOLOCK)
				join [CVPROD].CampusVue.dbo.Adenroll   as AE2 WITH (NOLOCK)
					on AE2.AdEnrollID = ADT.AdEnrollID
				join [CVPROD].CampusVue.dbo.AdProgramVersion   as APV2 WITH (NOLOCK)
					on APV2.AdProgramVersionID = AE2.AdProgramVersionID
			where APV2.CIPCode = APV.CIPCode
			 and ae2.systudentid = ssc.systudentid
			) as FirstATT --finds the first date of attendance for the student in any program that shares the same CIP code
where SST.Category = 'A'
    and SSTi.Category <> 'A'
    and SSC.NewSySchoolStatusID <> SSC.PrevSySchoolStatusID
	and (   SSC.DateAdded between @AYStart and @AYEnd --captures students that went active in the award year in question
		 or isnull(NxtOut.DateAdded,'3099-12-31') between @AYStart and @AYEnd --captures students that were active before the award year started and went inactive during the award year
		 or @AYStart between SSC.DateAdded and isnull(NxtOut.DateAdded,'3099-12-31')) --captures students that were active before the award year and after the award year without interuption
  
order by systudentid, ssc.dateadded

create nonclustered index NIX_Enrolls on ##EnrollCohort (AdEnrollID, SyStudentID, DateActive,DateInactive)


-- Step 3.Determine which students in the cohort are Title IV recipients
Select stn.systudentid 
	 , T4Rcvd = sum(case when FFS.Title4 = 1 and FR.SaStipendID is null then case STN.Type 
																				when 'I' then STN.Amount
																				when 'D' then STN.Amount
																				else -STN.Amount
																			   End
						 else 0 
					 end ) 

into ##T4Recipients
from [CVPROD].CampusVue.dbo.SaTrans   as STN WITH (NOLOCK)
	join ##EnrollCohort as E
		on E.SyStudentID = STN.SyStudentID
	LEFT join [CVPROD].CampusVue.dbo.FaStudentAid   as FSA WITH (NOLOCK)
		on FSA.FaStudentAidID = STN.FaStudentAidID
	LEFT join [CVPROD].CampusVue.dbo.FaFundSource   as FFS WITH (NOLOCK)
		on FFS.FaFundSourceId = FSA.FaFundSourceID
	left join [CVPROD].CampusVue.dbo.FaRefund   as FR WITH (NOLOCK)
		on FR.FaRefundID = STN.FaRefundID
group by STN.SyStudentID
having sum(case when FFS.Title4 = 1 and FR.SaStipendID is null then case STN.Type 
																		when 'I' then STN.Amount
																		when 'D' then STN.Amount
																		else -STN.Amount
																	   End
			 else 0 
			end ) <0 

create clustered index IX_systudentid on ##T4Recipients (systudentid)


Select T4.systudentid
	 ,  isnull(A.Descrip,'Full Time') as EnrollStat
	into ##EnrollStat
from ##T4Recipients as T4 
 outer apply ( select top 1 AAS.Descrip
				from [CVPROD].CampusVue.dbo.systatchange   as SSC WITH (NOLOCK)
				join [CVPROD].CampusVue.dbo.adAttStat   as AAS WITH (NOLOCK)
					on AAS.AdAttStatID = SSC.NewAdAttStatID
				where ssc.systudentid = t4.systudentid 
				  --and ssc.dateadded between @AYstart and @AYEnd
				 order by SSC.dateadded desc  )as A

create clustered index IX_IDs on ##EnrollStat (systudentid) 

--Step 4.Gather info on the tuition billed and private loans disbursed
Select E.SyStatChangeID --use this id so that we can match up the correct numbers with each reported instance of enrollment
	 , E.SyStudentID 
	 , Sum(Case when STN.Date<= isnull(E.DateInactive,@AYEnd) and SBC.TitleIVCharge = 1 and APVstn.CIPCode = APV.CIPCode and STN.SaBillCode not in ('BOOK','BOOKP') then
																														 Case STN.Type
																															when 'I' then STN.Amount
																															when 'D' then STN.Amount
																															else -STN.Amount
																														  end 
				else 0 
			end ) as Billed
	 , Sum(Case when STN.Date<= isnull(E.DateInactive,@AYEnd) and SBC.TitleIVCharge = 1 and APVstn.CIPCode = APV.CIPCode and STN.SaBillCode in ('BOOK','BOOKP')  then
																														 Case STN.Type
																															when 'I' then STN.Amount
																															when 'D' then STN.Amount
																															else -STN.Amount
																														  end 
				else 0 
			end ) as Books
	 , Sum(Case when STN.Date<= isnull(E.DateInactive,@AYEnd) and FSA.FaFundSourceID in (
																						  731	--Alternative Loan
																						 ,3		--Alternative Loans
																						 ,852	--CITIASSIST
																						 ,977	--CitiAssist Alternative Loan - CommonLine
																						 ,6552	--CITIASSIST GLOBAL
																						 ,7835	--Citibank Loan EFT Payment
																						 ,7836	--Citibank Loan Payment
																						 ,958	--Citibank Universal  Loan
																						 ,978	--Citibank Universal Loan - CommonLine
																						 ,4497	--Discover Private Loan
																						 ,4330	--National Citibank
																						 ,693	--Outside Loans
																						 ,938	--Sallie Mae Signature Loan
																						 ,606	--SLM Alternative Loan
																						 ,1127	--Smart Option Student Loan
																						)  and APVstn.CIPCode = APV.CIPCode then 
																																Case STN.Type
																																	when 'I' then STN.Amount
																																	when 'D' then STN.Amount
																																	else -STN.Amount
																																  end 
				else 0 
			end ) as PrivateLoans
	 , Sum(case when STN.Date<= isnull(E.DateInactive,@AYEnd) then 
															Case STN.Type
															when 'I' then STN.Amount
															when 'D' then STN.Amount
															else -STN.Amount
															end 
				else 0 
			end ) as Balance
into ##TUITandLoans
from ##EnrollCohort as E
	left join [CVPROD].CampusVue.dbo.SaTrans   as STN WITH (NOLOCK)
		on STN.systudentid = E.SyStudentID
	left join [CVPROD].CampusVue.Dbo.AdEnroll   as AEstn WITH (NOLOCK)
		on AEstn.AdEnrollID = STN.AdEnrollID
	left join [CVPROD].CampusVue.dbo.AdProgramVersion   as APVstn WITH (NOLOCK)
		on APVstn.AdProgramVersionID = AEstn.AdProgramVersionID 
	left join [CVPROD].CampusVue.dbo.SaBillCode   as SBC WITH (NOLOCK)
		on SBC.Code = STN.SaBillCode
	join [CVPROD].CampusVue.dbo.AdEnroll   as AE WITH (NOLOCK)
		on AE.Adenrollid = E.AdEnrollID 
	join [CVPROD].CampusVue.dbo.AdProgramVersion   as APV WITH (NOLOCK)
		on APV.AdProgramVersionID = AE.AdProgramVersionID 
	left join [CVPROD].CampusVue.dbo.FaStudentAid   as FSA WITH (NOLOCK)
		on FSA.FaStudentAidID = STN.FaStudentAidID
group by E.SyStatChangeID 
, E.SyStudentID 

create clustered index IX_IDs on ##TUITandLoans (SyStatChangeID,SyStudentID)


--select count(*)
--from ##EnrollCohort

--select count(*) 
--from ##T4Recipients



--Step 5.Filter out non-T4 recipients and compile all the dates,demographics, and financials
Select AwardYear = @AYOut
	 ,  SSN = replace(SS.SSN,'-','')
	 , left(rtrim(replace(replace(SS.FirstName,char(13),''),char(10),'')),35) as FirstName
	 , left(rtrim(replace(replace(replace(replace(replace(SS.MiddleName,char(13),''),char(10),''),'.',''),'(',''),')','')),35) as MiddleName
	 , left(rtrim(replace(SS.LastName,char(13),'')),35) as LastName
	 , DOB = isnull(convert(varchar(8),SS.DOB,112),'00000000')
	 , InstitutionCode = SC.OPEID+convert(varchar(2),SC.PellIDBranch) 
	 , InstitutionName = case when left(SC.Descrip,3) like 'GAA%' Then REPLACE(SC.Descrip,'GAA - ','Virginia College - Golfa Academy of America - ') 
							  when left(SC.Descrip,3) like 'VC%' Then REPLACE(SC.Descrip,'VC', 'Virginia College')
							  when left(SC.Descrip,3) like 'ECO%' Then  REPLACE(SC.Descrip,'ECO - ','Virginia College - EcoTech Institute - ')
							  when left(SC.Descrip,3) like 'BW%'  Then  REPLACE(SC.Descrip,'BW - ','Brightwood College - ')
							  When SC.Descrip like 'KHEC%' then REPLACE(SC.Descrip,'KHEC', 'Brightwood College - ')
							  else 'Error' + SC.Descrip
							  end 
	 , Filler1 =' '
	 , ProgramName = rtrim(APV.Descrip)
	 , convert(varchar(6),replace(APV.CIPcode,'.','')) as CIPCode
	 , [Credential Level]=case when AD.Descrip like '%single%'then '01' 
							   when AD.Descrip like '%Diploma%' then  '01'
							   when AD.Descrip like '%Certificate%' then '01'
							   when AD.Descrip like '%Associate%' then '02'
							   when AD.Descrip like '%Bachelor%' then '03'
							   when AD.Descrip like '%Master%' then '05'
							   when APV.Descrip= 'Accounting and Finance - Forensic Accounting'  then '08'
							   when APV.Descrip= 'Accounting and Finance - Basic'  then '08'
							   when APV.Descrip= 'Accounting and Finance - Intermediate'  then '08'
							end 
	 , [Medical or Dental Internship or Residency] = 'N'
	 , Filler2 = ' '
	 , [Progam Attendance Begin Date] = isnull(convert(varchar(8),CASE WHEN E.FirstDateAttd>(case when E.ActiveEffectiveDate < @AYStart then @AYStart
																								  else E.ActiveEffectiveDate
																							 end) THEN (case when E.ActiveEffectiveDate < @AYStart then @AYStart
																											  else E.ActiveEffectiveDate
																										 end) 
																		ELSE E.FirstDateAttd
																		end  ,112),'00000000') 
	 , [Program Attendance Begin Date for AY] = case when E.ActiveEffectiveDate < @AYStart then convert(varchar(8),@AYStart, 112) else convert(varchar(8),E.ActiveEffectiveDate,112) end 
												 
	 , [Program Attendance Status During Award Year] = case when E.InactiveStatusID is null then 'E'
															When E.InactiveStatusID = 17 then 'G'
															else 'W'
															end 
     , [Program Attendance Status Date] = Case when E.InactiveStatusID is null then '' 
											   else 
													case when E.InactiveEffectiveDate < E.ActiveEffectiveDate then convert(varchar(8),dateadd(day,1,E.ActiveEffectiveDate),112) 
														 else  convert(varchar(8),E.InactiveEffectiveDate,112)
													end   
										    end
	 , [Private Loans Amount] = Case when E.InactiveStatusID is null then '' else  convert(varchar(max),convert(int,round(abs(TL.PrivateLoans),0))) end 
	 , [Institutional Debt] = case when E.InactiveStatusID is not null and TL.balance >0  then convert(varchar(max),convert(int,round(TL.Balance,0))) else '' end 
	 , [Tuition and Fees Amount] = Case when E.InactiveStatusID is null then '' else  convert(varchar(max),convert(int,round(TL.Billed,0))) end 
	 , [Allowance for Books, Supplies, and Equipment] = Case when E.InactiveStatusID is null then '' else  convert(varchar(max),convert(int,round(TL.Books,0))) end 
	 , [Length of GE Program] =case when len(convert(varchar(max),convert(int,apv.totalweeks)))<6 then left('000000',6-len(convert(varchar(max),convert(int,apv.totalweeks)))) +convert(varchar(max),convert(int,apv.totalweeks))
								else left(convert(varchar(max),convert(int,apv.totalweeks)),6)
								end 
	 , [Length of GE Program Measurement] = 'W'
	 , EnrollStatus = case ES.EnrollStat
						   when 'Full Time' then 'F'
						   when '3/4 Time' then 'Q'
						   when 'Less than half time' then 'L'
						   when 'Half Time' then 'H'
						   else 'N'
						   end 
	 , filler3 = ' '

from ##EnrollCohort E
join [CVPROD].CampusVue.dbo.Adenroll   as AE WITH (NOLOCK)
	on AE.AdEnrollID = E.AdEnrollID 
Join [CVPROD].CampusVue.dbo.SyStudent   as SS WITH (NOLOCK)
	on SS.SyStudentID = E.SyStudentID 
join [CVPROD].CampusVue.dbo.SyCampus   as SC WITH (NOLOCK)
	on SC.SyCampusID = AE.SyCampusID
Join [CVPROD].CampusVue.dbo.AdProgramVersion   as APV WITH (NOLOCK)
	on APV.AdProgramVersionID = AE.AdProgramVersionID
Join [CVPROD].CampusVue.dbo.AdDegree   as AD WITH (NOLOCK)
	on AD.AdDegreeID = APV.AdDegreeID
Join ##T4Recipients as T4
	on T4.SyStudentID = E.SyStudentID 
left join ##TUITandLoans as TL
	on TL.SyStatChangeID = E.SyStatChangeID
left join ##EnrollStat as ES
	on  ES.systudentid = E.systudentid 

drop table ##Attended
drop Table ##EnrollCohort
drop table ##T4Recipients
drop table ##TUITandLoans
drop table ##EnrollStat