

--with cohort_cte
--as 
--(
--select  ssc.systudentid , max (SSC.DateAdded) as dateadded, [dbo].[fn_StatusAsOfByStudent] (ssc.systudentid, '2016-12-31') as Something
--from CampusVue.dbo.systatchange (nolock) as SSC
--join CampusVue.dbo.AdEnroll (nolock) as AE
--	on ae.AdEnrollID = ssc.AdEnrollID
--where ssc.newsyschoolstatusid in (67,101) 
--  and ssc.prevsyschoolstatusid in (13,111,128,92,60,61) 
--  and ae.SySchoolStatusID = ssc.NewSySchoolStatusID
--   and datepart(year,ssc.dateadded)='2016'
--group by ssc.systudentid 
--)


--select systudentid 
--,[dbo].[fn_StatusAsOfByStudent] (systudentid, '2016-12-31')
--from systudent where currentlda = '2016-12-07' 




if object_id ('Tempdb..##Enrollments') is not null 
begin 
drop table ##Enrollments
end 

/*Latest Enrollment*/
select SS.SyStudentID
	 , A.AdEnrollid
	 , A.LDA
	 , A.AdProgramVersionID
	 ,A.DropDate
	 ,A.SySchoolStatusID
	 ,A.SyCampusID
	 ,[CampusVue].dbo.[fn_StatusAsOfByStudent] (ss.systudentid, '2016-12-31') as [Status 12-31-2016]
	 ,A.Descrip as ProgramVersion
	 
into ##Enrollments
from CampusVue.dbo.Systudent (nolock) as SS
outer apply ( select top 1 AE.adenrollid , AE.LDA,AE.GPA, AE.AdProgramVersionID, AE.DropDate, AE.SySchoolStatusID, APV.Descrip, AE.SyCampusid
				from campusvue.dbo.adenroll (nolock) as AE
						left join CampusVue.dbo.AdProgramVersion (nolock) as APV
							on APV.AdProgramVersionID=AE.adProgramVersionID
					where ae.systudentid = SS.systudentid 
					 and AE.LDA is not null
					  Order by AE.LDA Desc
										) as A
create clustered index AEID on ##Enrollments (adenrollid)


select StudentName = RTRIM(SS.LastName)+', '+RTRIM(SS.FirstName)
	 , ss.ssn
	 , SC.Descrip as Campus 
	  ,SC.OPEID
	 , APV.Descrip as Program
	 , AE.LDA
	 , SSS.Descrip as [School Statud 12.31.16]
	 , case when SST.Category = 'A' or sss.descrip like '%grad%' then 'Active' 
									else 'Inactive'
									end as [Report Category]
	 , TotalAidReceived = SUM(case when STN.Date between '2016-01-01' and '2016-12-31' 
									and FSA.FaFundSourceID in (1,2,5,7,9) 
									and FR.SaStipendID is null
									and FR.FaRefundID is null then
										Case STN.Type 
										  When 'I' Then STN.Amount
										  When 'D' Then STN.Amount
										  Else STN.Amount * -1 end	
								   else 0 
								end )*-1
	 , Pell = SUM(case when STN.Date between '2016-01-01' and '2016-12-31' 
									and FSA.FaFundSourceID in (1) 
									and FR.SaStipendID is null
									and FR.FaRefundID is null then
										Case STN.Type 
										  When 'I' Then STN.Amount
										  When 'D' Then STN.Amount
										  Else STN.Amount * -1 end	
								   else 0 
								end )*-1


											
	 , SEOG = SUM(case when STN.Date between '2016-01-01' and '2016-12-31' 
									and FSA.FaFundSourceID in (2) 
									and FR.SaStipendID is null
									and FR.FaRefundID is null then
										Case STN.Type 
										  When 'I' Then STN.Amount
										  When 'D' Then STN.Amount
										  Else STN.Amount * -1 end	
								   else 0 
								end )*-1
																																  
	 , SubsidizedLoans = SUM(case when STN.Date between '2016-01-01' and '2016-12-31' 
									and FSA.FaFundSourceID in (5) 
									and FR.SaStipendID is null
									and FR.FaRefundID is null then
										Case STN.Type 
										  When 'I' Then STN.Amount
										  When 'D' Then STN.Amount
										  Else STN.Amount * -1 end	
								   else 0 
								end )*-1
	 , UnSubsidizedLoans = SUM(case when STN.Date between '2016-01-01' and '2016-12-31' 
									and FSA.FaFundSourceID in (7) 
									and FR.SaStipendID is null
									and FR.FaRefundID is null then
										Case STN.Type 
										  When 'I' Then STN.Amount
										  When 'D' Then STN.Amount
										  Else STN.Amount * -1 end	
								   else 0 
								end )*-1											
	 , PlusLoans = SUM(case when STN.Date between '2016-01-01' and '2016-12-31' 
									and FSA.FaFundSourceID in (9) 
									and FR.SaStipendID is null
									and FR.FaRefundID is null then
										Case STN.Type 
										  When 'I' Then STN.Amount
										  When 'D' Then STN.Amount
										  Else STN.Amount * -1 end	
								   else 0 
								end )*-1

	, PellRefunded = SUM(case when STN.Date between '2016-01-01' and '2016-12-31' 
									and FSA.FaFundSourceID in (1) 
									and FR.SaStipendID is null
									and FR.FaRefundID is not null then
										Case STN.Type 
										  When 'I' Then STN.Amount
										  When 'D' Then STN.Amount
										  Else STN.Amount * -1 end	
								   else 0 
								end )*-1


											
	 , SEOGRefunded = SUM(case when STN.Date between '2016-01-01' and '2016-12-31' 
									and FSA.FaFundSourceID in (2) 
									and FR.SaStipendID is null
									and FR.FaRefundID is not null then
										Case STN.Type 
										  When 'I' Then STN.Amount
										  When 'D' Then STN.Amount
										  Else STN.Amount * -1 end	
								   else 0 
								end )*-1
																																  
	 , SubsidizedLoansRefunded = SUM(case when STN.Date between '2016-01-01' and '2016-12-31' 
									and FSA.FaFundSourceID in (5) 
									and FR.SaStipendID is null
									and FR.FaRefundID is not null then
										Case STN.Type 
										  When 'I' Then STN.Amount
										  When 'D' Then STN.Amount
										  Else STN.Amount * -1 end	
								   else 0 
								end )*-1
	 , UnSubsidizedLoansRefunded = SUM(case when STN.Date between '2016-01-01' and '2016-12-31' 
									and FSA.FaFundSourceID in (7) 
									and FR.SaStipendID is null
									and FR.FaRefundID is not null then
										Case STN.Type 
										  When 'I' Then STN.Amount
										  When 'D' Then STN.Amount
										  Else STN.Amount * -1 end	
								   else 0 
								end )*-1											
	 , PlusLoansRefunded = SUM(case when STN.Date between '2016-01-01' and '2016-12-31' 
									and FSA.FaFundSourceID in (9) 
									and FR.SaStipendID is null
									and FR.FaRefundID is not null then
										Case STN.Type 
										  When 'I' Then STN.Amount
										  When 'D' Then STN.Amount
										  Else STN.Amount * -1 end	
								   else 0 
								end )*-1


	, TotalAidRefunded = SUM(case when STN.Date between '2016-01-01' and '2016-12-31' 
									and FSA.FaFundSourceID in (1,2,5,7,9) 
									and FR.SaStipendID is null
									and FR.FaRefundID is not null then
										Case STN.Type 
										  When 'I' Then STN.Amount
										  When 'D' Then STN.Amount
										  Else STN.Amount * -1 end	
								   else 0 
								end )*-1

	,TotalFWS= (FWS1.[42384]+FWS1.[42398]+FWS1.[42412]+FWS1.[42426]+
				FWS1.[42440]+FWS1.[42454]+FWS1.[42468]+FWS1.[42482]+FWS1.[42496]+
				FWS1.[42510]+FWS1.[42524]+FWS1.[42538]+FWS1.[42552]+FWS1.[42566])
				+FWS2.[Total Paid ]

	
	

from ##Enrollments (nolock) as AE
join campusvue.dbo.syStudent (nolock) as SS
	on SS.SyStudentId = AE.SyStudentID 
join campusvue.dbo.SaTrans (nolock) as STN
	on STN.SyStudentID = SS.SyStudentId 
join campusvue.dbo.AdProgramVersion (nolock) as APV 
	on APV.AdProgramVersionID = AE.adProgramVersionID
join campusvue.dbo.FaStudentAid (nolock) as FSA
	on FSA.FaStudentAidId = STN.FaStudentAidID
join campusvue.dbo.syschoolstatus (nolock) as SSS
	on sss.syschoolstatusid = AE.[Status 12-31-2016]
join CampusVue.dbo.syStatus (nolock) as SST
	on sst.systatusid = sss.systatusid
left join campusvue.dbo.FaRefund (nolock) as FR
	on FR.FaRefundID = STN.FaRefundID
join campusvue.dbo.SyCampus (nolock) as SC
	on SC.SyCampusID = AE.SyCampusID
left join [Financial Planning].dbo.[1516FWS] (nolock) as FWS1
	on FWS1.[SSN (Formatted)]=SS.SSN
left join [Financial Planning].dbo.[1617FWS] (nolock) as FWS2
	on FWS2.[SSN (Formatted)]=SS.SSN

	where datepart(year,STN.Date)='2016' 
	
	
	

group by SS.LastName
	 , SS.FirstName
	 ,ss.SystudentID
	 , ss.ssn
	 , APV.Descrip
	 , ae.LDA
	 , SC.Descrip 
	 ,SC.OPEID	 
	 ,FWS2.[Total Paid ]
	 ,FWS1.[42384]
	 , FWS1.[42398], 
FWS1.[42412], 
FWS1.[42426], 
FWS1.[42440], 
FWS1.[42454], 
FWS1.[42468], 
FWS1.[42482], 
FWS1.[42496], 
FWS1.[42510], 
FWS1.[42524], 
FWS1.[42538], 
FWS1.[42552], 
FWS1.[42566],
SSS.Descrip 
	 , case when SST.Category = 'A' or sss.descrip like '%grad%' then 'Active' 
									else 'Inactive'
									end

	 having  (SUM(case when STN.Date between '2016-01-01' and '2016-12-31' 
									and FSA.FaFundSourceID in (1,2,5,7,9) 
									and FR.SaStipendID is null
									and FR.FaRefundID is null then
										Case STN.Type 
										  When 'I' Then STN.Amount
										  When 'D' Then STN.Amount
										  Else STN.Amount * -1 end	
								   else 0 
								end )*-1)>0
or (FWS1.[42384]+FWS1.[42398]+FWS1.[42412]+FWS1.[42426]+
				FWS1.[42440]+FWS1.[42454]+FWS1.[42468]+FWS1.[42482]+FWS1.[42496]+
				FWS1.[42510]+FWS1.[42524]+FWS1.[42538]+FWS1.[42552]+FWS1.[42566])
				+FWS2.[Total Paid ]>0




drop table ##Enrollments