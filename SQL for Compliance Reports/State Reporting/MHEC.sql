

declare @LoDate as Datetime = '2014-07-01' , @HiDate as datetime = '2016-06-30'



Select distinct AE.SyStudentID 
into ##SL
from adattend (nolock) as ADT
	join adenroll (nolock) as AE
		on ae.adenrollid = adt.adenrollid 
where adt.attend >0 
  and adt.date between @LoDate and @HiDate
  and ae.sycampusid in (select sycampusid 
						from sycampus 
						where state = 'MD')
						

Select s.systudentid 
	, sum(case when FSA.FaFundsourceid in (
											1 --Pell Grant
										  ) 
				and FR.SaStipendID is null then 
												case stn.type
													when  'I' then stn.amount
													when 'D' then stn.amount
													else -stn.amount
												end 
			   else 0 
		   end ) as Pell
	, sum(case when FSA.FaFundsourceid in (
											2	, --SEOG
											 682	 --KHEC Education Corporation Match
										  ) 
				and FR.SaStipendID is null then 
												case stn.type
													when  'I' then stn.amount
													when 'D' then stn.amount
													else -stn.amount
												end 
			   else 0 
		   end ) as SEOG
	, sum(case when FSA.FaFundsourceid in (
											3110	 --Education Assistance Grant
										  ) 
				and FR.SaStipendID is null then 
												case stn.type
													when  'I' then stn.amount
													when 'D' then stn.amount
													else -stn.amount
												end 
			   else 0 
		   end ) as 'D Educatoin Assistance Grants' 
	, sum(case when FSA.FaFundsourceid in (
											
											1219	, --Kaplan Grant Match
											1228	, --Kaplan RC Grant
											1264	, --KINC GOK
											1265	, --KHEG GOK
											1266	, --KHEC GOK
											1267	, --KU GOK
											5745	, --KHE Institutional Grant
											6906	, --Kaplan Opportunity Grant
											6907	  --Kaplan Supplemental Grant
										  ) 
				and FR.SaStipendID is null then 
												case stn.type
													when  'I' then stn.amount
													when 'D' then stn.amount
													else -stn.amount
												end 
			   else 0 
		   end ) as 'H. Institutional Grants' 
	, sum(case when FSA.FaFundsourceid in (
											5	 --Direct Subsidized Loan
										  ) 
				and FR.SaStipendID is null then 
												case stn.type
													when  'I' then stn.amount
													when 'D' then stn.amount
													else -stn.amount
												end 
			   else 0 
		   end ) as 'II B. William D. Ford Subsidized Loans'
	 , sum(case when FSA.FaFundsourceid in (
											7	 --Direct Loan Unsubsidized Loan
										  ) 
				and FR.SaStipendID is null then 
												case stn.type
													when  'I' then stn.amount
													when 'D' then stn.amount
													else -stn.amount
												end 
			   else 0 
		   end ) as 'II C. Federal Stafford Loans Unsubsidized (1203)'	
	 , sum(case when FSA.FaFundsourceid in (
											9	 --Direct Plus Loan
										  ) 
				and FR.SaStipendID is null then 
												case stn.type
													when  'I' then stn.amount
													when 'D' then stn.amount
													else -stn.amount
												end 
			   else 0 
		   end ) as 	'II D. Federal PLUS Loans (1204)'
	 , sum(case when FSA.FaFundsourceid in (
											21	, --Institutional Loan
											29	, --Career Loan Program
											30	, --Opportunity Loan
											36	, --Term Loan
											38	, --Nursing Career Loan Program
											39	, --Bridge Loan
											54	, --Career Loan Program
											802	, --Key Loans
											807	, --Kaplan Loan
											1112	, --Kaplan Choice Loan
											1213	, --Kaplan Value Loan
											1231	, --Kaplan Choice Loan*
											1232	, --Kaplan Value Loan*
											2113	 --Career Training Loan
										  ) 
				and FR.SaStipendID is null then 
												case stn.type
													when  'I' then stn.amount
													when 'D' then stn.amount
													else -stn.amount
												end 
			   else 0 
		   end ) as 'II F. Institutional Loans (1206)'	
	 , sum(case when FSA.FaFundsourceid in (
											3	, --Alternative Loans
											31	, --Tiered Risk Loan
											32	, --Palmetto Loan
											606	, --SLM Alternative Loan
											691	, --One Choice Alternative Loan
											693	, --Outside Loans
											731	, --Alternative Loan
											803	, --Bank of America Teri Loans
											815	, --TERI Loan
											938	, --Sallie Mae Signature Loan
											958	, --Citibank Universal  Loan
											977	, --CitiAssist Alternative Loan - CommonLine
											978	, --Citibank Universal Loan - CommonLine
											1079	, --Chase Select Alternative Loan- All KHE
											1081	, --Comerica, Nelnet, Teri Alt Loan
											1124	, --Wells Fargo Connection Loan
											1127	, --Smart Option Student Loan
											1282	, --Nursing Education Assistance Loan
											2272	, --Wells Fargo Collegiate Loan
											4497	, --Discover Private Loan
											6386	, --NLSC Quest Loan
											7753	, --102 Plus Loan
											7835	, --Citibank Loan EFT Payment
											7836	 --Citibank Loan Payment
										  ) 
				and FR.SaStipendID is null then 
												case stn.type
													when  'I' then stn.amount
													when 'D' then stn.amount
													else -stn.amount
												end 
			   else 0 
		   end ) as 'II G. Loans  from Private Sources (1207)'
	 , sum(case when FSA.FaFundsourceid in (
											3106	 --Senatorial Scholarship
										  ) 
				and FR.SaStipendID is null then 
												case stn.type
													when  'I' then stn.amount
													when 'D' then stn.amount
													else -stn.amount
												end 
			   else 0 
		   end ) as 'III C. Senatorial Scholarship (1305)' 
	 , sum(case when FSA.FaFundsourceid in (
											12	, --Scholarships (Institutional)
											45	, --Scholarship  ECA - Military 50%
											46	, --Scholarship  ECA - Military 25%
											47	, --Scholarship - Armed Forces Recognition
											51	, --Scholarship - Yellow Ribbon
											56	, --Scholarship ECA - Military 10%
											57	, --Scholarship ECA - Military 15%
											81	, --Scholarship ECA - Military 5%
											84	, --Working Parent Scholarship
											597	, --Internal Student Scholarship
											715	, --Presidential Scholarship
											717	, --KHEC Student Scholarship
											1284	, --Scholarship ECA - 30%
											5586	, --Presidential Scholarship
											5675	, --Dependent Children Scholarship
											6853	, --Allied Healthcare Scholarship
											6854	, --Associate Degree Nursing Scholarship
											6856	  --Vocational Nurse Scholarship
										  ) 
				and FR.SaStipendID is null then 
												case stn.type
													when  'I' then stn.amount
													when 'D' then stn.amount
													else -stn.amount
												end 
			   else 0 
		   end ) as 'III H. Other Institutional  Scholarships (1318)'
	 , sum(case when FSA.FaFundsourceid in (
											13	, --Scholarship (Outside)
											35	, --Scholarship (FCA)
											83	, --Military - Ch. 33 - Frye Scholarship
											595	, --Ward Hamilton Scholarship
											614	, --Outside Student Scholarship
											715	, --Presidential Scholarship
											954	, --Golden State Scholarshare Trust
											1274	, --My Success Graduate Scholarship
											3107	, --Delegate Scholarship
											4022	, --Southampton Employee Scholarship
											4225	, --Jeffrey Modispaw Scholarship
											4285	, --CHI Scholarship
											5497	, --Opportunity Scholarship
											5586	, --Presidential Scholarship
											5661	, --Arizona Early Graduation Scholarship
											6853	, --Allied Healthcare Scholarship
											7745	, --Andover Scholarship
											7853	, --Make the Grade Scholarship
											7854	, --SACMDA Scholarship
											8099	, --PTA Charter Scholarship
											8945	  --MARS Scholarship
										  ) 
				and FR.SaStipendID is null then 
												case stn.type
													when  'I' then stn.amount
													when 'D' then stn.amount
													else -stn.amount
												end 
			   else 0 
		   end ) as 'III J. Other Private Scholarships (1320)'
	 , sum(case when (FSA.FaFundsourceid in (
											609	, --KHEC Employee Scholarship
											4022	, --Southampton Employee Scholarship
											5323	, --Dayton Employee Scholarship
											6363	, --San Antonio Employee Scholarship
											7535	, --Dallas Employee Scholarship
											8046	 --Arlington Texas Employee

										  ) or stn.sabillcode = 'EEWAIV')
				and FR.SaStipendID is null then 
												case stn.type
													when  'I' then stn.amount
													when 'D' then stn.amount
													else -stn.amount
												end 
			   else 0 
		   end ) as 'III K. Tuition Waivers / Remission of Fees to Employees and Dependents (1321)'
	 , sum(case when FSA.FaFundsourceid in (
											26	 --Federal Work Study
										  )
				and FR.SaStipendID is null then 
												case stn.type
													when  'I' then stn.amount
													when 'D' then stn.amount
													else -stn.amount
												end 
			   else 0 
		   end ) as 'IV A. Federal Work-Study (FWS)   (1401)'
into ##Awards
from SaTrans (nolock) as STN
join ##SL as S
	on S.systudentid = STN.SyStudentID
join fastudentaid (nolock) as FSA
	on fsa.fastudentaidid = stn.fastudentaidid
left join farefund (Nolock) as FR
	on fr.farefundid = stn.farefundid 
where stn.date between @loDate and @HiDate
group by S.Systudentid 


Select S.SyStudentID 
	 , case when A.[Pell]<0 then A.[Pell]		 else 0 end as [Pell Amount]	 , case when A.[Pell]<0 then 1		 else 0 end as [Pell Count]
	 , case when A.[SEOG]<0 then A.[SEOG]		 else 0 end as [SEOG Amount]	 , case when A.[SEOG]<0 then 1		 else 0 end as [SEOG Count]
	 , case when A.[D Educatoin Assistance Grants]<0 then A.[D Educatoin Assistance Grants]		 else 0 end as [D Educatoin Assistance Grants Amount]	 , case when A.[D Educatoin Assistance Grants]<0 then 1		 else 0 end as [D Educatoin Assistance Grants Count]
	 , case when A.[H. Institutional Grants]<0 then A.[H. Institutional Grants]		 else 0 end as [H. Institutional Grants Amount]	 , case when A.[H. Institutional Grants]<0 then 1		 else 0 end as [H. Institutional Grants Count]
	  ,  case when A.[Pell]<0  
			 OR A.[SEOG]<0 
			 OR A.[D Educatoin Assistance Grants]<0 
			 OR A.[H. Institutional Grants]<0 
				then 1 
				else 0 end as [Unduplicated Grant Count] 	 , case when A.[II B. William D. Ford Subsidized Loans]<0 then A.[II B. William D. Ford Subsidized Loans]		 else 0 end as [II B. William D. Ford Subsidized Loans Amount]	 , case when A.[II B. William D. Ford Subsidized Loans]<0 then 1		 else 0 end as [II B. William D. Ford Subsidized Loans Count]
	 , case when A.[II C. Federal Stafford Loans Unsubsidized (1203)]<0 then A.[II C. Federal Stafford Loans Unsubsidized (1203)]		 else 0 end as [II C. Federal Stafford Loans Unsubsidized (1203) Amount]	 , case when A.[II C. Federal Stafford Loans Unsubsidized (1203)]<0 then 1		 else 0 end as [II C. Federal Stafford Loans Unsubsidized (1203) Count]
	 , case when A.[II D. Federal PLUS Loans (1204)]<0 then A.[II D. Federal PLUS Loans (1204)]		 else 0 end as [II D. Federal PLUS Loans (1204) Amount]	 , case when A.[II D. Federal PLUS Loans (1204)]<0 then 1		 else 0 end as [II D. Federal PLUS Loans (1204) Count]
	 , case when A.[II F. Institutional Loans (1206)]<0 then A.[II F. Institutional Loans (1206)]		 else 0 end as [II F. Institutional Loans (1206) Amount]	 , case when A.[II F. Institutional Loans (1206)]<0 then 1		 else 0 end as [II F. Institutional Loans (1206) Count]
	 , case when A.[II G. Loans  from Private Sources (1207)]<0 then A.[II G. Loans  from Private Sources (1207)]		 else 0 end as [II G. Loans  from Private Sources (1207) Amount]	 , case when A.[II G. Loans  from Private Sources (1207)]<0 then 1		 else 0 end as [II G. Loans  from Private Sources (1207) Count]
	  ,  case when  A.[II B. William D. Ford Subsidized Loans]<0 
			 OR A.[II C. Federal Stafford Loans Unsubsidized (1203)]<0 
			 OR A.[II D. Federal PLUS Loans (1204)]<0 
			 OR A.[II F. Institutional Loans (1206)]<0 
			 OR A.[II G. Loans  from Private Sources (1207)]<0 
				then 1 
				else 0 end as [Unduplicated Loan Count] 	 , case when A.[III C. Senatorial Scholarship (1305)]<0 then A.[III C. Senatorial Scholarship (1305)]		 else 0 end as [III C. Senatorial Scholarship (1305) Amount]	 , case when A.[III C. Senatorial Scholarship (1305)]<0 then 1		 else 0 end as [III C. Senatorial Scholarship (1305) Count]
	 , case when A.[III H. Other Institutional  Scholarships (1318)]<0 then A.[III H. Other Institutional  Scholarships (1318)]		 else 0 end as [III H. Other Institutional  Scholarships (1318) Amount]	 , case when A.[III H. Other Institutional  Scholarships (1318)]<0 then 1		 else 0 end as [III H. Other Institutional  Scholarships (1318) Count]
	 , case when A.[III J. Other Private Scholarships (1320)]<0 then A.[III J. Other Private Scholarships (1320)]		 else 0 end as [III J. Other Private Scholarships (1320) Amount]	 , case when A.[III J. Other Private Scholarships (1320)]<0 then 1		 else 0 end as [III J. Other Private Scholarships (1320) Count]
	 , case when A.[III K. Tuition Waivers / Remission of Fees to Employees and Dependents (1321)]<0 then A.[III K. Tuition Waivers / Remission of Fees to Employees and Dependents (1321)]		 else 0 end as [III K. Tuition Waivers / Remission of Fees to Employees and Dependents (1321) Amount]	 , case when A.[III K. Tuition Waivers / Remission of Fees to Employees and Dependents (1321)]<0 then 1		 else 0 end as [III K. Tuition Waivers / Remission of Fees to Employees and Dependents (1321) Count]
	  ,  case when A.[III C. Senatorial Scholarship (1305)]<0 
			 OR A.[III H. Other Institutional  Scholarships (1318)]<0 
			 OR A.[III J. Other Private Scholarships (1320)]<0 
			 OR A.[III K. Tuition Waivers / Remission of Fees to Employees and Dependents (1321)]<0 
				then 1 
				else 0 end as [Unduplicated Scholarship Count] 	 , case when A.[IV A. Federal Work-Study (FWS)   (1401)]<0 then A.[IV A. Federal Work-Study (FWS)   (1401)]		 else 0 end as [IV A. Federal Work-Study (FWS)   (1401) Amount]	 , case when A.[IV A. Federal Work-Study (FWS)   (1401)]<0 then 1		 else 0 end as [IV A. Federal Work-Study (FWS)   (1401) Count]
	 ,  case when A.[Pell]<0  
			 OR A.[SEOG]<0 
			 OR A.[D Educatoin Assistance Grants]<0 
			 OR A.[H. Institutional Grants]<0 
			 OR A.[II B. William D. Ford Subsidized Loans]<0 
			 OR A.[II C. Federal Stafford Loans Unsubsidized (1203)]<0 
			 OR A.[II D. Federal PLUS Loans (1204)]<0 
			 OR A.[II F. Institutional Loans (1206)]<0 
			 OR A.[II G. Loans  from Private Sources (1207)]<0 
			 OR A.[III C. Senatorial Scholarship (1305)]<0 
			 OR A.[III H. Other Institutional  Scholarships (1318)]<0 
			 OR A.[III J. Other Private Scholarships (1320)]<0 
			 OR A.[III K. Tuition Waivers / Remission of Fees to Employees and Dependents (1321)]<0 
			 OR A.[IV A. Federal Work-Study (FWS)   (1401)]<0 
				then 1 
				else 0 end as [Unduplicated Count] 

from ##SL s
left join ##Awards A
	on A.systudentid = S.Systudentid 


drop table ##SL
drop table ##Awards