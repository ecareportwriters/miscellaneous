

set nocount on
set ansi_warnings off
set ansi_nulls off


declare @AY2   as varchar (7)
declare @ISIRTable2 as varchar(8)
declare @ISIRID2 as varchar(10)
declare @SQL2 as varchar(1000)
declare @HighDate as date
declare @LowDate as date





set @AY2 = '2011-12'
set @ISIRTable2 = 'FAIsir'+RIGHT(@AY2,2)
set @ISIRID2	 = 'FAIsir'+RIGHT(@AY2,2)+'ID'
set @HighDate =  GETDATE()
set @LowDate = '2012-09-01'


set @SQL2= 
'select Isir.TransactionID 
     , FM.SyStudentID   
     , FM.FAISIRID
     , FM.male
     , FM.StudentNumFamily
     , FM.FirstCollegeHousing
     , Dependecy = case when FM.Model <>''I'' then ''D'' else ''I''
     , income = convert( int,convert(varchar,left(FM.StudentIncome,5))+ case when right(FM.studentincome,1) = ''{'' then ''0'' else convert(varchar,(ASCII(right(FM.StudentIncome,1))-ASCII(''A'')+1)) end )
     , case when Isir.PEFC <> '''' then Isir.PEFC
            else 0 end as PEFC
into ##ISIRs2
from FAISIRMatch (nolock) as FM
join '+ @ISIRTable2+ '(nolock) as Isir
on Isir.'+@ISIRID2+' = FM.FaIsirId
where FM.AwardYear = ''' +@AY2+
''' order by  TransactionID desc'

exec (@sql2)

select studentname = RTRIM(SS.lastname)+', '+RTRIM(SS.firstname)
	 , SS.SSN
	 , EFC = ISNULL((select top 1 PEFC
						from ##ISIRs2
						where SS.SyStudentID = ##ISIRS2.SystudentID),-1)
from syStudent (nolock) as SS
where SS.SySchoolStatusID = 13

--select top 10  model,StudentNumFamily, FaISIR12.StudentGross,FaISIR12.FirstCollegeHousing, income = convert( int,convert(varchar,left(FaISIR12.StudentIncome,5))+ case when right(faisir12.studentincome,1) = '{' then '0' else convert(varchar,(ASCII(right(FaISIR12.StudentIncome,1))-ASCII('A')+1)) end ),FaISIR12.StudentIncome,*  from faisir12
--where model	in ('X','Y')
--select distinct model from faisir12

--print ASCII('Z') - ASCII('A') + 1


drop table ##ISIRs2