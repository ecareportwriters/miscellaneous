
with StudentList_cte
	as
(
Select distinct AE.SyStudentID 
	 , AT.AdTermID
	-- , AT.Code
from adattend (nolock) as ADT
	join AdEnroll (nolock) as AE
		on AE.AdEnrollID = ADT.AdEnrollID
	join AdEnrollSched (nolock) as AES
		on AES.AdEnrollSchedID = ADT.AdEnrollSchedID
	join AdTermRelationship (nolock) as ATR
		on ATR.ChildAdTermID = AES.AdTermID
	join AdTerm (nolock) as AT
		on at.AdTermID = ATR.ParentAdTermID
where ADT.Date between '2013-07-01' and '2014-06-30'
)


, GrossFunds_cte
as
(
select STN.SyStudentID
	 --, AT.Code
	 
	 , Tuition = SUM( case when SaBillCode in ('TUIT','TUITADJ','TUIT CR','INT TUIT','TERMGRAD','TERMNIGH','TUITCRSF','TUITCRAD') then STN.Amount else 0 end )
	 , Directloans = SUM( case when FSA.FaFundSourceID in (
															5  --Direct Subsidized
														   ,7  --Direct Unsubsidized
														   ,9  --Direct Plus
														   ,60 --Direct Grad Plus
														   ) and FR.SaStipendID is null then STN.Amount else 0 end ) 
	 , Pell =  SUM( case when FSA.FaFundSourceID in (1 --Pell Grant
													 ) and FR.SaStipendID is null then STN.Amount else 0 end ) 
	 , Privateloans = SUM( case when FSA.FaFundSourceID in (202 -- Alternative Loans
															) and FR.SaStipendID is null then STN.Amount else 0 end )
	 , SelfPay = SUM( case when FSA.FaFundSourceID in (28  --Student payment
												      ,59  --SLM Pay Plan
												       ) and FR.SaStipendID is null then STN.Amount else 0 end )
			    + SUM( case when SaBillCode in ('COBD') then STN.Amount else 0 end )
	 , InstitutionFunding = SUM( case when FSA.FaFundSourceID in (56,57,81,12,46,45,36,21,12) and FR.SaStipendID is null then STN.Amount else 0 end )
	 , WIA = SUM( case when FSA.FaFundSourceID in (11  --WIA
										           ) and FR.SaStipendID is null then STN.Amount else 0 end )
	 , VABenefits = SUM( case when FSA.FaFundSourceID in (42	--MILAI   	Military - AI
														 ,77 --	MILAFTA 	Military - Air Force Tuition Assistance
														 ,75	--MILATA  	Military - Army Tution Assistance
														 ,79	--MCH1606 	Military - Ch. 1606 Reserve GI Bill
														 ,62	--MILCH160	Military - Ch. 1607 : Research EAP
														 ,63	--MILCH30 	Military - Ch. 30 Montgomery GI Bill
														 ,65	--MCH33100	Military - Ch. 33 - 100% Eligibility
														 ,71	--MCH3340 	Military - Ch. 33 - 40% Eligibility
														 ,70	--MCH3350 	Military - Ch. 33 - 50% Eligibility
														 ,69	--MCH3360 	Military - Ch. 33 - 60% Eligibility
														 ,68	--MCH3370 	Military - Ch. 33 - 70% Eligibility
														 ,67	--MCH3380 	Military - Ch. 33 - 80% Eligibility
														 ,66	--MCH3390 	Military - Ch. 33 - 90% Eligibility
														 ,83	--MCH33FRY	Military - Ch. 33 - Frye Scholarship
														 ,82	--MCH33TEB	Military - Ch. 33 - Transfer of Eligibil
														 ,72	--MCH35DS 	Military - Ch. 35 - D & S EAP
														 ,52	--MILGOARM	Military - Go Army
														 ,73	--MILMYCAA	Military - My Career Advancement Account
														 ,74	--MILNTA  	Military - Navy Tuition Assistance
														 ,49	--MILDIR  	Military - VA Once (Direct)
														 ,50	--MILVAONC	Military - VA Once (Other)
														 ,80	--MILVRAP 	Military - VRAP
														 ,43	--MILWAWF 	Military - WAWF
														 ,48	--MILYELLO	Military - Yellow Ribbon
														 ,76	--MILCTA  	Military -Coast Guard Tuition Assistance
														 ,58	--MILSTU  	Military VA ONCE - Student Payment
														 ,78	--MILMTA  	Military-Marine Corps Tuition Assistance
										           ) and FR.SaStipendID is null then STN.Amount else 0 end )

	 , FSEOG = SUM( case when FSA.FaFundSourceID in (2  --SEOG
										           ) and FR.SaStipendID is null then STN.Amount else 0 end )

	 , TNgrant = SUM( case when FSA.FaFundSourceID in (34  --State Grant
										           ) and FR.SaStipendID is null then STN.Amount else 0 end )
	 , VocRehab = SUM( case when FSA.FaFundSourceID in (37	--VOCREHS 	Vocational Rehab (State)
										           ) and FR.SaStipendID is null then STN.Amount else 0 end )

	 , FundedScholarships = SUM( case when FSA.FaFundSourceID in (27	--AGENCY  	Agency Payment
										           ) and FR.SaStipendID is null then STN.Amount else 0 end )

	
from StudentList_cte as SL
	join SaTrans (nolock) as STN
		on STN.SyStudentID = SL.SyStudentID
		and STN.AdTermID = SL.AdTermID
	left join FaStudentAid (nolock) as FSA
		on FSA.FaStudentAidId = STN.FaStudentAidID
	left join FaRefund (nolock) as FR
		on FR.FaRefundID = STN.FaRefundID
	join AdTerm (nolock) as AT
		on AT.AdTermID = STN.AdTermID
group by STN.SyStudentID
		--, AT.Code

	)
--,Work_cte
--as
--(	
	select GR.*
		 ,  SC.Descrip as Campus 
		 , DLPart = case when GR.Directloans < GR.Tuition then Directloans else Tuition end
		 , PellPart = case when  (Tuition-Directloans)>0 then 
						 case when Pell< (Tuition-Directloans) then Pell else (Tuition-Directloans) end
					 else 0 end 
		 , PrivateLoansPart = case when (Tuition-Directloans-Pell)>0 then 
								case when Privateloans< (Tuition-Directloans-Pell) then Privateloans else (Tuition-Directloans-Pell) end
					 else 0 end 
		 , SelfPayPart = case when  (Tuition-Directloans-Pell-Privateloans)>0 then 
								case when SelfPay< (Tuition-Directloans-Pell-Privateloans) then SelfPay else (Tuition-Directloans-Pell-Privateloans) end
					 else 0 end 
		 , InstitutionFundingPart = case when  (Tuition-Directloans-Pell-Privateloans-SelfPay)>0 then 
						 case when InstitutionFunding< (Tuition-Directloans-Pell-Privateloans-SelfPay) then InstitutionFunding else (Tuition-Directloans-Pell-Privateloans-SelfPay) end
					 else 0 end 
		 , WIAPart =  case when  (Tuition-Directloans-Pell-Privateloans-SelfPay-InstitutionFunding) >0 then 
						case when WIA< (Tuition-Directloans-Pell-Privateloans-SelfPay-InstitutionFunding) then WIA else (Tuition-Directloans-Pell-Privateloans-SelfPay-InstitutionFunding) end
					 else 0 end 
		 , VAPart =  case when  (Tuition-Directloans-Pell-Privateloans-SelfPay-InstitutionFunding-WIA)>0 then 
						case when VABenefits< (Tuition-Directloans-Pell-Privateloans-SelfPay-InstitutionFunding-WIA) then VABenefits else (Tuition-Directloans-Pell-Privateloans-SelfPay-InstitutionFunding-WIA) end	
					 else 0 end 	 		 
		 , SEOGPart =  case when  (Tuition-Directloans-Pell-Privateloans-SelfPay-InstitutionFunding-WIA-VABenefits)>0 then 
						case when FSEOG< (Tuition-Directloans-Pell-Privateloans-SelfPay-InstitutionFunding-WIA-VABenefits) then FSEOG else (Tuition-Directloans-Pell-Privateloans-SelfPay-InstitutionFunding-WIA-VABenefits) end	
					 else 0 end 
		 , TNGrantGPart =  case when  (Tuition-Directloans-Pell-Privateloans-SelfPay-InstitutionFunding-WIA-VABenefits-FSEOG)>0 then 
						case when TNgrant< (Tuition-Directloans-Pell-Privateloans-SelfPay-InstitutionFunding-WIA-VABenefits-FSEOG) then TNgrant else (Tuition-Directloans-Pell-Privateloans-SelfPay-InstitutionFunding-WIA-VABenefits-FSEOG) end	
					 else 0 end 
		 , VocRehabGPart =  case when  (Tuition-Directloans-Pell-Privateloans-SelfPay-InstitutionFunding-WIA-VABenefits-FSEOG-TNgrant)>0 then 
						case when VocRehab< (Tuition-Directloans-Pell-Privateloans-SelfPay-InstitutionFunding-WIA-VABenefits-FSEOG-TNgrant) then VocRehab else (Tuition-Directloans-Pell-Privateloans-SelfPay-InstitutionFunding-WIA-VABenefits-FSEOG-TNgrant) end	
					 else 0 end 
		 , FundedSchsGPart =  case when  (Tuition-Directloans-Pell-Privateloans-SelfPay-InstitutionFunding-WIA-VABenefits-FSEOG-TNgrant-VocRehab)>0 then 
						case when FundedScholarships< (Tuition-Directloans-Pell-Privateloans-SelfPay-InstitutionFunding-WIA-VABenefits-FSEOG-TNgrant-VocRehab) then FundedScholarships else (Tuition-Directloans-Pell-Privateloans-SelfPay-InstitutionFunding-WIA-VABenefits-FSEOG-TNgrant-VocRehab) end	
					 else 0 end 
		 , RemainingBalance = case when  (Tuition-Directloans-Pell-Privateloans-SelfPay-InstitutionFunding-WIA-VABenefits-FSEOG-TNgrant-VocRehab-FundedScholarships)>0 then (Tuition-Directloans-Pell-Privateloans-SelfPay-InstitutionFunding-WIA-VABenefits-FSEOG-TNgrant-VocRehab-FundedScholarships)
									 else 0 end 

from GrossFunds_cte as GR
join syStudent (Nolock) as SS
	on SS.SyStudentId = GR.SyStudentID
join SyCampus (nolock) as SC 
	on SC.SyCampusID = SS.SyCampusID 
where SC.SyCampusID in (16,40)
)

select Campus	
	 , Tuition = sum(W.Tuition) 
	 , [Federal Loans] = sum(W.DLPart)
	 , [Federal Loans Count] = sum(case when W.DLPart >0 then 1 else 0 end ) 
	 , [Federal Pell] = sum (W.PellPart)
	  , [Federal Pell Count] = sum (case when W.PellPart>0 then 1 else 0 end )
	 , [Private Loans] = sum(W.PrivateLoansPart)
	 , [Private Loans Count] = sum(case when W.PrivateLoansPart>0 then 1 else 0 end )
, [Self Pay] = sum(W.SelfPayPart)
	 , [Self pay Count] = sum(case when W.SelfPayPart>0 then 1 else 0 end )
, [In House funding] = sum(W.InstitutionFundingPart)
	 , [In House Funding Count] = sum(case when W.InstitutionFundingPart>0 then 1 else 0 end )
, [WIA] = sum(W.WIAPart)
	 , [WIA Count] = sum(case when W.WIAPart>0 then 1 else 0 end )
, [VA Benefits] = sum(W.VAPart)
	 , [VA Benefits Count] = sum(case when W.VAPart>0 then 1 else 0 end )
, [SEOG] = sum(W.SEOGPart)
	 , [SEOG Count] = sum(case when W.SEOGPart>0 then 1 else 0 end )
, [TN Grant] = sum(W.TNGrantGPart)
	 , [TN Grant Count] = sum(case when W.TNGrantGPart>0 then 1 else 0 end )
, [Vocational Rehab] = sum(W.VocRehabGPart)
	 , [Vocational Rehab Count] = sum(case when W.VocRehabGPart>0 then 1 else 0 end )
, [Funded Scholarship] = sum(W.FundedSchsGPart)
	 , [Funded Scholarship Count] = sum(case when W.FundedSchsGPart>0 then 1 else 0 end )
, [Remaining Balance] = sum(W.RemainingBalance)
	 , [Remaining Balance Count] = sum(case when W.RemainingBalance>0 then 1 else 0 end )
	  
	from Work_cte as W
group by campus