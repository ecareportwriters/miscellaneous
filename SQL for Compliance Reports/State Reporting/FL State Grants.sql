select StudentName = Rtrim(SS.LastName)+', '+Rtrim(SS.firstname)
     , SS.SyStudentId
     , SS.SSN
     , SS.State
     , SC.Descrip as Campus
     , FFS.Descrip as FundSource
     , sum(STN.Amount)as TotalReceived
     
from systudent (nolock) as SS
join fastudentaid (nolock) as FSA
on FSA.SyStudentID = SS.systudentID
join SaTrans (nolock) as STN
on STN.fastudentaidID = FSA.FaStudentAidID
join FaFundSource (nolock) as FFS
on FFS.FaFundSourceID = FSA.FaFundSourceID
join sycampus (nolock) as SC
on SC.sycampusid = SS.sycampusid
where FFS.FaFundSourceID = 34
  and SS.State = 'FL'
  and STN.Date between '2010-07-01' and '2011-06-30'
group by SS.LastName
	 , SS.firstname
     , SS.SyStudentId
     , SS.SSN
     , SS.State
     , SC.Descrip
     , FFS.Descrip 
order by Campus, StudentName