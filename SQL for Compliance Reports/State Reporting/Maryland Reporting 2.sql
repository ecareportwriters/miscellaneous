--get cohort from academics, put socials in at end to filter for correct cohort, then change the term codes


set nocount on
set ansi_warnings off
set ansi_nulls off


declare @AY2   as varchar (7)
declare @ISIRTable2 as varchar(8)
declare @ISIRID2 as varchar(10)
declare @SQL2 as varchar(1000)
declare @HighDate as date
declare @LowDate as date





set @AY2 = '2011-12'
set @ISIRTable2 = 'FAIsir'+RIGHT(@AY2,2)
set @ISIRID2	 = 'FAIsir'+RIGHT(@AY2,2)+'ID'
set @HighDate =  GETDATE()
set @LowDate = '2012-09-01'




select Isir.TransactionID 
     , FM.SyStudentID   
     , FM.FAISIRID
     , Isir.male
     , Isir.StudentNumFamily
     , Isir.ParentNumFamily
     , Isir.FirstCollegeHousing
     , Housing = case	when Isir.FirstCollegeHousing = 1 then 3 else Isir.FirstCollegeHousing end 
     , Dependency = case when Isir.Model <>'I'then'D' else'I' end 
     ,income = CAST (case	when ISIR.StudentGross like '%{'	then replace(ISIR.StudentGross,'{','0')
							when ISIR.StudentGross like '%a'	then replace(ISIR.StudentGross,'a','1')
							when ISIR.StudentGross like '%b'	then replace(ISIR.StudentGross,'b','2')
							when ISIR.StudentGross like '%c'	then replace(ISIR.StudentGross,'c','3')
							when ISIR.StudentGross like '%d'	then replace(ISIR.StudentGross,'d','4')
							when ISIR.StudentGross like '%e'	then replace(ISIR.StudentGross,'e','5')
							when ISIR.StudentGross like '%f'	then replace(ISIR.StudentGross,'f','6')
							when ISIR.StudentGross like '%g'	then replace(ISIR.StudentGross,'g','7')
							when ISIR.StudentGross like '%h'	then replace(ISIR.StudentGross,'h','8')
							when ISIR.StudentGross like '%i'	then replace(ISIR.StudentGross,'i','9')
							when ISIR.StudentGross like '%j'	then replace(ISIR.StudentGross,'j','1')
							when ISIR.StudentGross like '%k'	then replace(ISIR.StudentGross,'k','2')
							when ISIR.StudentGross like '%l'	then replace(ISIR.StudentGross,'l','3')
							when ISIR.StudentGross like '%m'	then replace(ISIR.StudentGross,'m','4')
							when ISIR.StudentGross like '%n'	then replace(ISIR.StudentGross,'n','5')
							when ISIR.StudentGross like '%o'	then replace(ISIR.StudentGross,'o','6')
							when ISIR.StudentGross like '%p'	then replace(ISIR.StudentGross,'p','7')
							when ISIR.StudentGross like '%q'	then replace(ISIR.StudentGross,'q','8')
							when ISIR.StudentGross like '%r'	then replace(ISIR.StudentGross,'r','9')
							when ISIR.StudentGross ='  12 0'	then '0'
							when rtrim(ISIR.StudentGross) = '' then '0'
							else replace(ISIR.StudentGross,'}','0')
					  end as  MONEY)
		,Pincome = CAST (case	when ISIR.ParentGross like '%{'	then replace(ISIR.ParentGross,'{','0')
							when ISIR.ParentGross like '%a'	then replace(ISIR.ParentGross,'a','1')
							when ISIR.ParentGross like '%b'	then replace(ISIR.ParentGross,'b','2')
							when ISIR.ParentGross like '%c'	then replace(ISIR.ParentGross,'c','3')
							when ISIR.ParentGross like '%d'	then replace(ISIR.ParentGross,'d','4')
							when ISIR.ParentGross like '%e'	then replace(ISIR.ParentGross,'e','5')
							when ISIR.ParentGross like '%f'	then replace(ISIR.ParentGross,'f','6')
							when ISIR.ParentGross like '%g'	then replace(ISIR.ParentGross,'g','7')
							when ISIR.ParentGross like '%h'	then replace(ISIR.ParentGross,'h','8')
							when ISIR.ParentGross like '%i'	then replace(ISIR.ParentGross,'i','9')
							when ISIR.ParentGross like '%j'	then replace(ISIR.ParentGross,'j','1')
							when ISIR.ParentGross like '%k'	then replace(ISIR.ParentGross,'k','2')
							when ISIR.ParentGross like '%l'	then replace(ISIR.ParentGross,'l','3')
							when ISIR.ParentGross like '%m'	then replace(ISIR.ParentGross,'m','4')
							when ISIR.ParentGross like '%n'	then replace(ISIR.ParentGross,'n','5')
							when ISIR.ParentGross like '%o'	then replace(ISIR.ParentGross,'o','6')
							when ISIR.ParentGross like '%p'	then replace(ISIR.ParentGross,'p','7')
							when ISIR.ParentGross like '%q'	then replace(ISIR.ParentGross,'q','8')
							when ISIR.ParentGross like '%r'	then replace(ISIR.ParentGross,'r','9')
							when ISIR.ParentGross ='  12 0'	then '0'
							when rtrim(ISIR.ParentGross) = '' then '0'
							else replace(ISIR.ParentGross,'}','0')
					  end as  MONEY)
		, case when Isir.PEFC <> '' then Isir.PEFC
            else 0 end as PEFC
into ##ISIRs2
from FAISIRMatch (nolock) as FM
join FAISIR12 (nolock) as Isir
on Isir.FaISIR12ID = FM.FaIsirId
where FM.AwardYear = '2011-12'
 order by  TransactionID desc


select Isir.TransactionID 
     , FM.SyStudentID   
     , FM.FAISIRID
     , Isir.male
     , Isir.StudentNumFamily
     , Isir.ParentNumFamily
     , Housing = case	when Isir.FirstCollegeHousing = 1 then 3 else Isir.FirstCollegeHousing end 
     , Dependency = case when Isir.Model <>'I'then'D' else'I' end 
     ,income = CAST (case	when ISIR.StudentGross like '%{'	then replace(ISIR.StudentGross,'{','0')
							when ISIR.StudentGross like '%a'	then replace(ISIR.StudentGross,'a','1')
							when ISIR.StudentGross like '%b'	then replace(ISIR.StudentGross,'b','2')
							when ISIR.StudentGross like '%c'	then replace(ISIR.StudentGross,'c','3')
							when ISIR.StudentGross like '%d'	then replace(ISIR.StudentGross,'d','4')
							when ISIR.StudentGross like '%e'	then replace(ISIR.StudentGross,'e','5')
							when ISIR.StudentGross like '%f'	then replace(ISIR.StudentGross,'f','6')
							when ISIR.StudentGross like '%g'	then replace(ISIR.StudentGross,'g','7')
							when ISIR.StudentGross like '%h'	then replace(ISIR.StudentGross,'h','8')
							when ISIR.StudentGross like '%i'	then replace(ISIR.StudentGross,'i','9')
							when ISIR.StudentGross like '%j'	then replace(ISIR.StudentGross,'j','1')
							when ISIR.StudentGross like '%k'	then replace(ISIR.StudentGross,'k','2')
							when ISIR.StudentGross like '%l'	then replace(ISIR.StudentGross,'l','3')
							when ISIR.StudentGross like '%m'	then replace(ISIR.StudentGross,'m','4')
							when ISIR.StudentGross like '%n'	then replace(ISIR.StudentGross,'n','5')
							when ISIR.StudentGross like '%o'	then replace(ISIR.StudentGross,'o','6')
							when ISIR.StudentGross like '%p'	then replace(ISIR.StudentGross,'p','7')
							when ISIR.StudentGross like '%q'	then replace(ISIR.StudentGross,'q','8')
							when ISIR.StudentGross like '%r'	then replace(ISIR.StudentGross,'r','9')
							when ISIR.StudentGross ='  12 0'	then '0'
							when rtrim(ISIR.StudentGross) = '' then '0'
							else replace(ISIR.StudentGross,'}','0')
					  end as  MONEY)
		,Pincome = CAST (case	when ISIR.ParentGross like '%{'	then replace(ISIR.ParentGross,'{','0')
							when ISIR.ParentGross like '%a'	then replace(ISIR.ParentGross,'a','1')
							when ISIR.ParentGross like '%b'	then replace(ISIR.ParentGross,'b','2')
							when ISIR.ParentGross like '%c'	then replace(ISIR.ParentGross,'c','3')
							when ISIR.ParentGross like '%d'	then replace(ISIR.ParentGross,'d','4')
							when ISIR.ParentGross like '%e'	then replace(ISIR.ParentGross,'e','5')
							when ISIR.ParentGross like '%f'	then replace(ISIR.ParentGross,'f','6')
							when ISIR.ParentGross like '%g'	then replace(ISIR.ParentGross,'g','7')
							when ISIR.ParentGross like '%h'	then replace(ISIR.ParentGross,'h','8')
							when ISIR.ParentGross like '%i'	then replace(ISIR.ParentGross,'i','9')
							when ISIR.ParentGross like '%j'	then replace(ISIR.ParentGross,'j','1')
							when ISIR.ParentGross like '%k'	then replace(ISIR.ParentGross,'k','2')
							when ISIR.ParentGross like '%l'	then replace(ISIR.ParentGross,'l','3')
							when ISIR.ParentGross like '%m'	then replace(ISIR.ParentGross,'m','4')
							when ISIR.ParentGross like '%n'	then replace(ISIR.ParentGross,'n','5')
							when ISIR.ParentGross like '%o'	then replace(ISIR.ParentGross,'o','6')
							when ISIR.ParentGross like '%p'	then replace(ISIR.ParentGross,'p','7')
							when ISIR.ParentGross like '%q'	then replace(ISIR.ParentGross,'q','8')
							when ISIR.ParentGross like '%r'	then replace(ISIR.ParentGross,'r','9')
							when ISIR.ParentGross ='  12 0'	then '0'
							when rtrim(ISIR.ParentGross) = '' then '0'
							else replace(ISIR.ParentGross,'}','0')
					  end as  MONEY)
		, case when Isir.PEFC <> '' then Isir.PEFC
            else 0 end as PEFC
	
into ##ISIRs
from FAISIRMatch (nolock) as FM
join FAISIR11 (nolock) as Isir
on Isir.FaISIR11ID = FM.FaIsirId
where FM.AwardYear = '2010-11'
 order by  TransactionID desc


select AES.SyStudentID
	 , FALL = SUM(case when AT.AdTermID =30568  then AES.Credits else 0 end )
	 , SPR =  0--SUM(case when AT.AdTermID =30565  then AES.Credits else 0 end )
into #RCBT
from AdEnrollSched (nolock) as AES
join AdTermRelationship (nolock) as ATR
on ATR.ChildAdTermID = AES.AdTermID 
join AdTerm (nolock) as AT 
on AT.AdTermID = ATR.ParentAdTermID
where AT.AdTermID in (30787,30568)
group by 
AES.SyStudentID


select OPEID = '03010600'
	 , SS.SSN  
	 , IdType = 2
	 , AMS.Descrip as Sex
	 , Gender = case when FSA.AwardYear = '2011-12' then   ISNULL((select top 1 MALE
																	from ##ISIRs2
																	where SS.SyStudentID = ##ISIRS2.SystudentID),-1)
				     when FSA.AwardYear = '2010-11' then   ISNULL((select top 1 MALE
																	from ##ISIRs
																	where SS.SyStudentID = ##ISIRS.SystudentID),-1)
				   END 
	 , FaAppStatus = case when FSA.AwardYear = '2011-12' then ISNULL((select COUNT(faisirid)
																		from ##ISIRs2
																		where SS.SyStudentID = ##ISIRS2.SystudentID),1)
						  when FSA.AwardYear = '2010-11' then ISNULL((select COUNT(faisirid)
																		from ##ISIRs
																		where SS.SyStudentID = ##ISIRS.SystudentID),1)
					   end 
	 , FALL = case when RCBT.FALL >=12 then 1 
				   when RCBT.FALL between 1 and 11 then 2
				   else 0 
				end 
	 , SPR = case when RCBT.SPR >=12 then 1 
				   when RCBT.SPR between 1 and 11 then 2
				   else 0 
				end 
	 , FamilySizeKid = case when FSA.AwardYear = '2011-12' then ISNULL((select top 1 StudentNumFamily
																		from ##ISIRs2
																		where SS.SyStudentID = ##ISIRS2.SystudentID),-1)
						 when FSA.AwardYear = '2010-11' then ISNULL((select top 1 StudentNumFamily
																		from ##ISIRs
																		where SS.SyStudentID = ##ISIRS.SystudentID),-1)
					  END
	 , FamilySizeParent = case when FSA.AwardYear = '2011-12' then ISNULL((select top 1 ParentNumFamily
																		from ##ISIRs2
																		where SS.SyStudentID = ##ISIRS2.SystudentID),-1)
						 when FSA.AwardYear = '2010-11' then ISNULL((select top 1 ParentNumFamily
																		from ##ISIRs
																		where SS.SyStudentID = ##ISIRS.SystudentID),-1)
					  END	 
     , DependencyStatus = case when FSA.AwardYear = '2011-12' then ISNULL((select top 1 Dependency
																				from ##ISIRs2
																				where SS.SyStudentID = ##ISIRS2.SystudentID),'')
								when FSA.AwardYear = '2010-11' then ISNULL((select top 1 Dependency
																				from ##ISIRs
																				where SS.SyStudentID = ##ISIRS.SystudentID),'')
							END		
	 , CommuterStatus = case when FSA.AwardYear = '2011-12' then ISNULL((select top 1 Housing
																				from ##ISIRs2
																				where SS.SyStudentID = ##ISIRS2.SystudentID),-1)
								when FSA.AwardYear = '2010-11' then ISNULL((select top 1 Housing
																				from ##ISIRs
																				where SS.SyStudentID = ##ISIRS.SystudentID),-1)
							END	
	 , EFC = case when FSA.AwardYear = '2011-12' then ISNULL((select top 1 PEFC
																from ##ISIRs2
																where SS.SyStudentID = ##ISIRS2.SystudentID),-1)
				  when FSA.AwardYear = '2010-11' then ISNULL((select top 1 PEFC
																from ##ISIRs
																where SS.SyStudentID = ##ISIRS.SystudentID),-1)
				END
	 , AGI = case when FSA.AwardYear = '2011-12' then ISNULL((select top 1 Income + Pincome
																from ##ISIRs2
																where SS.SyStudentID = ##ISIRS2.SystudentID),0)
				  when FSA.AwardYear = '2010-11' then ISNULL((select top 1 Income + Pincome
																from ##ISIRs
																where SS.SyStudentID = ##ISIRS.SystudentID),0)
				END
     , case when AE.AdGradeLevelID = 7 then case when FFS.fafundsourceid = 5 then 4702
									             when FFS.FaFundSourceID = 7 then 4703
									             when FFS.faFundSourceID = 60 then 4707
			
									         end 
		    else case when FFS.fafundsourceid = 5 then 1202
		              when FFS.FaFundSourceID = 7 then 1203
		              when FFS.faFundSourceID = 60 then 1204
		              when FFS.faFundSourceID = 1 then 1101
		              when FFS.faFundSourceID = 2 then 1102
		              when FFS.faFundSourceID is null and STN.SaBillCode = 'TUITWAIV' then 1323
		              when FFS.faFundSourceID is null and STN.SaBillCode = 'EEWAIV' then 1321

		           end  
		    
		 end   AS  FundType
	 , SUM(STN.Amount)as TotalRcvd
	 , COAtt = FAY.Tuition +FAY.RoomBoard + FAY.BooksSupplies+ FAY.Travel+FAY.OtherAmount1+FAY.OtherAmount2+FAY.OtherAmount3+FAY.BankFees
into #work
from SaTrans (nolock) as STN
join  syStudent (nolock) as SS
on SS.SyStudentId = STN.SyStudentID
join FaStudentAid (nolock) as FSA
on FSA.FaStudentAidId = STN.FaStudentAidID 
join FaFundSource (nolock) as FFS 
on FFS.faFundSourceID = FSA.FaFundSourceID
join AdEnroll (nolock) as AE
on AE.AdEnrollID = STN.AdEnrollID
left join FaRefund (nolock) as FR
on FR.FaRefundID = STN.FaRefundID
join amSex (nolock) as AMS
on AMS.amSexID = SS.AmSexID
--left join AdGradeLevel (nolock) as AGL
--on AGL.AdGradeLevelID = AE.AdGradeLevelID
left join #RCBT (nolock) as RCBT
on RCBT.systudentID = STN.SyStudentID 
left join Fastudentay (nolock) as FAY
on FAY.FAStudentAYID = FSA.FaStudentAYID
where STN.AdTermID in (30568)
  and FR.SaStipendID is null
  and AE.SyCampusID =13
group by 
SS.SSN
, FFS.faFundSourceID
, STN.SaBillCode
, AE.AdGradeLevelID
, SS.SyStudentId
, FSA.AwardYear
,FSA.FaStudentAidId
, AMS.Descrip
,RCBT.FALL
,RCBT.SPR
,FAY.Tuition 
,FAY.RoomBoard 
, FAY.BooksSupplies
, FAY.Travel
,FAY.OtherAmount1
,FAY.OtherAmount2
,FAY.OtherAmount3
,FAY.BankFees




Select OPEID
	 , SSN
	 , IdType 
	 , GEND = case when Sex = 'Male' then 1
				   when Sex =  'Female' then 2
				   else 3 
				end 
	 , FAStatus = case when FaAppStatus>0 then 0 else 1 end 
	 , FallATT = Fall
	 , SPRATT = SPR
	 , FamilySz = case when DependencyStatus = 'D' and FamilySizeParent <>'' then FamilySizeParent
					   when DependencyStatus = 'I' and FamilySizeKid <>'' then FamilySizeKid
					   when DependencyStatus = 'D' and FamilySizeParent ='' then 2
					   when DependencyStatus = 'I' and FamilySizeKid ='' then 1
					   else 1 
					end 
	 , Dependency = case when DependencyStatus not in ('D','I') then 'I' else DependencyStatus end 
	 , CommuterSt = case when CommuterStatus not in (2,3) then 3 else CommuterStatus end 
	 , EstimatedFamContribution=  case when EFC =-1 or EFC is null then 0 else EFC end 
	 , COA =  (FALL*4*365)+(SPR*4*365)+5500
	 , AdjustedGrossIncome = ISNULL(AGI,0)
	 , FundType
	 , TotalRcvd
from #work 
where TotalRcvd > 0
   and fundtype is not null
  --and (Fall>0 or SPR>0)
 and  fall>0
and   #Work.SSN in ('198-38-5860',
'239-67-0216',
'216-90-3759',
'455-65-5039',
'578-68-0612',
'219-02-5838',
'084-62-3458',
'216-88-8075',
'251-47-5905',
'216-02-8898',
'579-04-3958',
'217-74-1825',
'220-57-9543',
'141-56-7955',
'215-92-8534',
'212-92-7656',
'574-92-6051',
'220-94-3163',
'024-64-6174',
'579-78-9179',
'219-02-6767',
'579-82-6732',
'214-70-8156',
'197-68-0911',
'249-06-1262',
'229-53-6550',
'578-82-5008',
'211-68-2657',
'062-88-2729',
'577-98-0085',
'216-90-2167',
'577-17-4697',
'436-47-6711',
'214-31-2540',
'215-92-4472',
'212-88-8194',
'212-92-8246',
'217-15-9172',
'082-72-3802',
'221-50-5973',
'577-74-6647',
'577-82-1074')







drop table ##ISIRs2
drop table ##ISIRs
drop table #RCBT
drop table #Work



