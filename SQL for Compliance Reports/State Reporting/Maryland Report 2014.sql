declare @AY as varchar(7)
declare @AYStart as datetime
declare @AYEnd as Datetime
declare @FIT as int 

declare @AY2   as varchar (7)
declare @ISIRTable2 as varchar(8)
declare @ISIRID2 as varchar(10)
declare @SQL2 as varchar(1000)
declare @HighDate as date
declare @LowDate as date





set @AY2 = '2011-12'
set @ISIRTable2 = 'FAIsir'+RIGHT(@AY2,2)
set @ISIRID2	 = 'FAIsir'+RIGHT(@AY2,2)+'ID'
set @HighDate =  GETDATE()
set @LowDate = '2012-09-01'


set @AY = '2012-13'												--Set Award Year for report
set @AYStart = (select startdate								--Get strat date of designated award year
				from FaYear
				where Code = @AY)
set @AYEnd = (select EndDate									--Get end date of designated award year
				from FaYear
				where Code = @AY)

------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
/****
Gather 2012-13 ISIR Data
****/
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
				
declare @IsirLink as Table (SSN  varchar(11)
							,TransID  varchar(13)
							, FaIsirMainID int
							, SyStudentID int
							)			;

with Base_cte
as 
(
select  SSN = LEFT(FIM.ISIRSSN,3)+'-'+RIGHT(LEFT(FIM.ISIRSSN,5),2)+'-'+RIGHT(FIM.ISIRSSN,4)
     , max(FIM.TransactionId) as TransID
from FaISIRMain (nolock) as FIM
JOIN FaISIRSparseData (NOLOCK)as FISD
	on FISD.FaISIRMainId = FIM.FaISIRMainId
where FIM.FaISIRAwardYearSchemaId = 2
  and FIM.FaISIRMainId <> 141489
  and FISD.FaISIRSparseDataId <>141615
  and (FISD.c207 is null or FISD.C207 = '')
  and CONVERT(date, FISD.C163,112)= (select max(CONVERT(date, FISD2.C163,112))
												from FaISIRMain (nolock) as FIM2
												JOIN FaISIRSparseData (NOLOCK)as FISD2
													on FISD2.FaISIRMainId = FIM2.FaISIRMainId
												where FIM2.ISIRSsn = FIM.ISIRSsn
												  and FIM2.FaISIRMainId <> 141489
												  and FISD2.FaISIRSparseDataId <>141615
												  and (FISD2.c207 is null or FISD2.C207 = '')
												  and FIM2.FaISIRAwardYearSchemaId=2
												  )
group by FIM.ISIRSsn
)
insert into @IsirLink 
		select B.*
			 , FIM.FaISIRMainId
			 , SS.SyStudentId
		from Base_cte as B
		join FaISIRMain (nolock) as FIM
			on FIM.TransactionId =  B.TransID
		join syStudent (nolock) as SS
			on SS.SSN = B.SSN
		where FIM.FaISIRAwardYearSchemaId = 2;




------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
/****
Gather 2011-12 ISIR Data
****/
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
	/*
select Isir.TransactionID 
     , FM.SyStudentID   
     , FM.FAISIRID
     , Isir.male
     , Isir.StudentNumFamily
     , Isir.ParentNumFamily
     , Isir.FirstCollegeHousing
     , Housing = case	when Isir.FirstCollegeHousing = 1 then 3 else Isir.FirstCollegeHousing end 
     , Dependency = case when Isir.Model <>'I'then'D' else'I' end 
     ,income = CAST (case	when ISIR.StudentGross like '%{'	then replace(ISIR.StudentGross,'{','0')
							when ISIR.StudentGross like '%a'	then replace(ISIR.StudentGross,'a','1')
							when ISIR.StudentGross like '%b'	then replace(ISIR.StudentGross,'b','2')
							when ISIR.StudentGross like '%c'	then replace(ISIR.StudentGross,'c','3')
							when ISIR.StudentGross like '%d'	then replace(ISIR.StudentGross,'d','4')
							when ISIR.StudentGross like '%e'	then replace(ISIR.StudentGross,'e','5')
							when ISIR.StudentGross like '%f'	then replace(ISIR.StudentGross,'f','6')
							when ISIR.StudentGross like '%g'	then replace(ISIR.StudentGross,'g','7')
							when ISIR.StudentGross like '%h'	then replace(ISIR.StudentGross,'h','8')
							when ISIR.StudentGross like '%i'	then replace(ISIR.StudentGross,'i','9')
							when ISIR.StudentGross like '%j'	then replace(ISIR.StudentGross,'j','1')
							when ISIR.StudentGross like '%k'	then replace(ISIR.StudentGross,'k','2')
							when ISIR.StudentGross like '%l'	then replace(ISIR.StudentGross,'l','3')
							when ISIR.StudentGross like '%m'	then replace(ISIR.StudentGross,'m','4')
							when ISIR.StudentGross like '%n'	then replace(ISIR.StudentGross,'n','5')
							when ISIR.StudentGross like '%o'	then replace(ISIR.StudentGross,'o','6')
							when ISIR.StudentGross like '%p'	then replace(ISIR.StudentGross,'p','7')
							when ISIR.StudentGross like '%q'	then replace(ISIR.StudentGross,'q','8')
							when ISIR.StudentGross like '%r'	then replace(ISIR.StudentGross,'r','9')
							when ISIR.StudentGross ='  12 0'	then '0'
							when rtrim(ISIR.StudentGross) = '' then '0'
							else replace(ISIR.StudentGross,'}','0')
					  end as  MONEY)
		,Pincome = CAST (case	when ISIR.ParentGross like '%{'	then replace(ISIR.ParentGross,'{','0')
							when ISIR.ParentGross like '%a'	then replace(ISIR.ParentGross,'a','1')
							when ISIR.ParentGross like '%b'	then replace(ISIR.ParentGross,'b','2')
							when ISIR.ParentGross like '%c'	then replace(ISIR.ParentGross,'c','3')
							when ISIR.ParentGross like '%d'	then replace(ISIR.ParentGross,'d','4')
							when ISIR.ParentGross like '%e'	then replace(ISIR.ParentGross,'e','5')
							when ISIR.ParentGross like '%f'	then replace(ISIR.ParentGross,'f','6')
							when ISIR.ParentGross like '%g'	then replace(ISIR.ParentGross,'g','7')
							when ISIR.ParentGross like '%h'	then replace(ISIR.ParentGross,'h','8')
							when ISIR.ParentGross like '%i'	then replace(ISIR.ParentGross,'i','9')
							when ISIR.ParentGross like '%j'	then replace(ISIR.ParentGross,'j','1')
							when ISIR.ParentGross like '%k'	then replace(ISIR.ParentGross,'k','2')
							when ISIR.ParentGross like '%l'	then replace(ISIR.ParentGross,'l','3')
							when ISIR.ParentGross like '%m'	then replace(ISIR.ParentGross,'m','4')
							when ISIR.ParentGross like '%n'	then replace(ISIR.ParentGross,'n','5')
							when ISIR.ParentGross like '%o'	then replace(ISIR.ParentGross,'o','6')
							when ISIR.ParentGross like '%p'	then replace(ISIR.ParentGross,'p','7')
							when ISIR.ParentGross like '%q'	then replace(ISIR.ParentGross,'q','8')
							when ISIR.ParentGross like '%r'	then replace(ISIR.ParentGross,'r','9')
							when ISIR.ParentGross ='  12 0'	then '0'
							when rtrim(ISIR.ParentGross) = '' then '0'
							else replace(ISIR.ParentGross,'}','0')
					  end as  MONEY)
		, case when Isir.PEFC <> '' then Isir.PEFC
            else 0 end as PEFC
into ##ISIRs2
from FAISIRMatch (nolock) as FM
join FAISIR12 (nolock) as Isir
on Isir.FaISIR12ID = FM.FaIsirId
where FM.AwardYear = '2011-12'
 order by  TransactionID desc

*/

------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
/****
Compile Report
****/
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
with RCBT_cte 
as ( select aes.SyStudentID
		  ,  sum(case when  AT.AdTermID = 30837  then		
									 case when apv.faacademiccalendar = 5 then AES.HoursAttempt else AES.CreditsAttempt End
								else 0 
						end ) as [Fall Attendance]
		 ,  sum(case when  AT.AdTermID = 30844 and AES.Status in ('C', 'P')  then		
									 case when apv.faacademiccalendar = 5 then AES.Hours else AES.Credits End
								else 0 
						end ) as [Spring Attendance]
	from campusvue.dbo.adenrollsched (nolock) as AES
	join campusvue.dbo.adenroll (nolock) as AE
	on AE.AdEnrollID = AES.AdEnrollID
join CampusVue.dbo.AdProgramVersion (nolock) as APV
	on APV.AdProgramVersionID = AE.adProgramVersionID
join campusvue.dbo.AdTermRelationship (nolock) as ATR
	on ATR.ChildAdTermID = AES.AdTermID 
join campusvue.dbo.AdTerm (nolock) as AT
	on AT.AdTermID = ATR.ParentAdTermID

where AES.SyStudentId in (2347742,
3919464,
3803997,
1779666,
4057518,
3790507,
2214943,
2347742,
3946085,
3317739,
2321894,
2372970,
2019908,
2728486,
2762632,
2564369,
3865831,
2350323,
1537627,
2991894,
2846807,
3549031,
3778198,
1897503,
1178308,
2892032,
3640371)
group by AES.SyStudentID 
	
)
select OPEID = '03010600' 
	 ,SS.SyStudentId
	 , 2  as [ID Number Type]
     , case when SEX.Descrip like 'Male' then 1 else 2 end  as Gender
     , case when ISIR13.FaIsirMainID is null then 1 else 0 end  as [Financial Aid Application Status]
	 , case when  rcbt.[Fall Attendance]  =0 then 0 
		    when rcbt.[Fall Attendance] between 1 and 11 then 2 
		    when rcbt.[Fall Attendance]>=12 then 1
		    end as [Fall Attendance]
	 , case when  rcbt.[Spring Attendance]   =0 then 0 
		    when rcbt.[Spring Attendance]  between 1 and 11 then 2 
		    when rcbt.[Spring Attendance] >=12 then 1
		    end as [Spring Attendance] 
	-- , FISD.FaISIRMainId
	 , [Family Size] = case case FISD.C161
								when 'D' then 1
								when 'I' then 2
								when 'X' then 1
								when 'Y' then 2
								else 0
							end
						 when 0 then 0 
						 when 1 then FISD.C92
						 when 2 then FISD.C126
						 else 'Error' 
						 end    
	 , [Dependency Status] = case FISD.C161
								when 'D' then 1
								when 'I' then 2
								when 'X' then 1
								when 'Y' then 2
								else 0
							end  
	 , [Commuter Status] = isnull(FISD.C135,0)
	 , [Expected Family Contribution] = case when FSA.AwardYear = '2012-13' then  ISNULL(convert(int, FISD.c246), 9999999)
											 when FSA.AwardYear = '2013-14' then  ISNULL(convert(int, ISIR14.pefc), 9999999)
											 else ISNULL(convert(int, FISD.c246), 9999999)
											 end 
	 , [Cost of Attendance] = (select SUM( case when stn2.sabillcode in ('TUIT') then 
											Case STN2.Type 
										  When 'I' Then STN2.Amount
										  When 'D' Then STN2.Amount
										  Else STN2.Amount * -1 end 
										 else 0 end ) 
										 from CampusVue.dbo.SaTrans (nolock) as STN2
										 where STN2.SyStudentID = SS.SyStudentId
										  and STN2.AdTermID in (30837,   --Fall
															30844)   --Winter
															)  +5500
	 , [Adjusted Gross Income] = case case FISD.C161
								when 'D' then 1
								when 'I' then 2
								when 'X' then 1
								when 'Y' then 2
								else 0
							end
						 when 0 then 0 
						 when 2 then isnull(FISD.c41,0)
						 when 1 then isnull(FISD.c103,0)+isnull(FISD.c41,0)
						 else 'Error' 
						 end   
	 , FFS.Descrip as FundSource
	 , -1* SUM(case when FR.SaStipendID is null then Case STN.Type 
													  When 'I' Then STN.Amount
													  When 'D' Then STN.Amount
													  Else STN.Amount * -1 end
					else 0 end   ) as [Amount of Award]
from campusvue.dbo.syStudent (nolock) as SS
join campusvue.dbo.SaTrans (nolock) as STN
	on STN.SyStudentID = SS.SyStudentId
	and STN.AdTermID in (30837,   --Fall
						30844)   --Winter
left join CampusVue.dbo.FaRefund (nolock) as FR
	on FR.FaRefundID = STN.FaRefundID

 join campusvue.dbo.FaStudentAid (nolock) as FSA
	on FSA.FaStudentAidId = STN.FaStudentAidID 
 join campusvue.dbo.FaFundSource (nolock) as FFS
	on FFS.faFundSourceID = FSA.FaFundSourceID 
left join @IsirLink as ISIR13
	on ISIR13.SyStudentID =  SS.SyStudentId 
left join campusvue.dbo.FaISIRSparseData (nolock) as FISD
	on FISD.FaISIRMainId = ISIR13.FaIsirMainID
left join campusvue.dbo.amSex (nolock) as SEX
	on SEX.amSexID = SS.AmSexID
left join RCBT_cte as rcbt 
	on rcbt.SyStudentID = SS.SyStudentId
left join [eca-dc-sqlrep1].[Financial Planning].dbo.[1314Summary]  as ISIR14
 on ISIR14.SSN = SS.SSN
where SS.SyStudentId in (2347742,
3919464,
3803997,
1779666,
4057518,
3790507,
2214943,
2347742,
3946085,
3317739,
2321894,
2372970,
2019908,
2728486,
2762632,
2564369,
3865831,
2350323,
1537627,
2991894,
2846807,
3549031,
3778198,
1897503,
1178308,
2892032,
3640371)
group by SS.SyStudentId
	 , SEX.Descrip 
	 , ISIR13.FaIsirMainID 
	 , FISD.C161
	 , FISD.C92
	 , FISD.C126
	 , FISD.C135
	 , FISD.c246
	 , rcbt.[Fall Attendance] 
	 , rcbt.[Spring Attendance]
	 , FFS.Descrip 
	 , ISIR14.pefc
	 , FSA.AwardYear
	 , FISD.c103
	 , FISD.c41
	 --, FISD.FaISIRMainId
	 
	 
	 --select * from FaISIRSparseData 
	 --where FaISIRMainId = 43713
	 
