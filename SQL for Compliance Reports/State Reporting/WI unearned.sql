select RTRIM(SS.lastname)+', '+RTRIM(SS.FirstName)
     , SS.SSN
     , SC.Descrip
     , AmountRcvd= SUM(case when stn.sabillcode in ('TUIT','TUIT CR','WAIVTUIT','INT TUIT','TUITCRSF','TUITCRAD','TUITCRHR') then case stn.type 
																																	when 'I' then STN.Amount
																																	when 'D' then STN.Amount
																																	else -STN.Amount
																																	end  else 0 end)
	 , Refunded = SUM(case when stn.sabillcode in ('TUITADJ ') then case stn.type 
																	when 'I' then STN.Amount
																	when 'D' then STN.Amount
																	else -STN.Amount
																	end  else 0 end)
	 , sum(case when ffs.title4 = 1 and FR.SaStipendID is null then  case stn.type 
																		when 'I' then STN.Amount
																		when 'D' then STN.Amount
																		else -STN.Amount
																		end 
			 else 0
			 end 
											)
from satrans (nolock) as STN
left join FaStudentAid (nolock) as FSA
on FSA.FaStudentAidId = STN.FaStudentAidID
left join fafundsource (nolock) as FFS
	on FFS.FaFundsourceid = fsa.fafundsourceid
left join farefund (nolock) as FR
	on FR.farefundid = stn.farefundid 
join syStudent (nolock) as SS
on SS.SyStudentId = STN.SyStudentID
join SyCampus (nolock) as SC
on SC.SyCampusID = SS.SyCampusID
where SS.State like  '%WI%'
 --and FSA.FaFundSourceID in (1,2,5,7,9)
  --and stn.SaBillCode in ('TUIT','TUIT CR','TUITADJ ','WAIVTUIT','INT TUIT','TUITCRSF','TUITCRAD','TUITCRHR')
  --and STN.Date> SS.StartDate
  and DATEPART(year,stn.date) = 2015
  and SC.SyCampusID = 13
group by  SS.lastname
	 , SS.FirstName
     , SS.SSN
     , SC.Descrip
  --select * from FaFundSource order by Descrip