select SC.Descrip as [Institution Name]
,SC.Addr1 as Address
, '' as [Main Campus] 
,[Additional Location]=''
,[President/Director]=SSTF.Descrip
,RDSF.RDSF as [Financial Aid Director/Supervisor]
,[Business Manager] = ''
,[No. of SFA Recipients]=T4.Recipients
,[FA Office # Professional Staff]=''
,[FA Office # Clerical Staff]=''
,[Registar's Office # Professional Staff]=''
,[Registar's Office # Clerical Staff]=''
,[Business Office # Professional Staff]=''
,[Business Office # Clerical Staff]=''



from CampusVue.dbo.SyCampus (nolock) as SC
	left join CampusVue.dbo.SyCampusDetails (nolock) as SCD
		on SCD.SyCampusID=SC.SyCampusID
	left join CampusVue.dbo.SyStaff (nolock) as SSTF
		on SCD.CampusPresidentID=SSTF.SyStaffID
	left join [Financial Planning].dbo.RDSFCampusAssignments (nolock) as RDSF
		on RDSF.SyCampusID=SC.SyCampusID
	left join [Financial Planning].dbo.vw_T4RecipientsSFAAudit (nolock) as T4
		on t4.Campus=SC.descrip
		
union all

select SC.Descrip as [Institution Name]
,SC.Addr1 as Address
, '' as [Main Campus] 
,[Additional Location]=''
,[President/Director]=SSTF.Descrip
,RDSF.RDSF as [Financial Aid Director/Supervisor]
,[Business Manager] = ''
,[No. of SFA Recipients]=T4.Recipients
,[FA Office # Professional Staff]=''
,[FA Office # Clerical Staff]=''
,[Registar's Office # Professional Staff]=''
,[Registar's Office # Clerical Staff]=''
,[Business Office # Professional Staff]=''
,[Business Office # Clerical Staff]=''



from CampusVue_NECB.dbo.SyCampus (nolock) as SC
	left join CampusVue_NECB.dbo.SyCampusDetails (nolock) as SCD
		on SCD.SyCampusID=SC.SyCampusID
	left join CampusVue_NECB.dbo.SyStaff (nolock) as SSTF
		on SCD.CampusPresidentID=SSTF.SyStaffID
	left join [Financial Planning].dbo.RDSFCampusAssignments (nolock) as RDSF
		on RDSF.SyCampusID=SC.SyCampusID
	left join [Financial Planning].dbo.vw_T4RecipientsSFAAudit_NECB (nolock) as T4
		on t4.Campus=SC.descrip

	order by SC.DEscrip desc

--select * from SyCampus (nolock) SC

