set nocount on 

if object_id('TempDb..##Attended') is not null 
begin 
drop table ##Attended
end 

if object_id('TempDb..##SaTrans') is not null 
begin 
drop table ##SaTrans
end 

if object_id('TempDb..##Cohort') is not null 
begin 
drop table ##Cohort
end 


declare @Y as int = 2016

select distinct ae.systudentid 
into ##Attended
from campusvue.dbo.AdAttend (nolock) as AA
	join campusvue.dbo.adenroll (nolock) as AE
		on ae.adenrollid = AA.adenrollid 
	where datepart(year, AA.date) = datepart(year, getdate())

create clustered index IX_IDs on ##Attended (SyStudentID)

Select Distinct STN.SyStudentID
into ##SaTrans
from campusvue.dbo.satrans (Nolock) as STN
	where datepart(year,stn.date) = datepart(year,getdate())

create clustered index IX_stn on ##SaTrans (SyStudentID)

select distinct A.SyStudentID
into ##Cohort
from ##Attended as A
union
Select S.SyStudentID 
from ##SaTrans as S


Select SC.Code as Campus
	 , SS.Systudentid 
	 , StudentName = rtrim(SS.Lastname)+', '+Rtrim(SS.Firstname)
	 , ss.SSN
	 , replace(replace(isnull(ss.addr1,'') +' '+ isnull(SS.Addr2,''),Char(13),''),char(10),'') as [Address]
	 , SS.City
	 , SS.State
	 , SS.Zip
	 , Box1 = 0 
	 , Box2 = sum( case when (sbc.sa1098t = 1 or FFS.institutional  =1)  and (FFS.faFundSourceID <> 682 or FFS.FaFundSourceID is null)
						 and DatePart(year,SS.CurrentLDA)>= DATEPART(year,getdate())
						 and (
						      datepart(year,AT.StartDate)>= datepart(year,getdate()) 
							  or datepart(year,FSAP.StartDate)>= datepart(year,getdate())
							  or AT.AdtermID in ( 30857		, --AY1PP1          
												  30864		, --AY1PP2          
												  30865		, --AY2PP1          
												  30929		, --AY2PP2          
												  4176911	, --AY3PP1          
												  4176912	, --AY3PP2          
												  4176913	, --AY4PP1          
												  4176914	 --AY4PP2          
												)
							 ) 
							 then 
									case stn.type 
									when 'I' then stn.amount 
									when 'D' then stn.amount
									else -stn.amount
									end
						else 0 
					 end )
					 
	 , Box3 = ''
	 , Box4 = sum( case when (sbc.sa1098t = 1 or (FFS.IncludeIn1098T = 1 and FFS.institutional  =1  ) )
						 and FFS.faFundSourceID <> 682
						 and (
						      datepart(year,AT.StartDate)< datepart(year,getdate()) 
							  or datepart(year,FSAP.StartDate) < datepart(year,getdate())
							  or DatePart(year,SS.CurrentLDA)< DATEPART(year,getdate())
							 ) 
							 then 
									case stn.type 
									when 'I' then stn.amount 
									when 'D' then stn.amount
									else -stn.amount
									end
						else 0 
					 end )
			  
	 , Box5 = sum( case when (
							  (FFS.IncludeIn1098T = 1 and FFS.institutional  = 0 )
							 ) 
						 and DatePart(year,SS.CurrentLDA)>= DATEPART(year,getdate())
						 and 
						     (
						      datepart(year,AT.StartDate)>= datepart(year,getdate()) 
							  or datepart(year,FSAP.StartDate)>= datepart(year,getdate())
							  or AT.AdtermID in ( 30857		, --AY1PP1          
												  30864		, --AY1PP2          
												  30865		, --AY2PP1          
												  30929		, --AY2PP2          
												  4176911	, --AY3PP1          
												  4176912	, --AY3PP2          
												  4176913	, --AY4PP1          
												  4176914	 --AY4PP2          
												)
							 ) 
						then 
							case stn.type 
							when 'I' then stn.amount 
							when 'D' then stn.amount
							else -stn.amount
							end
					    else 0 
				    end ) *-1
	 , Box6 = sum( case when (
							 (FFS.IncludeIn1098T = 1 and FFS.institutional  = 0 )
							 )  
						 and 
						     (
						      datepart(year,AT.StartDate)< datepart(year,getdate()) 
							  or datepart(year,FSAP.StartDate)< datepart(year,getdate())
							  or DatePart(year,SS.CurrentLDA)< DATEPART(year,getdate())
							 ) 
						then 
							case stn.type 
							when 'I' then stn.amount 
							when 'D' then stn.amount
							else -stn.amount
							end
					    else 0 
				    end ) *-1
from ##Cohort as A
 left join campusvue.dbo.satrans (nolock) as STN
	on stn.systudentid = A.Systudentid 
	and  datepart(year,stn.date) = datepart(year,getdate())
 left join campusvue.dbo.sabillcode (nolock) as SBC
	on SBC.code = STN.SaBillCode
 left join campusvue.dbo.FaStudentAid (nolock) as FSA
	on FSA.Fastudentaidid = stn.fastudentaidid 
 left join campusvue.dbo.FaFundSource (nolock) as FFS
	on ffs.fafundsourceid = fsa.fafundsourceid
 join campusvue.dbo.syStudent (nolock) as SS
	on SS.SyStudentid = A.systudentid 
 join campusvue.dbo.sycampus (nolock) as SC
	on sc.sycampusid = ss.sycampusid 
 left join campusvue.dbo.adterm (nolock) as AT
	on at.adtermid = stn.adtermid 
 left join campusvue.dbo.FaRefund (nolock) as FR
	on FR.FaRefundID = stn.FaRefundID
 left join campusvue.dbo.FaStudentAYPaymentPeriod (nolock) as FSAP
	on FSAP.FaStudentAYPaymentPeriodID = STN.FaStudentAYPaymentPeriodID
where fr.sastipendid is null
group by  SC.Code 
	 , SS.Systudentid 
	 , SS.Lastname
	 , SS.Firstname
	 , ss.SSN
	 , ss.addr1
	 , SS.Addr2
	 , SS.City
	 , SS.State
	 , SS.Zip


drop table ##Attended
drop table ##Cohort
drop table ##SaTrans