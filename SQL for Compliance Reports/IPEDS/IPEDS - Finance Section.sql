
declare @FYBegin as datetime = '2014-01-01'
	  , @FYEnd as Datetime = '2014-12-31'
	  
if object_id('Tempdb..##PandL') is not null 
begin 
  drop table ##PandL
  end


select  A.[Year]	
     , A.Segment2 as CampusID
	 , A.Segment4 as CompanyID
	 , A.Campus
	 , sum(A.[Tuition Revenue]) as [Tuition Revenue]
	 , sum(A.[Onground / Online Adjustment]) as [Onground / Online Adjustment]
	 , sum(A.[Scholarship]) as [Scholarship]
	 , sum(A.[Tuition Adjustment Revenue]) as [Tuition Adjustment Revenue]
	 , sum(A.[Other Revenue]) as [Other Revenue]
	 , sum(A.[Total Revenue]) as [Total Revenue]
	 , sum(A.[Instructional Wages]) as [Instructional Wages]
	 , sum(A.[Non-Instructional Wages]) as [Non-Instructional Wages]   
	 , sum(A.[Academic Overtime]) as [Academic Overtime]
	 , sum(A.[Workers Comp]) as [Workers Comp]
	 , [Instructional Worker's Comp] =  case when sum(A.[Total Wages]) >0 then  sum(A.[Workers Comp])- ((sum(A.[Non-Instructional Wages]) / (sum(A.[Instructional Wages])+sum(A.[Non-Instructional Wages])+sum(A.[Academic Overtime])))* sum(A.[Workers Comp]))
												else 0 
											end 
	 , [Non-Instructional Worker's Comp] =	case when sum(A.[Total Wages]) >0 then   (sum(A.[Non-Instructional Wages]) / (sum(A.[Instructional Wages])+sum(A.[Non-Instructional Wages])+sum(A.[Academic Overtime])))* sum(A.[Workers Comp])
											else 0 
											end 
	 , sum(A.[Total Wages]) as [Total Wages]
	 , sum(A.[Training & Development]) as [Training & Development]   
	 , sum(A.[Textbooks]) as [Textbooks]
	 , sum(A.[Library Books]) as [Library Books]
	 , sum(A.[Instructional Expense]) as [Instructional Expense]
	 , sum(A.[IE - Food,Parts,Green]) as [IE - Food,Parts,Green]
	 , sum(A.[Copier & Printing]) as [Copier & Printing]
	 , sum(A.[Academic Payroll Tax]) as [Academic Payroll Tax]
	 , [Instructional Academic Payroll Tax] =case when sum(A.[Total Wages]) >0 then   sum(A.[Academic Payroll Tax])-((sum(A.[Non-Instructional Wages]) / (sum(A.[Instructional Wages])+sum(A.[Non-Instructional Wages])+sum(A.[Academic Overtime])))*sum(A.[Academic Payroll Tax]))
	 											else 0 
											end 
	 
	 , [Non-Instructional Academic Payroll Tax] =  case when sum(A.[Total Wages]) >0 then   (sum(A.[Non-Instructional Wages]) / (sum(A.[Instructional Wages])+sum(A.[Non-Instructional Wages])+sum(A.[Academic Overtime])))*sum(A.[Academic Payroll Tax])
	 											         else 0 
											         end 
	 , sum(A.[Academic Benefits]) as [Academic Benefits]
	 , [Instructional Academic Benefits] =   case when sum(A.[Total Wages]) >0 then   sum(A.[Academic Benefits])-((sum(A.[Non-Instructional Wages]) / (sum(A.[Instructional Wages])+sum(A.[Non-Instructional Wages])+sum(A.[Academic Overtime])))* sum(A.[Academic Benefits]))
	 											else 0 
											end 
	 , [Non-Instructional Academic Benefits] =  case when sum(A.[Total Wages]) >0 then   (sum(A.[Non-Instructional Wages]) / (sum(A.[Instructional Wages])+sum(A.[Non-Instructional Wages])+sum(A.[Academic Overtime])))* sum(A.[Academic Benefits])
	 											else 0 
											end 
	 , sum(A.[Total Academic]) as [Total Academic]
	 , sum(A.[Contribution Margin]) as [Contribution Margin]
	 , sum(A.[Administrative salaries]) as [Administrative salaries]  
	 , sum(A.[Contract labor]) as [Contract labor]
	 , sum(A.[Admin Overtime]) as [Admin Overtime]
	 , sum(A.[Administrative payroll taxes]) as [Administrative payroll taxes]
	 , sum(A.[Administrative benefits]) as [Administrative benefits]
	 , sum(A.[Recruiting and relocation]) as [Recruiting and relocation]
	 , sum(A.[Corporate allocation]) as [Corporate allocation]
	 , sum(A.[Admin Training and development]) as [Admin Training and development]
	 , sum(A.[Payment processing]) as [Payment processing]
	 , sum(A.[Copier and printing]) as [Copier and printing]
	 , sum(A.[Postage]) as [Postage]
	 , sum(A.[Telephone]) as [Telephone]
	 , sum(A.[Business insurance]) as [Business insurance] 
	 , sum(A.[Office supplies]) as [Office supplies]
	 , sum(A.[Rent expense]) as [Rent expense]
	 , sum(A.[Repairs and maintenance]) as [Repairs and maintenance]
	 , sum(A.[Utilities expense]) as [Utilities expense]
	 , sum(A.[Personal property taxes]) as [Personal property taxes]
	 , sum(A.[Occupancy-Other]) as [Occupancy-Other]
	 , sum(A.[Dues and administrative]) as [Dues and administrative]
	 , sum(A.[Accreditation expense]) as [Accreditation expense]
	 , sum(A.[Taxes and licenses]) as [Taxes and licenses]
	 , sum(A.[Professional, audit, and legal]) as [Professional, audit, and legal]
	 , sum(A.[Depreciation and amortization]) as [Depreciation and amortization]
	 , sum(A.[Travel and entertainment]) as [Travel and entertainment]
	 , sum(A.[Tuition waiver]) as [Tuition waiver]
	 , sum(A.[SEOG]) as [SEOG]
	 , sum(A.[Charitable contributions]) as [Charitable contributions]
	 , sum(A.[Bad debt]) as [Bad debt]
	 , sum(A.[TOTAL ADMINISTRATIVE EXPENSES]) as [TOTAL ADMINISTRATIVE EXPENSES]
	 , sum(A.[Admissions Salaries]) as [Admissions Salaries]
	 , sum(A.[Admissions Overtime]) as [Admissions Overtime]
	 , sum(A.[Admissions Payroll Taxes]) as [Admissions Payroll Taxes]
	 , sum(A.[Admissions Benefits]) as [Admissions Benefits]
	 , sum(A.[Admissions Training & Development]) as [Admissions Training & Development]
	 , sum(A.[Admissions Other]) as [Admissions Other]
	 , sum(A.[TOTAL ADMISSIONS]) as [TOTAL ADMISSIONS]
	 , sum(A.[Telemarketing]) as [Telemarketing]
	 , sum(A.[Media]) as [Media]
	 , sum(A.[Internet]) as [Internet]
	 , sum(A.[Other Advertising]) as [Other Advertising]
	 , sum(A.[TOTAL ADVERTISING]) as [TOTAL ADVERTISING]
	 , sum(A.[TOTAL MARKETING EXPENSES]) as [TOTAL MARKETING EXPENSES]
	 , sum(A.[Career Services Salaries]) as [Career Services Salaries]
	 , sum(A.[Student Services Salaries]) as [Student Services Salaries]
	 , sum(A.[Career Services Overtime]) as [Career Services Overtime]
	 , sum(A.[Student Services Overtime]) as [Student Services Overtime]
	 , sum(A.[CS Payroll Taxes]) as [CS Payroll Taxes]
	 , sum(A.[SS Payroll Taxes]) as [SS Payroll Taxes]
	 , sum(A.[CS Benefits]) as [CS Benefits]
	 , sum(A.[SS Benefits]) as [SS Benefits]
	 , sum(A.[Externship Costs]) as [Externship Costs]
	 , sum(A.[Graduation Costs]) as [Graduation Costs]
	 , sum(A.[Retention Expense]) as [Retention Expense]
	 , sum(A.[TOTAL STUDENT SERVICES]) as [TOTAL STUDENT SERVICES]
	 , sum(A.[TOTAL EXPENSES]) as [TOTAL EXPENSES]
	 , sum(A.[Total Revenue])-sum(A.[TOTAL EXPENSES]) as [Net Income]
	 , sum(A.[Interest Expense]) as [Interest Expense]
	 , sum(A.[Income Taxes]) as [Income Taxes]
	 , sum(A.[Interest Income]) as [Interest Income]
	 , sum(A.[Gain (Loss) on Disposal of Fixed Assets]) as [Gain (Loss) on Disposal of Fixed Assets]
	 , sum(A.[Federal Taxes]) as [Federal Taxes]
	 , sum(A.[State Taxes]) as [State Taxes]
	 , [Total O+M] =  sum(A.[Business insurance])+sum(A.[Repairs and maintenance])+sum(A.[Utilities expense])+sum(A.[Personal property taxes])+sum(A.[Occupancy-Other]) 
	 , [Instruction(Initial)] = sum(A.[Instructional Wages]) + sum(A.[Academic Overtime]) 
							  + sum(A.[Training & Development]) + sum(A.[Textbooks])
							  + sum(A.[Instructional Expense]) + sum(A.[IE - Food,Parts,Green])+ sum(A.[Copier & Printing])
							  + case when sum(A.[Total Wages]) >0 then  sum(A.[Workers Comp])- ((sum(A.[Non-Instructional Wages]) / (sum(A.[Instructional Wages])+sum(A.[Non-Instructional Wages])+sum(A.[Academic Overtime])))* sum(A.[Workers Comp]))
									 else 0 
								end 
							  + case when sum(A.[Total Wages]) >0 then   sum(A.[Academic Payroll Tax])-((sum(A.[Non-Instructional Wages]) / (sum(A.[Instructional Wages])+sum(A.[Non-Instructional Wages])+sum(A.[Academic Overtime])))*sum(A.[Academic Payroll Tax]))
	 								 else 0 
								end 
							  + case when sum(A.[Total Wages]) >0 then   sum(A.[Academic Benefits])-((sum(A.[Non-Instructional Wages]) / (sum(A.[Instructional Wages])+sum(A.[Non-Instructional Wages])+sum(A.[Academic Overtime])))* sum(A.[Academic Benefits]))
	 								 else 0 
								end 	
     ,[Instruction(Initial) Percent] =  (sum(A.[Instructional Wages]) + sum(A.[Academic Overtime]) 
									  + sum(A.[Training & Development]) + sum(A.[Textbooks])
									  + sum(A.[Instructional Expense]) + sum(A.[IE - Food,Parts,Green])+ sum(A.[Copier & Printing])
									  + case when sum(A.[Total Wages]) >0 then  sum(A.[Workers Comp])- ((sum(A.[Non-Instructional Wages]) / (sum(A.[Instructional Wages])+sum(A.[Non-Instructional Wages])+sum(A.[Academic Overtime])))* sum(A.[Workers Comp]))
											 else 0 
										end 
									  + case when sum(A.[Total Wages]) >0 then   sum(A.[Academic Payroll Tax])-((sum(A.[Non-Instructional Wages]) / (sum(A.[Instructional Wages])+sum(A.[Non-Instructional Wages])+sum(A.[Academic Overtime])))*sum(A.[Academic Payroll Tax]))
	 										 else 0 
										end 
									  + case when sum(A.[Total Wages]) >0 then   sum(A.[Academic Benefits])-((sum(A.[Non-Instructional Wages]) / (sum(A.[Instructional Wages])+sum(A.[Non-Instructional Wages])+sum(A.[Academic Overtime])))* sum(A.[Academic Benefits]))
	 										 else 0 
										end 	)
										/ 
										(sum(A.[Instructional Wages]) + sum(A.[Academic Overtime]) 
									  + sum(A.[Training & Development]) + sum(A.[Textbooks])
									  + sum(A.[Instructional Expense]) + sum(A.[IE - Food,Parts,Green])+ sum(A.[Copier & Printing])
									  + case when sum(A.[Total Wages]) >0 then  sum(A.[Workers Comp])- ((sum(A.[Non-Instructional Wages]) / (sum(A.[Instructional Wages])+sum(A.[Non-Instructional Wages])+sum(A.[Academic Overtime])))* sum(A.[Workers Comp]))
											 else 0 
										end 
									  + case when sum(A.[Total Wages]) >0 then   sum(A.[Academic Payroll Tax])-((sum(A.[Non-Instructional Wages]) / (sum(A.[Instructional Wages])+sum(A.[Non-Instructional Wages])+sum(A.[Academic Overtime])))*sum(A.[Academic Payroll Tax]))
	 										 else 0 
										end 
									  + case when sum(A.[Total Wages]) >0 then   sum(A.[Academic Benefits])-((sum(A.[Non-Instructional Wages]) / (sum(A.[Instructional Wages])+sum(A.[Non-Instructional Wages])+sum(A.[Academic Overtime])))* sum(A.[Academic Benefits]))
	 										 else 0 
										end 	
									  +sum(A.[Non-Instructional Wages])+sum(A.[Library Books])
											  + case when sum(A.[Total Wages]) >0 then   (sum(A.[Non-Instructional Wages]) / (sum(A.[Instructional Wages])+sum(A.[Non-Instructional Wages])+sum(A.[Academic Overtime])))* sum(A.[Workers Comp])
													 else 0 
												end 
											  +	case when sum(A.[Total Wages]) >0 then   (sum(A.[Non-Instructional Wages]) / (sum(A.[Instructional Wages])+sum(A.[Non-Instructional Wages])+sum(A.[Academic Overtime])))*sum(A.[Academic Payroll Tax])
	 												 else 0 
												end 
											  + case when sum(A.[Total Wages]) >0 then   (sum(A.[Non-Instructional Wages]) / (sum(A.[Instructional Wages])+sum(A.[Non-Instructional Wages])+sum(A.[Academic Overtime])))* sum(A.[Academic Benefits])
	 												 else 0 
												end 
									  +  sum(A.[Administrative salaries]+A.[Contract labor]+A.[Admin Overtime]
										   +A.[Administrative payroll taxes]+A.[Administrative benefits]
										   +A.[Recruiting and relocation]+A.[Corporate allocation]
										   +A.[Admin Training and development]+A.[Payment processing]+A.[Copier and printing]
										   +A.[Postage]+A.[Telephone]+A.[Office supplies]+A.[Rent expense]
										   +A.[Dues and administrative]+A.[Accreditation expense]+A.[Taxes and licenses]
										   +A.[Professional, audit, and legal]+A.[Travel and entertainment]
										   +A.[Charitable contributions]+A.[Bad debt])
									  + sum( A.[Total Admissions] + A.[Total Advertising] + A.[Total Student Services])	)
	 , [Academic Support(Initial)] = sum(A.[Non-Instructional Wages])+sum(A.[Library Books])
									  + case when sum(A.[Total Wages]) >0 then   (sum(A.[Non-Instructional Wages]) / (sum(A.[Instructional Wages])+sum(A.[Non-Instructional Wages])+sum(A.[Academic Overtime])))* sum(A.[Workers Comp])
											 else 0 
										end 
									  +	case when sum(A.[Total Wages]) >0 then   (sum(A.[Non-Instructional Wages]) / (sum(A.[Instructional Wages])+sum(A.[Non-Instructional Wages])+sum(A.[Academic Overtime])))*sum(A.[Academic Payroll Tax])
	 										 else 0 
										end 
									  + case when sum(A.[Total Wages]) >0 then   (sum(A.[Non-Instructional Wages]) / (sum(A.[Instructional Wages])+sum(A.[Non-Instructional Wages])+sum(A.[Academic Overtime])))* sum(A.[Academic Benefits])
	 										 else 0 
										end 		  
     , [Academic Support(Initial) Percent] =( sum(A.[Non-Instructional Wages])+sum(A.[Library Books])
									  + case when sum(A.[Total Wages]) >0 then   (sum(A.[Non-Instructional Wages]) / (sum(A.[Instructional Wages])+sum(A.[Non-Instructional Wages])+sum(A.[Academic Overtime])))* sum(A.[Workers Comp])
											 else 0 
										end 
									  +	case when sum(A.[Total Wages]) >0 then   (sum(A.[Non-Instructional Wages]) / (sum(A.[Instructional Wages])+sum(A.[Non-Instructional Wages])+sum(A.[Academic Overtime])))*sum(A.[Academic Payroll Tax])
	 										 else 0 
										end 
									  + case when sum(A.[Total Wages]) >0 then   (sum(A.[Non-Instructional Wages]) / (sum(A.[Instructional Wages])+sum(A.[Non-Instructional Wages])+sum(A.[Academic Overtime])))* sum(A.[Academic Benefits])
	 										 else 0 
										end )
									/ 
										(sum(A.[Instructional Wages]) + sum(A.[Academic Overtime]) 
									  + sum(A.[Training & Development]) + sum(A.[Textbooks])
									  + sum(A.[Instructional Expense]) + sum(A.[IE - Food,Parts,Green])+ sum(A.[Copier & Printing])
									  + case when sum(A.[Total Wages]) >0 then  sum(A.[Workers Comp])- ((sum(A.[Non-Instructional Wages]) / (sum(A.[Instructional Wages])+sum(A.[Non-Instructional Wages])+sum(A.[Academic Overtime])))* sum(A.[Workers Comp]))
											 else 0 
										end 
									  + case when sum(A.[Total Wages]) >0 then   sum(A.[Academic Payroll Tax])-((sum(A.[Non-Instructional Wages]) / (sum(A.[Instructional Wages])+sum(A.[Non-Instructional Wages])+sum(A.[Academic Overtime])))*sum(A.[Academic Payroll Tax]))
	 										 else 0 
										end 
									  + case when sum(A.[Total Wages]) >0 then   sum(A.[Academic Benefits])-((sum(A.[Non-Instructional Wages]) / (sum(A.[Instructional Wages])+sum(A.[Non-Instructional Wages])+sum(A.[Academic Overtime])))* sum(A.[Academic Benefits]))
	 										 else 0 
										end 	
									  +sum(A.[Non-Instructional Wages])+sum(A.[Library Books])
											  + case when sum(A.[Total Wages]) >0 then   (sum(A.[Non-Instructional Wages]) / (sum(A.[Instructional Wages])+sum(A.[Non-Instructional Wages])+sum(A.[Academic Overtime])))* sum(A.[Workers Comp])
													 else 0 
												end 
											  +	case when sum(A.[Total Wages]) >0 then   (sum(A.[Non-Instructional Wages]) / (sum(A.[Instructional Wages])+sum(A.[Non-Instructional Wages])+sum(A.[Academic Overtime])))*sum(A.[Academic Payroll Tax])
	 												 else 0 
												end 
											  + case when sum(A.[Total Wages]) >0 then   (sum(A.[Non-Instructional Wages]) / (sum(A.[Instructional Wages])+sum(A.[Non-Instructional Wages])+sum(A.[Academic Overtime])))* sum(A.[Academic Benefits])
	 												 else 0 
												end 
									  +  sum(A.[Administrative salaries]+A.[Contract labor]+A.[Admin Overtime]
										   +A.[Administrative payroll taxes]+A.[Administrative benefits]
										   +A.[Recruiting and relocation]+A.[Corporate allocation]
										   +A.[Admin Training and development]+A.[Payment processing]+A.[Copier and printing]
										   +A.[Postage]+A.[Telephone]+A.[Office supplies]+A.[Rent expense]
										   +A.[Dues and administrative]+A.[Accreditation expense]+A.[Taxes and licenses]
										   +A.[Professional, audit, and legal]+A.[Travel and entertainment]
										   +A.[Charitable contributions]+A.[Bad debt])
									  + sum( A.[Total Admissions] + A.[Total Advertising] + A.[Total Student Services])	)		
	 , [Institutional Support(Initial)]= sum(A.[Administrative salaries]+A.[Contract labor]+A.[Admin Overtime]
								   +A.[Administrative payroll taxes]+A.[Administrative benefits]
								   +A.[Recruiting and relocation]+A.[Corporate allocation]
								   +A.[Admin Training and development]+A.[Payment processing]+A.[Copier and printing]
								   +A.[Postage]+A.[Telephone]+A.[Office supplies]+A.[Rent expense]
								   +A.[Dues and administrative]+A.[Accreditation expense]+A.[Taxes and licenses]
								   +A.[Professional, audit, and legal]+A.[Travel and entertainment]
								   +A.[Charitable contributions]+A.[Bad debt])
	 , [Institutional Support(Initial) Percent]= sum(A.[Administrative salaries]+A.[Contract labor]+A.[Admin Overtime]
								   +A.[Administrative payroll taxes]+A.[Administrative benefits]
								   +A.[Recruiting and relocation]+A.[Corporate allocation]
								   +A.[Admin Training and development]+A.[Payment processing]+A.[Copier and printing]
								   +A.[Postage]+A.[Telephone]+A.[Office supplies]+A.[Rent expense]
								   +A.[Dues and administrative]+A.[Accreditation expense]+A.[Taxes and licenses]
								   +A.[Professional, audit, and legal]+A.[Travel and entertainment]
								   +A.[Charitable contributions]+A.[Bad debt])
								   / 
										(sum(A.[Instructional Wages]) + sum(A.[Academic Overtime]) 
									  + sum(A.[Training & Development]) + sum(A.[Textbooks])
									  + sum(A.[Instructional Expense]) + sum(A.[IE - Food,Parts,Green])+ sum(A.[Copier & Printing])
									  + case when sum(A.[Total Wages]) >0 then  sum(A.[Workers Comp])- ((sum(A.[Non-Instructional Wages]) / (sum(A.[Instructional Wages])+sum(A.[Non-Instructional Wages])+sum(A.[Academic Overtime])))* sum(A.[Workers Comp]))
											 else 0 
										end 
									  + case when sum(A.[Total Wages]) >0 then   sum(A.[Academic Payroll Tax])-((sum(A.[Non-Instructional Wages]) / (sum(A.[Instructional Wages])+sum(A.[Non-Instructional Wages])+sum(A.[Academic Overtime])))*sum(A.[Academic Payroll Tax]))
	 										 else 0 
										end 
									  + case when sum(A.[Total Wages]) >0 then   sum(A.[Academic Benefits])-((sum(A.[Non-Instructional Wages]) / (sum(A.[Instructional Wages])+sum(A.[Non-Instructional Wages])+sum(A.[Academic Overtime])))* sum(A.[Academic Benefits]))
	 										 else 0 
										end 	
									  +sum(A.[Non-Instructional Wages])+sum(A.[Library Books])
											  + case when sum(A.[Total Wages]) >0 then   (sum(A.[Non-Instructional Wages]) / (sum(A.[Instructional Wages])+sum(A.[Non-Instructional Wages])+sum(A.[Academic Overtime])))* sum(A.[Workers Comp])
													 else 0 
												end 
											  +	case when sum(A.[Total Wages]) >0 then   (sum(A.[Non-Instructional Wages]) / (sum(A.[Instructional Wages])+sum(A.[Non-Instructional Wages])+sum(A.[Academic Overtime])))*sum(A.[Academic Payroll Tax])
	 												 else 0 
												end 
											  + case when sum(A.[Total Wages]) >0 then   (sum(A.[Non-Instructional Wages]) / (sum(A.[Instructional Wages])+sum(A.[Non-Instructional Wages])+sum(A.[Academic Overtime])))* sum(A.[Academic Benefits])
	 												 else 0 
												end 
									  +  sum(A.[Administrative salaries]+A.[Contract labor]+A.[Admin Overtime]
										   +A.[Administrative payroll taxes]+A.[Administrative benefits]
										   +A.[Recruiting and relocation]+A.[Corporate allocation]
										   +A.[Admin Training and development]+A.[Payment processing]+A.[Copier and printing]
										   +A.[Postage]+A.[Telephone]+A.[Office supplies]+A.[Rent expense]
										   +A.[Dues and administrative]+A.[Accreditation expense]+A.[Taxes and licenses]
										   +A.[Professional, audit, and legal]+A.[Travel and entertainment]
										   +A.[Charitable contributions]+A.[Bad debt])
									  + sum( A.[Total Admissions] + A.[Total Advertising] + A.[Total Student Services])	)
	 , [Student Services(Initial)] = sum( A.[Total Admissions] + A.[Total Advertising] + A.[Total Student Services])
	 , [Student Services(Initial) Percent] = sum( A.[Total Admissions] + A.[Total Advertising] + A.[Total Student Services])
											/ 
										(sum(A.[Instructional Wages]) + sum(A.[Academic Overtime]) 
									  + sum(A.[Training & Development]) + sum(A.[Textbooks])
									  + sum(A.[Instructional Expense]) + sum(A.[IE - Food,Parts,Green])+ sum(A.[Copier & Printing])
									  + case when sum(A.[Total Wages]) >0 then  sum(A.[Workers Comp])- ((sum(A.[Non-Instructional Wages]) / (sum(A.[Instructional Wages])+sum(A.[Non-Instructional Wages])+sum(A.[Academic Overtime])))* sum(A.[Workers Comp]))
											 else 0 
										end 
									  + case when sum(A.[Total Wages]) >0 then   sum(A.[Academic Payroll Tax])-((sum(A.[Non-Instructional Wages]) / (sum(A.[Instructional Wages])+sum(A.[Non-Instructional Wages])+sum(A.[Academic Overtime])))*sum(A.[Academic Payroll Tax]))
	 										 else 0 
										end 
									  + case when sum(A.[Total Wages]) >0 then   sum(A.[Academic Benefits])-((sum(A.[Non-Instructional Wages]) / (sum(A.[Instructional Wages])+sum(A.[Non-Instructional Wages])+sum(A.[Academic Overtime])))* sum(A.[Academic Benefits]))
	 										 else 0 
										end 	
									  +sum(A.[Non-Instructional Wages])+sum(A.[Library Books])
											  + case when sum(A.[Total Wages]) >0 then   (sum(A.[Non-Instructional Wages]) / (sum(A.[Instructional Wages])+sum(A.[Non-Instructional Wages])+sum(A.[Academic Overtime])))* sum(A.[Workers Comp])
													 else 0 
												end 
											  +	case when sum(A.[Total Wages]) >0 then   (sum(A.[Non-Instructional Wages]) / (sum(A.[Instructional Wages])+sum(A.[Non-Instructional Wages])+sum(A.[Academic Overtime])))*sum(A.[Academic Payroll Tax])
	 												 else 0 
												end 
											  + case when sum(A.[Total Wages]) >0 then   (sum(A.[Non-Instructional Wages]) / (sum(A.[Instructional Wages])+sum(A.[Non-Instructional Wages])+sum(A.[Academic Overtime])))* sum(A.[Academic Benefits])
	 												 else 0 
												end 
									  +  sum(A.[Administrative salaries]+A.[Contract labor]+A.[Admin Overtime]
										   +A.[Administrative payroll taxes]+A.[Administrative benefits]
										   +A.[Recruiting and relocation]+A.[Corporate allocation]
										   +A.[Admin Training and development]+A.[Payment processing]+A.[Copier and printing]
										   +A.[Postage]+A.[Telephone]+A.[Office supplies]+A.[Rent expense]
										   +A.[Dues and administrative]+A.[Accreditation expense]+A.[Taxes and licenses]
										   +A.[Professional, audit, and legal]+A.[Travel and entertainment]
										   +A.[Charitable contributions]+A.[Bad debt])
									  + sum( A.[Total Admissions] + A.[Total Advertising] + A.[Total Student Services])	)
into ##PandL
from Accounting.dbo.Pull_PandL (@FYBegin,@FYEnd,'000','999','000','999') as A
group by A.[Year]	
     , A.Segment2
	 , A.Campus
	 , A.Segment4
having (sum(A.[Instructional Wages]) + sum(A.[Academic Overtime]) 
									  + sum(A.[Training & Development]) + sum(A.[Textbooks])
									  + sum(A.[Instructional Expense]) + sum(A.[IE - Food,Parts,Green])+ sum(A.[Copier & Printing])
									  + case when sum(A.[Total Wages]) >0 then  sum(A.[Workers Comp])- ((sum(A.[Non-Instructional Wages]) / (sum(A.[Instructional Wages])+sum(A.[Non-Instructional Wages])+sum(A.[Academic Overtime])))* sum(A.[Workers Comp]))
											 else 0 
										end 
									  + case when sum(A.[Total Wages]) >0 then   sum(A.[Academic Payroll Tax])-((sum(A.[Non-Instructional Wages]) / (sum(A.[Instructional Wages])+sum(A.[Non-Instructional Wages])+sum(A.[Academic Overtime])))*sum(A.[Academic Payroll Tax]))
	 										 else 0 
										end 
									  + case when sum(A.[Total Wages]) >0 then   sum(A.[Academic Benefits])-((sum(A.[Non-Instructional Wages]) / (sum(A.[Instructional Wages])+sum(A.[Non-Instructional Wages])+sum(A.[Academic Overtime])))* sum(A.[Academic Benefits]))
	 										 else 0 
										end 	
									  +sum(A.[Non-Instructional Wages])+sum(A.[Library Books])
											  + case when sum(A.[Total Wages]) >0 then   (sum(A.[Non-Instructional Wages]) / (sum(A.[Instructional Wages])+sum(A.[Non-Instructional Wages])+sum(A.[Academic Overtime])))* sum(A.[Workers Comp])
													 else 0 
												end 
											  +	case when sum(A.[Total Wages]) >0 then   (sum(A.[Non-Instructional Wages]) / (sum(A.[Instructional Wages])+sum(A.[Non-Instructional Wages])+sum(A.[Academic Overtime])))*sum(A.[Academic Payroll Tax])
	 												 else 0 
												end 
											  + case when sum(A.[Total Wages]) >0 then   (sum(A.[Non-Instructional Wages]) / (sum(A.[Instructional Wages])+sum(A.[Non-Instructional Wages])+sum(A.[Academic Overtime])))* sum(A.[Academic Benefits])
	 												 else 0 
												end 
									  +  sum(A.[Administrative salaries]+A.[Contract labor]+A.[Admin Overtime]
										   +A.[Administrative payroll taxes]+A.[Administrative benefits]
										   +A.[Recruiting and relocation]+A.[Corporate allocation]
										   +A.[Admin Training and development]+A.[Payment processing]+A.[Copier and printing]
										   +A.[Postage]+A.[Telephone]+A.[Office supplies]+A.[Rent expense]
										   +A.[Dues and administrative]+A.[Accreditation expense]+A.[Taxes and licenses]
										   +A.[Professional, audit, and legal]+A.[Travel and entertainment]
										   +A.[Charitable contributions]+A.[Bad debt])
									  + sum( A.[Total Admissions] + A.[Total Advertising] + A.[Total Student Services]))>0 

SELECT 
	-- A.Institution,
	 --, A.City ,
/*General Information*/
	  'UNITID='+convert(varchar(6),[Unit ID])as UNITID
	 , SURVSECT = 'SURVSECT=F3A'
	 , PART= 'PART=9'
	 , FY_BEGIN_MONTH='FY_BEGIN_MONTH=01'
	 , FY_BEGIN_YEAR='FY_BEGIN_YEAR=2014'
	 , FY_END_MONTH='FY_END_MONTH=12'
	 , FY_END_YEAR='FY_END_YEAR=2014'
	 , GPFS= 'GPFS=1' --1 = unqualified, 2 = Qualified, 3 = Don't Know
	 , PELL_GRANTS = 'PELL_GRANTS=1' --1=Pass through (agency) 2=Federal grants 3=Does not award Pell grants		
	 , BIS_STRUCT = 'BIS_STRUCT=5' --1=Sole Proprietorship, 2=Partnership (General, Limited, Limited Liability), 3=C Corporation, 4=S Corporation, 5=Limited Liability Company (LLC)
/*Part (F) - Income Tax Expenses
Tax Expenses, Part F
Line 	Income Tax Expenses
1 - 	Federal income tax expenses
2 - 	State and local income tax expenses
3 - 	Please designate who paid the reported tax expenses for your institution:
For line 3, the value should be 1-3
1 - 	Taxes were aggregate amounts paid by the multi-institution or multi-campus organization indicated in IC Header for all associated institutions
2 - 	Taxes were aggregate amounts paid by a multi-institution or multi-campus organization NOT indicated in IC Header for all associated institutions
3 - 	Taxes were amounts paid by the reporting institution*/
	--line 1 
	 , 'UNITID='+convert(varchar(6),[Unit ID])as UNITID
	 , SURVSECT = 'SURVSECT=F3A'
	 , PART= 'PART=F'
	 , LINE = 'LINE=1'
	 , F3AF1F101 = 'AMOUNT='+convert(varchar(max),convert(int,round(PL.[Federal Taxes],0)))
	--line 2 
	 , 'UNITID='+convert(varchar(6),[Unit ID])as UNITID
	 , SURVSECT = 'SURVSECT=F3A'
	 , PART= 'PART=F'
	 , LINE = 'LINE=2'
	 , F3AF2F101 = 'AMOUNT='+convert(varchar(max),convert(int,round(PL.[State Taxes],0)))
	--line 3 
	 , 'UNITID='+convert(varchar(6),[Unit ID])as UNITID
	 , SURVSECT = 'SURVSECT=F3A'
	 , PART= 'PART=F'
	 , LINE = 'LINE=3'
	 , F3AF3F101 = 'AMOUNT=3'


/*Balance Sheet Information, Part A
Line 	Assets, Liabilities, and Equity
1 - 	Total assets
1a - 	Long-term investments
1b - 	Property, plant, and equipment, net of accumulated depreciation
1c - 	Intangible assets, net of accumulated amortization
2 - 	Total liabilities
2a - 	Debt related to property, plant, and equipment
3 - 	Total equity (Do not include in import file. Will be generated.)
4 - 	Total liabilities and equity (Do not include in import file. Will be generated.)
5 - 	Land and land improvements
6 - 	Buildings
7 - 	Equipment, including art and library collections
8 - 	Construction in Progress
9 - 	Other
10 - 	Total Plant, Property, and Equipment (Do not include in import file. Will be generated.)
11 - 	Accumulated depreciation
12 - 	Property, Plant, and Equipment, net of accumulated depreciation (from A1b)
*/

	--line 1 
	 , 'UNITID='+convert(varchar(6),[Unit ID])as UNITID
	 , SURVSECT = 'SURVSECT=F3A'
	 , PART= 'PART=A'
	 , LINE = 'LINE=1'
	 , F3AA1F101 = 'AMOUNT=0'
	--line 1a 
	 , 'UNITID='+convert(varchar(6),[Unit ID])as UNITID
	 , SURVSECT = 'SURVSECT=F3A'
	 , PART= 'PART=A'
	 , LINE = 'LINE=1'
	 , LINE150= 'LINE150=a'
	 , F3AA1aF101 = 'AMOUNT=0'
	--line 1b
	 , 'UNITID='+convert(varchar(6),[Unit ID])as UNITID
	 , SURVSECT = 'SURVSECT=F3A'
	 , PART= 'PART=A'
	 , LINE = 'LINE=1'
	 , LINE150= 'LINE150=b'
	 , F3AA1bF101 = 'AMOUNT=0'
	--line 1c 
	 , 'UNITID='+convert(varchar(6),[Unit ID])as UNITID
	 , SURVSECT = 'SURVSECT=F3A'
	 , PART= 'PART=A'
	 , LINE = 'LINE=1'
	 , LINE150= 'LINE150=c'
	 , F3AA1cF101 = 'AMOUNT=0'
	--line 2 
	 , 'UNITID='+convert(varchar(6),[Unit ID])as UNITID
	 , SURVSECT = 'SURVSECT=F3A'
	 , PART= 'PART=A'
	 , LINE = 'LINE=2'
	 , F3AA2F101 = 'AMOUNT=0'
	--line 2a
	 , 'UNITID='+convert(varchar(6),[Unit ID])as UNITID
	 , SURVSECT = 'SURVSECT=F3A'
	 , PART= 'PART=A'
	 , LINE = 'LINE=2'
	 , LINE150= 'LINE150=a'
	 , F3AA2aF101 = 'AMOUNT=0'
	--line 3
	 , 'UNITID='+convert(varchar(6),[Unit ID])as UNITID
	 , SURVSECT = 'SURVSECT=F3A'
	 , PART= 'PART=A'
	 , LINE = 'LINE=3'
	 , F3AA3F101 = 'AMOUNT=0'
	--line 4
	, 'UNITID='+convert(varchar(6),[Unit ID])as UNITID
	 , SURVSECT = 'SURVSECT=F3A'
	 , PART= 'PART=A'
	 , LINE = 'LINE=4'
	 , F3AA4F101 = 'AMOUNT=0'
	--line 5
	 , 'UNITID='+convert(varchar(6),[Unit ID])as UNITID
	 , SURVSECT = 'SURVSECT=F3A'
	 , PART= 'PART=A'
	 , LINE = 'LINE=5'
	 , F3AA5F101 = 'AMOUNT=0'
	--line 6
	 , 'UNITID='+convert(varchar(6),[Unit ID])as UNITID
	 , SURVSECT = 'SURVSECT=F3A'
	 , PART= 'PART=A'
	 , LINE = 'LINE=6'
	 , F3AA6F101 = 'AMOUNT=0'
	--line 7
     , 'UNITID='+convert(varchar(6),[Unit ID])as UNITID
	 , SURVSECT = 'SURVSECT=F3A'
	 , PART= 'PART=A'
	 , LINE = 'LINE=7'
	 , F3AA7F101 = 'AMOUNT=0'
	--line 8
	 , 'UNITID='+convert(varchar(6),[Unit ID])as UNITID
	 , SURVSECT = 'SURVSECT=F3A'
	 , PART= 'PART=A'
	 , LINE = 'LINE=8'
	 , F3AA8F101 = 'AMOUNT=0'
	--line 9
	 , 'UNITID='+convert(varchar(6),[Unit ID])as UNITID
	 , SURVSECT = 'SURVSECT=F3A'
	 , PART= 'PART=A'
	 , LINE = 'LINE=9'
	 , F3AA9F101 = 'AMOUNT=0'
	--line 10
	 , 'UNITID='+convert(varchar(6),[Unit ID])as UNITID
	 , SURVSECT = 'SURVSECT=F3A'
	 , PART= 'PART=A'
	 , LINE = 'LINE=10'
	 , F3AA10F101 = 'AMOUNT=0'
	--line 11
	 , 'UNITID='+convert(varchar(6),[Unit ID])as UNITID
	 , SURVSECT = 'SURVSECT=F3A'
	 , PART= 'PART=A'
	 , LINE = 'LINE=11'
	 , F3AA11F101 = 'AMOUNT=0'
	--line 12
	 , 'UNITID='+convert(varchar(6),[Unit ID])as UNITID
	 , SURVSECT = 'SURVSECT=F3A'
	 , PART= 'PART=A'
	 , LINE = 'LINE=12'
	 , F3AA12F101 = 'AMOUNT=0'

/*
Summary of Changes in Equity, Part B
Line 	Revenues, Expenses, Gains and Losses
1 - 	Total revenues and investment return
2 - 	Total expenses
3 - 	Sum of specific changes in equity (Do not include in import file. Will be generated as follows; Line 4 minus (line 1 minus line 2))
4 - 	Net income
5 - 	Other changes in equity
6 - 	Equity, beginning of year
7 - 	Adjustments to beginning net equity (Do not include in import file. Will be generated as follows; Line 8 minus sum of (lines 4-6))
8 - 	Equity, end of year will be transferred from Part A, line 3. Do not include in import file.
*/
	--line 1 
	 , 'UNITID='+convert(varchar(6),[Unit ID])as UNITID
	 , SURVSECT = 'SURVSECT=F3A'
	 , PART= 'PART=B'
	 , LINE = 'LINE=1'
	 , F3AB1F101 = 'AMOUNT='
	--line 2 
	 , 'UNITID='+convert(varchar(6),[Unit ID])as UNITID
	 , SURVSECT = 'SURVSECT=F3A'
	 , PART= 'PART=B'
	 , LINE = 'LINE=2'
	 , F3AB2F101 = 'AMOUNT='
	--line 3
	 , 'UNITID='+convert(varchar(6),[Unit ID])as UNITID
	 , SURVSECT = 'SURVSECT=F3A'
	 , PART= 'PART=B'
	 , LINE = 'LINE=3'
	 , F3AB3F101 = 'AMOUNT='
	--line 4
	 , 'UNITID='+convert(varchar(6),[Unit ID])as UNITID
	 , SURVSECT = 'SURVSECT=F3A'
	 , PART= 'PART=B'
	 , LINE = 'LINE=4'
	 , F3AB4F101 = 'AMOUNT='
	--line 5
	 , 'UNITID='+convert(varchar(6),[Unit ID])as UNITID
	 , SURVSECT = 'SURVSECT=F3A'
	 , PART= 'PART=B'
	 , LINE = 'LINE=5'
	 , F3AB5F101 = 'AMOUNT='
	--line 6
	 , 'UNITID='+convert(varchar(6),[Unit ID])as UNITID
	 , SURVSECT = 'SURVSECT=F3A'
	 , PART= 'PART=B'
	 , LINE = 'LINE=6'
	 , F3AB6F101 = 'AMOUNT='
	--line 7
	 , 'UNITID='+convert(varchar(6),[Unit ID])as UNITID
	 , SURVSECT = 'SURVSECT=F3A'
	 , PART= 'PART=B'
	 , LINE = 'LINE=7'
	 , F3AB7F101 = 'AMOUNT='
	--line 8
	 , 'UNITID='+convert(varchar(6),[Unit ID])as UNITID
	 , SURVSECT = 'SURVSECT=F3A'
	 , PART= 'PART=B'
	 , LINE = 'LINE=8'
	 , F3AB8F101 = 'AMOUNT='

/*
Scholarships and Fellowships, Part C
Line 	Scholarships and Fellowships
1 - 	Pell grants (federal)
2 - 	Other federal grants
3a - 	State grants
3b - 	Local grants (government)
4 - 	Institutional grants
5 - 	Total scholarships and fellowships (Do not include in import file. Will be generated as follows; sum of (lines 1-4))
6 - 	Allowances (scholarships) applied to tuition and fees
7 - 	Allowances (scholarships) applied to auxiliary enterprise revenues
*/

--line 1 Pell grants (federal)
     , 'UNITID='+convert(varchar(6),[Unit ID])as UNITID
	 , SURVSECT = 'SURVSECT=F3A'
	 , PART= 'PART=C'
	 , LINE = 'LINE=1'
	 , F3AC1F101 = 'AMOUNT=' + convert(varchar(max),-1*convert(int,round(case when GPCompanyID = '08' then (select sum(case stn.type 
																	when 'I' then Stn.amount
																	when 'D' then Stn.amount
																	else -1* stn.amount 
																   end)
														from campusvue_necb.dbo.satrans (nolock) as STN
															JOIN campusvue_necb.dbo.fastudentaid (nolock) as FSA
																on FSA.fastudentaidid = stn.fastudentaidid
														where stn.sycampusid = A.F14
														  and FSA.Fafundsourceid = 1
														  and stn.date between @FYBegin and @FYEnd)
						 else  (select sum(case stn.type 
											when 'I' then Stn.amount
											when 'D' then Stn.amount
											else -1* stn.amount 
											end)
								from campusvue.dbo.satrans (nolock) as STN
									JOIN campusvue.dbo.fastudentaid (nolock) as FSA
										on FSA.fastudentaidid = stn.fastudentaidid
								where stn.sycampusid = A.F14
									and FSA.Fafundsourceid = 1
									and stn.date between @FYBegin and @FYEnd) 
					end ,0)))
	--line 2 Other federal grants
	 , 'UNITID='+convert(varchar(6),[Unit ID])as UNITID
	 , SURVSECT = 'SURVSECT=F3A'
	 , PART= 'PART=C'
	 , LINE = 'LINE=2'
	 , F3AC2F101 = 'AMOUNT=' + isnull(convert(varchar(max),-1*convert(int,round( case when GPCompanyID = '08' then (select sum(case stn.type 
																	when 'I' then Stn.amount
																	when 'D' then Stn.amount
																	else -1* stn.amount 
																   end)
														from campusvue_necb.dbo.satrans (nolock) as STN
															JOIN campusvue_necb.dbo.fastudentaid (nolock) as FSA
																on FSA.fastudentaidid = stn.fastudentaidid
														where stn.sycampusid = A.F14
														  and FSA.Fafundsourceid = 2
														  and stn.date between @FYBegin and @FYEnd)
						 else  (select sum(case stn.type 
											when 'I' then Stn.amount
											when 'D' then Stn.amount
											else -1* stn.amount 
											end)
								from campusvue.dbo.satrans (nolock) as STN
									JOIN campusvue.dbo.fastudentaid (nolock) as FSA
										on FSA.fastudentaidid = stn.fastudentaidid
								where stn.sycampusid = A.F14
									and FSA.Fafundsourceid = 2
									and stn.date between @FYBegin and @FYEnd) 
					end ,0))),0)
	--line 3a State grants
	 , 'UNITID='+convert(varchar(6),[Unit ID])as UNITID
	 , SURVSECT = 'SURVSECT=F3A'
	 , PART= 'PART=C'
	 , LINE = 'LINE=3'
	 , LINE150= 'LINE150=a'
	 , F3AC3aF101 = 'AMOUNT=' + isnull(convert(varchar(max), -1* convert(int,round(case when GPCompanyID = '08' then (select sum(case stn.type 
																	when 'I' then Stn.amount
																	when 'D' then Stn.amount
																	else -1* stn.amount 
																   end)
														from campusvue_necb.dbo.satrans (nolock) as STN
															JOIN campusvue_necb.dbo.fastudentaid (nolock) as FSA
																on FSA.fastudentaidid = stn.fastudentaidid
														where stn.sycampusid = A.F14
														  and FSA.Fafundsourceid = 1
														  and stn.date between @FYBegin and @FYEnd)
						 else  (select sum(case stn.type 
											when 'I' then Stn.amount
											when 'D' then Stn.amount
											else -1* stn.amount 
											end)
								from campusvue.dbo.satrans (nolock) as STN
									JOIN campusvue.dbo.fastudentaid (nolock) as FSA
										on FSA.fastudentaidid = stn.fastudentaidid
								where stn.sycampusid = A.F14
									and FSA.Fafundsourceid in ( 868, --Alaska State Grant
																6443, --California State Grant
																6468, --California State Grant A
																6469, --California State Grant B
																6470, --California State Grant C
																5666, --Georgia Leveraging Educ Asst Part Grt
																5493, --Gov. F. O'Bannen State Award
																3109, --Guaranteed Access Grant
																4243, --Illinois State Grant
																5367, --Indiana State Grant
																1201, --Iowa Department of the Blind
																5203, --Iowa National Guard
																5669, --Iowa War Orphans Educational Aid
																7496, --Iowa Workforce
																3108, --Maryland State Grant
																6533, --Nevada State Grant
																972, --Ohio College Opportunity Grant
																841, --Ohio State Grant
																940, --Ohio State Grant Part Time
																1126, --Ohio War Orphans
																900, --PA  State Grant
																4183, --PA State Grant - Fall
																4278, --PA State Grant - Fall
																4184, --PA State Grant - Spring
																4280, --PA State Grant - Spring
																4122, --PA State Grant - Summer
																4281, --PA State Grant - Summer
																4182, --PA State Grant - Winter
																4279, --PA State Grant - Winter
																4369, --State Wag Grant
																2393, --Texas Career Opportunity Grant
																842, --Texas Employment  Commission
																2119, --Texas Rehab Commision
																2415, --Texas Tomorrow Fund
																1992, --Texas Work Force Commission
																837, --Chaffee Grant
																1091, --Gear Up
																3396, --GEAR-UP - Maryland Scholarship
																34,   --State Grant
																5387, --Tennessee State Grant
																37, --Vocational Rehab (State)
																5662  --AZ College Access Aid Program (ACAAP)
																)
																and stn.date between @FYBegin and @FYEnd
																)	
					end ,0))),0)
	--line 3b Local grants (government)
	 , 'UNITID='+convert(varchar(6),[Unit ID])as UNITID
	 , SURVSECT = 'SURVSECT=F3A'
	 , PART= 'PART=C'
	 , LINE = 'LINE=3'
	 , LINE150= 'LINE150=b'
	 , F3AC3bF101 =  'AMOUNT=' + isnull(convert(varchar(max),-1*convert(int,round(case when GPCompanyID = '08' then (select sum(case stn.type 
																	when 'I' then Stn.amount
																	when 'D' then Stn.amount
																	else -1* stn.amount 
																   end)
														from campusvue_necb.dbo.satrans (nolock) as STN
															JOIN campusvue_necb.dbo.fastudentaid (nolock) as FSA
																on FSA.fastudentaidid = stn.fastudentaidid
														where stn.sycampusid = A.F14
														  and FSA.Fafundsourceid = 1
														  and stn.date between @FYBegin and @FYEnd)
						 else  (select sum(case stn.type 
											when 'I' then Stn.amount
											when 'D' then Stn.amount
											else -1* stn.amount 
											end)
								from campusvue.dbo.satrans (nolock) as STN
									JOIN campusvue.dbo.fastudentaid (nolock) as FSA
										on FSA.fastudentaidid = stn.fastudentaidid
								where stn.sycampusid = A.F14
									and FSA.Fafundsourceid in ( 8046, --Arlington Texas Employee
																4357, --Beaver County Funding
																7535, --Dallas Employee Scholarship
																5323, --Dayton Employee Scholarship
																7639, --Mayors Work Force
																4317, --Philadelphia Workforce Development
																4485, --Pittsburgh Promise Scholarship
																5639, --River Valley Resources Agency
																4022 --Southampton Employee Scholarship
															   )
															   and stn.date between @FYBegin and @FYEnd
																)
				      end ,0))),0)

	--line 4 Institutional grants
	 , 'UNITID='+convert(varchar(6),[Unit ID])as UNITID
	 , SURVSECT = 'SURVSECT=F3A'
	 , PART= 'PART=C'
	 , LINE = 'LINE=4'
	 , F3AC4F101 =  'AMOUNT='
	--line 6 Allowances (scholarships) applied to tuition and fees
	 , 'UNITID='+convert(varchar(6),[Unit ID])as UNITID
	 , SURVSECT = 'SURVSECT=F3A'
	 , PART= 'PART=C'
	 , LINE = 'LINE=6'
	 , F3AC6F101 =  'AMOUNT=' + convert(varchar(max),convert(int,round(PL.[Scholarship] + PL.[Tuition Adjustment Revenue],0) ))
	--line 7 Allowances (scholarships) applied to auxiliary enterprise revenues
	 , 'UNITID='+convert(varchar(6),[Unit ID])as UNITID
	 , SURVSECT = 'SURVSECT=F3A'
	 , PART= 'PART=C'
	 , LINE = 'LINE=7'
	 , F3AC7F101 =  'AMOUNT=0' 

/*
Revenues and Investment Return, Part D
Line 	Source of Funds
1 - 	Tuition and fees (net of amount reported in Part C, line 6)
2a - 	Federal appropriations
2b - 	Federal grants and contracts
3a - 	State appropriations
3b - 	State grants and contracts
3c - 	Local government appropriations
3d - 	Local government grants and contracts
4 - 	Private gifts grants and contracts
5 - 	Investment income and investment gains (losses) included in net income
6 - 	Sales and services of educational activities
7 - 	Sales and services of auxiliary enterprises (net of amount reported in Part C, line 7)
8 - 	Other revenue (Do not include in import file. Will be generated as follows; Line 9 minus sum of (lines 1-7))
9 - 	Total revenues and investment return will be transferred from Part B, line 1. Report an amount here only if data for Parts A and B are reported by a parent institution.
10 - 	12-month Student FTE (Do not include in import file. Will be generated)
11 - 	Total revenues and investment return per student FTE (Do not include in import file. Will be generated as the ratio of lines D9 over D10)
12 - 	Hospital revenue
*/ 
 
 	--line 1 
	 , 'UNITID='+convert(varchar(6),[Unit ID])as UNITID
	 , SURVSECT = 'SURVSECT=F3A'
	 , PART= 'PART=D'
	 , LINE = 'LINE=1'
	 , F3AD1F101 = 'AMOUNT=' + convert(varchar(max),convert(int,round(PL.[Tuition Revenue] + PL.[Onground / Online Adjustment] + PL.[Other Revenue],0)))
	--line 1c 
	 , 'UNITID='+convert(varchar(6),[Unit ID])as UNITID
	 , SURVSECT = 'SURVSECT=F3A'
	 , PART= 'PART=D'
	 , LINE = 'LINE=1'
	 , LINE150 = 'LINE150=c'
	 , F3AD1cF101 =  'AMOUNT=0' 
	--line 2a
	 , 'UNITID='+convert(varchar(6),[Unit ID])as UNITID
	 , SURVSECT = 'SURVSECT=F3A'
	 , PART= 'PART=D'
	 , LINE = 'LINE=2'
	 , LINE150 = 'LINE150=a'
	 , F3AD2aF101 =  'AMOUNT=0'
	--line 2b
	 , 'UNITID='+convert(varchar(6),[Unit ID])as UNITID
	 , SURVSECT = 'SURVSECT=F3A'
	 , PART= 'PART=D'
	 , LINE = 'LINE=2'
	 , LINE150 = 'LINE150=b'
	 , F3AD2bF101 =  'AMOUNT=0' 
	--line 3a
	 , 'UNITID='+convert(varchar(6),[Unit ID])as UNITID
	 , SURVSECT = 'SURVSECT=F3A'
	 , PART= 'PART=D'
	 , LINE = 'LINE=3'
	 , LINE150 = 'LINE150=a'
	 , F3AD3aF101 =  'AMOUNT=0' 
	--line 3b
	 , 'UNITID='+convert(varchar(6),[Unit ID])as UNITID
	 , SURVSECT = 'SURVSECT=F3A'
	 , PART= 'PART=D'
	 , LINE = 'LINE=3'
	 , LINE150 = 'LINE150=b'
	 , F3AD3bF101 =  'AMOUNT=0' 
	--line 3c
	 , 'UNITID='+convert(varchar(6),[Unit ID])as UNITID
	 , SURVSECT = 'SURVSECT=F3A'
	 , PART= 'PART=D'
	 , LINE = 'LINE=1'
	 , LINE150 = 'LINE150=c'
	 , F3AD3cF101 =  'AMOUNT=0' 
	--line 3d
	 , 'UNITID='+convert(varchar(6),[Unit ID])as UNITID
	 , SURVSECT = 'SURVSECT=F3A'
	 , PART= 'PART=D'
	 , LINE = 'LINE=3'
	 , LINE150 = 'LINE150=d'
	 , F3AD3dF101 =  'AMOUNT=0' 
	--line 4
	 , 'UNITID='+convert(varchar(6),[Unit ID])as UNITID
	 , SURVSECT = 'SURVSECT=F3A'
	 , PART= 'PART=D'
	 , LINE = 'LINE=4'
	 , F3AD4F101 = 'AMOUNT=0' 
	--line 5
	 , 'UNITID='+convert(varchar(6),[Unit ID])as UNITID
	 , SURVSECT = 'SURVSECT=F3A'
	 , PART= 'PART=D'
	 , LINE = 'LINE=5'
	 , F3AD5F101 = 'AMOUNT=0' 
	--line 6
	 , 'UNITID='+convert(varchar(6),[Unit ID])as UNITID
	 , SURVSECT = 'SURVSECT=F3A'
	 , PART= 'PART=D'
	 , LINE = 'LINE=6'
	 , F3AD6F101 = 'AMOUNT=0' 
	--line 7
	 , 'UNITID='+convert(varchar(6),[Unit ID])as UNITID
	 , SURVSECT = 'SURVSECT=F3A'
	 , PART= 'PART=D'
	 , LINE = 'LINE=7'
	 , F3AD7F101 = 'AMOUNT=0' 
	--line 8
	 , 'UNITID='+convert(varchar(6),[Unit ID])as UNITID
	 , SURVSECT = 'SURVSECT=F3A'
	 , PART= 'PART=D'
	 , LINE = 'LINE=8'
	 , F3AD8F101 = 'AMOUNT=0' 
	--line 9
	 , 'UNITID='+convert(varchar(6),[Unit ID])as UNITID
	 , SURVSECT = 'SURVSECT=F3A'
	 , PART= 'PART=D'
	 , LINE = 'LINE=9'
	 , F3AD9F101 = 'AMOUNT=' + convert(varchar(max),convert(int,round(PL.[Tuition Revenue] + PL.[Onground / Online Adjustment] + PL.[Other Revenue],0)))
	--line 12
	 , 'UNITID='+convert(varchar(6),[Unit ID])as UNITID
	 , SURVSECT = 'SURVSECT=F3A'
	 , PART= 'PART=D'
	 , LINE = 'LINE=12'
	 , F3AD12F101 = 'AMOUNT=0' 

/*
 Expenses by Functional and Natural Classification, Part E
Line,
column(i,j) 	Functional Classification
	Instruction
(1,1) - 	Total amount
(1,2) - 	Salaries and wages
(1,3) - 	Benefits
(1,4) - 	Operation and maintenance of plant
(1,5) - 	Depreciation
(1,6) - 	Interest
(1,7) - 	All other (Do not include in import file. Will be generated as follows: Column 1 minus sum of lines 2 through 6.)
	Research
(2a,1) - 	Total amount
(2a,2) - 	Salaries and wages
(2a,3) - 	Benefits
(2a,4) - 	Operation and maintenance of plant
(2a,5) - 	Depreciation
(2a,6) - 	Interest
(2a,7) - 	All other (Do not include in import file. Will be generated as follows: Column 1 minus sum of lines 2 through 6.)
	Public service
(2b,1) - 	Total amount
(2b,2) - 	Salaries and wages
(2b,3) - 	Benefits
(2b,4) - 	Operation and maintenance of plant
(2b,5) - 	Depreciation
(2b,6) - 	Interest
(2b,7) - 	All other (Do not include in import file. Will be generated as follows: Column 1 minus sum of lines 2 through 6.)
	Academic support
(3a,1) - 	Total amount
(3a,2) - 	Salaries and wages
(3a,3) - 	Benefits
(3a,4) - 	Operation and maintenance of plant
(3a,5) - 	Depreciation
(3a,6) - 	Interest
(3a,7) - 	All other (Do not include in import file. Will be generated as follows: Column 1 minus sum of lines 2 through 6.)
	Student services
(3b,1) - 	Total amount
(3b,2) - 	Salaries and wages
(3b,3) - 	Benefits
(3b,4) - 	Operation and maintenance of plant
(3b,5) - 	Depreciation
(3b,6) - 	Interest
(3b,7) - 	All other (Do not include in import file. Will be generated as follows: Column 1 minus sum of lines 2 through 6.)
	Institutional support
(3c,1) - 	Total amount
(3c,2) - 	Salaries and wages
(3c,3) - 	Benefits
(3c,4) - 	Operation and maintenance of plant
(3c,5) - 	Depreciation
(3c,6) - 	Interest
(3c,7) - 	All other (Do not include in import file. Will be generated as follows: Column 1 minus sum of lines 2 through 6.)
	Auxiliary enterprises
(4,1) - 	Total amount
(4,2) - 	Salaries and wages
(4,3) - 	Benefits
(4,4) - 	Operation and maintenance of plant
(4,5) - 	Depreciation
(4,6) - 	Interest
(4,7) - 	All other (Do not include in import file. Will be generated as follows: Column 1 minus sum of lines 2 through 6.)
	Net grant aid to students
(5,1) - 	Total amount
(5,7) - 	All other (Do not include in import file. Will be generated as follows: Column 1 minus sum of lines 2 through 6.)
	Hospital services (not to be reported by less-than-4-year institutions)
(10,1) - 	Total amount
(10,2) - 	Salaries and wages
(10,3) - 	Benefits
(10,4) - 	Operation and maintenance of plant
(10,5) - 	Depreciation
(10,6) - 	Interest
(10,7) - 	All other (Do not include in import file. Will be generated as follows: Column 1 minus sum of lines 2 through 6.)
	Operation and maintenance of plant (This row is for distribution of expenses to other functions.)
(11,1) - 	Total amount (Do not include in import file. This amount will be zero. Other amounts on this line should offset each other.)
(11,2) - 	Salaries and wages
(11,3) - 	Benefits
(11,4) - 	Operation and maintenance of plant (should be entered as a negative number to show distribution to other functions and natural classifications)
(11,5) - 	Depreciation
(11,6) - 	Interest
(11,7) - 	All other (Do not include in import file. Will be generated as follows: Column 1 minus sum of lines 2 through 6.)
	Other expenses
(6,1) - 	Total amount (Do not include in import file. Will be generated as follows: Column 1 line 13 minus the sum of (lines 1 through 11))
(6,2) - 	Salaries and wages (Do not include in import file. Will be generated as follows: Column 2 line 13 minus the sum of (lines 1 through 11))
(6,3) - 	Benefits (Do not include in import file. Will be generated as follows: Column 3 line 13 minus the sum of (lines 1 through 11))
(6,4) - 	Operation and maintenance of plant (Do not include in import file. Will be generated as follows: Column 4 line 13 minus the sum of (lines 1 through 11))
(6,5) - 	Depreciation (Do not include in import file. Will be generated as follows: Column 5 line 13 minus the sum of (lines 1 through 11))
(6,6) - 	Interest (Do not include in import file. Will be generated as follows: Column 6 line 13 minus the sum of (lines 1 through 11))
(6,7) - 	All other (Do not include in import file. Will be generated as follows: Column 1 minus sum of lines 2 through 6.)
	Total expenses
(7,1) - 	Total amount transferred from Part B, line 2. Report an amount here only if data for Parts A and B are reported by a parent institution.
(7,2) - 	Salaries and wages
(7,3) - 	Benefits
(7,4) - 	Operation and maintenance of plant (Do not include in import file. This amount will be zero.)
(7,5) - 	Depreciation
(7,6) - 	Interest
(7,7) - 	All other (Do not include in import file. Will be generated as follows: Column 1 minus sum of lines 2 through 6.)
(8,1) - 	12-month Student FTE from E12 (Do not include in import file. Will be generated)
(9,1) - 	Total expenses per student FTE (Do not include in import file. Will be generated as the ratio of lines E13 over E14)
 */
 -- 	, PL.[Instruction(Initial) Percent]
	--, PL.[Academic Support(Initial) Percent]
	--, PL.[Student Services(Initial) Percent]
	--,PL.[Institutional Support(Initial) Percen
	--line 1,1   PL.[Administrative Payroll Taxes] + PL.[Administrative benefits]+PL.[Admissions payroll taxes]+ PL.[Admissions benefits]+ PL.[CS payroll taxes] + PL.[SS payroll taxes] + PL.[CS benefits] + PL.[SS benefits]+PL.[Non-Instructional Academic Payroll Tax] + PL.[Non-Instructional Academic Benefits]+PL.[Instructional Academic Payroll Tax] + PL.[Instructional Academic Benefits]
	 , 'UNITID='+convert(varchar(6),[Unit ID])as UNITID
	 , SURVSECT = 'SURVSECT=F3A'
	 , PART= 'PART=E'
	 , LINE = 'LINE=1'
	 , [COLUMN] = 'COLUMN=1'
	 , F3AE11F101 =  'AMOUNT=' + convert(varchar(max),convert(int,round(PL.[Instruction(Initial)]+(PL.[Total O+M] * PL.[Instruction(Initial) Percent])+(PL.[Depreciation and amortization] * PL.[Instruction(Initial) Percent]) + (PL.[Interest Expense] * PL.[Instruction(Initial) Percent]),0)))
	--line 1,2
	 , 'UNITID='+convert(varchar(6),[Unit ID])as UNITID
	 , SURVSECT = 'SURVSECT=F3A'
	 , PART= 'PART=E'
	 , LINE = 'LINE=1'
	 , [COLUMN] = 'COLUMN=2'
	 , F3AE12F101 =  'AMOUNT=' + convert(varchar(max),convert(int,round(PL.[Instructional Wages] + PL.[Academic Overtime],0)))
	--line 1,3
	 , 'UNITID='+convert(varchar(6),[Unit ID])as UNITID
	 , SURVSECT = 'SURVSECT=F3A'
	 , PART= 'PART=E'
	 , LINE = 'LINE=1'
	 , [COLUMN] = 'COLUMN=3'
	 , F3AE13F101 = 'AMOUNT=' + convert(varchar(max), convert(int,round(PL.[Instructional Academic Payroll Tax] + PL.[Instructional Academic Benefits],0)))
	--line 1,4
	 , 'UNITID='+convert(varchar(6),[Unit ID])as UNITID
	 , SURVSECT = 'SURVSECT=F3A'
	 , PART= 'PART=E'
	 , LINE = 'LINE=1'
	 , [COLUMN] = 'COLUMN=4'
	 , F3AE14F101 =  'AMOUNT=' + convert(varchar(max),convert(int,round(PL.[Total O+M] * PL.[Instruction(Initial) Percent],0)))
	--line 1,5
	 , 'UNITID='+convert(varchar(6),[Unit ID])as UNITID
	 , SURVSECT = 'SURVSECT=F3A'
	 , PART= 'PART=E'
	 , LINE = 'LINE=1'
	 , [COLUMN] = 'COLUMN=5'
	 , F3AE15F101 =  'AMOUNT=' + convert(varchar(max),convert(int,round(PL.[Depreciation and amortization] * PL.[Instruction(Initial) Percent],0)))
	--line 1,6
	 , 'UNITID='+convert(varchar(6),[Unit ID])as UNITID
	 , SURVSECT = 'SURVSECT=F3A'
	 , PART= 'PART=E'
	 , LINE = 'LINE=1'
	 , [COLUMN] = 'COLUMN=6'
	 , F3AE16F101 =  'AMOUNT=' + convert(varchar(max),convert(int,round(PL.[Interest Expense] * PL.[Instruction(Initial) Percent],0)))
	--line 1,7
	 , 'UNITID='+convert(varchar(6),[Unit ID])as UNITID
	 , SURVSECT = 'SURVSECT=F3A'
	 , PART= 'PART=E'
	 , LINE = 'LINE=1'
	 , [COLUMN] = 'COLUMN=7'
	 , F3AE17F101 =  'AMOUNT=' + convert(varchar(max), convert(int,round(PL.[Instruction(Initial)]
		          - (PL.[Instructional Wages] + PL.[Academic Overtime])
				  - ( PL.[Instructional Academic Payroll Tax] + PL.[Instructional Academic Benefits]),0)))

  	--line 2a,1 
	 , 'UNITID='+convert(varchar(6),[Unit ID])as UNITID
	 , SURVSECT = 'SURVSECT=F3A'
	 , PART= 'PART=E'
	 , LINE = 'LINE=2a'
	 , [COLUMN] = 'COLUMN=1'
	 , F3AE2a1F101 = 'AMOUNT=0' 
	--line 2a,2
	 , 'UNITID='+convert(varchar(6),[Unit ID])as UNITID
	 , SURVSECT = 'SURVSECT=F3A'
	 , PART= 'PART=E'
	 , LINE = 'LINE=2a'
	 , [COLUMN] = 'COLUMN=2'
	 , F3AE2a2F101 = 'AMOUNT=0' 
	--line 2a,3
	 , 'UNITID='+convert(varchar(6),[Unit ID])as UNITID
	 , SURVSECT = 'SURVSECT=F3A'
	 , PART= 'PART=E'
	 , LINE = 'LINE=2a'
	 , [COLUMN] = 'COLUMN=3'
	 , F3AE2a3F101 = 'AMOUNT=0' 
	--line 2a,4
	 , 'UNITID='+convert(varchar(6),[Unit ID])as UNITID
	 , SURVSECT = 'SURVSECT=F3A'
	 , PART= 'PART=E'
	 , LINE = 'LINE=2a'
	 , [COLUMN] = 'COLUMN=4'
	 , F3AE2a4F101 = 'AMOUNT=0' 
	--line 2a,5
	 , 'UNITID='+convert(varchar(6),[Unit ID])as UNITID
	 , SURVSECT = 'SURVSECT=F3A'
	 , PART= 'PART=E'
	 , LINE = 'LINE=2a'
	 , [COLUMN] = 'COLUMN=5'
	 , F3AE2a5F101 = 'AMOUNT=0' 
	--line 2a,6
	 , 'UNITID='+convert(varchar(6),[Unit ID])as UNITID
	 , SURVSECT = 'SURVSECT=F3A'
	 , PART= 'PART=E'
	 , LINE = 'LINE=2a'
	 , [COLUMN] = 'COLUMN=6'
	 , F3AE2a6F101 = 'AMOUNT=0' 
	--line 2a,7
	 , 'UNITID='+convert(varchar(6),[Unit ID])as UNITID
	 , SURVSECT = 'SURVSECT=F3A'
	 , PART= 'PART=E'
	 , LINE = 'LINE=2a'
	 , [COLUMN] = 'COLUMN=7'
	 , F3AE2a7F101 = 'AMOUNT=0' 

  	--line 2b,1 
	 , 'UNITID='+convert(varchar(6),[Unit ID])as UNITID
	 , SURVSECT = 'SURVSECT=F3A'
	 , PART= 'PART=E'
	 , LINE = 'LINE=2b'
	 , [COLUMN] = 'COLUMN=1'
	 , F3AE2b1F101 = 'AMOUNT=0' 
	--line 2b,2
	 , 'UNITID='+convert(varchar(6),[Unit ID])as UNITID
	 , SURVSECT = 'SURVSECT=F3A'
	 , PART= 'PART=E'
	 , LINE = 'LINE=2b'
	 , [COLUMN] = 'COLUMN=2'
	 , F3AE2b2F101 = 'AMOUNT=0' 
	--line 2b,3
	 , 'UNITID='+convert(varchar(6),[Unit ID])as UNITID
	 , SURVSECT = 'SURVSECT=F3A'
	 , PART= 'PART=E'
	 , LINE = 'LINE=2b'
	 , [COLUMN] = 'COLUMN=3'
	 , F3AE2b3F101 = 'AMOUNT=0' 
	--line 2b,4
	 , 'UNITID='+convert(varchar(6),[Unit ID])as UNITID
	 , SURVSECT = 'SURVSECT=F3A'
	 , PART= 'PART=E'
	 , LINE = 'LINE=2b'
	 , [COLUMN] = 'COLUMN=4'
	 , F3AE2b4F101 = 'AMOUNT=0' 
	--line 2b,5
	 , 'UNITID='+convert(varchar(6),[Unit ID])as UNITID
	 , SURVSECT = 'SURVSECT=F3A'
	 , PART= 'PART=E'
	 , LINE = 'LINE=2b'
	 , [COLUMN] = 'COLUMN=5'
	 , F3AE2b5F101 = 'AMOUNT=0' 
	--line 2b,6
	 , 'UNITID='+convert(varchar(6),[Unit ID])as UNITID
	 , SURVSECT = 'SURVSECT=F3A'
	 , PART= 'PART=E'
	 , LINE = 'LINE=2b'
	 , [COLUMN] = 'COLUMN=6'
	 , F3AE2b6F101 = 'AMOUNT=0' 
	--line 2b,7 
	 , 'UNITID='+convert(varchar(6),[Unit ID])as UNITID
	 , SURVSECT = 'SURVSECT=F3A'
	 , PART= 'PART=E'
	 , LINE = 'LINE=2b'
	 , [COLUMN] = 'COLUMN=7'
	 , F3AE2b7F101 = 'AMOUNT=0' 
	
  	--line 3a,1 convert(int,round(PL.[Non-instructional Wages],0))+ convert(int,round(PL.[Non-Instructional Academic Payroll Tax] + PL.[Non-Instructional Academic Benefits],0))+convert(int,round((PL.[Total O+M] * PL.[Academic Support(Initial) Percent]),0))+convert(int,round(PL.[Depreciation and amortization] * PL.[Academic Support(Initial) Percent],0)) +convert(int,round(PL.[Interest Expense] * PL.[Academic Support(Initial) Percent],0))
	 , 'UNITID='+convert(varchar(6),[Unit ID])as UNITID
	 , SURVSECT = 'SURVSECT=F3A'
	 , PART= 'PART=E'
	 , LINE = 'LINE=3a'
	 , [COLUMN] = 'COLUMN=1'
	 , F3AE3a1F101 =  'AMOUNT=' + convert(varchar(max),convert(int,round(PL.[Non-instructional Wages],0))+ convert(int,round(PL.[Non-Instructional Academic Payroll Tax] + PL.[Non-Instructional Academic Benefits],0))+convert(int,round((PL.[Total O+M] * PL.[Academic Support(Initial) Percent]),0))+convert(int,round(PL.[Depreciation and amortization] * PL.[Academic Support(Initial) Percent],0)) +convert(int,round(PL.[Interest Expense] * PL.[Academic Support(Initial) Percent],0))+convert(int,round(PL.[Academic Support(Initial)] 
				   - PL.[Non-instructional Wages] 
				   - PL.[Non-Instructional Academic Payroll Tax] 
				   - PL.[Non-Instructional Academic Benefits],0)))
	--line 3a,2
	 , 'UNITID='+convert(varchar(6),[Unit ID])as UNITID
	 , SURVSECT = 'SURVSECT=F3A'
	 , PART= 'PART=E'
	 , LINE = 'LINE=3a'
	 , [COLUMN] = 'COLUMN=2'
	 , F3AE3a2F101 =  'AMOUNT=' + convert(varchar(max), convert(int,round(PL.[Non-instructional Wages],0)))
	--line 3a,3 
	 , 'UNITID='+convert(varchar(6),[Unit ID])as UNITID
	 , SURVSECT = 'SURVSECT=F3A'
	 , PART= 'PART=E'
	 , LINE = 'LINE=3a'
	 , [COLUMN] = 'COLUMN=3'
	 , F3AE3a3F101 =  'AMOUNT=' + convert(varchar(max), convert(int,round(PL.[Non-Instructional Academic Payroll Tax] + PL.[Non-Instructional Academic Benefits],0)))
	--line 3a,4 
	 , 'UNITID='+convert(varchar(6),[Unit ID])as UNITID
	 , SURVSECT = 'SURVSECT=F3A'
	 , PART= 'PART=E'
	 , LINE = 'LINE=3a'
	 , [COLUMN] = 'COLUMN=4'
	 , F3AE3a4F101 =  'AMOUNT=' + convert(varchar(max),convert(int,round((PL.[Total O+M] * PL.[Academic Support(Initial) Percent]),0)))
	--line 3a,5 
	 , 'UNITID='+convert(varchar(6),[Unit ID])as UNITID
	 , SURVSECT = 'SURVSECT=F3A'
	 , PART= 'PART=E'
	 , LINE = 'LINE=3a'
	 , [COLUMN] = 'COLUMN=5'
	 , F3AE3a5F101 =  'AMOUNT=' + convert(varchar(max),convert(int,round(PL.[Depreciation and amortization] * PL.[Academic Support(Initial) Percent],0)))
	--line 3a,6 
	 , 'UNITID='+convert(varchar(6),[Unit ID])as UNITID
	 , SURVSECT = 'SURVSECT=F3A'
	 , PART= 'PART=E'
	 , LINE = 'LINE=3a'
	 , [COLUMN] = 'COLUMN=6'
	 , F3AE3a6F101 =  'AMOUNT=' + convert(varchar(max),convert(int,round(PL.[Interest Expense] * PL.[Academic Support(Initial) Percent],0)))
	--line 3a,7
	 , 'UNITID='+convert(varchar(6),[Unit ID])as UNITID
	 , SURVSECT = 'SURVSECT=F3A'
	 , PART= 'PART=E'
	 , LINE = 'LINE=3a'
	 , [COLUMN] = 'COLUMN=7'
	 , F3AE3a7F101 =  'AMOUNT=' + convert(varchar(max), convert(int,round(PL.[Academic Support(Initial)] 
				   - PL.[Non-instructional Wages] 
				   - PL.[Non-Instructional Academic Payroll Tax] 
				   - PL.[Non-Instructional Academic Benefits],0)))

  	--line 3b,1
	 , 'UNITID='+convert(varchar(6),[Unit ID])as UNITID
	 , SURVSECT = 'SURVSECT=F3A'
	 , PART= 'PART=E'
	 , LINE = 'LINE=3b'
	 , [COLUMN] = 'COLUMN=1'
	 , F3AE3b1F101 =  'AMOUNT=' + convert(varchar(max),convert(int,round(PL.[Student Services(Initial)]+(PL.[Total O+M] * PL.[Student Services(Initial) Percent])+(PL.[Depreciation and amortization] * PL.[Student Services(Initial) Percent]) + (PL.[Interest Expense] * PL.[Student Services(Initial) Percent]),0)))
	--line 3b,2
	 , 'UNITID='+convert(varchar(6),[Unit ID])as UNITID
	 , SURVSECT = 'SURVSECT=F3A'
	 , PART= 'PART=E'
	 , LINE = 'LINE=3b'
	 , [COLUMN] = 'COLUMN=2'
	 , F3AE3b2F101 =  'AMOUNT=' + convert(varchar(max), convert(int,round(PL.[Admissions Salaries] +PL.[Career Services Salaries] + PL.[Student Services Salaries] + PL.[Career Services overtime] + PL.[Student Services Overtime],0)))
	--line 3b,3
	 , 'UNITID='+convert(varchar(6),[Unit ID])as UNITID
	 , SURVSECT = 'SURVSECT=F3A'
	 , PART= 'PART=E'
	 , LINE = 'LINE=3b'
	 , [COLUMN] = 'COLUMN=3'
	 , F3AE3b3F101 =  'AMOUNT=' + convert(varchar(max),convert(int,round(PL.[Admissions payroll taxes]+ PL.[Admissions benefits]+ PL.[CS payroll taxes] + PL.[SS payroll taxes] + PL.[CS benefits] + PL.[SS benefits],0)))
	--line 3b,4
	 , 'UNITID='+convert(varchar(6),[Unit ID])as UNITID
	 , SURVSECT = 'SURVSECT=F3A'
	 , PART= 'PART=E'
	 , LINE = 'LINE=3b'
	 , [COLUMN] = 'COLUMN=4'
	 , F3AE3b4F101 =  'AMOUNT=' + convert(varchar(max),convert(int,round(PL.[Total O+M] * PL.[Student Services(Initial) Percent],0)))
	--line 3b,5
	 , 'UNITID='+convert(varchar(6),[Unit ID])as UNITID
	 , SURVSECT = 'SURVSECT=F3A'
	 , PART= 'PART=E'
	 , LINE = 'LINE=3b'
	 , [COLUMN] = 'COLUMN=5'
	 , F3AE3b5F101 =  'AMOUNT=' + convert(varchar(max),convert(int,round(PL.[Depreciation and amortization] * PL.[Student Services(Initial) Percent],0)))
	--line 3b,6
	 , 'UNITID='+convert(varchar(6),[Unit ID])as UNITID
	 , SURVSECT = 'SURVSECT=F3A'
	 , PART= 'PART=E'
	 , LINE = 'LINE=3b'
	 , [COLUMN] = 'COLUMN=6'
	 , F3AE3b6F101 =  'AMOUNT=' + convert(varchar(max),convert(int,round(PL.[Interest Expense] * PL.[Student Services(Initial) Percent],0)))
	--line 3b,7
	 , 'UNITID='+convert(varchar(6),[Unit ID])as UNITID
	 , SURVSECT = 'SURVSECT=F3A'
	 , PART= 'PART=E'
	 , LINE = 'LINE=3b'
	 , [COLUMN] = 'COLUMN=7'
	 , F3AE3b7F101 =  'AMOUNT=' + convert(varchar(max),convert(int,round(PL.[Student Services(Initial)]
				   - ( PL.[Admissions Salaries] +PL.[Career Services Salaries] + PL.[Student Services Salaries] + PL.[Career Services overtime] + PL.[Student Services Overtime])
				   - ( PL.[Admissions payroll taxes]+ PL.[Admissions benefits]+ PL.[CS payroll taxes] + PL.[SS payroll taxes] + PL.[CS benefits] + PL.[SS benefits])
				   ,0)))
  	--line 3c,1 
	 , 'UNITID='+convert(varchar(6),[Unit ID])as UNITID
	 , SURVSECT = 'SURVSECT=F3A'
	 , PART= 'PART=E'
	 , LINE = 'LINE=3c'
	 , [COLUMN] = 'COLUMN=1'
	 , F3AE3c1F101 = 'AMOUNT=' + convert(varchar(max), convert(int,round(PL.[Institutional Support(Initial)]+(PL.[Total O+M] * PL.[Institutional Support(Initial) Percent])+(PL.[Depreciation and amortization] * PL.[Institutional Support(Initial) Percent]) + (PL.[Interest Expense] * PL.[Institutional Support(Initial) Percent]),0)))
	--line 3c,2
	, 'UNITID='+convert(varchar(6),[Unit ID])as UNITID
	 , SURVSECT = 'SURVSECT=F3A'
	 , PART= 'PART=E'
	 , LINE = 'LINE=3c'
	 , [COLUMN] = 'COLUMN=2'
	 , F3AE3c2F101 =  'AMOUNT=' + convert(varchar(max),convert(int,round(PL.[Administrative salaries] + PL.[Admin Overtime],0)))
	--line 3c,3
	 , 'UNITID='+convert(varchar(6),[Unit ID])as UNITID
	 , SURVSECT = 'SURVSECT=F3A'
	 , PART= 'PART=E'
	 , LINE = 'LINE=3c'
	 , [COLUMN] = 'COLUMN=3'
	 , F3AE3c3F101 =  'AMOUNT=' + convert(varchar(max),convert(int,round(PL.[Administrative Payroll Taxes] + PL.[Administrative benefits],0)))
	--line 3c,4
	 , 'UNITID='+convert(varchar(6),[Unit ID])as UNITID
	 , SURVSECT = 'SURVSECT=F3A'
	 , PART= 'PART=E'
	 , LINE = 'LINE=3c'
	 , [COLUMN] = 'COLUMN=4'
	 , F3AE3c4F101 =  'AMOUNT=' + convert(varchar(max),convert(int,round(PL.[Total O+M] * PL.[Institutional Support(Initial) Percent],0)))
	--line 3c,5
	 , 'UNITID='+convert(varchar(6),[Unit ID])as UNITID
	 , SURVSECT = 'SURVSECT=F3A'
	 , PART= 'PART=E'
	 , LINE = 'LINE=3c'
	 , [COLUMN] = 'COLUMN=5'
	 , F3AE3c5F101 =  'AMOUNT=' + convert(varchar(max),convert(int,round(PL.[Depreciation and amortization] * PL.[Institutional Support(Initial) Percent],0)))
	--line 3c,6
	 , 'UNITID='+convert(varchar(6),[Unit ID])as UNITID
	 , SURVSECT = 'SURVSECT=F3A'
	 , PART= 'PART=E'
	 , LINE = 'LINE=3c'
	 , [COLUMN] = 'COLUMN=6'
	 , F3AE3c6F101 =  'AMOUNT=' + convert(varchar(max), convert(int,round(PL.[Interest Expense] * PL.[Institutional Support(Initial) Percent],0)))
	--line 3c,7
	 , 'UNITID='+convert(varchar(6),[Unit ID])as UNITID
	 , SURVSECT = 'SURVSECT=F3A'
	 , PART= 'PART=E'
	 , LINE = 'LINE=3c'
	 , [COLUMN] = 'COLUMN=7'
	 , F3AE3c7F101 =  'AMOUNT=' + convert(varchar(max), convert(int,round(PL.[Institutional Support(Initial)]
				   - (PL.[Administrative salaries] + PL.[Admin Overtime])
				   - ( PL.[Administrative Payroll Taxes] + PL.[Administrative benefits]),0)))

  	--line 4,1 
	 , 'UNITID='+convert(varchar(6),[Unit ID])as UNITID
	 , SURVSECT = 'SURVSECT=F3A'
	 , PART= 'PART=E'
	 , LINE = 'LINE=4'
	 , [COLUMN] = 'COLUMN=1'
	 , F3AE41F101 = 'AMOUNT=0' 
	--line 4,2
	, 'UNITID='+convert(varchar(6),[Unit ID])as UNITID
	 , SURVSECT = 'SURVSECT=F3A'
	 , PART= 'PART=E'
	 , LINE = 'LINE=4'
	 , [COLUMN] = 'COLUMN=2'
	 , F3AE42F101 = 'AMOUNT=0' 
	--line 4,3
	 , 'UNITID='+convert(varchar(6),[Unit ID])as UNITID
	 , SURVSECT = 'SURVSECT=F3A'
	 , PART= 'PART=E'
	 , LINE = 'LINE=4'
	 , [COLUMN] = 'COLUMN=3'
	 , F3AE43F101 = 'AMOUNT=0' 
	--line 4,4
	 , 'UNITID='+convert(varchar(6),[Unit ID])as UNITID
	 , SURVSECT = 'SURVSECT=F3A'
	 , PART= 'PART=E'
	 , LINE = 'LINE=4'
	 , [COLUMN] = 'COLUMN=4'
	 , F3AE44F101 = 'AMOUNT=0' 
	--line 4,5
	 , 'UNITID='+convert(varchar(6),[Unit ID])as UNITID
	 , SURVSECT = 'SURVSECT=F3A'
	 , PART= 'PART=E'
	 , LINE = 'LINE=4'
	 , [COLUMN] = 'COLUMN=5'
	 , F3AE45F101 = 'AMOUNT=0' 
	--line 4,6
	 , 'UNITID='+convert(varchar(6),[Unit ID])as UNITID
	 , SURVSECT = 'SURVSECT=F3A'
	 , PART= 'PART=E'
	 , LINE = 'LINE=4'
	 , [COLUMN] = 'COLUMN=6'
	 , F3AE46F101 = 'AMOUNT=0' 
	--line 4,7
	 , 'UNITID='+convert(varchar(6),[Unit ID])as UNITID
	 , SURVSECT = 'SURVSECT=F3A'
	 , PART= 'PART=E'
	 , LINE = 'LINE=4'
	 , [COLUMN] = 'COLUMN=7'
	 , F3AE47F101 = 'AMOUNT=0' 

  	--line 5,1 
	 , 'UNITID='+convert(varchar(6),[Unit ID])as UNITID
	 , SURVSECT = 'SURVSECT=F3A'
	 , PART= 'PART=E'
	 , LINE = 'LINE=5'
	 , [COLUMN] = 'COLUMN=1'
	 , F3AE51F101 = 'AMOUNT=0' 
	--line 5,7
	 , 'UNITID='+convert(varchar(6),[Unit ID])as UNITID
	 , SURVSECT = 'SURVSECT=F3A'
	 , PART= 'PART=E'
	 , LINE = 'LINE=5'
	 , [COLUMN] = 'COLUMN=7'
	 , F3AE57F101 = 'AMOUNT=0' 

  	--line 10,1 
	 , 'UNITID='+convert(varchar(6),[Unit ID])as UNITID
	 , SURVSECT = 'SURVSECT=F3A'
	 , PART= 'PART=E'
	 , LINE = 'LINE=10'
	 , [COLUMN] = 'COLUMN=1'
	 , F3AE101F101 = 'AMOUNT=0' 
	--line 10,2
	, 'UNITID='+convert(varchar(6),[Unit ID])as UNITID
	 , SURVSECT = 'SURVSECT=F3A'
	 , PART= 'PART=E'
	 , LINE = 'LINE=10'
	 , [COLUMN] = 'COLUMN=2'
	 , F3AE102F101 = 'AMOUNT=0' 
	--line 10,3
	, 'UNITID='+convert(varchar(6),[Unit ID])as UNITID
	 , SURVSECT = 'SURVSECT=F3A'
	 , PART= 'PART=E'
	 , LINE = 'LINE=10'
	 , [COLUMN] = 'COLUMN=3'
	 , F3AE103F101 = 'AMOUNT=0' 
	--line 10,4
	 , 'UNITID='+convert(varchar(6),[Unit ID])as UNITID
	 , SURVSECT = 'SURVSECT=F3A'
	 , PART= 'PART=E'
	 , LINE = 'LINE=10'
	 , [COLUMN] = 'COLUMN=4'
	 , F3AE104F101 = 'AMOUNT=0' 
	--line 10,5
	 , 'UNITID='+convert(varchar(6),[Unit ID])as UNITID
	 , SURVSECT = 'SURVSECT=F3A'
	 , PART= 'PART=E'
	 , LINE = 'LINE=10'
	 , [COLUMN] = 'COLUMN=5'
	 , F3AE105F101 = 'AMOUNT=0' 
	--line 10,6
	 , 'UNITID='+convert(varchar(6),[Unit ID])as UNITID
	 , SURVSECT = 'SURVSECT=F3A'
	 , PART= 'PART=E'
	 , LINE = 'LINE=10'
	 , [COLUMN] = 'COLUMN=6'
	 , F3AE106F101 = 'AMOUNT=0' 
	--line 10,7
	 , 'UNITID='+convert(varchar(6),[Unit ID])as UNITID
	 , SURVSECT = 'SURVSECT=F3A'
	 , PART= 'PART=E'
	 , LINE = 'LINE=10'
	 , [COLUMN] = 'COLUMN=7'
	 , F3AE107F101 = 'AMOUNT=0' 

  	--line 11,1 
	 , 'UNITID='+convert(varchar(6),[Unit ID])as UNITID
	 , SURVSECT = 'SURVSECT=F3A'
	 , PART= 'PART=E'
	 , LINE = 'LINE=11'
	 , [COLUMN] = 'COLUMN=1'
	 , F3AE111F101 =  'AMOUNT=' + convert(varchar(max), convert(int,round(PL.[Total O+M],0)))
	--line 11,2
	, 'UNITID='+convert(varchar(6),[Unit ID])as UNITID
	 , SURVSECT = 'SURVSECT=F3A'
	 , PART= 'PART=E'
	 , LINE = 'LINE=11'
	 , [COLUMN] = 'COLUMN=2'
	 , F3AE112F101 = 'AMOUNT=0' 
	--line 11,3
	 , 'UNITID='+convert(varchar(6),[Unit ID])as UNITID
	 , SURVSECT = 'SURVSECT=F3A'
	 , PART= 'PART=E'
	 , LINE = 'LINE=11'
	 , [COLUMN] = 'COLUMN=3'
	 , F3AE113F101 = 'AMOUNT=0' 
	--line 11,4
	 , 'UNITID='+convert(varchar(6),[Unit ID])as UNITID
	 , SURVSECT = 'SURVSECT=F3A'
	 , PART= 'PART=E'
	 , LINE = 'LINE=11'
	 , [COLUMN] = 'COLUMN=4'
	 , F3AE114F101 =  'AMOUNT=' + convert(varchar(max),-1*convert(int,round(PL.[Total O+M],0)))
	--line 11,5
	 , 'UNITID='+convert(varchar(6),[Unit ID])as UNITID
	 , SURVSECT = 'SURVSECT=F3A'
	 , PART= 'PART=E'
	 , LINE = 'LINE=11'
	 , [COLUMN] = 'COLUMN=5'
	 , F3AE115F101 = 'AMOUNT=0' 
	--line 11,6
	 , 'UNITID='+convert(varchar(6),[Unit ID])as UNITID
	 , SURVSECT = 'SURVSECT=F3A'
	 , PART= 'PART=E'
	 , LINE = 'LINE=11'
	 , [COLUMN] = 'COLUMN=6'
	 , F3AE116F101 = 'AMOUNT=0' 
	--line 11,7
	 , 'UNITID='+convert(varchar(6),[Unit ID])as UNITID
	 , SURVSECT = 'SURVSECT=F3A'
	 , PART= 'PART=E'
	 , LINE = 'LINE=11'
	 , [COLUMN] = 'COLUMN=7'
	 , F3AE117F101 = 'AMOUNT=0' 

  	--line 6,1 
	 , 'UNITID='+convert(varchar(6),[Unit ID])as UNITID
	 , SURVSECT = 'SURVSECT=F3A'
	 , PART= 'PART=E'
	 , LINE = 'LINE=6'
	 , [COLUMN] = 'COLUMN=1'
	 , F3AE61F101 =  'AMOUNT=' + convert(varchar(max),convert(int,round(PL.[Total Expenses] - PL.[Institutional Support(Initial)]-PL.[Student Services(Initial)] - PL.[Academic Support(Initial)] -PL.[Instruction(Initial)] - PL.[Total O+M] - PL.[Depreciation and amortization] - PL.[Interest Expense] ,0)))
	--line 6,2
	, 'UNITID='+convert(varchar(6),[Unit ID])as UNITID
	 , SURVSECT = 'SURVSECT=F3A'
	 , PART= 'PART=E'
	 , LINE = 'LINE=6'
	 , [COLUMN] = 'COLUMN=2'
	 , F3AE62F101 = 'AMOUNT=0' 
	--line 6,3
	 , 'UNITID='+convert(varchar(6),[Unit ID])as UNITID
	 , SURVSECT = 'SURVSECT=F3A'
	 , PART= 'PART=E'
	 , LINE = 'LINE=6'
	 , [COLUMN] = 'COLUMN=3'
	 , F3AE63F101 = 'AMOUNT=0' 
	--line 6,4
	 , 'UNITID='+convert(varchar(6),[Unit ID])as UNITID
	 , SURVSECT = 'SURVSECT=F3A'
	 , PART= 'PART=E'
	 , LINE = 'LINE=6'
	 , [COLUMN] = 'COLUMN=4'
	 , F3AE64F101 = 'AMOUNT=0' 
	--line 6,5
	, 'UNITID='+convert(varchar(6),[Unit ID])as UNITID
	 , SURVSECT = 'SURVSECT=F3A'
	 , PART= 'PART=E'
	 , LINE = 'LINE=6'
	 , [COLUMN] = 'COLUMN=5'
	 , F3AE65F101 = 'AMOUNT=0' 
	--line 6,6
	 , 'UNITID='+convert(varchar(6),[Unit ID])as UNITID
	 , SURVSECT = 'SURVSECT=F3A'
	 , PART= 'PART=E'
	 , LINE = 'LINE=6'
	 , [COLUMN] = 'COLUMN=6'
	 , F3AE66F101 = 'AMOUNT=0' 
	--line 6,7
	 , 'UNITID='+convert(varchar(6),[Unit ID])as UNITID
	 , SURVSECT = 'SURVSECT=F3A'
	 , PART= 'PART=E'
	 , LINE = 'LINE=6'
	 , [COLUMN] = 'COLUMN=7'
	 , F3AE67F101 = 'AMOUNT=0' 


  	--line 7,1 
	 , 'UNITID='+convert(varchar(6),[Unit ID])as UNITID
	 , SURVSECT = 'SURVSECT=F3A'
	 , PART= 'PART=E'
	 , LINE = 'LINE=7'
	 , [COLUMN] = 'COLUMN=1'
	 , F3AE71F101 =  'AMOUNT=' + convert(varchar(max),convert(int,round(PL.[TOTAL EXPENSES],0)))
	--line 7,2
	  , 'UNITID='+convert(varchar(6),[Unit ID])as UNITID
	 , SURVSECT = 'SURVSECT=F3A'
	 , PART= 'PART=E'
	 , LINE = 'LINE=7'
	 , [COLUMN] = 'COLUMN=2'
	 , F3AE72F101 =  'AMOUNT=' + convert(varchar(max),convert(int,round(PL.[Administrative salaries] + PL.[Admin Overtime]+PL.[Admissions Salaries] +PL.[Career Services Salaries] + PL.[Student Services Salaries] + PL.[Career Services overtime] + PL.[Student Services Overtime]+PL.[Non-instructional Wages]+PL.[Instructional Wages] + PL.[Academic Overtime],0)))
	--line 7,3
	 , 'UNITID='+convert(varchar(6),[Unit ID])as UNITID
	 , SURVSECT = 'SURVSECT=F3A'
	 , PART= 'PART=E'
	 , LINE = 'LINE=7'
	 , [COLUMN] = 'COLUMN=3'
	 , F3AE73F101 =  'AMOUNT=' + convert(varchar(max),convert(int,round(PL.[Administrative Payroll Taxes] + PL.[Administrative benefits]+PL.[Admissions payroll taxes]+ PL.[Admissions benefits]+ PL.[CS payroll taxes] + PL.[SS payroll taxes] + PL.[CS benefits] + PL.[SS benefits]+PL.[Non-Instructional Academic Payroll Tax] + PL.[Non-Instructional Academic Benefits]+PL.[Instructional Academic Payroll Tax] + PL.[Instructional Academic Benefits],0)))
	--line 7,4
	 , 'UNITID='+convert(varchar(6),[Unit ID])as UNITID
	 , SURVSECT = 'SURVSECT=F3A'
	 , PART= 'PART=E'
	 , LINE = 'LINE=7'
	 , [COLUMN] = 'COLUMN=4'
	 , F3AE74F101 =  'AMOUNT=' + convert(varchar(max),convert(int,round(PL.[Total O+M],0)))
	--line 7,5
	 , 'UNITID='+convert(varchar(6),[Unit ID])as UNITID
	 , SURVSECT = 'SURVSECT=F3A'
	 , PART= 'PART=E'
	 , LINE = 'LINE=7'
	 , [COLUMN] = 'COLUMN=5'
	 , F3AE75F101 =  'AMOUNT=' + convert(varchar(max),convert(int,round(PL.[Depreciation and amortization] ,0)))
	--line 7,6
	 , 'UNITID='+convert(varchar(6),[Unit ID])as UNITID
	 , SURVSECT = 'SURVSECT=F3A'
	 , PART= 'PART=E'
	 , LINE = 'LINE=7'
	 , [COLUMN] = 'COLUMN=6'
	 , F3AE76F101 =   'AMOUNT=' + convert(varchar(max),convert(int,round(PL.[Interest Expense] ,0)))
	--line 7,7
	 , 'UNITID='+convert(varchar(6),[Unit ID])as UNITID
	 , SURVSECT = 'SURVSECT=F3A'
	 , PART= 'PART=E'
	 , LINE = 'LINE=7'
	 , [COLUMN] = 'COLUMN=7'
	 , F3AE77F101 =  'AMOUNT=' + convert(varchar(max), convert(int,round(PL.[TOTAL EXPENSES]
				  - (PL.[Administrative salaries] + PL.[Admin Overtime]+PL.[Admissions Salaries] +PL.[Career Services Salaries] + PL.[Student Services Salaries] + PL.[Career Services overtime] + PL.[Student Services Overtime]+PL.[Non-instructional Wages]+PL.[Instructional Wages] + PL.[Academic Overtime])
				  - (PL.[Administrative Payroll Taxes] + PL.[Administrative benefits]+PL.[Admissions payroll taxes]+ PL.[Admissions benefits]+ PL.[CS payroll taxes] + PL.[SS payroll taxes] + PL.[CS benefits] + PL.[SS benefits]+PL.[Non-Instructional Academic Payroll Tax] + PL.[Non-Instructional Academic Benefits]+PL.[Instructional Academic Payroll Tax] + PL.[Instructional Academic Benefits])
				  ,0)))
  	--line 8,1 
	 , 'UNITID='+convert(varchar(6),[Unit ID])as UNITID
	 , SURVSECT = 'SURVSECT=F3A'
	 , PART= 'PART=E'
	 , LINE = 'LINE=8'
	 , [COLUMN] = 'COLUMN=1'
	 , F3AE81F101 = 'AMOUNT=0' 

	--line 9,1
	 , 'UNITID='+convert(varchar(6),[Unit ID])as UNITID
	 , SURVSECT = 'SURVSECT=F3A'
	 , PART= 'PART=E'
	 , LINE = 'LINE=9'
	 , [COLUMN] = 'COLUMN=1'
	 , F3AE91F101 = 'AMOUNT=0' 
	

  FROM  [Accounting].[dbo].[IPEDS_Finance Attributes] as A
  join ##PandL as PL
	on PL.CampusID = A.GPCampusID
	where A.F14 <49





  drop table ##PandL