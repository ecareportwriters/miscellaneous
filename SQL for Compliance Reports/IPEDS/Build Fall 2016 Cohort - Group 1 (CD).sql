set nocount on
set ansi_warnings off

declare @lower datetime
declare @upper datetime

set @lower = '2015-08-01'
set @upper = '2015-10-31'



--Gather ISIR Data
declare @AY as varchar(7)
declare @AYStart as datetime
declare @AYEnd as Datetime
declare @FIT as int 
declare @FaISIRAwardyearSchemaID as int


set @AY = '2015-16'												--Set Award Year for report
set @AYStart = (select startdate								--Get start date of designated award year
				from campusvue.dbo.FaYear
				where Code = @AY)
set @AYEnd = (select EndDate									--Get end date of designated award year
				from campusvue.dbo.FaYear
				where Code = @AY)
set @FaISIRAwardyearSchemaID = (Select FaIsirAwardYearSchemaID 
									from Campusvue.dbo.FaIsirAwardYearSchema (nolock) as FIAY
									where FIAY.FaYearID =(select FaYearID									--Get end date of designated award year
															from campusvue.dbo.FaYear
															where Code = @AY))
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
/****
Gather ISIR Data
****/
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

 		
declare @IsirLink as Table (SSN  varchar(11)
							,TransID  varchar(13)
							, FaIsirMainID int
							, SyStudentID int
							, Dependency Varchar(1)
							, AutoZero bit	
							, Degree varchar(25)
							, HousingStatus varchar(15)
							, FISAP_TotalIncome bigint 
							, IncomeCat Varchar(20)
							)			;

with Base_cte
as 
(
select  FIM.ISIRSSN
     , left(FIM.ISIRSsn,3)+'-'+right(left(FIM.ISIRSsn,5),2)+'-'+right(FIM.ISIRSsn,4) as SSN
	 , max(FIM.TransactionId) as TransID

from campusvue.dbo.FaISIRMain (nolock) as FIM
--JOIN campusvue.dbo.FaISIRSparseData (NOLOCK)as FISD
--	on FISD.FaISIRMainId = FIM.FaISIRMainId
where FIM.FaISIRAwardYearSchemaId = @FaISIRAwardyearSchemaID
--  and FIM.FaISIRMainId <> 141489
--  and FISD.FaISIRSparseDataId <>141615
--  and (FISD.c207 is null or FISD.C207 = '')
--  and CONVERT(date, FISD.C163,112)= (select max(CONVERT(date, FISD2.C163,112))
--												from campusvue.dbo.FaISIRMain (nolock) as FIM2
--												JOIN campusvue.dbo.FaISIRSparseData (NOLOCK)as FISD2
--													on FISD2.FaISIRMainId = FIM2.FaISIRMainId
--												where FIM2.ISIRSsn = FIM.ISIRSsn
--												  and (FISD2.c207 is null or FISD2.C207 = '')
--												  --and FIM2.FaISIRAwardYear
--												  and FIM2.FaISIRMainId <> 141489
--												  and FISD2.FaISIRSparseDataId <>141615)
group by FIM.ISIRSsn
)
insert into @IsirLink 
		select B.SSN
			 , B.TransID
			 , FIM.FaISIRMainId
			 , SS.SyStudentId
			 , Dependency = case FISD.C161
						when 'D' then 'D'
						when 'I' then 'I'
						when 'X' then 'D'
						when 'Y' then 'I'
					end  
	  , AuotZero = case FISD.C210 
					  when 'Y' then 1 
					  else 0 
				   end
	  , Degree = case FISD.c34 
					when 1 then 'With Bac/Prof'
					when 2 then 'Without Bac/Prof'
				  end 
	  , HousingStatus = case FISD.C135				
							when 3 then 'Off Campus'
							when 2 then 'With Parent'
							when 1 then 'Unknown'
							else 'Unknown'
						end
	  , FISAP_Total_Income =  isnull(FISD.C293,case FISD.C161 when 'D' then FISD.C288+FISD.C273 when 'X' then FISD.C288+FISD.C273 else FISD.C273 end   )			   
	  , FISAP_Total_Income_Category  = isnull(case  when  isnull(FISD.C293,case FISD.C161 when 'D' then FISD.C288+FISD.C273 when 'X' then FISD.C288+FISD.C273 else FISD.C273 end   )		 between -999999 and  30000  then '$0 - $30,000'
													when  isnull(FISD.C293,case FISD.C161 when 'D' then FISD.C288+FISD.C273 when 'X' then FISD.C288+FISD.C273 else FISD.C273 end   )		 between   30001 and  48000  then '$30,001 - $48,000'
													when  isnull(FISD.C293,case FISD.C161 when 'D' then FISD.C288+FISD.C273 when 'X' then FISD.C288+FISD.C273 else FISD.C273 end   )		 between   48001 and  75000  then '$48,001 - $75,000'
													when  isnull(FISD.C293,case FISD.C161 when 'D' then FISD.C288+FISD.C273 when 'X' then FISD.C288+FISD.C273 else FISD.C273 end   )		 between   75001 and  110000 then '$75,001 - $110,000'
													when  isnull(FISD.C293,case FISD.C161 when 'D' then FISD.C288+FISD.C273 when 'X' then FISD.C288+FISD.C273 else FISD.C273 end   )		   >=     110000             then '$110,001 and more '
													else null
												end ,'$0 - $2999')
										  
							              

		from Base_cte as B
		 join campusvue.dbo.FaISIRMain (nolock) as FIM
			on FIM.TransactionId =  B.TransID
		--join campusvue.dbo.faisirstudentmatch (nolock) as FISM
		--	on FISM.faisirmainid = FIM.faisirmainid
		join campusvue.dbo.syStudent (nolock) as SS
			on SS.ssn = B.SSN
		join campusvue.dbo.faisirsparsedata (nolock) as FISD
			on FISD.FaISIRMainID = FIM.FaISIRMainID

		where FIM.FaISIRAwardYearSchemaId = @FaISIRAwardyearSchemaID



--Get all AdEnrollIDS for students who had attendance scheduled the week of October 15
declare @enr table
(
ID int identity(1,1) PRIMARY KEY CLUSTERED
,AdEnrollID int
)

insert into @enr
select distinct
	att.AdEnrollID
from
	CampusVue.dbo.AdAttend att
where
	att.[Date] between @lower and @upper	

--Get SSN for above enrollments
declare @ssn table
(
ID int identity(1,1) PRIMARY KEY CLUSTERED
,AdEnrollID int
,SSN varchar(30)
)	

insert into @ssn	
select distinct
	e.AdEnrollID
	,sys.SSN
from
	@enr e
	inner join CampusVue.dbo.AdEnroll ade (nolock) on e.AdEnrollID = ade.AdEnrollID
	inner join CampusVue.dbo.SyStudent sys (nolock) on ade.SyStudentID = sys.SyStudentID
where
	sys.SSN is not null


--Bring in initial data from the satus change view
declare @stat table
(
SSN varchar(30)
,AdEnrollID int
,NewSySchoolStatusID int
,DateAdded datetime
,DateToActive datetime
)

--Pending Grads
insert into @stat
select
	stat.SSN
	,s.AdEnrollID
	,stat.NewSySchoolStatusID
	,max(stat.DateAdded) as DateAdded
	,null
from
	CampusVue.dbo.cst_AdStatusChanges_vw stat
	inner join @ssn s on stat.SSN = s.SSN
		and stat.EnrollID = s.AdEnrollID
where
	stat.DateAdded <= @lower
	and
	stat.NewSySchoolStatusID in (128,92)
group by stat.SSN, s.AdEnrollID, stat.NewSySchoolStatusID


--Active
insert into @stat
select
	stat.SSN
	,s.AdEnrollID
	,stat.NewSySchoolStatusID
	,max(stat.DateAdded) as DateAdded
	,null
from
	CampusVue.dbo.cst_AdStatusChanges_vw stat
	inner join @ssn s on stat.SSN = s.SSN
		and stat.EnrollID = s.AdEnrollID
	left outer join @stat n on s.SSN = n.SSN
where
	stat.DateAdded <= @lower
	and
	stat.NewSySchoolStatusID in (13,111)
	and
	n.AdEnrollID is null
group by stat.SSN, s.AdEnrollID, stat.NewSySchoolStatusID


--Provisionals who made it to active
insert into @stat
select
	A.SSN
	,A.AdEnrollID
	,A.NewSySchoolStatusID
	,A.DateAdded
	,B.DateAdded as DateToActive
from
	(
	select
	statA.SSN
	,sA.AdEnrollID
	,statA.NewSySchoolStatusID
	,max(statA.DateAdded) as DateAdded
	from
	CampusVue.dbo.cst_AdStatusChanges_vw statA
	inner join @ssn sA on statA.SSN = sA.SSN
		and statA.EnrollID = sA.AdEnrollID
	where
	statA.DateAdded <= @lower
	and
	statA.NewSySchoolStatusID in (124,116)
	group by statA.SSN, sA.AdEnrollID, statA.NewSySchoolStatusID
	) as A
	inner join
	(
	select
	statB.SSN
	,sB.AdEnrollID
	,statB.NewSySchoolStatusID
	,min(statB.DateAdded) as DateAdded
	from
	CampusVue.dbo.cst_AdStatusChanges_vw statB
	inner join @ssn sB on statB.SSN = sB.SSN
		and statB.EnrollID = sB.AdEnrollID
	where
	statB.DateAdded >= @lower
	and
	statB.PrevSySchoolStatusID in (124,116)
	and
	statB.NewSySchoolStatusID in (13,111)
	group by statB.SSN, sB.AdEnrollID, statB.NewSySchoolStatusID
	) as B on A.AdEnrollID = B.AdEnrollID
	left outer join @stat n on A.SSN = n.SSN
where
	n.AdEnrollID is null


--De-dup active-to-active or future-to-future entries
declare @deA table
(
SSN varchar(30)
,AdEnrollID int
,NewSySchoolStatusID int
,DateAdded datetime
,DateToActive datetime
)


declare @deB table
(
SSN varchar(30)
,AdEnrollID int
,NewSySchoolStatusID int
,DateAdded datetime
,DateToActive datetime
)


insert into @deA
select distinct
	a.SSN
	,a.AdEnrollID
	,a.NewSySchoolStatusID
	,a.DateAdded
	,a.DateToActive
from
	@stat a
	inner join
	(
	select m.SSN, m.AdEnrollID, max(m.DateAdded) as DateAdded
	from @stat m
	group by m.SSN, m.AdEnrollID
	) as b on a.SSN = b.SSN
		and a.AdEnrollID = b.AdEnrollID
		and a.DateAdded = b.DateAdded
		
insert into @deB
select distinct
	a.SSN
	,a.AdEnrollID
	,a.NewSySchoolStatusID
	,a.DateAdded
	,a.DateToActive
from
	@deA a
	inner join
	(
	select m.SSN, max(m.AdEnrollID) as AdEnrollID
	from @deA m
	group by m.SSN
	) as b on a.SSN = b.SSN
		and a.AdEnrollID = b.AdEnrollID
		

--Add StatusAsOf, Campus, and Student Name from the status change view
declare @saoc table
(
Campus varchar(50)
,StudentName varchar(55)
,SSN varchar(30)
,AdEnrollID int
,StatusAsOf varchar(35)
,DateToActive varchar(10)
)

insert into @saoc
select
	vw.Campus
	,vw.StudentName
	,tmp.SSN
	--,tmp.DateAdded
	,tmp.AdEnrollID
	,vw.SySchoolStatusDescrip as StatusAsOf
	--,vw.PrevStatusDescrip
	,isnull(convert(varchar,tmp.DateToActive,101),'') as DateToActive
from
	@deB tmp
	inner join CampusVue.dbo.cst_AdStatusChanges_vw vw on tmp.SSN = vw.SSN
		and vw.EnrollID = tmp.AdEnrollID
		and vw.NewSySchoolStatusID = tmp.NewSySchoolStatusID
		and tmp.DateAdded = vw.DateAdded
		


--Return detailed results	
select
	tmp.*
	,adg.descrip as GradeLevel
	,ats.descrip as AttStat
into ##Cohort
from
	@saoc tmp
	inner join CampusVue.dbo.AdEnroll ade on tmp.AdEnrollID = ade.AdEnrollID
	inner join CampusVue.dbo.AdGradeLevel adg on ade.AdGradeLevelID = adg.AdGradeLevelID
	inner join CampusVue.dbo.AdAttStat ats on ade.AdAttStatID = ats.AdAttStatID
where ade.sycampusid <49
order by tmp.Campus, tmp.SSN




Select ID.[Unit ID], C.*, FAS.StartDate, FAS.EndDate, 
  case when FAS.startdate <=@Lower and FAS.EndDate between @Lower and @Upper then datediff(day,@Lower,FAS.EndDate)
      WHEN FAS.EndDate between @lower and @upper and FAS.StartDAte between @Lower and @Upper THEN datediff(day,FAS.StartDate, FAS.EndDate)
      when (@Upper between FAS.Startdate and FAS.EndDate and  @Lower between FAS.StartDate and FAS.EndDate) then datediff(day,@lower,@Upper)
	  when FAS.StartDAte  between @lower and @upper and FAS.EndDAte>= @upper then DATEDIFF(day, FAS.StartDAte, @Upper)
	  end as DaysInPeriod
	  , Rank() over(partition by C.Adenrollid order by   case when FAS.startdate <=@Lower and FAS.EndDate between @Lower and @Upper then datediff(day,@Lower,FAS.EndDate)
																WHEN FAS.EndDate between @lower and @upper and FAS.StartDAte between @Lower and @Upper THEN datediff(day,FAS.StartDate, FAS.EndDate)
																when (@Upper between FAS.Startdate and FAS.EndDate and  @Lower between FAS.StartDate and FAS.EndDate) then datediff(day,@lower,@Upper)
																 when FAS.StartDAte  between @lower and @upper and FAS.EndDAte>= @upper then DATEDIFF(day, FAS.StartDAte, @Upper)
																end 
	  desc,FAS.StartDate asc) as Ranking
	  , IL.IncomeCat
	  , IL.HousingStatus
	  , SUM(Case when FSA.FaFundsourceid = 1 then FSA.Amountpackaged + FSA.AmountCanceled
				 else 0 
			 end ) as PellAmountSched
	  , SUM(Case when FSA.FaFundsourceid = 2 then FSA.Amountpackaged + FSA.AmountCanceled
				 else 0 
			 end ) as SEOGAmountSched
	  , SUM(Case when FSA.FaFundsourceid in(5,7,9) then FSA.Amountpackaged + FSA.AmountCanceled
				 else 0 
			 end ) as LoanAmountInclPlusSched
	
	  , SUM(Case when FSA.FaFundsourceid in(5,7) then FSA.Amountpackaged + FSA.AmountCanceled
				 else 0 
			 end ) as LoanAmountNoPlusSched
	  , isnull(FWS.[Total Paid],0) as FWS
	  ,  Group2 = case when GradeLevel = '1st year, never attended college' then 1 
						else 0 end 
	  ,  Group3 = case when GradeLevel = '1st year, never attended college' and 
					 SUM(Case when FSA.FaFundsourceid in (1,2/*insert other grants fafundourceids*/) then FSA.Amountpackaged + FSA.AmountCanceled
												 else 0 
											   end )>0 then 1
						else 0 
						end    
	, group4 = 		 case when GradeLevel = '1st year, never attended college' and  (SUM(Case when FSA.FaFundsourceid in (1,2/*insert other grants fafundourceids*/) then FSA.Amountpackaged + FSA.AmountCanceled
												 else 0 
											   end )>0 
											   or SUM(Case when FSA.FaFundsourceid in (1,2,5,7,9/*insert other T4 fafundourceids*/) then FSA.Amountpackaged + FSA.AmountCanceled
																				 else 0 
																			   end ) >0)  then 1
																
						else 0 
						end							 
into ##groups								 
from ##Cohort as C
join campusvue.dbo.fastudentAY (nolock) as FAS
	on FAS.Adenrollid = C.Adenrollid
left join CampusVue.dbo.FaStudentAid (nolock) as FSA
	on fsa.fastudentayid = fas.fastudentayid
join campusvue.dbo.adenroll (nolock) as AE
	on ae.adenrollid = C.AdEnrollid
left join @ISIRLink IL
	on IL.systudentid = AE.SyStudentid
left join campusvue.dbo.sycampus (nolock) as SC
	on SC.DEscrip=C.Campus
left join [Financial Planning].dbo.IPEDS_UnitID (nolock) as ID
	on ID.sycampusID=SC.SyCampusID
left Join [Financial Planning].[dbo].[1516FWS](nolock) as FWS
	on FWS.systudentid = AE.systudentid 
where ((FAS.startdate <=@Lower and FAS.EndDate between @Lower and @Upper )
      or (FAS.EndDate between @lower and @upper and FAS.StartDAte between @Lower and @Upper )
      or ( (@Upper between FAS.Startdate and FAS.EndDate and  @Lower between FAS.StartDate and FAS.EndDate) )
	  or( FAS.StartDAte  between @lower and @upper and FAS.EndDAte>= @upper))
	  and C.GradeLevel <> 'Graduate/Professional or beyond'

group by C.Campus
	   , C.StudentName
	   , C.SSN
	   , C.Adenrollid
	   , C.StatusAsOf
	   , C.DateToActive
	   , C.GradeLevel
	   , C.AttStat
	   , FAS.StartDate
	   , FAS.EndDate
	   , IL.IncomeCat
	  , IL.HousingStatus
	  , ID.[Unit ID]
	  ,isnull(FWS.[Total Paid],0) 
	   , case when FAS.startdate <=@Lower and FAS.EndDate between @Lower and @Upper then datediff(day,@Lower,FAS.EndDate)
      WHEN FAS.EndDate between @lower and @upper and FAS.StartDAte between @Lower and @Upper THEN datediff(day,FAS.StartDate, FAS.EndDate)
      when (@Upper between FAS.Startdate and FAS.EndDate and  @Lower between FAS.StartDate and FAS.EndDate) then datediff(day,@lower,@Upper)
	  when FAS.StartDAte  between @lower and @upper and FAS.EndDAte>= @upper then DATEDIFF(day, FAS.StartDAte, @Upper)
	  end

order by C.Campus, C.StudentName,fas.STARTDATE,DaysInPeriod
--1st year, never attended college
--Full Time


select [Unit ID]
	 , count (*) as Group1Count
	 , sum(group2) as Group2Count
	 , sum(Group3) as Group3Count
	 , sum(group4) as Group4Count

from ##Groups
group by [Unit ID]






drop table ##Cohort
drop table ##Groups