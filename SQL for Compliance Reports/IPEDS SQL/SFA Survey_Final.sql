with Disbursed_cte
as
(
select distinct SS.SSN
,SC.Descrip as Campus
       ,PELLDisbursed =isnull((SUM( case when STN.Type='I' Then STN.Amount
												  When stn.type ='D' Then STN.Amount
												  Else STN.Amount  end)),0)
		
								   
from campusvue.dbo.SaTrans (nolock) as STN
left join campusvue.dbo.FaStudentAid (nolock) as FSA
on FSA.FaStudentAidID = STN.FaStudentAidID
left join campusvue.dbo.FaRefund (nolock) as FR
on FR.FaRefundID = STN.FaRefundID
join campusvue.dbo.AdTerm (nolock) as AT
on AT.AdTermID = STN.AdTermID 
join campusvue.dbo.AdEnroll (nolock) as AE
on AE.AdEnrollID = STN.AdEnrollID
join campusvue.dbo.AdProgramVersion (nolock) as APV
on APV.AdProgramVersionID = AE.adProgramVersionID
join campusvue.dbo.systudent (nolock) as SS
	on ss.SyStudentId=ae.SyStudentID
join CampusVue.dbo.sycampus (nolock) as SC
on SC.sycampusid=SS.sycampusid
where  FR.SaStipendID is null
and FR.FARefundID is null
 and stn.date between '2013-07-01' and '2014-06-30'
 and FSA.FaFundSourceID in (1)
  and stn.Descrip like '%2013-14%'
group by  SS.SSN ,SC.descrip
)

,Disbursed2_cte
as
(
select distinct SS.SSN
,SC.Descrip as Campus
       ,OtherFedGrantDisbursed =isnull((SUM( case when STN.Type='I' Then STN.Amount
												  When stn.type ='D' Then STN.Amount
												  Else STN.Amount  end)),0)
		
								   
from campusvue.dbo.SaTrans (nolock) as STN
left join campusvue.dbo.FaStudentAid (nolock) as FSA
on FSA.FaStudentAidID = STN.FaStudentAidID
left join campusvue.dbo.FaRefund (nolock) as FR
on FR.FaRefundID = STN.FaRefundID
join campusvue.dbo.AdTerm (nolock) as AT
on AT.AdTermID = STN.AdTermID 
join campusvue.dbo.AdEnroll (nolock) as AE
on AE.AdEnrollID = STN.AdEnrollID
join campusvue.dbo.AdProgramVersion (nolock) as APV
on APV.AdProgramVersionID = AE.adProgramVersionID
join campusvue.dbo.systudent (nolock) as SS
	on ss.SyStudentId=ae.SyStudentID
join CampusVue.dbo.sycampus (nolock) as SC
	on SC.sycampusid=SS.sycampusid
where  FR.SaStipendID is null
and FR.FARefundID is null
 and stn.date between '2013-07-01' and '2014-06-30'
 and FSA.FaFundSourceID in (2)
  and stn.Descrip like '%2013-14%'
group by  SS.SSN ,SC.descrip
)

,Disbursed3_cte
as
(
select distinct SS.SSN
,SC.Descrip as Campus
       ,StateLocalGrantDisbursed =isnull((SUM( case when STN.Type='I' Then STN.Amount
												  When stn.type ='D' Then STN.Amount
												  Else STN.Amount  end)),0)
		
								   
from campusvue.dbo.SaTrans (nolock) as STN
left join campusvue.dbo.FaStudentAid (nolock) as FSA
on FSA.FaStudentAidID = STN.FaStudentAidID
left join campusvue.dbo.FaRefund (nolock) as FR
on FR.FaRefundID = STN.FaRefundID
join campusvue.dbo.AdTerm (nolock) as AT
on AT.AdTermID = STN.AdTermID 
join campusvue.dbo.AdEnroll (nolock) as AE
on AE.AdEnrollID = STN.AdEnrollID
join campusvue.dbo.AdProgramVersion (nolock) as APV
on APV.AdProgramVersionID = AE.adProgramVersionID
join campusvue.dbo.systudent (nolock) as SS
	on ss.SyStudentId=ae.SyStudentID
join CampusVue.dbo.sycampus (nolock) as SC
	on SC.sycampusid=SS.sycampusid
where  FR.SaStipendID is null
and FR.FARefundID is null
 and stn.date between '2013-07-01' and '2014-06-30'
 and FSA.FaFundSourceID in (11,34,37)
  and stn.Descrip like '%2013-14%'
group by  SS.SSN ,SC.descrip
)

,Disbursed4_cte
as
(
select distinct SS.SSN
,SC.Descrip as Campus
       ,InstGrantDisbursed =isnull((SUM( case when STN.Type='I' Then STN.Amount
												  When stn.type ='D' Then STN.Amount
												  Else STN.Amount  end)),0)
		
								   
from campusvue.dbo.SaTrans (nolock) as STN
left join campusvue.dbo.FaStudentAid (nolock) as FSA
on FSA.FaStudentAidID = STN.FaStudentAidID
left join campusvue.dbo.FaRefund (nolock) as FR
on FR.FaRefundID = STN.FaRefundID
join campusvue.dbo.AdTerm (nolock) as AT
on AT.AdTermID = STN.AdTermID 
join campusvue.dbo.AdEnroll (nolock) as AE
on AE.AdEnrollID = STN.AdEnrollID
join campusvue.dbo.AdProgramVersion (nolock) as APV
on APV.AdProgramVersionID = AE.adProgramVersionID
join campusvue.dbo.systudent (nolock) as SS
	on ss.SyStudentId=ae.SyStudentID
join CampusVue.dbo.sycampus (nolock) as SC
	on SC.sycampusid=SS.sycampusid
where  FR.SaStipendID is null
and FR.FARefundID is null
 and stn.date between '2013-07-01' and '2014-06-30'
 and FSA.FaFundSourceID in (12,45,46,56,57,81)
   and stn.Descrip like '%2013-14%'
group by  SS.SSN ,SC.descrip
)
,Disbursed5_cte
as
(
select distinct SS.SSN
,SC.Descrip as Campus
       ,OtherGrantDisbursed =isnull((SUM( case when STN.Type='I' Then STN.Amount
												  When stn.type ='D' Then STN.Amount
												  Else STN.Amount  end)),0)
		
								   
from campusvue.dbo.SaTrans (nolock) as STN
left join campusvue.dbo.FaStudentAid (nolock) as FSA
on FSA.FaStudentAidID = STN.FaStudentAidID
left join campusvue.dbo.FaRefund (nolock) as FR
on FR.FaRefundID = STN.FaRefundID
join campusvue.dbo.AdTerm (nolock) as AT
on AT.AdTermID = STN.AdTermID 
join campusvue.dbo.AdEnroll (nolock) as AE
on AE.AdEnrollID = STN.AdEnrollID
join campusvue.dbo.AdProgramVersion (nolock) as APV
on APV.AdProgramVersionID = AE.adProgramVersionID
join campusvue.dbo.systudent (nolock) as SS
	on ss.SyStudentId=ae.SyStudentID
join CampusVue.dbo.sycampus (nolock) as SC
	on SC.sycampusid=SS.sycampusid
where  FR.SaStipendID is null
and FR.FARefundID is null
 and stn.date between '2013-07-01' and '2014-06-30'
 and FSA.FaFundSourceID in (13,20,27)
 and stn.Descrip like '%2013-14%'
group by  SS.SSN ,SC.descrip
)



,Disbursed6_cte
as
(
select distinct SS.SSN
,SC.Descrip as Campus
       ,FedLoansDisbursed =isnull((SUM( case when STN.Type='I' Then STN.Amount
												  When stn.type ='D' Then STN.Amount
												  Else STN.Amount  end)),0)
		
								   
from campusvue.dbo.SaTrans (nolock) as STN
left join campusvue.dbo.FaStudentAid (nolock) as FSA
on FSA.FaStudentAidID = STN.FaStudentAidID
left join campusvue.dbo.FaRefund (nolock) as FR
on FR.FaRefundID = STN.FaRefundID
join campusvue.dbo.AdTerm (nolock) as AT
on AT.AdTermID = STN.AdTermID 
join campusvue.dbo.AdEnroll (nolock) as AE
on AE.AdEnrollID = STN.AdEnrollID
join campusvue.dbo.AdProgramVersion (nolock) as APV
on APV.AdProgramVersionID = AE.adProgramVersionID
join campusvue.dbo.systudent (nolock) as SS
	on ss.SyStudentId=ae.SyStudentID
join CampusVue.dbo.sycampus (nolock) as SC
	on SC.sycampusid=SS.sycampusid
where  FR.SaStipendID is null
and FR.FARefundID is null
 and stn.date between '2013-07-01' and '2014-06-30'
 and FSA.FaFundSourceID in (5,6,7,8)
 and stn.Descrip like '%2013-14%'
group by  SS.SSN ,SC.descrip
)

,Disbursed7_cte
as
(
select distinct SS.SSN
,SC.Descrip as Campus
       ,OtherLoansDisbursed =isnull((SUM( case when STN.Type='I' Then STN.Amount
												  When stn.type ='D' Then STN.Amount
												  Else STN.Amount  end)),0)
		
								   
from campusvue.dbo.SaTrans (nolock) as STN
left join campusvue.dbo.FaStudentAid (nolock) as FSA
on FSA.FaStudentAidID = STN.FaStudentAidID
left join campusvue.dbo.FaRefund (nolock) as FR
on FR.FaRefundID = STN.FaRefundID
join campusvue.dbo.AdTerm (nolock) as AT
on AT.AdTermID = STN.AdTermID 
join campusvue.dbo.AdEnroll (nolock) as AE
on AE.AdEnrollID = STN.AdEnrollID
join campusvue.dbo.AdProgramVersion (nolock) as APV
on APV.AdProgramVersionID = AE.adProgramVersionID
join campusvue.dbo.systudent (nolock) as SS
	on ss.SyStudentId=ae.SyStudentID
join CampusVue.dbo.sycampus (nolock) as SC
	on SC.sycampusid=SS.sycampusid
where  FR.SaStipendID is null
and FR.FARefundID is null
 and stn.date between '2013-07-01' and '2014-06-30'
 and FSA.FaFundSourceID in (3,21,36,39,54)
 and stn.Descrip like '%2013-14%'
group by  SS.SSN ,SC.descrip
)

,Disbursed8_cte
as
(
select distinct SS.SSN
,SC.Descrip as Campus
       ,FWSDisbursed =isnull((SUM( case when STN.Type='I' Then STN.Amount
												  When stn.type ='D' Then STN.Amount
												  Else STN.Amount  end)),0)
		
								   
from campusvue.dbo.SaTrans (nolock) as STN
left join campusvue.dbo.FaStudentAid (nolock) as FSA
on FSA.FaStudentAidID = STN.FaStudentAidID
left join campusvue.dbo.FaRefund (nolock) as FR
on FR.FaRefundID = STN.FaRefundID
join campusvue.dbo.AdTerm (nolock) as AT
on AT.AdTermID = STN.AdTermID 
join campusvue.dbo.AdEnroll (nolock) as AE
on AE.AdEnrollID = STN.AdEnrollID
join campusvue.dbo.AdProgramVersion (nolock) as APV
on APV.AdProgramVersionID = AE.adProgramVersionID
join campusvue.dbo.systudent (nolock) as SS
	on ss.SyStudentId=ae.SyStudentID
join CampusVue.dbo.sycampus (nolock) as SC
	on SC.sycampusid=SS.sycampusid
where  FR.SaStipendID is null
and FR.FARefundID is null
 and stn.date between '2013-07-01' and '2014-06-30'
 and FSA.FaFundSourceID in (26)
group by  SS.SSN ,SC.descrip
)

,Disbursed9_cte
as
(
select distinct SS.SSN
,SC.Descrip as Campus
       ,PostGIDisbursed =isnull((SUM( case when STN.Type='I' Then STN.Amount
												  When stn.type ='D' Then STN.Amount
												  Else STN.Amount  end)),0)
		
								   
from campusvue.dbo.SaTrans (nolock) as STN
left join campusvue.dbo.FaStudentAid (nolock) as FSA
on FSA.FaStudentAidID = STN.FaStudentAidID
left join campusvue.dbo.FaRefund (nolock) as FR
on FR.FaRefundID = STN.FaRefundID
join campusvue.dbo.AdTerm (nolock) as AT
on AT.AdTermID = STN.AdTermID 
join campusvue.dbo.AdEnroll (nolock) as AE
on AE.AdEnrollID = STN.AdEnrollID
join campusvue.dbo.AdProgramVersion (nolock) as APV
on APV.AdProgramVersionID = AE.adProgramVersionID
join campusvue.dbo.systudent (nolock) as SS
	on ss.SyStudentId=ae.SyStudentID
join CampusVue.dbo.sycampus (nolock) as SC
	on SC.sycampusid=SS.sycampusid
where  FR.SaStipendID is null
and FR.FARefundID is null
 and stn.date between '2013-07-01' and '2014-06-30'
 and FSA.FaFundSourceID in (48,49,50,51,58,62,63,64,65,66,67,68,69,70,71,72,79,80,82,83)
 and stn.Descrip like '%2013-14%'
group by  SS.SSN ,SC.descrip
)

,Disbursed10_cte
as
(
select distinct SS.SSN
,SC.Descrip as Campus
       ,DoDDisbursed =isnull((SUM( case when STN.Type='I' Then STN.Amount
												  When stn.type ='D' Then STN.Amount
												  Else STN.Amount  end)),0)
		
								   
from campusvue.dbo.SaTrans (nolock) as STN
left join campusvue.dbo.FaStudentAid (nolock) as FSA
on FSA.FaStudentAidID = STN.FaStudentAidID
left join campusvue.dbo.FaRefund (nolock) as FR
on FR.FaRefundID = STN.FaRefundID
join campusvue.dbo.AdTerm (nolock) as AT
on AT.AdTermID = STN.AdTermID 
join campusvue.dbo.AdEnroll (nolock) as AE
on AE.AdEnrollID = STN.AdEnrollID
join campusvue.dbo.AdProgramVersion (nolock) as APV
on APV.AdProgramVersionID = AE.adProgramVersionID
join campusvue.dbo.systudent (nolock) as SS
	on ss.SyStudentId=ae.SyStudentID
join CampusVue.dbo.sycampus (nolock) as SC
	on SC.sycampusid=SS.sycampusid
where  FR.SaStipendID is null
and FR.FARefundID is null
 and stn.date between '2013-07-01' and '2014-06-30'
 and FSA.FaFundSourceID in (42,43,52,73,74,75,76,77,78)
 and stn.Descrip like '%2013-14%'
group by  SS.SSN ,SC.descrip
)





select SFA.StudentName
,SFA.SSN
,SFA.Campus
,SFA.AttStat
,SFA.Degree
,SFA.[Dependency 1213]
,SFA.[Dependency 1314]

,SFA.[Grade Level in College]
,SFA.[GL in College]
,SFA.[FTI_1213]
,SFA.sti_1213
,SFA.TI_1213
,SFA.[FTI_1314]
,SFA.sti_1314
,SFA.TI_1314
,SFA.[Housing 1213]
,SFA.[Housing 1314]
,Housing= case when SFA.[Housing 1314] is null then SFA.[housing 1213] else SFA.[Housing 1314] end
,isnull((D1.PellDisbursed),0) as PellDisbursed
,isnull((D2.OtherFedGrantDisbursed),0) as OtherFedGrantDisbursed
,isnull((D3.StateLocalGrantDisbursed),0) as StateLocalGrantDisbursed
,isnull((D4.InstGrantDisbursed),0) as InstGrantDisbursed 
,isnull((D5.OtherGrantDisbursed),0) as OtherGrantDisbursed
,isnull((D6.FedLoansDisbursed),0) as FedLoansDisbursed
,isnull((D7.OtherLoansDisbursed),0) as OtherLoansDisbursed
,isnull((D8.FWSDisbursed),0) as FWSDisbursed
,isnull((D9.PostGIDisbursed),0) as PostGIDisbursed
,isnull((D10.DoDDisbursed),0) as DoDDisbursed
,[T4 Received]= case when isnull((D1.PellDisbursed),0)+isnull((D2.OtherFedGrantDisbursed),0)+isnull((D6.FedLoansDisbursed),0)>0 then 1 else 0 end
,[FWS?]=FWS.FWS

,Income1213= case when (case when SFA.FTI_1213=0 then 
case when SFA.[Dependency 1213]='I' then SFA.STI_1213
	 when SFA.[Dependency 1213]='D' then (SFA.STI_1213+SFA.TI_1213) else 0  end

	 else SFA.FTI_1213 end) 	 
	 between 0 and 30000 then '0-30,000' 

when (case when SFA.FTI_1213=0 then 
case when SFA.[Dependency 1213]='I' then SFA.STI_1213
	 when SFA.[Dependency 1213]='D' then (SFA.STI_1213+SFA.TI_1213) else 0  end

	 else SFA.FTI_1213 end) between 30001 and 48000 then '30001-48000' 

	 when (case when SFA.FTI_1213=0 then 
case when SFA.[Dependency 1213]='I' then SFA.STI_1213
	 when SFA.[Dependency 1213]='D' then (SFA.STI_1213+SFA.TI_1213) else 0  end

	 else SFA.FTI_1213 end) between 48001 and 75000 then '48001-75000'
	 when (case when SFA.FTI_1213=0 then 
case when SFA.[Dependency 1213]='I' then SFA.STI_1213
	 when SFA.[Dependency 1213]='D' then (SFA.STI_1213+SFA.TI_1213) else 0  end

	 else SFA.FTI_1213 end) between 75001 and 110000 then '75001-110000'
	 when (case when SFA.FTI_1213=0 then 
case when SFA.[Dependency 1213]='I' then SFA.STI_1213
	 when SFA.[Dependency 1213]='D' then (SFA.STI_1213+SFA.TI_1213) else 0  end

	 else SFA.FTI_1213 end) >110000 then '110,000+'
	 else 'NULL'
	 end

,Income1314= case when (case when SFA.FTI_1314=0 then 
case when SFA.[Dependency 1314]='I' then SFA.STI_1314
	 when SFA.[Dependency 1314]='D' then (SFA.STI_1314+SFA.TI_1314) else 0  end

	 else SFA.FTI_1314 end) 	 
	 between 0 and 30000 then '0-30,000' 

when (case when SFA.FTI_1314=0 then 
case when SFA.[Dependency 1314]='I' then SFA.STI_1314
	 when SFA.[Dependency 1314]='D' then (SFA.STI_1314+SFA.TI_1314) else 0  end

	 else SFA.FTI_1314 end) between 30001 and 48000 then '30001-48000' 

	 when (case when SFA.FTI_1314=0 then 
case when SFA.[Dependency 1314]='I' then SFA.STI_1314
	 when SFA.[Dependency 1314]='D' then (SFA.STI_1314+SFA.TI_1314) else 0  end

	 else SFA.FTI_1314 end) between 48001 and 75000 then '48001-75000'
	 when (case when SFA.FTI_1314=0 then 
case when SFA.[Dependency 1314]='I' then SFA.STI_1314
	 when SFA.[Dependency 1314]='D' then (SFA.STI_1314+SFA.TI_1314) else 0  end

	 else SFA.FTI_1314 end) between 75001 and 110000 then '75001-110000'
	 when (case when SFA.FTI_1314=0 then 
case when SFA.[Dependency 1314]='I' then SFA.STI_1314
	 when SFA.[Dependency 1314]='D' then (SFA.STI_1314+SFA.TI_1314) else 0  end

	 else SFA.FTI_1314 end) >110000 then '110,000+'
	 else 'NULL'
	 end

,Income= case when  (case when SFA.FTI_1314=0 then 
case when SFA.[Dependency 1314]='I' then SFA.STI_1314
	 when SFA.[Dependency 1314]='D' then (SFA.STI_1314+SFA.TI_1314) else 0  end

	 else SFA.FTI_1314 end)>=0
	  then 
	  case when (case when SFA.FTI_1314=0 then 
case when SFA.[Dependency 1314]='I' then SFA.STI_1314
	 when SFA.[Dependency 1314]='D' then (SFA.STI_1314+SFA.TI_1314) else 0  end

	 else SFA.FTI_1314 end) 	 
	 between 0 and 30000 then '0-30,000' 

when (case when SFA.FTI_1314=0 then 
case when SFA.[Dependency 1314]='I' then SFA.STI_1314
	 when SFA.[Dependency 1314]='D' then (SFA.STI_1314+SFA.TI_1314) else 0  end

	 else SFA.FTI_1314 end) between 30001 and 48000 then '30001-48000' 

	 when (case when SFA.FTI_1314=0 then 
case when SFA.[Dependency 1314]='I' then SFA.STI_1314
	 when SFA.[Dependency 1314]='D' then (SFA.STI_1314+SFA.TI_1314) else 0  end

	 else SFA.FTI_1314 end) between 48001 and 75000 then '48001-75000'
	 when (case when SFA.FTI_1314=0 then 
case when SFA.[Dependency 1314]='I' then SFA.STI_1314
	 when SFA.[Dependency 1314]='D' then (SFA.STI_1314+SFA.TI_1314) else 0  end

	 else SFA.FTI_1314 end) between 75001 and 110000 then '75001-110000'
	 when (case when SFA.FTI_1314=0 then 
case when SFA.[Dependency 1314]='I' then SFA.STI_1314
	 when SFA.[Dependency 1314]='D' then (SFA.STI_1314+SFA.TI_1314) else 0  end

	 else SFA.FTI_1314 end) >110000 then '110,000+'
	 else 'NULL'
	 end

	 else
	 case when (case when SFA.FTI_1213=0 then 
case when SFA.[Dependency 1213]='I' then SFA.STI_1213
	 when SFA.[Dependency 1213]='D' then (SFA.STI_1213+SFA.TI_1213) else 0  end

	 else SFA.FTI_1213 end) 	 
	 between 0 and 30000 then '0-30,000' 

when (case when SFA.FTI_1213=0 then 
case when SFA.[Dependency 1213]='I' then SFA.STI_1213
	 when SFA.[Dependency 1213]='D' then (SFA.STI_1213+SFA.TI_1213) else 0  end

	 else SFA.FTI_1213 end) between 30001 and 48000 then '30001-48000' 

	 when (case when SFA.FTI_1213=0 then 
case when SFA.[Dependency 1213]='I' then SFA.STI_1213
	 when SFA.[Dependency 1213]='D' then (SFA.STI_1213+SFA.TI_1213) else 0  end

	 else SFA.FTI_1213 end) between 48001 and 75000 then '48001-75000'
	 when (case when SFA.FTI_1213=0 then 
case when SFA.[Dependency 1213]='I' then SFA.STI_1213
	 when SFA.[Dependency 1213]='D' then (SFA.STI_1213+SFA.TI_1213) else 0  end

	 else SFA.FTI_1213 end) between 75001 and 110000 then '75001-110000'
	 when (case when SFA.FTI_1213=0 then 
case when SFA.[Dependency 1213]='I' then SFA.STI_1213
	 when SFA.[Dependency 1213]='D' then (SFA.STI_1213+SFA.TI_1213) else 0  end

	 else SFA.FTI_1213 end) >110000 then '110,000+'
	 else 'NULL'
	 end
	 end



--,Income= case when case when (case when SFA.FTI_1314=0 then 
--case when SFA.[Dependency 1314]='I' then SFA.STI_1314
--	 when SFA.[Dependency 1314]='D' then (SFA.STI_1314+SFA.TI_1314) else 0  end

--	 else SFA.FTI_1314 end) 	 
--	 between 0 and 30000 then '0-30,000' 

--when (case when SFA.FTI_1314=0 then 
--case when SFA.[Dependency 1314]='I' then SFA.STI_1314
--	 when SFA.[Dependency 1314]='D' then (SFA.STI_1314+SFA.TI_1314) else 0  end

--	 else SFA.FTI_1314 end) between 30001 and 48000 then '30001-48000' 

--	 when (case when SFA.FTI_1314=0 then 
--case when SFA.[Dependency 1314]='I' then SFA.STI_1314
--	 when SFA.[Dependency 1314]='D' then (SFA.STI_1314+SFA.TI_1314) else 0  end

--	 else SFA.FTI_1314 end) between 48001 and 75000 then '48001-75000'
--	 when (case when SFA.FTI_1314=0 then 
--case when SFA.[Dependency 1314]='I' then SFA.STI_1314
--	 when SFA.[Dependency 1314]='D' then (SFA.STI_1314+SFA.TI_1314) else 0  end

--	 else SFA.FTI_1314 end) between 75001 and 110000 then '75001-110000'
--	 when (case when SFA.FTI_1314=0 then 
--case when SFA.[Dependency 1314]='I' then SFA.STI_1314
--	 when SFA.[Dependency 1314]='D' then (SFA.STI_1314+SFA.TI_1314) else 0  end

--	 else SFA.FTI_1314 end) >110000 then '110,000+'
--	 else 'NULL'
--	 end
	 
--	  is null then 
--	  case when (case when SFA.FTI_1213=0 then 
--case when SFA.[Dependency 1213]='I' then SFA.STI_1213
--	 when SFA.[Dependency 1213]='D' then (SFA.STI_1213+SFA.TI_1213) else 0  end

--	 else SFA.FTI_1213 end) 	 
--	 between 0 and 30000 then '0-30,000' 

--when (case when SFA.FTI_1213=0 then 
--case when SFA.[Dependency 1213]='I' then SFA.STI_1213
--	 when SFA.[Dependency 1213]='D' then (SFA.STI_1213+SFA.TI_1213) else 0  end

--	 else SFA.FTI_1213 end) between 30001 and 48000 then '30001-48000' 

--	 when (case when SFA.FTI_1213=0 then 
--case when SFA.[Dependency 1213]='I' then SFA.STI_1213
--	 when SFA.[Dependency 1213]='D' then (SFA.STI_1213+SFA.TI_1213) else 0  end

--	 else SFA.FTI_1213 end) between 48001 and 75000 then '48001-75000'
--	 when (case when SFA.FTI_1213=0 then 
--case when SFA.[Dependency 1213]='I' then SFA.STI_1213
--	 when SFA.[Dependency 1213]='D' then (SFA.STI_1213+SFA.TI_1213) else 0  end

--	 else SFA.FTI_1213 end) between 75001 and 110000 then '75001-110000'
--	 when (case when SFA.FTI_1213=0 then 
--case when SFA.[Dependency 1213]='I' then SFA.STI_1213
--	 when SFA.[Dependency 1213]='D' then (SFA.STI_1213+SFA.TI_1213) else 0  end

--	 else SFA.FTI_1213 end) >110000 then '110,000+'
--	 else 'NULL'
--	 end
	 
--	  else
	  
	  
--	   case when (case when SFA.FTI_1314=0 then 
--case when SFA.[Dependency 1314]='I' then SFA.STI_1314
--	 when SFA.[Dependency 1314]='D' then (SFA.STI_1314+SFA.TI_1314) else 0  end

--	 else SFA.FTI_1314 end) 	 
--	 between 0 and 30000 then '0-30,000' 

--when (case when SFA.FTI_1314=0 then 
--case when SFA.[Dependency 1314]='I' then SFA.STI_1314
--	 when SFA.[Dependency 1314]='D' then (SFA.STI_1314+SFA.TI_1314) else 0  end

--	 else SFA.FTI_1314 end) between 30001 and 48000 then '30001-48000' 

--	 when (case when SFA.FTI_1314=0 then 
--case when SFA.[Dependency 1314]='I' then SFA.STI_1314
--	 when SFA.[Dependency 1314]='D' then (SFA.STI_1314+SFA.TI_1314) else 0  end

--	 else SFA.FTI_1314 end) between 48001 and 75000 then '48001-75000'
--	 when (case when SFA.FTI_1314=0 then 
--case when SFA.[Dependency 1314]='I' then SFA.STI_1314
--	 when SFA.[Dependency 1314]='D' then (SFA.STI_1314+SFA.TI_1314) else 0  end

--	 else SFA.FTI_1314 end) between 75001 and 110000 then '75001-110000'
--	 when (case when SFA.FTI_1314=0 then 
--case when SFA.[Dependency 1314]='I' then SFA.STI_1314
--	 when SFA.[Dependency 1314]='D' then (SFA.STI_1314+SFA.TI_1314) else 0  end

--	 else SFA.FTI_1314 end) >110000 then '110,000+'
--	 else 'NULL'
--	 end
--	  end








,Group2A= case when (isnull((D6.FedLoansDisbursed),0)+isnull((D7.OtherLoansDisbursed),0)
+isnull((D2.OtherFedGrantDisbursed),0)+isnull((D1.PellDisbursed),0)
+isnull((D3.StateLocalGrantDisbursed),0)+isnull((D4.InstGrantDisbursed),0)
+isnull((D5.OtherGrantDisbursed),0))>0 then 'Yes' else 'No' end

,Group2B=  case when (isnull((D6.FedLoansDisbursed),0)+isnull((D7.OtherLoansDisbursed),0)
+isnull((D2.OtherFedGrantDisbursed),0)+isnull((D1.PellDisbursed),0)
+isnull((D3.StateLocalGrantDisbursed),0)+isnull((D4.InstGrantDisbursed),0))>0 then 'Yes' else 'No' end

,Group3=  case when (isnull((D2.OtherFedGrantDisbursed),0)+isnull((D1.PellDisbursed),0)
+isnull((D3.StateLocalGrantDisbursed),0)+isnull((D4.InstGrantDisbursed),0))>0 then 'Yes' else 'No' end

,Group4= case when isnull((D1.PellDisbursed),0)+isnull((D6.FedLoansDisbursed),0)>0 then 'Yes' else 'No' end

,ALLGrants=  case when (isnull((D2.OtherFedGrantDisbursed),0)+isnull((D1.PellDisbursed),0)
+isnull((D3.StateLocalGrantDisbursed),0)+isnull((D4.InstGrantDisbursed),0)+isnull((D5.OtherGrantDisbursed),0))>0 then 'Yes' else 'No' end

,NONPrivateGrants=  case when (isnull((D2.OtherFedGrantDisbursed),0)+isnull((D1.PellDisbursed),0)
+isnull((D3.StateLocalGrantDisbursed),0)+isnull((D4.InstGrantDisbursed),0))>0 then 'Yes' else 'No' end

,FEDGrant= case when (isnull((D2.OtherFedGrantDisbursed),0)+isnull((D1.PellDisbursed),0))>0 then 'Yes' else 'No' end

,Loans= case when (isnull((D6.FedLoansDisbursed),0)+isnull((D7.OtherLoansDisbursed),0))>0 then 'Yes' else 'No' end



from [Financial Planning].dbo.vw_IPEDS_SFA2015 (nolock) as SFA
left join Disbursed_cte as D1
	on D1.SSN=SFA.SSN and D1.Campus=SFA.Campus
left join Disbursed2_cte as D2
	on D2.SSN=SFA.SSN and D2.Campus=SFA.Campus
left join Disbursed3_cte as D3
	on D3.SSN=SFA.SSN and D3.Campus=SFA.Campus
left join Disbursed4_cte as D4
	on D4.SSN=SFA.SSN and D4.Campus=SFA.Campus
left join Disbursed5_cte as D5
	on D5.SSN=SFA.SSN and D5.Campus=SFA.Campus
left join Disbursed6_cte as D6
	on D6.SSN=SFA.SSN and D6.Campus=SFA.Campus
left join Disbursed7_cte as D7
	on D7.SSN=SFA.SSN and D7.Campus=SFA.Campus
left join Disbursed8_cte as D8
	on D8.SSN=SFA.SSN and D8.Campus=SFA.Campus
left join Disbursed9_cte as D9
	on D9.SSN=SFA.SSN and D9.Campus=SFA.Campus
left join Disbursed10_cte as D10
	on D10.SSN=SFA.SSN and D10.Campus=SFA.Campus
left join [Financial Planning].dbo.IPEDS_1314FWS (nolock) as FWS
	on FWS.SSN=SFA.SSN