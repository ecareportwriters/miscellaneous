with Cohort_cte
as
(
select distinct adt.AdEnrollID
from adattend (nolock) as ADT
join AdEnrollSched (nolock) as AES
on AES.AdEnrollSchedID = ADT.AdEnrollSchedID
where ADT.Date between '2013-07-03' and '2014-06-30'

)


select distinct  SS.systudentid
			 -- , AE.AdEnrollID
	          ,AX.Descrip as Gender
	          , isnull(AR.Descrip,PRI.Race)  as Race --isnull(AR.Descrip,PRI.Race) 
	          , SC.Descrip as Campus
	          , ADG.GraduateLevel
	        --  ,[Credit/Clock?]= case when APV.FaAcademicCalendar=5 then 'Clock' else 'Credit' end
	          , (select SUM(case when AES.AdGradeLetterCode <>'' 
								  and AES.AdGradeLetterCode<>'W' 
								  and AES.AdGradeLetterCode is not null 
						          and AES.StartDate between '2013-07-01' and '2014-06-30'
							      and AES.StartDate IS not null
												then case when APV2.FaAcademicCalendar <> 5 then   AES.CreditsAttempt 									
												else 0 end
												 else 0 
												end)
												from AdEnrollSched (nolock) as AES
													join AdEnroll (nolock) as AE2
													on AES.AdEnrollID = AE2.AdEnrollID
													join AdProgramVersion (nolock) as APV2
													on APV2.AdProgramVersionID = AE2.adProgramVersionID
													join Cohort_cte as C2
													on c2.AdEnrollID= AES.AdEnrollID
												   where AE2.SyStudentID =SS.SyStudentID )  as Credits
				 ,(select SUM(case when AES.AdGradeLetterCode <>'' 
								  and AES.AdGradeLetterCode<>'W' 
								  and AES.AdGradeLetterCode is not null 
						          and AES.StartDate between '2013-07-01' and '2014-06-30'
							      and AES.StartDate IS not null
												then case when APV2.FaAcademicCalendar = 5 then   AES.HoursAttempt 									
												else 0 end
												 else 0 
												end)
												from AdEnrollSched (nolock) as AES
													join AdEnroll (nolock) as AE2
													on AES.AdEnrollID = AE2.AdEnrollID
													join AdProgramVersion (nolock) as APV2
													on APV2.AdProgramVersionID = AE2.adProgramVersionID
													join Cohort_cte as C2
													on c2.AdEnrollID= AES.AdEnrollID
												   where AE2.SyStudentID = SS.SyStudentID ) as Hours 
		
			
							
from  AdEnroll (nolock) as AE
	join syStudent (nolock) as SS
		on SS.SyStudentId =AE.SyStudentID
	left join amSex (nolock) as AX
		on AX.amSexID = SS.AmSexID
	left join amRace (nolock) as AR
		on AR.AmRaceID = SS.AmRaceID
	join SyCampus (nolock) as SC
		on SC.SyCampusID = AE.SyCampusID
	join AdProgramVersion (nolock) as APV
		on APV.AdProgramVersionID = AE.adProgramVersionID
	join AdDegree (nolock) as ADG
		on ADG.AdDegreeID = APV.AdDegreeID
	join cohort_cte as C
		on C.AdEnrollID = AE.AdEnrollID
	left join [Financial Planning].dbo.PortalRaceInfo (nolock) as PRI
		on pri.systudentid = SS.SyStudentId
		
		--where SS.SyStudentId=3753204

	
	
		
group by  SS.systudentid
	          ,AX.Descrip 
	          ,AR.Descrip
	          , SC.Descrip 
	          , ADG.GraduateLevel
	         -- , AE.AdEnrollID
	       --   ,APV.FaAcademicCalendar
	          , PRI.Race
	          
	          
