set nocount on
set ansi_warnings off

declare @lower datetime
declare @upper datetime

set @lower = '2011-09-16'
set @upper = '2011-09-22'



--Get all AdEnrollIDS for students who had attendance scheduled the week of October 15
declare @enr table
(
ID int identity(1,1) PRIMARY KEY CLUSTERED
,AdEnrollID int
)

insert into @enr
select distinct
	att.AdEnrollID
from
	CampusVue.dbo.AdAttend att
where
	att.[Date] between @lower and @upper	

--Get SSN for above enrollments
declare @ssn table
(
ID int identity(1,1) PRIMARY KEY CLUSTERED
,AdEnrollID int
,SSN varchar(30)
)	

insert into @ssn	
select distinct
	e.AdEnrollID
	,sys.SSN
from
	@enr e
	inner join CampusVue.dbo.AdEnroll ade (nolock) on e.AdEnrollID = ade.AdEnrollID
	inner join CampusVue.dbo.SyStudent sys (nolock) on ade.SyStudentID = sys.SyStudentID
where
	sys.SSN is not null
	

--Bring in initial data from the satus change view
declare @stat table
(
SSN varchar(30)
,AdEnrollID int
,NewSySchoolStatusID int
,DateAdded datetime
,DateToActive datetime
)

--Pending Grads
insert into @stat
select
	stat.SSN
	,s.AdEnrollID
	,stat.NewSySchoolStatusID
	,max(stat.DateAdded) as DateAdded
	,null
from
	CampusVue.dbo.cst_AdStatusChanges_vw stat
	inner join @ssn s on stat.SSN = s.SSN
		and stat.EnrollID = s.AdEnrollID
where
	stat.DateAdded <= @lower
	and
	stat.NewSySchoolStatusID in (128,92)
group by stat.SSN, s.AdEnrollID, stat.NewSySchoolStatusID


--Active
insert into @stat
select
	stat.SSN
	,s.AdEnrollID
	,stat.NewSySchoolStatusID
	,max(stat.DateAdded) as DateAdded
	,null
from
	CampusVue.dbo.cst_AdStatusChanges_vw stat
	inner join @ssn s on stat.SSN = s.SSN
		and stat.EnrollID = s.AdEnrollID
	left outer join @stat n on s.SSN = n.SSN
where
	stat.DateAdded <= @lower
	and
	stat.NewSySchoolStatusID in (13,111)
	and
	n.AdEnrollID is null
group by stat.SSN, s.AdEnrollID, stat.NewSySchoolStatusID


--Provisionals who made it to active
insert into @stat
select
	A.SSN
	,A.AdEnrollID
	,A.NewSySchoolStatusID
	,A.DateAdded
	,B.DateAdded as DateToActive
from
	(
	select
	statA.SSN
	,sA.AdEnrollID
	,statA.NewSySchoolStatusID
	,max(statA.DateAdded) as DateAdded
	from
	CampusVue.dbo.cst_AdStatusChanges_vw statA
	inner join @ssn sA on statA.SSN = sA.SSN
		and statA.EnrollID = sA.AdEnrollID
	where
	statA.DateAdded <= @lower
	and
	statA.NewSySchoolStatusID in (124,116)
	group by statA.SSN, sA.AdEnrollID, statA.NewSySchoolStatusID
	) as A
	inner join
	(
	select
	statB.SSN
	,sB.AdEnrollID
	,statB.NewSySchoolStatusID
	,min(statB.DateAdded) as DateAdded
	from
	CampusVue.dbo.cst_AdStatusChanges_vw statB
	inner join @ssn sB on statB.SSN = sB.SSN
		and statB.EnrollID = sB.AdEnrollID
	where
	statB.DateAdded >= @lower
	and
	statB.PrevSySchoolStatusID in (124,116)
	and
	statB.NewSySchoolStatusID in (13,111)
	group by statB.SSN, sB.AdEnrollID, statB.NewSySchoolStatusID
	) as B on A.AdEnrollID = B.AdEnrollID
	left outer join @stat n on A.SSN = n.SSN
where
	n.AdEnrollID is null


--De-dup active-to-active or future-to-future entries
declare @deA table
(
SSN varchar(30)
,AdEnrollID int
,NewSySchoolStatusID int
,DateAdded datetime
,DateToActive datetime
)


declare @deB table
(
SSN varchar(30)
,AdEnrollID int
,NewSySchoolStatusID int
,DateAdded datetime
,DateToActive datetime
)


insert into @deA
select distinct
	a.SSN
	,a.AdEnrollID
	,a.NewSySchoolStatusID
	,a.DateAdded
	,a.DateToActive
from
	@stat a
	inner join
	(
	select m.SSN, m.AdEnrollID, max(m.DateAdded) as DateAdded
	from @stat m
	group by m.SSN, m.AdEnrollID
	) as b on a.SSN = b.SSN
		and a.AdEnrollID = b.AdEnrollID
		and a.DateAdded = b.DateAdded
		
insert into @deB
select distinct
	a.SSN
	,a.AdEnrollID
	,a.NewSySchoolStatusID
	,a.DateAdded
	,a.DateToActive
from
	@deA a
	inner join
	(
	select m.SSN, max(m.AdEnrollID) as AdEnrollID
	from @deA m
	group by m.SSN
	) as b on a.SSN = b.SSN
		and a.AdEnrollID = b.AdEnrollID
		

--Add StatusAsOf, Campus, and Student Name from the status change view
declare @saoc table
(
Campus varchar(50)
,StudentName varchar(55)
,SSN varchar(30)
,AdEnrollID int
,StatusAsOf varchar(35)
,DateToActive varchar(10)
)

insert into @saoc
select
	vw.Campus
	,vw.StudentName
	,tmp.SSN
	--,tmp.DateAdded
	,tmp.AdEnrollID
	,vw.SySchoolStatusDescrip as StatusAsOf
	--,vw.PrevStatusDescrip
	,isnull(convert(varchar,tmp.DateToActive,101),'') as DateToActive
from
	@deB tmp
	inner join CampusVue.dbo.cst_AdStatusChanges_vw vw on tmp.SSN = vw.SSN
		and vw.EnrollID = tmp.AdEnrollID
		and vw.NewSySchoolStatusID = tmp.NewSySchoolStatusID
		and tmp.DateAdded = vw.DateAdded
		


--Return detailed results	
select
	tmp.*
	,adg.descrip as GradeLevel
	,ats.descrip as AttStat
from
	@saoc tmp
	inner join CampusVue.dbo.AdEnroll ade on tmp.AdEnrollID = ade.AdEnrollID
	inner join CampusVue.dbo.AdGradeLevel adg on ade.AdGradeLevelID = adg.AdGradeLevelID
	inner join CampusVue.dbo.AdAttStat ats on ade.AdAttStatID = ats.AdAttStatID
--where
--	adg.AdGradeLevelID = 1
--	and
--	ats.AdAttStatID = 1
order by tmp.Campus, tmp.SSN