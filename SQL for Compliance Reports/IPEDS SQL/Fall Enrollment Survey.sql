declare @IsirLink as Table (SSN  varchar(11)
                                                ,TransID  varchar(13)
                                                , FaIsirMainID int
                                                , SyStudentID int
                                                )                    ;

with Base_cte
as 
(
select  SSN = LEFT(FIM.ISIRSSN,3)+'-'+RIGHT(LEFT(FIM.ISIRSSN,5),2)+'-'+RIGHT(FIM.ISIRSSN,4)
     , max(FIM.TransactionId) as TransID
from campusvue.dbo.FaISIRMain (nolock) as FIM
JOIN campusvue.dbo.FaISIRSparseData (NOLOCK)as FISD
       on FISD.FaISIRMainId = FIM.FaISIRMainId
where FIM.FaISIRAwardYearSchemaId = 5
  and FIM.FaISIRMainId <> 141489
  and FISD.FaISIRSparseDataId <>141615
  and (FISD.c207 is null or FISD.C207 = '')
  and CONVERT(date, FISD.C163,112)= (select max(CONVERT(date, FISD2.C163,112))
                                                                                  from campusvue.dbo.FaISIRMain (nolock) as FIM2
                                                                                  JOIN campusvue.dbo.FaISIRSparseData (NOLOCK)as FISD2
                                                                                         on FISD2.FaISIRMainId = FIM2.FaISIRMainId
                                                                                  where FIM2.ISIRSsn = FIM.ISIRSsn
                                                                                    and (FISD2.c207 is null or FISD2.C207 = '')
                                                                                    --and FIM2.FaISIRAwardYear
                                                                                    and FIM2.FaISIRMainId <> 141489
                                                                                    and FISD2.FaISIRSparseDataId <>141615)
group by FIM.ISIRSsn
)
insert into @IsirLink 
              select B.*
                     , FIM.FaISIRMainId
                     , SS.SyStudentId
              from Base_cte as B
              join campusvue.dbo.FaISIRMain (nolock) as FIM
                     on FIM.TransactionId =  B.TransID
              join campusvue.dbo.syStudent (nolock) as SS
                     on SS.SSN = B.SSN
              where FIM.FaISIRAwardYearSchemaId = 5;



select distinct GRAD08.StudentName
,sc.descrip as Campus
,GRAD08.SSN
,SS.systudentid
,[Age]=round(DATEDIFF(DAY,SS.DOB,'2014-10-06')/365,0,0)
,SS.State
,dbo.Fn_StrAdmissions_IPEDS_Ethnicity_Configuration_Get(SS.SyStudentID) as Ethnicity
--,isnull(R.Descrip,PRI.Race) as Race
--,R.Descrip as Race
,G.Descrip as Gender
,GRAD08.StatusAsOf
,HS.[HS Graduate Year]
,ISIRSD.c35 as GL
,YearCollege= case when ISIRSD.c35 is null then case when (select sum(AES.credits) from CampusVue_NECB.dbo.AdEnrollSched (nolock) as AES 
					where AES.Systudentid=AE.Systudentid and  AES.AdGradeLetterCode='TC' and AES.DateGradePosted<AE.NSLDSWithdrawalDate ) >0 
					then 'Transferred Credits' else Grad08.GradeLevel end 

					else ISIRSD.c35
					end

,StartingGradeLevel= case when (select sum(AES.credits) from CampusVue.dbo.AdEnrollSched (nolock) as AES 
					where AES.Systudentid=AE.Systudentid and  AES.AdGradeLetterCode='TC' and AES.DateGradePosted<AE.NSLDSWithdrawalDate ) >0 
					then 'Transferred Credits' else Grad08.GradeLevel end
,SSS.Descrip as AESchoolStatus
,APV.CIPCode
,APV.Descrip as ProgramVersion
,GRAD08.AttStat
,AE.StartDate
,AE.ExpStartDate
--,[GradDate]= case when SSS.Descrip= 'Graduate' then AE.GradDate  
--						 else '1900-01-01'  end  

,Degree=case

              when AD.Descrip like '%Bachelor%' then 'Bachelors degrees'
              when AD.Descrip like '%Master%' then 'Masters degrees'                      
              when (APV.CreditsReq<90) then 'Less than 2 Academic Years'
			  when (APV.CreditsReq between 90 and 180) then 'At Least 2 but Less than 4 Academic Years'
              when (APV.FaAcademicCalendar=5) then 'Less than 2 Academic Years'            
              end


--, [Program Credits]=  case when APV.FaAcademicCalendar=5 then APV.HoursReq else APV.CreditsReq end
--,[Years to Normal Completion] = case when APV.FaAcademicCalendar=5 then '2' else APV.CreditsReq/48 end

--,SSS2.Descrip as SySchoolStatus
--,APV2.Descrip as Program2
--,SSS3.Descrip as AESchoolStatus2
--,Degree2=case

--              when AD2.Descrip like '%Bachelor%' then 'Bachelors degrees'
--              when AD2.Descrip like '%Master%' then 'Masters degrees'                     
--              when (APV2.CreditsReq<90) then 'Less than 2 Academic Years'
--			  when (APV2.CreditsReq between 90 and 180) then 'At Least 2 but Less than 4 Academic Years'
--              when (APV2.FaAcademicCalendar=5) then 'Less than 2 Academic Years'
            
--              end


,[Hybrid?]= case when GRAD.systudentid is not null then 'Hybrid' 
								
								else 'NotHybrid'
								end
,AdR.Descrip as Exclusion


from(select * from [Financial Planning].dbo.IPEDS_2014_FallEnrollment (nolock) as GRAD08
       where GRAD08.Campus not like 'New England College of Business and Finance') as Grad08


join CampusVue.dbo.AdEnroll (nolock) as AE
       on AE.adEnrollID=GRAD08.adenrollid
join CampusVue.dbo.Systudent (nolock) as SS
       on ss.systudentid = ae.systudentid 
left join CampusVue.dbo.AdProgramVersion (nolock) AS APV
       on APV.AdProgramVersionID=AE.AdProgramVersionID
left join CampusVue.dbo.AdDegree (nolock) AS AD
       on AD.AdDegreeID=APV.AdDegreeID
left join CampusVue.dbo.SyschoolStatus (nolock) as SSS
	 on SSS.Syschoolstatusid=AE.Syschoolstatusid
left join CampusVue.dbo.amSex (nolock) as G
       on G.amSexID=SS.AmSexID 
join CampusVue.dbo.sycampus (nolock) as SC 
       on sc.sycampusid = ae.sycampusid
left join CampusVue.dbo.AdEnrollSched (nolock) as AES
	 on AE.Adenrollid=AES.Adenrollid
left join CampusVue.dbo.Adreason (nolock) as AdR
	 on AdR.AdReasonid=AE.AdReasonID
left join @ISIRLink as ISIR
	on ISIR.SystudentID=SS.Systudentid
left join [Financial Planning].dbo.HS_Grad_Dates (nolock) as HS
	 on HS.SSN=GRAD08.SSN
left join CampusVue.dbo.FaISIRSparseData (nolock) as ISIRSD
	on ISIRSD.FaISIRMainID=ISIR.FaIsirMainID

left join (select distinct AE1.systudentid 

from CampusVue.dbo.AdAttend (nolock) as AdT
join CampusVue.dbo.AdEnrollSched (nolock) as AES1 
	on AES1.AdEnrollSchedID=AdT.AdEnrollSchedID
join CampusVue.dbo.AdClassSched (nolock) as ACS1
on ACS1.AdClassSchedID = AES1.AdClassSchedID
join CampusVue.dbo.AdEnroll (nolock) as AE1
on AE1.AdEnrollID = AES1.AdEnrollID
where  Acs1.Section like 'x%' and Acs1.StartDate between '2014-08-28' and '2014-10-03' and AdT.Attend>0) as GRAD
					on GRAD.systudentid=ss.Systudentid




union all

select distinct GRAD08.StudentName
,GRAD08.Campus

,GRAD08.SSN
,SS.Systudentid
,[Age]=round(DATEDIFF(DAY,SS.DOB,'2014-10-06')/365,0,0)
,SS.State
,dbo.Fn_StrAdmissions_IPEDS_Ethnicity_Configuration_Get(SS.SyStudentID) as Ethnicity

,G.Descrip as Gender
,GRAD08.StatusAsOf
,HS.[HS Graduate Year]
,ISIRSD.c35 as GL
,YearCollege= case when ISIRSD.c35 is null then case when (select sum(AES.credits) from CampusVue_NECB.dbo.AdEnrollSched (nolock) as AES 
					where AES.Systudentid=AE.Systudentid and  AES.AdGradeLetterCode='TC' and AES.DateGradePosted<AE.NSLDSWithdrawalDate ) >0 
					then 'Transferred Credits' else Grad08.GradeLevel end 

					else ISIRSD.c35
					end

--,Grad08.GradeLevel
,StartingGradeLevel= case when (select sum(AES.credits) from CampusVue_NECB.dbo.AdEnrollSched (nolock) as AES 
					where AES.Systudentid=AE.Systudentid and  AES.AdGradeLetterCode='TC' and AES.DateGradePosted<AE.NSLDSWithdrawalDate ) >0 
					then 'Transferred Credits' else Grad08.GradeLevel end

,SSS.Descrip as AESchoolStatus
,APV.CIPCode
,APV.Descrip as ProgramVersion
,GRAD08.AttStat
,AE.StartDate
,AE.ExpStartDate
--,[GradDate]= case when SSS.Descrip= 'Graduate' then AE.GradDate  
--						 else '1900-01-01'  end 


,Degree=case 

              when AD.Descrip like '%Bachelor%' then 'Bachelors degrees'
              when AD.Descrip like '%Master%' then 'Masters degrees'                      
              when (APV.CreditsReq<90) then 'Less than 2 Academic Years'
			  when (APV.CreditsReq between 90 and 180) then 'At Least 2 but Less than 4 Academic Years'
              when (APV.FaAcademicCalendar=5) then 'Less than 2 Academic Years'           
              end

--, [Program Credits]=  case when APV.FaAcademicCalendar=5 then APV.HoursReq else APV.CreditsReq end
,[Hybrid?]= case when GRAD.systudentid is not null then 'Hybrid' 
								
								else 'NotHybrid'
								end
,AdR.Descrip as Exclusion

from (select * from [Financial Planning].dbo.IPEDS_2014_FallEnrollment (nolock) as GRAD08
       where GRAD08.Campus like 'New England College of Business and Finance') as Grad08


join CampusVue_NECB.dbo.AdEnroll (nolock) as AE
       on AE.adEnrollID=GRAD08.adenrollid
join CampusVue_NECB.dbo.Systudent (nolock) as SS
       on ss.systudentid = ae.systudentid 

left join CampusVue_NECB.dbo.AdProgramVersion (nolock) AS APV
       on APV.AdProgramVersionID=AE.AdProgramVersionID
left join CampusVue_NECB.dbo.AdDegree (nolock) AS AD
       on AD.AdDegreeID=APV.AdDegreeID
left join CampusVue_NECB.dbo.SyschoolStatus (nolock) as SSS
	 on SSS.Syschoolstatusid=AE.Syschoolstatusid

left join CampusVue_NECB.dbo.amSex (nolock) as G
       on G.amSexID=SS.AmSexID
left join CampusVue_NECB.dbo.AdEnrollSched (nolock) as AES
	 on AE.Adenrollid=AES.Adenrollid	    

left join CampusVue_NECB.dbo.Adreason (nolock) as AdR
	 on AdR.AdReasonid=AE.AdReasonID
left join @ISIRLink as ISIR
	on ISIR.SystudentID=SS.Systudentid
left join [Financial Planning].dbo.HS_Grad_Dates (nolock) as HS
	 on HS.SSN=GRAD08.SSN
left join CampusVue.dbo.FaISIRSparseData (nolock) as ISIRSD
	on ISIRSD.FaISIRMainID=ISIR.FaIsirMainID

left join (select distinct AE1.systudentid 

from CampusVue_NECB.dbo.AdAttend (nolock) as AdT
join  CampusVue_NECB.dbo.AdEnrollSched (nolock) as AES1 
	on AES1.AdEnrollSchedID=AdT.AdEnrollSchedID
join  CampusVue_NECB.dbo.AdClassSched (nolock) as ACS1
on ACS1.AdClassSchedID = AES1.AdClassSchedID
join  CampusVue_NECB.dbo.AdEnroll (nolock) as AE1
on AE1.AdEnrollID = AES1.AdEnrollID
where  Acs1.Section like 'x%'and Acs1.StartDate between '2014-08-28' and '2014-10-03' and AdT.attend>0) as GRAD
					on GRAD.systudentid=ss.Systudentid

order by GRAD08.StudentName
