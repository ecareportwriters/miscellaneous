with ISIR09_cte 
as
(
select distinct rtrim(SS.LastName)+', '+RTRIM(SS.FirstName) as [Full Name]
,SS.Systudentid
,SS.SSN
, sm.awardyear 
, min(convert( date,case when sm.awardyear = '2008-09' then I09.DateCompleted
         else null
  end,101)) as DateCompleted
,min(case when sm.awardyear = '2008-09' then I09.YearCollege
         else null
  end) as YearCollege
--,min( case when sm.awardyear = '2008-09' then I09.TransactionID
--         else null
--  end ) as TransactionID

from CampusVue.dbo.Systudent (nolock) as SS
       join CampusVue.dbo.FaISIRMatch (nolock) as SM
              on SM.systudentid=SS.Systudentid
                       and sm.AwardYear in ('2008-09')
       
          left join CampusVue.dbo.FaISIR09 (nolock) as I09
              on I09.Faisir09id = sm.faisirid 
                       and sm.AwardYear = '2008-09'
group by SS.LastName, SS.FirstName, SS.Systudentid, SS.SSN, sm.AwardYear


union all

select distinct rtrim(SS.LastName)+', '+RTRIM(SS.FirstName) as [Full Name]
,SS.Systudentid
,SS.SSN
, sm.awardyear 
, min(convert( date,case when sm.awardyear = '2008-09' then I09.DateCompleted
         else null
  end,101)) as DateCompleted
,min(case when sm.awardyear = '2008-09' then I09.YearCollege
         else null
  end ) as YearCollege
--,min( case when sm.awardyear = '2008-09' then I09.TransactionID
--         else null
--  end ) as TransactionID

from CampusVue_NECB.dbo.Systudent (nolock) as SS
       join CampusVue_NECB.dbo.FaISIRMatch (nolock) as SM
              on SM.systudentid=SS.Systudentid
                       and sm.AwardYear in ('2008-09')       
       left join CampusVue_NECB.dbo.FaISIR09 (nolock) as I09
              on I09.Faisir09id = sm.faisirid 
                       and sm.AwardYear = '2008-09'
group by SS.SSN,SS.LastName, SS.FirstName, SS.Systudentid,  sm.AwardYear 

)


select distinct GRAD08.StudentName
,sc.descrip as Campus
,GRAD08.SSN
,SS.systudentid
,dbo.Fn_StrAdmissions_IPEDS_Ethnicity_Configuration_Get(SS.SyStudentID) as Ethnicity
--,isnull(R.Descrip,PRI.Race) as Race
--,R.Descrip as Race
,G.Descrip as Gender
,GRAD08.StatusAsOf
,YearCollege= case when I09.YearCollege is null then case when (select sum(AES.credits) from CampusVue.dbo.AdEnrollSched (nolock) as AES 
					where AES.Systudentid=AE.Systudentid and  AES.AdGradeLetterCode='TC' and AES.DateGradePosted<AE.NSLDSWithdrawalDate ) >0 
					then 'Transferred Credits' else Grad08.GradeLevel end 
					else I09.YearCollege
					end

,GRAD08.GradeLevel
,StartingGradeLevel= case when (select sum(AES.credits) from CampusVue.dbo.AdEnrollSched (nolock) as AES 
					where AES.Systudentid=AE.Systudentid and  AES.AdGradeLetterCode='TC' and AES.DateGradePosted<AE.NSLDSWithdrawalDate ) >0 
					then 'Transferred Credits' else Grad08.GradeLevel end
,SSS.Descrip as AESchoolStatus
,GRAD08.AttStat
,AE.StartDate
,AE.ExpStartDate
,[GradDate]= case when SSS.Descrip= 'Graduate' then AE.GradDate  
						 else '1900-01-01'  end  
,APV.Descrip as Program
,Degree=case

              when AD.Descrip like '%Bachelor%' then 'Bachelors degrees'
              when AD.Descrip like '%Master%' then 'Masters degrees'
                      
              when (APV.CreditsReq<90) then 'Less than 2 Academic Years'
			  when (APV.CreditsReq between 90 and 180) then 'At Least 2 but Less than 4 Academic Years'
              when (APV.FaAcademicCalendar=5) then 'Less than 2 Academic Years'
            
              end
, [Program Credits]=  case when APV.FaAcademicCalendar=5 then APV.HoursReq else APV.CreditsReq end
,[Years to Normal Completion] = case when APV.FaAcademicCalendar=5 then '2' else APV.CreditsReq/48 end

,SSS2.Descrip as SySchoolStatus
,APV2.Descrip as Program2
,SSS3.Descrip as AESchoolStatus2
,Degree2=case

              when AD2.Descrip like '%Bachelor%' then 'Bachelors degrees'
              when AD2.Descrip like '%Master%' then 'Masters degrees'
                     
              when (APV2.CreditsReq<90) then 'Less than 2 Academic Years'
			  when (APV2.CreditsReq between 90 and 180) then 'At Least 2 but Less than 4 Academic Years'
              when (APV2.FaAcademicCalendar=5) then 'Less than 2 Academic Years'
            
              end
,AE2.ExpStartDate as ExpStartDate2
,[GradDate2]= case when SSS3.Descrip= 'Graduate' then AE2.GradDate  
						 else '1900-01-01'  end 

,[Years to Normal Completion2] = case when APV2.FaAcademicCalendar=5 then '2' else APV2.CreditsReq/48 end

,[Completions-Years]= case when SSS.Descrip like '%Graduate%' then (datediff(day,AE.StartDate,(case when SSS.Descrip= 'Graduate' then AE.GradDate  
						 else '1900-01-01'  end )))/365.00

	when SSS.Descrip not like '%Graduate%' then 
			case when (case when AD.Descrip like '%Bachelor%' then 'Bachelors degrees'
              when AD.Descrip like '%Master%' then 'Masters degrees'                      
              when (APV.CreditsReq<90) then 'Less than 2 Academic Years'
			  when (APV.CreditsReq between 90 and 180) then 'At Least 2 but Less than 4 Academic Years'
              when (APV.FaAcademicCalendar=5) then 'Less than 2 Academic Years'            
              end)=
			  (case when AD2.Descrip like '%Bachelor%' then 'Bachelors degrees'
              when AD2.Descrip like '%Master%' then 'Masters degrees'                  
              when (APV2.CreditsReq<90) then 'Less than 2 Academic Years'
			  when (APV2.CreditsReq between 90 and 180) then 'At Least 2 but Less than 4 Academic Years'
              when (APV2.FaAcademicCalendar=5) then 'Less than 2 Academic Years'       
              end) 
		then (datediff(day,AE.StartDate,(case when SSS3.Descrip= 'Graduate' then AE2.GradDate  
						 else '1900-01-01'  end )))/365.00
	when (case when AD.Descrip like '%Bachelor%' then 'Bachelors degrees'
              when AD.Descrip like '%Master%' then 'Masters degrees'                       
              when (APV.CreditsReq<90) then 'Less than 2 Academic Years'
			  when (APV.CreditsReq between 90 and 180) then 'At Least 2 but Less than 4 Academic Years'
              when (APV.FaAcademicCalendar=5) then 'Less than 2 Academic Years'             
              end)
			  <>
			  (case when AD2.Descrip like '%Bachelor%' then 'Bachelors degrees'
              when AD2.Descrip like '%Master%' then 'Masters degrees'                     
              when (APV2.CreditsReq<90) then 'Less than 2 Academic Years'
			  when (APV2.CreditsReq between 90 and 180) then 'At Least 2 but Less than 4 Academic Years'
              when (APV2.FaAcademicCalendar=5) then 'Less than 2 Academic Years'           
              end)  
	then (datediff(day,AE2.StartDate,(case when SSS3.Descrip= 'Graduate' then AE2.GradDate  
						 else '1900-01-01'  end )))/365.00
						 end
			 
			end

,[Completions-%]= case when SSS.Descrip like '%Graduate%' then ((datediff(day,AE.StartDate,(case when SSS.Descrip= 'Graduate' then AE.GradDate  
						 else '1900-01-01'  end )))/365.00)/( case when APV.FaAcademicCalendar=5 then '2' else APV.CreditsReq/48 end)

	 when SSS.Descrip not like '%Graduate%' then 
				case when 
			  (case  when AD.Descrip like '%Bachelor%' then 'Bachelors degrees'
						 when AD.Descrip like '%Master%' then 'Masters degrees'
						 when (APV.CreditsReq<90) then 'Less than 2 Academic Years'
						 when (APV.CreditsReq between 90 and 180) then 'At Least 2 but Less than 4 Academic Years'
						 when (APV.FaAcademicCalendar=5) then 'Less than 2 Academic Years'
						end)
						=
			  (case when AD2.Descrip like '%Bachelor%' then 'Bachelors degrees'
						when AD2.Descrip like '%Master%' then 'Masters degrees'
                        when (APV2.CreditsReq<90) then 'Less than 2 Academic Years'
						when (APV2.CreditsReq between 90 and 180) then 'At Least 2 but Less than 4 Academic Years'
						when (APV2.FaAcademicCalendar=5) then 'Less than 2 Academic Years'
						 end) 
		then ((datediff(day,AE.StartDate,(case when SSS3.Descrip= 'Graduate' then AE2.GradDate  
						 else '1900-01-01'  end )))/365.00)/( case when APV.FaAcademicCalendar=5 then '2' else APV.CreditsReq/48 end)
	when
		(case when AD.Descrip like '%Bachelor%' then 'Bachelors degrees'
              when AD.Descrip like '%Master%' then 'Masters degrees'              
              when (APV.CreditsReq<90) then 'Less than 2 Academic Years'
			  when (APV.CreditsReq between 90 and 180) then 'At Least 2 but Less than 4 Academic Years'
              when (APV.FaAcademicCalendar=5) then 'Less than 2 Academic Years'           
              end)
			  <>
		(case when AD2.Descrip like '%Bachelor%' then 'Bachelors degrees'
              when AD2.Descrip like '%Master%' then 'Masters degrees'                    
              when (APV2.CreditsReq<90) then 'Less than 2 Academic Years'
			  when (APV2.CreditsReq between 90 and 180) then 'At Least 2 but Less than 4 Academic Years'
              when (APV2.FaAcademicCalendar=5) then 'Less than 2 Academic Years'             
              end)  
		then ((datediff(day,AE2.StartDate,(case when SSS3.Descrip= 'Graduate' then AE2.GradDate  
						 else '1900-01-01'  end )))/365.00)/( case when APV2.FaAcademicCalendar=5 then '2' else APV2.CreditsReq/48 end)
						 end
			 
			end



,AdR.Descrip as Exclusion


from(select * from [Financial Planning].dbo.IPEDS_GRAD_2008Cohort (nolock) as GRAD08
       where GRAD08.Campus not like 'New England College of Business and Finance') as Grad08


join CampusVue.dbo.AdEnroll (nolock) as AE
       on AE.adEnrollID=GRAD08.adenrollid
join CampusVue.dbo.Systudent (nolock) as SS
       on ss.systudentid = ae.systudentid 

left join CampusVue.dbo.AdProgramVersion (nolock) AS APV
       on APV.AdProgramVersionID=AE.AdProgramVersionID
left join CampusVue.dbo.AdDegree (nolock) AS AD
       on AD.AdDegreeID=APV.AdDegreeID
left join CampusVue.dbo.SyschoolStatus (nolock) as SSS
	 on SSS.Syschoolstatusid=AE.Syschoolstatusid

left join CampusVue.dbo.amSex (nolock) as G
       on G.amSexID=SS.AmSexID 
join CampusVue.dbo.sycampus (nolock) as SC 
       on sc.sycampusid = ae.sycampusid
left join CampusVue.dbo.AdEnrollSched (nolock) as AES
	 on AE.Adenrollid=AES.Adenrollid

left join (select AEgrad.systudentid, max(AEgrad.AdEnrollID) as AdEnrollID 
			from CampusVue.dbo.Adenroll (nolock) as AEgrad 
			where AEgrad.SyschoolStatusid=17 
			group by AEgrad.Systudentid) 
			as AEgrad
	on AEgrad.systudentid=AE.Systudentid

left join CampusVue.dbo.Adenroll (nolock) as AE2
	on AE2.AdEnrollID=AEgrad.AdEnrollID
left join CampusVue.dbo.AdProgramVersion (nolock) AS APV2
       on APV2.AdProgramVersionID=AE2.AdProgramVersionID
left join CampusVue.dbo.AdDegree (nolock) AS AD2
       on AD2.AdDegreeID=APV2.AdDegreeID
left join CampusVue.dbo.SyschoolStatus (nolock) as SSS3
	 on SSS3.Syschoolstatusid=AE2.Syschoolstatusid
left join CampusVue.dbo.SyschoolStatus (nolock) as SSS2
	 on SSS2.Syschoolstatusid=SS.Syschoolstatusid
left join CampusVue.dbo.Adreason (nolock) as AdR
	 on AdR.AdReasonid=AE.AdReasonID
left join ISIR09_cte as I09
	on I09.systudentid=ss.systudentid and I09.SSN=SS.SSN



union all

select distinct GRAD08.StudentName
,GRAD08.Campus

,GRAD08.SSN
,SS.Systudentid
,dbo.Fn_StrAdmissions_IPEDS_Ethnicity_Configuration_Get(SS.SyStudentID) as Ethnicity

,G.Descrip as Gender
,GRAD08.StatusAsOf
,YearCollege= case when I09.YearCollege is null then case when (select sum(AES.credits) from CampusVue_NECB.dbo.AdEnrollSched (nolock) as AES 
					where AES.Systudentid=AE.Systudentid and  AES.AdGradeLetterCode='TC' and AES.DateGradePosted<AE.NSLDSWithdrawalDate ) >0 
					then 'Transferred Credits' else Grad08.GradeLevel end 

					else I09.YearCollege
					end

,Grad08.GradeLevel
,StartingGradeLevel= case when (select sum(AES.credits) from CampusVue_NECB.dbo.AdEnrollSched (nolock) as AES 
					where AES.Systudentid=AE.Systudentid and  AES.AdGradeLetterCode='TC' and AES.DateGradePosted<AE.NSLDSWithdrawalDate ) >0 
					then 'Transferred Credits' else Grad08.GradeLevel end
,SSS.Descrip as AESchoolStatus
,GRAD08.AttStat
,AE.StartDate
,AE.ExpStartDate
,[GradDate]= case when SSS.Descrip= 'Graduate' then AE.GradDate  
						 else '1900-01-01'  end 

,APV.Descrip as Program
,Degree=case 

              when AD.Descrip like '%Bachelor%' then 'Bachelors degrees'
              when AD.Descrip like '%Master%' then 'Masters degrees'
                      
              when (APV.CreditsReq<90) then 'Less than 2 Academic Years'
			  when (APV.CreditsReq between 90 and 180) then 'At Least 2 but Less than 4 Academic Years'
              when (APV.FaAcademicCalendar=5) then 'Less than 2 Academic Years'
           
              end
, [Program Credits]=  case when APV.FaAcademicCalendar=5 then APV.HoursReq else APV.CreditsReq end
,[Years to Normal Completion] = case when APV.FaAcademicCalendar=5 then '2' else APV.CreditsReq/36 end

,SSS2.Descrip as SySchoolStatus
,APV2.Descrip as Program2
,SSS3.Descrip as AESchoolStatus2
,Degree2=case when AD2.Descrip like '%Bachelor%' then 'Bachelors degrees'
              when AD2.Descrip like '%Master%' then 'Masters degrees'
                  
              when (APV2.CreditsReq<90) then 'Less than 2 Academic Years'
			  when (APV2.CreditsReq between 90 and 180) then 'At Least 2 but Less than 4 Academic Years'
              when (APV2.FaAcademicCalendar=5) then 'Less than 2 Academic Years'
      
              end
,AE2.StartDate as StartDate2
,[GradDate2]= case when SSS3.Descrip= 'Graduate' then AE2.GradDate  
						 else '1900-01-01'  end 

,[Years to Normal Completion2] = case when APV2.FaAcademicCalendar=5 then '2' else APV2.CreditsReq/48 end
,[Completions-Years]= case when SSS.Descrip like '%Graduate%' then (datediff(day,AE.StartDate,(case when SSS.Descrip= 'Graduate' then AE.GradDate  
						 else '1900-01-01'  end )))/365.00

	when SSS.Descrip not like '%Graduate%' then 
			case when (case when AD.Descrip like '%Bachelor%' then 'Bachelors degrees'
              when AD.Descrip like '%Master%' then 'Masters degrees'                      
              when (APV.CreditsReq<90) then 'Less than 2 Academic Years'
			  when (APV.CreditsReq between 90 and 180) then 'At Least 2 but Less than 4 Academic Years'
              when (APV.FaAcademicCalendar=5) then 'Less than 2 Academic Years'            
              end)=
			  (case when AD2.Descrip like '%Bachelor%' then 'Bachelors degrees'
              when AD2.Descrip like '%Master%' then 'Masters degrees'                  
              when (APV2.CreditsReq<90) then 'Less than 2 Academic Years'
			  when (APV2.CreditsReq between 90 and 180) then 'At Least 2 but Less than 4 Academic Years'
              when (APV2.FaAcademicCalendar=5) then 'Less than 2 Academic Years'       
              end) 
		then (datediff(day,AE.StartDate,(case when SSS3.Descrip= 'Graduate' then AE2.GradDate  
						 else '1900-01-01'  end )))/365.00
	when (case when AD.Descrip like '%Bachelor%' then 'Bachelors degrees'
              when AD.Descrip like '%Master%' then 'Masters degrees'                       
              when (APV.CreditsReq<90) then 'Less than 2 Academic Years'
			  when (APV.CreditsReq between 90 and 180) then 'At Least 2 but Less than 4 Academic Years'
              when (APV.FaAcademicCalendar=5) then 'Less than 2 Academic Years'             
              end)
			  <>
			  (case when AD2.Descrip like '%Bachelor%' then 'Bachelors degrees'
              when AD2.Descrip like '%Master%' then 'Masters degrees'                     
              when (APV2.CreditsReq<90) then 'Less than 2 Academic Years'
			  when (APV2.CreditsReq between 90 and 180) then 'At Least 2 but Less than 4 Academic Years'
              when (APV2.FaAcademicCalendar=5) then 'Less than 2 Academic Years'           
              end)  
	then (datediff(day,AE2.StartDate,(case when SSS3.Descrip= 'Graduate' then AE2.GradDate  
						 else '1900-01-01'  end )))/365.00
						 end
			 
			end

,[Completions-%]= case when SSS.Descrip like '%Graduate%' then ((datediff(day,AE.StartDate,(case when SSS.Descrip= 'Graduate' then AE.GradDate  
						 else '1900-01-01'  end )))/365.00)/( case when APV.FaAcademicCalendar=5 then '2' else APV.CreditsReq/48 end)

	 when SSS.Descrip not like '%Graduate%' then 
				case when 
			  (case  when AD.Descrip like '%Bachelor%' then 'Bachelors degrees'
						 when AD.Descrip like '%Master%' then 'Masters degrees'
						 when (APV.CreditsReq<90) then 'Less than 2 Academic Years'
						 when (APV.CreditsReq between 90 and 180) then 'At Least 2 but Less than 4 Academic Years'
						 when (APV.FaAcademicCalendar=5) then 'Less than 2 Academic Years'
						end)
						=
			  (case when AD2.Descrip like '%Bachelor%' then 'Bachelors degrees'
						when AD2.Descrip like '%Master%' then 'Masters degrees'
                        when (APV2.CreditsReq<90) then 'Less than 2 Academic Years'
						when (APV2.CreditsReq between 90 and 180) then 'At Least 2 but Less than 4 Academic Years'
						when (APV2.FaAcademicCalendar=5) then 'Less than 2 Academic Years'
						 end) 
		then ((datediff(day,AE.StartDate,(case when SSS3.Descrip= 'Graduate' then AE2.GradDate  
						 else '1900-01-01'  end )))/365.00)/( case when APV.FaAcademicCalendar=5 then '2' else APV.CreditsReq/48 end)
	when
		(case when AD.Descrip like '%Bachelor%' then 'Bachelors degrees'
              when AD.Descrip like '%Master%' then 'Masters degrees'              
              when (APV.CreditsReq<90) then 'Less than 2 Academic Years'
			  when (APV.CreditsReq between 90 and 180) then 'At Least 2 but Less than 4 Academic Years'
              when (APV.FaAcademicCalendar=5) then 'Less than 2 Academic Years'           
              end)
			  <>
		(case when AD2.Descrip like '%Bachelor%' then 'Bachelors degrees'
              when AD2.Descrip like '%Master%' then 'Masters degrees'                    
              when (APV2.CreditsReq<90) then 'Less than 2 Academic Years'
			  when (APV2.CreditsReq between 90 and 180) then 'At Least 2 but Less than 4 Academic Years'
              when (APV2.FaAcademicCalendar=5) then 'Less than 2 Academic Years'             
              end)  
		then ((datediff(day,AE2.StartDate,(case when SSS3.Descrip= 'Graduate' then AE2.GradDate  
						 else '1900-01-01'  end )))/365.00)/( case when APV2.FaAcademicCalendar=5 then '2' else APV2.CreditsReq/48 end)
						 end
			 
			end

,AdR.Descrip as Exclusion

from (select * from [Financial Planning].dbo.IPEDS_GRAD_2008Cohort (nolock) as GRAD08
       where GRAD08.Campus like 'New England College of Business and Finance') as Grad08


join CampusVue_NECB.dbo.AdEnroll (nolock) as AE
       on AE.adEnrollID=GRAD08.adenrollid
join CampusVue_NECB.dbo.Systudent (nolock) as SS
       on ss.systudentid = ae.systudentid 

left join CampusVue_NECB.dbo.AdProgramVersion (nolock) AS APV
       on APV.AdProgramVersionID=AE.AdProgramVersionID
left join CampusVue_NECB.dbo.AdDegree (nolock) AS AD
       on AD.AdDegreeID=APV.AdDegreeID
left join CampusVue_NECB.dbo.SyschoolStatus (nolock) as SSS
	 on SSS.Syschoolstatusid=AE.Syschoolstatusid

left join CampusVue_NECB.dbo.amSex (nolock) as G
       on G.amSexID=SS.AmSexID
left join CampusVue_NECB.dbo.AdEnrollSched (nolock) as AES
	 on AE.Adenrollid=AES.Adenrollid	    
left join (select AEgrad.systudentid, max(AEgrad.AdEnrollID) as AdEnrollID from CampusVue_NECB.dbo.Adenroll (nolock) as AEgrad where AEgrad.SyschoolStatusid=17 group by AEgrad.Systudentid) as AEgrad
	on AEgrad.systudentid=AE.Systudentid
left join CampusVue_NECB.dbo.Adenroll (nolock) as AE2
	on AE2.AdEnrollID=AEgrad.AdEnrollID
left join CampusVue_NECB.dbo.AdProgramVersion (nolock) AS APV2
       on APV2.AdProgramVersionID=AE2.AdProgramVersionID
left join CampusVue_NECB.dbo.AdDegree (nolock) AS AD2
       on AD2.AdDegreeID=APV2.AdDegreeID
left join CampusVue_NECB.dbo.SyschoolStatus (nolock) as SSS3
	 on SSS3.Syschoolstatusid=AE2.Syschoolstatusid
left join CampusVue_NECB.dbo.SyschoolStatus (nolock) as SSS2
	 on SSS2.Syschoolstatusid=SS.Syschoolstatusid
left join CampusVue_NECB.dbo.Adreason (nolock) as AdR
	 on AdR.AdReasonid=AE.AdReasonID
left join ISIR09_cte as I09
	on I09.systudentid=ss.systudentid and I09.SSN=SS.SSN
order by GRAD08.StudentName
