select SS.SyStudentId
,SC.Descrip as Campus
,APV.CIPCode
--,AD.Descrip as Degree
,isnull(R.Descrip,PRI.Race) as Race
,G.Descrip as Gender
--,SS.DOB
--,AE.LDA
,APV.Descrip as Program

,[Age]=round(DATEDIFF(DAY,SS.DOB,AE.LDA)/365,0,0) 
, Degree=case when AD.Descrip like '%Associate%' then 'Associates degrees'
			  when AD.Descrip like '%Bachelor%' then 'Bachelors degrees'
			  when AD.Descrip like '%Master%' then 'Masters degrees'
			  when (APV.CreditsReq/36)<1 and (APV.FaAcademicCalendar<>5) then 'Postsecondary award, certificate, or diploma of (less than 1 academic year)'
		when (APV.CreditsReq/36) between 1 and 2 then 'Postsecondary award, certificate, or diploma of (at least 1 but less than 2 academic years)'
		when (APV.CreditsReq/36) between 2 and 4 then 'Postsecondary award, certificate, or diploma of (at least 2 but less than 4 academic years)'
		when (APV.FaAcademicCalendar=5) then 'Postsecondary award, certificate, or diploma of (at least 1 but less than 2 academic years)'
		when APV.Descrip= 'Accounting and Finance - Forensic Accounting'  then 'Postbaccalaureate Certificate'
		when APV.Descrip= 'Accounting and Finance - Basic'  then 'Postsecondary award, certificate, or diploma of (at least 1 but less than 2 academic years)'
		when APV.Descrip= 'Accounting and Finance - Intermediate'  then 'Postsecondary award, certificate, or diploma of (at least 1 but less than 2 academic years)'
	
		end

--,ss.dob
--,ae.LDA


from AdEnroll (nolock) as AE
join Systudent (nolock) as SS
	on SS.systudentid=AE.SyStudentID
left join amRace (nolock) as R
	on R.AmRaceID=SS.AmRaceID
join amSex (nolock) as G
	on G.amSexID=SS.AmSexID 
join SyCampus (nolock) as SC
	on SC.sycampusid=AE.sycampusid
join AdProgramVersion (nolock) AS APV
	on APV.AdProgramVersionID=AE.AdProgramVersionID
join AdDegree (nolock) AS AD
	on AD.AdDegreeID=APV.AdDegreeID
left join [Financial Planning].dbo.PortalRaceInfo2 (nolock) as PRI
		on pri.systudentid = SS.SyStudentId and PRI.Campus=SC.Descrip
	
	
	
where AE.LDA between '2013-07-01' and '2014-06-30' and AE.SySchoolStatusID=17

--group by APV.CIPCode
--, R.Descrip
--, DATEDIFF(Year,SS.DOB,AE.LDA)
--,G.Descrip
----,AD.Descrip
--,SS.systudentid
--,SC.Descrip
--,SS.DOB
--,AE.LDA
--,APV.Descrip


order by SS.SyStudentId